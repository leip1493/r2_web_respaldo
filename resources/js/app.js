require('./bootstrap');

window.Vue = require('vue');

// COMPONENTE PRINCIPAL
import App from './App.vue';
// ROUTER
import router from './router/router';
// STORE
import store from './store';
// VUETIFY
import vuetify from './plugins/vuetify';
// VEE-VALIDATE
import './plugins/veeValidate/veeValidate';
// sweetalert
import Swal from 'sweetalert2';
// VUE APEX-CHART
// import './plugins/apex-chart';
// CSV DOWLOWAND
import Vue from 'vue'
import JsonCSV from 'vue-json-csv'

Vue.component('downloadCsv', JsonCSV)

store.dispatch("auth/TRY_AUTO_LOGIN");
// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

const app = new Vue({
    el: '#app',
    components: {App},
    vuetify,
    router,
    store,
});
