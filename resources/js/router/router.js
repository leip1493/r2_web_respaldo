import Vue from "vue";
import Router from "vue-router";
import store from "../store/index";

Vue.use(Router);

const router = new Router({
    routes: [
        {
            name: "login",
            path: "/",
            component: () =>
                import(
                    /* webpackChunkName: "login" */ "../components/auth/Login"
                )
        },
        {
            name: "layout",
            path: "/layout",
            meta: {
                requiresAuth: true
            },
            component: () =>
                import(
                    /* webpackChunkName: "layout" */ "../components/layout/Layout"
                ),
            children: [
                {
                    name: "user",
                    path: "/users",
                    meta: {
                        checkRole: [1, 2],
                        checkPermissions: 2
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/users/Index"
                        )
                },
                {
                    name: "report_1",
                    path: "/agent_hours_custom",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 20
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R1/R1"
                        )
                },
                {
                    name: "report_1_d",
                    path: "/agent_hours",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 19
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R1/R1_d"
                        )
                },
                {
                    name: "report_2",
                    path: "/daily_activity",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 20
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R2/R2TableReport"
                        )
                },
                {
                    name: "report_2_test",
                    path: "/report_2_test",
                    // meta: {
                    //     checkRole: [5, 4, 3, 2, 1]
                    // },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R2/R2.vue"
                        )
                },
                {
                    name: "report_2_old",
                    path: "/report_2_old",
                    // meta: {
                    //     checkRole: [5, 4, 3, 2, 1]
                    // },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R2/R2"
                        )
                },
                {
                    name: "agents",
                    path: "/agents",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 17
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R2/R2_agents"
                        )
                },
                {
                    name: "dispositions",
                    path: "/dispositions",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 18
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R2/R2_dispositions"
                        )
                },
                {
                    name: "layout1",
                    path: "/dashboard",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 0
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/layout/Dashboard"
                        )
                },
                {
                    name: "campaigns",
                    path: "/campaigns",
                    meta: {
                        checkRole: [1],
                        checkPermissions: 1
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/campaigns/Index"
                        )
                },
                {
                    name: "upload_call_log",
                    path: "/upload_call_log",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 16
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/upload/upload_call_log.vue"
                        )
                },
                {
                    name: "scorecard",
                    path: "/scorecard",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 22
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R3/Scorecard.vue"
                        )
                },
                {
                    name: "call_log",
                    path: "/call_log",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 23
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/reports/R4/CallLog.vue"
                        )
                },
                {
                    name: "audit",
                    path: "/audit",
                    meta: {
                        checkRole: [1, 2, 3],
                        checkPermissions: 23
                    },
                    component: () =>
                        import(
                            /* webpackChunkName: "user" */ "../components/audit/Index.vue"
                        )
                }
            ]
        }
        // {
        //     name: "user",
        //     path: "/addUser",
        //     component: () =>
        //         import(/* webpackChunkName: "user" */ "../components/users/Index"),
        // }
    ],
    mode: "history",
    scrollBehavior(to, from, savedPosition) {
        // return desired position
        return { x: 0, y: 0 };
    }
});

// PROTECCION DE RUTAS
router.beforeEach((to, from, next) => {
    // Consulta VUEX
    const authUser = store.getters["auth/IS_AUTHENTICATED"];
    const UserRole = JSON.parse(localStorage.getItem("role"));
    const UserPermissions = JSON.parse(localStorage.getItem("auth_permission"))?.map(item => item.id);
    UserPermissions?.push(0);
    UserPermissions?.push(1);
    UserPermissions?.push(2);

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!authUser) {
            next({ path: "/" });
        } else {
            if (to.meta.checkRole.includes(UserRole)) {
                if(UserRole == 3 ) {
                    if(UserPermissions?.includes(to.meta.checkPermissions)) next();
                    else next({ path: "/dashboard" });
                }
                next();
            } else {
                next({ path: "/dashboard" });
            } 
        }
    } else {
        next();
    }
});

export default router;
