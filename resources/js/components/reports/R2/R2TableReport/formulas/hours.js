/**
 * formulas horas
 */

import moment from 'moment';
import { fakeCsv } from '../__tests__';
import {
	substractTimes,
} from './helpers';

// const fakeagent = fakeCsv[0];

/**
 * [TotalHours description]
 * Sumatoria de Total Hours de todos los agentes HCC.
 * @param {any[]} dateAgent [description]
 */
export function TotalHours(dateAgent) {
	// tiempo eliminar AM
	// ej = "9:35:00 AM" -> "9:35:00"
	// logica AM, PM
	const timeDelta = substractTimes(
		dateAgent.telephonic_data.end_time.substring(0, 8).trim(),
		dateAgent.telephonic_data.start_time.substring(0, 8).trim(),
		// "9:35:00 ".trim(),
		// "09:35:00"
	);
	const secondTimeDelta = timeDelta.split(':').reverse().reduce(
		(prev, curr, i) => prev + curr*Math.pow(60, i), 0
	);

	const hoursTimeDetla = secondTimeDelta / 3600;

	const seconds = hoursTimeDetla + parseInt(dateAgent.telephonic_data.call_time)

	if (isNaN(seconds)) {
		return 0;
	}

	return seconds / 3600
}

/**
 * [DialsPerHour description]
 * Sumatoria de llamadas hechas en el día por agentes
 * que tienen propiedad “HCC” y  dividido entre Total Hours. 
 * @param {[type]} filterArrayLength [description]
 * @param {[type]} resultTotalHours  [description]
 */
export function DialsPerHour(filterArrayLength, resultTotalHours) {
	return filterArrayLength / resultTotalHours;
}

/**
 * [ContactsPerHour description]
 * Sumatoria de llamadas hechas en el día por agentes que tienen propiedad “HCC”
 * y  que tienen una disposition con propiedad “Contact” dividido entre Total Hours. 
 * @param {any[]} csvArrData [description]
 * @param {number} resultTotalHours  [description]
 */
export function ContactsPerHour(csvArrData, resultTotalHours) {
	return csvArrData.filter((dataElement) => {
		return dataElement.telephonic_data.disposition === 'Contact';
	}).length / resultTotalHours;
}

/**
 * [CallBacks description]
 * Sumatoria de llamadas hechas en el día por agentes que
 * tienen propiedad “HCC” y que tienen una disposition de nombre “Callback”
 * @param {any[]} csvArrData [description]
 */
export function CallBacks(csvArrData) {
	return csvArrData.filter((dataElement) => {
		return dataElement.telephonic_data.disposition === 'Callback';
	}).length
}

/**
 * Llamando formulas
 * Suponiendo que arreglo esta filtrado por un dia y todos los
 * agentes son HCC
 */

const totalHours = fakeCsv.map(TotalHours).reduce((accumulator, currentValue) => {
	return accumulator + currentValue;
});

// console.log(totalHours, 'total_hours');

const dialsPerHour = DialsPerHour(fakeCsv.length, totalHours);
// console.log(dialsPerHour, 'dials_Per_Hour');

const contactsPerHour = ContactsPerHour(fakeCsv, totalHours)
// console.log(contactsPerHour, 'contacts_Per_Hour');

const callBacks = CallBacks(fakeCsv)
// console.log(callBacks, 'call_Backs');
