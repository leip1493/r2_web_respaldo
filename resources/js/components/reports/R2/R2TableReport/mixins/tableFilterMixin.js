/**
 * [cleanDispName description]
 *
 * Usado para acceder al clousure de los filters
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
function cleanDispName(name) {
    const originName = name.trim();
    // PARSEAR TODOS LOS DISPOSITION SOLO LETRAS
    const processName = originName.replace(/[^a-zA-Z]+/g, " ").toLowerCase().trim();
    const t2 = processName.split(' ');
    return t2.join('_');
}

/**
 * Filtros de la tabla
 * @type {Object}
 */
export const tableFilterMixin = {

    filters: {

        /**
         * [toFixed description]
         *
         * Verificar que el valor sea numerico, en caso de no ser
         * numerico devolver el valor actual y permitir seguir
         * renderizando la tabla
         *
         * En caso de que valor sea igual devuelve una cadena vacia
         *
         * fatherDisposition, ignora el valor de numberOfDecimals y establece
         * uno por defecto, si coincide con un de los siguiente valores, en caso
         * de no coincidir continua la ejecucion con numberOfDecimals
         *
         * - total_hours: establece 2 decimales
         * 
         * 
         * @param  {number} value            [description]
         * @param  {number} numberOfDecimals [description]
         * @param  {string | null} fatherDisposition [description]
         * @return {string | any}                  [description]
         */
        toFixed(value, numberOfDecimals = 0, fatherDisposition = null) {
            if (typeof value !== 'number') {
                return value;
            }
            if (value === 0) {
                return "";
            }
            // reducir consumo memoria
            // if (typeof numberOfDecimals !== 'number') {
            //     return value;
            // }
            if (fatherDisposition !== null) {
                switch (cleanDispName(fatherDisposition)) {
                    case 'total_hours':
                        return value.toFixed(2);
                        break;
                    default:
                        break;
                }
            }
            return value.toFixed(numberOfDecimals);
        }

    },

};
