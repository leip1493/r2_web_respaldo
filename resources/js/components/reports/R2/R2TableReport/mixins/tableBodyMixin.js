import { fakeAgents, fakeCsv } from '../__tests__';

/**
 * [tableBodyMixin description]
 * Generar cuerpo de tabla
 * @type {Object}
 */
export const tableBodyMixin = {

	data() {
		return {
			disposition: [],
			contentTable: [],
			fatherNames: [
				'Total Hours',
				'Primary Sales',
				'Other Commissionable Sales',
				'Sales Pipeline',
				'Lead Analysis',
				'Calls (from log)',
				'Recycles',
				'Finalized Declines'
			],
			tableBodyTest: []
		};
	},

	computed: {
		SelectCampaign() {
			return this.$store.getters['campaigns/GET_SELECTED_CAMPAIGN'];
		}
	},

	methods: {

		/**
		   * [generateTableBody description]
		   * solo prototipo
		   *
		   * ejemplo estructura
		   * {
		   *		name: 'content 1',
	   *     children: [
	   *     // pendiente confirmacion de esta propiedad
	   *     // segunda opcion añadirla desde front end
	   *     // tercera opcion modificar esta propiedad por otro metodo de iteracion
	   *      {
	   *        name: 'content 1.1',
	   *      },
	   *      {
	   *        name: 'content 1.2',
	   *      }
	   *    ]
		   * }
		   * cambiar a generador por la posible sobrecarga
		   * quitar referencias a propiedades computadas
		   * definir como pasar enviar funciones de calculos
		   * @param  {any[]} targetArr [dispositions]
		   * @return {any[]}        [description]
		   */
		$_generateTableBody(targetArr) {
			const tableBody = [];
			for (const target of targetArr) {
				const ob = {};
				ob.name = target.name;
				ob.ytd = 0;
				// months properties
				for (const month of this.getLastMonths) {
					ob[month] = 0; // function
				}
				// days properties
				for (const dayob of this.days) {
					ob[dayob.day] = 0; // function
				}
				tableBody.push(ob);
			}
			return tableBody;
		},

		$_generateTableBody2(dispositionValues) {
			let tableBody = [];
			let primaryArray = [];
			let pipelineArray = [];
			let declineArray = [];
			let recycleArray = [];
			const test = this.disposition.filter(value => {
				if (value.final == 'recycle') {
					recycleArray.push(value)
				}
				if (value.primary == 1) {
					primaryArray.push(value)
				}
				if (value.pipeline == 1) {
					pipelineArray.push(value)
				}
				if (value.decline == 1) {
					declineArray.push(value)
				}
				tableBody[0] = {
					fatherName: 'Total Hours',
					children: [
						{ name: 'Dials per Hour' },
						{ name: 'Contacts per Hour' },
						{ name: 'Sales per Hour' }
					]
				}
				tableBody[1] = {
					fatherName: 'Primary Sales',
					children: primaryArray
				}
				tableBody[2] = {
					fatherName: 'Sales Pipeline',
					children: pipelineArray
				}
				tableBody[3] = {
					fatherName: 'Calls Log',
					children: [
						{ name: 'Inbound' },
						{ name: 'Outbound' }
					]
				}
				tableBody[4] = {
					fatherName: 'Recycles',
					children: recycleArray
				}
				tableBody[5] = {
					fatherName: 'Finalized Declines',
					children: declineArray
				}
				return value;
			});
			this.tableBodyTest = tableBody;
		},

		async getDisposition() {
			try {
				console.log("ENTRANDO")
				const request = {
					id: this.SelectCampaign.id === undefined ? 0 : this.SelectCampaign.id
				};
				const response = await this.$store.dispatch(
					"report_2/GET_LIST_DISPOSITION",
					request
				);
				this.disposition = response.data.data;
			} catch (error) {
				console.log(error);
			}
		},

	},
};
