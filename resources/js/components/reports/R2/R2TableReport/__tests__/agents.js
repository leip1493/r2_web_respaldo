export const fakeAgents = [
    {
        "id": 1,
        "name": "Manny Landeros",
        "created_at": "2020-04-02 18:17:45",
        "updated_at": "2020-04-02 18:17:45"
    },
    {
        "id": 2,
        "name": "Louis Smith",
        "created_at": "2020-04-02 18:17:45",
        "updated_at": "2020-04-02 18:17:45"
    },
    {
        "id": 3,
        "name": "Kevin West",
        "created_at": "2020-04-02 18:17:46",
        "updated_at": "2020-04-02 18:17:46"
    },
    {
        "id": 4,
        "name": "Arthur Martinez",
        "created_at": "2020-04-02 18:17:52",
        "updated_at": "2020-04-02 18:17:52"
    },
    {
        "id": 5,
        "name": "Jake Wolf",
        "created_at": "2020-04-02 18:17:56",
        "updated_at": "2020-04-02 18:17:56"
    }
];
