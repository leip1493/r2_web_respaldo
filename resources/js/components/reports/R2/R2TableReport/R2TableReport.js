import moment from "moment";
import Swal from "sweetalert2";

import { fakeDispositions } from './__tests__';
import { tableBodyMixin, tableFilterMixin } from './mixins';

export default {

	name: 'R2TableReport',

	mixins: [
		tableBodyMixin,
		tableFilterMixin
	],

	data() {
		return {
			// meses
			currentSelectMonth: moment().month(),
			currentMonth: moment().month(), // no mutar
			selectedMonth: moment().month(),
			dayOfMonth: "",
			showDays: false,
			days: [],
			length: 3,
			onboarding: 0,
			filters: {},
			// evitar la acumulacion de ciclos de actualizacion DOM
			loading: false
		};
	},

	computed: {

  	/**
  	 * [getLastMonths description]
  	 * @return {[type]} [description]
  	 */
		getLastMonths() {
			const months = moment.monthsShort();

			// cantidad de elementos añadir
			let quantityMonth = 3;
			const lastMonths = [];

			if (this.selectedMonth < 2) {
				quantityMonth = this.selectedMonth + 1;
			}

			for (let index = 0; index < quantityMonth; index++) {
				lastMonths.unshift(months[this.selectedMonth - index]);
			}
			// let lastMonths = [
			//   months[this.currentMonth - 2],
			//   months[this.currentMonth - 1],
			//   months[this.currentMonth]
			// ];

			return lastMonths;
		},

		/**
		 * [getAgents description]
		 * @return {[type]} [description]
		 */
		getAgents() {
			return this.$store.getters["report_1/GET_AGENTS"];
		},

		getCampaign() {
			return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
		}

	},

	async created() {
		this.getDays(this.currentMonth);
		await this.getDisposition();

		// solictar formulas / construir tabla
		await this.getFormulasMonth();
	},

	methods: {

  	/**
   		* [jsexport description]
   		* Arreglo de arreglos
   		* cada columna del csv debe es un arreglo de valores primitivos
   		* nota: el csv es una arreglo de arreglos
   		*
   		* referencia:
   		* https://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side
   		* @return {[type]} [description]
   	*/
		jsexport() {

			// URL Falsa para descargar
			let csvContent = "data:text/csv;charset=utf-8,";

			// formatos 
			const delimiter = ',';
			const fixedDecimal = 2;
			const dayFormat = 'day';

			// cabecera
			const csvHeader = [
				["Name", "YTD"],
			];

			this.getLastMonths.forEach((month) => {
				csvHeader[0].push(month)
			});

			this.days.forEach((day) => {
				csvHeader[0].push(day[dayFormat])
			});

			// cuerpo
			const csvBody = [];

			this.tableBodyTest.forEach((dispFather) => {
				// columna padre
				const dispFatherRowArray = [];
				dispFatherRowArray.push(dispFather.fatherName);
				for (const nameFormula in dispFather.formulas) {
					if (typeof dispFather.formulas[nameFormula] === 'number') {
						dispFatherRowArray.push(dispFather.formulas[nameFormula].toFixed(fixedDecimal));
					} else {
						dispFatherRowArray.push(dispFather.formulas[nameFormula]);
					}
				}
				// inserta columna padre
				csvBody.push(dispFatherRowArray);

				// guardar hijos
				for (const dispSon of dispFather.children) {
					// columna hijo
					const dispSonRowArray = [];
					dispSonRowArray.push(dispSon.name);
					for (const nameSonFormula in dispSon.formulas) {
						if (typeof dispSon.formulas[nameSonFormula] === 'number') {
							dispSonRowArray.push(dispSon.formulas[nameSonFormula].toFixed(fixedDecimal));
						} else {
							dispSonRowArray.push(dispSon.formulas[nameSonFormula]);
						}
					}
					// inserta columna hijo
					csvBody.push(dispSonRowArray);
				}
			});

			// añadir resultados url falsa
			csvHeader.forEach((rowArray) => {
				let row = rowArray.join(delimiter);
				csvContent += row + "\r\n";
			});
			csvBody.forEach((rowArray) => {
				let row = rowArray.join(delimiter);
				csvContent += row + "\r\n";
			});
			const nameCSV = `${
				this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"].name
				}-Daily-Activity-${moment().format('YYYY-MM-DD')}.csv`;
			// generar elemento del dom para poner el enlace
			const encodedUri = encodeURI(csvContent);
			const link = document.createElement("a");
			link.setAttribute("href", encodedUri);
			link.setAttribute("download", nameCSV);
			document.body.appendChild(link); // Required for FF
			// darle click 
			link.click();
		},

  	/**
  	 * [rotateCondition description]
  	 * dada una direccion, indice actual, longitud del arreglo
  	 * determinar si el elemento es el ultimo o primero, en que direccion
  	 * añadir boton
  	 *
  	 * @param  {string} direction 'left' | 'right' [description]
  	 * @param  {number} index     [description]
  	 * @param  {number} length    [description]
  	 * @return {boolean}           [description]
  	 */
		rotateCondition(direction, index, length) {
			if (index === length - 1 && direction === 'left') {
				return true;
			}
			if (index === 0 && direction === 'right') {
				return true;
			}
			return false;
		},

  	/**
  	 * [rotateMonths description]
  	 * @param  {number} rotate [1: incrementar , -1: dismincuir] 
  	 * 
  	 */
		async rotateMonths(rotate) {
			try {
				if (this.selectedMonth === 2 && rotate !== 1) {
					Swal.fire({
						icon: "warning",
						title: "Lower limit reached",
						confirmButtonColor: "#25A8E0"
					});
					return;
				}
				if (this.selectedMonth === moment().month() && rotate !== -1) {
					Swal.fire({
						icon: "warning",
						title: "Upper limit reached",
						confirmButtonColor: "#25A8E0"
					});
					return;
				}
				// rotar meses
				this.selectedMonth = this.selectedMonth + rotate;
				this.getFormulasMonth();
			} catch (error) {
				console.log(error);
			}
		},

  	/**
  	 * [getDays description]
  	 * retornar una lista con todos los dias de un determinado mes
  	 * interface R2ColumnTableDay {
  	 * 		day: string;
  	 * 		day_resp: string;
  	 * 		day_name: string;
  	 * }
  	 * @param  {number} month [description]
  	 */
		getDays(month) {
			this.showDays = false;
			this.currentMonth = month;
			const dayOfMonth = moment()
				.month(month)
				.daysInMonth();
			this.dayOfMonth = dayOfMonth;
			const currentDay = moment()
				.month(month)
				.format("MM/DD/YYYY");
			let arrayOfDays = [];
			for (let index = 0; index <= dayOfMonth; index++) {
				;
				arrayOfDays[index] = {
					num: index + 1,
					day: moment().month(month).startOf('month').add(index - 1, 'days').format('MM/DD'),
					day_resp: moment().month(month).startOf('month').add(index - 1, 'days').format('DD/MM/YYYY'),
					day_name: moment()
						.year(2020)
						.month(month)
						.date(index)
						.format("ddd")
				};
				// arrayOfDays.push(`${moment.months().indexOf(month)+1}/${index}/${moment().year()}`);
			}
			this.days = arrayOfDays;
			let value = this.days.shift();
			return arrayOfDays;
		},

		/**
		 * [getValues description]
		 * @return {[type]} [description]
		 */
		async getValues() {
			try {
				const request = {
					date1: moment()
						.set("month", this.getLastMonths[0])
						.format("MM/DD/YYYY"),
					date2: moment()
						.set("month", this.getLastMonths[2])
						.format("MM/DD/YYYY"),
					// campaign_id: this.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
					campaign_id: 1
				};
				const response = await this.$store.dispatch(
					"report_1/GET_FILE",
					request
				);

				const csvData = response.data;

			} catch (error) {
				console.log(error);
			}
		},
		async getFilters() {
			try {
				const response = await this.$store.dispatch(
					"report_2/GET_FILTERS"
				);
				this.filters = response.data.data;
			} catch (error) {
				console.log(error);
			}
		},

		/**
		 * [pruebav2 description]
		 *
		 * Añade en el prototipo del arreglo tableBodyTest una propiedad
		 * con nombre: formulas
		 *
		 * formulas en objecto clave - valor
		 * {
		 * 	  yt2: 0
		 * 	  mes_name: 0,
		 * 	  otros meses...
		 * 	  mm_dd_yy: 0
		 * 	  otros dias...
		 * }
		 *
		 * Motivos:
		 * Cada dispostion posee una formula y un resultado en la tabla
		 * sea padre o hijo, entonces añadirle una propiedad con el resultado
		 * preciso para cada caso posible y asignar valores por defecto en
		 * caso de que eso valores no existan, permite encerrar toda la generacion
		 * del cuerpo de la tabla en una o varias funciones
		 * (tortugas hasta el fondo)
		 *
		 * Disculpas: Por la poca depuracion, y el consumo excesivo de memoria
		 *
		 */
		pruebav2() {
			const daysResponse = Object.keys(this.filters);

			// **** convertir en estructura mas facil de mapear ****
			// arreglo de objetos
			// { date: 'dd/mm/yy', report: 'respuest api' }
			const arrayResponse = Object.entries(this.filters).map((values) => {
				const ob = {};
				ob.date = values[0];
				ob.report = values[1]
				return ob;
			});
			// console.log(arrayResponse)
			// **** Json Formateado Filters ****

			// evitar mutar directamente la tabla renderizada
			// Recordar asignar valores de clave en propiedad formulas
			// que se crean en este clon, con el orden que se quiere en la tabla
			// para evitar tener claves en el objeto desordenadas
			const cloneArr = [...this.tableBodyTest];

			// QUE SIGNIFICA YTD ?
			// SUMATORIA DE LOS 3 MESES QUE APARECEN EN LA TABLA
			// siempre se agrega como primera clave en formulas

			// **** NO USAR toFixed, puede alterar y truncar los resultados ****
			// en su lugar utilizar los filtros

			// iterar padre disposition
				let key_dispSon = 0;
			for (const dispFather of cloneArr) {

				// revisar que el nombre del padre este correcto
				const cleanFatherName = this.cleanDispositionName(dispFather.fatherName);

				// iteracion añadir formulas a los hijos
				for (const dispSon of dispFather.children) {
					// agregar propiedad formulas
					dispSon.formulas = {};
					dispSon.formulas.ytd = 0;

					// PARSEAR NOMBRES DEL DISPOSITION
					const cleanName = this.cleanDispositionName(dispSon.name);

					// agregar resultados meses (nombre de propiedades son los meses en string)
					// para asignar puede volver a iterarse de esta forma 
					// se debe llamar este antes que los dias para evitar que esten despues en las 
					// claves de diccionario (objeto)
					for (const month of this.getLastMonths) {
						dispSon.formulas[month] = 0; // function
					}
					// days properties (nombre de propiedade son los valores dias en propiedad day)
					for (const dayob of this.days) {
						// console.log('d', dayob)
						// por defecto colocar 0 en el resultado de la formula
						dispSon.formulas[dayob.day] = 0;
						if (daysResponse.includes(dayob.day_resp)) {
							// undefined (los nombre pueden no ser los mismos)
							// se deben eliminar espacios en blanco y colocar piso
							// colocar todas en miniscula

							const valueFormula = this.filters[dayob.day_resp][cleanName];
							if (valueFormula !== undefined) {
								dispSon.formulas[dayob.day] = valueFormula;
							}
						}
					}

					// ejemplo de volver a iterar los meses para asignar valor
					for (const month of this.getLastMonths) {
						const monthstr = moment().set("month", month).format("MM");
						// const daysOfMonth = moment().month(month).daysInMonth();
						// dispSon.formulas[month] = 10;
						dispSon.formulas[month] = arrayResponse.map((dayReport) => {
							const datesplit = dayReport.date.split('/');
							// ej = [d, m, y] => [30, 03, 2020]
							if (datesplit[1] === monthstr) {
								if (dayReport.report[cleanName] !== undefined) {
									return dayReport.report[cleanName];
								} else {
									return 0;
								}
							} else {
								return 0;
							}
						}).reduce((a, b) => {
							return a + b;
						}, 0);

						if (cleanFatherName === 'total_hours') {
							console.log('hijo', cleanName, 'padre', cleanFatherName);
							// dispSon.formulas[month] = dispSon.formulas[month] / daysOfMonth;
							console.log('+++++++ moment', daysResponse)

							let count_day = daysResponse.reduce((obj, current) => {
								const [,month,] = current.split("/");
								obj[month] = obj[month] ? obj[month] + 1 : 1
								return obj
							}, {});

							let daysInMonth = 1;
							 // moment().month(month).daysInMonth()

							for (const mes in count_day) {
								// statement
								let month_now = moment().month(month).format('MM')
								console.log('messssssss', mes)
								console.log('monthhhhhhh', month_now)
								if (mes == month_now) {
									daysInMonth = count_day[mes]
								}
							console.log('+++++++ count_day', daysInMonth)
							}
							dispSon.formulas[month] = dispSon.formulas[month] / daysInMonth;
							// dispSon.formulas[month] = `${daysInMonth}` ;
						}

						// agregar resultado en ytd (sumatoria de meses)
						dispSon.formulas.ytd = dispSon.formulas.ytd + dispSon.formulas[month];
					}
					key_dispSon++
					if (key_dispSon > 0 && key_dispSon < 4) {
						dispSon.formulas.ytd = dispSon.formulas.ytd / 3;						
					}
					// dispSon.formulas.ytd = key_dispSon;
				}

				// NOTA EN ESTE CASO SE ITERAN PRIMERO LOS HIJOS
				// POR LOS SIGUIENTES MOTIVOS:

				// - que se hace con los padres sin formula? ()
				// - el resultado de los padres es la suma de sus hijos? ()

				// agregar propiedad formulas
				dispFather.formulas = {};
				dispFather.formulas.ytd = 0;
				// agregar resultados meses (nombre de propiedades son los meses en string)
				// para asignar puede volver a iterarse de esta forma 
				// se debe llamar este antes que los dias para evitar que esten despues en las 
				// claves de diccionario (objeto)
				for (const month of this.getLastMonths) {
					dispFather.formulas[month] = 0; // function
				}

				for (const dayob of this.days) {
					// console.log('d', dayob)
					dispFather.formulas[dayob.day] = 0; // function
					if (daysResponse.includes(dayob.day_resp)) {
						// console.log('formula', this.filters[dayob.day_resp][dispFather.fatherName]); // no coinide

						const valuef = this.filters[dayob.day_resp][cleanFatherName];
						// asignar el valor
						if (valuef !== undefined) {
							dispFather.formulas[dayob.day] = valuef;
						}
					}
				}

				// ejemplo de volver a iterar los meses para asignar valor
				for (const month of this.getLastMonths) {
					// console.log('m', month);
					const monthstr = moment().set("month", month).format("MM");
					// console.log(monthstr);
					// const daysOfMonth = moment().month(month).daysInMonth();
					// console.log(dayOfMonth);
					// dispFather.formulas[month] = 10;
					dispFather.formulas[month] = arrayResponse.map((dayReport) => {
						const datesplit = dayReport.date.split('/');
						// ej = [d, m, y] => [30, 03, 2020]
						if (datesplit[1] === monthstr) {
							if (dayReport.report[cleanFatherName] !== undefined) {
								return dayReport.report[cleanFatherName];
							} else {
								return 0;
							}
						} else {
							return 0;
						}
					}).reduce((a, b) => {
						return a + b;
					}, 0);
					// dispFather.formulas[month] = dispFather.formulas[month] / daysOfMonth;
					// dispFather.formulas[month] = dispFather.formulas[month] / moment().month(month).daysInMonth();
					// agregar resultado en ytd (sumatoria de meses)
					dispFather.formulas.ytd = dispFather.formulas.ytd + dispFather.formulas[month];
				}
				// dispFather.formulas.ytd = dispFather.formulas.ytd / 3

				// NO BORRAR TODAVIA USADO COMO REFERENCIA

				// days properties (nombre de propiedade son los valores dias en propiedad day)
				// for (const dayob of this.days) {
				// 	// console.log('d', dayob)
				// 	dispFather.formulas[dayob.day] = 0; // function
				// 	// total hours valor que no depende los hijos
				// 	// revisar que el nombre del padre este correcto

				// 	// ETAPA DE PRUEBA ESTA CASADO CON = total hours
				// 	const FtotalHours = "total_hours"; // requerido este nombre
				// 	// se hacer las transformaciones
				// 	const originName = dispFather.fatherName;
				// 	// const processName = dispFather.fatherName.toLowerCase().trim();
				// 	// const t2 = processName.split(' ');
				// 	const t3 = this.cleanDispositionName(dispFather.fatherName);
				// 	if (FtotalHours === t3 && daysResponse.includes(dayob.day_resp)) {
				// 		console.log(originName, 'si es total_hours')
				// 		console.log('day', dayob.day_resp);
				// 		console.log('response', this.filters[dayob.day_resp]);
				// 		console.log('formula', this.filters[dayob.day_resp][dispFather.fatherName]); // no coinide
				// 		console.log('formula', this.filters[dayob.day_resp][t3]);

				// 		const valuef = this.filters[dayob.day_resp][t3];
				// 		// asigno el valor
				// 		dispFather.formulas[dayob.day] = valuef;

				// 		// ejemplo de volver a iterar los meses para asignar valor
				// 		for (const month of this.getLastMonths) {
				// 			// console.log('m', month);
				// 			const monthstr = moment().set("month", month).format("MM");
				// 			console.log(monthstr);
				// 			// dispFather.formulas[month] = 10;
				// 			dispFather.formulas[month] = arrayResponse.map((dayReport) => {
				// 				const datesplit = dayReport.date.split('/');
				// 				// ej = [d, m, y] => [30, 03, 2020]
				// 				if (datesplit[1] === monthstr) {
				// 					return dayReport.report.total_hours;
				// 				} else {
				// 					return 0;
				// 				}
				// 			}).reduce((a, b) => {
				// 				return a + b;
				// 			}, 0);
				// 			// agregar resultado en ytd (sumatoria de meses)
				// 			dispFather.formulas.ytd = dispFather.formulas.ytd + dispFather.formulas[month];
				// 		}
				// 	} else {
				// 		console.log(originName, 'no es total_hours')
				// 	}
				// }


			}

			// AÑADIENDO DAY RESP EN LOS DIAS GENERADOS POR GET_DAYS
			// PARA QUE POSEAN EL MISMO FORMATO QUE EL DE LA RESPUETA
			// DE BACKEND 

			// resultados formulas por dia
			// for (const dayob of this.days) {
			// 	console.log(dayob.day_resp);
			// 	if (daysResponse.includes(dayob.day_resp)) {
			// 		console.log('d', dayob.day_resp);
			// 		console.log('r', this.filters[dayob.day_resp]);
			// 	} else {
			// 		console.log(0)
			// 	}
			// }

			// reasignar la tabla
			this.tableBodyTest = cloneArr;
			let arrayMonths = [this.tableBodyTest[0]]
			let currentMoment2 = moment.monthsShort(moment().month());
			let arrayOfMonth = moment.monthsShort();
			console.log(currentMoment2)
			arrayOfMonth.splice(arrayOfMonth.indexOf(currentMoment2) + 1, arrayOfMonth.length)
			console.log("Array", arrayOfMonth)
			console.log(this.tableBodyTest)
			if (this.tableBodyTest.length !== 0) {
				let newValues = Object.entries(this.tableBodyTest[0].formulas);
				let filter = newValues.filter(value => {
					for (const iterator of value) {
						// console.log("iterator", iterator)
						if (iterator == 'Feb') {
							console.log(iterator[value])
						}
					}
				})
			}
		},

		/**
		 * [cleanDispositionName description]
		 * cambiar (" ") -> ("_")
		 * cambiar a minuscula
		 * @return {[type]} [description]
		 */
		cleanDispositionName(name) {
			const originName = name.trim();
			// PARSEAR TODOS LOS DISPOSITION SOLO LETRAS
			const processName = originName.replace(/[^a-zA-Z]+/g, " ").toLowerCase().trim();
			const t2 = processName.split(' ');
			return t2.join('_');
		},

		/**
		 * [getFormulasMonth description]
		 * @return {[type]} [description]
		 */
		async getFormulasMonth() {
			try {
				this.loading = true;
				const campaign_id = localStorage.getItem('campaign_id');
				const request = {
					start: `${moment()
						.set("month", this.getLastMonths[0])
						.startOf('month')
						.format("MM/DD/YYYY")}`,
					// start: `${moment()
					// 	.set("month", 'Jan')
					// 	.startOf('month')
					// 	.format("MM/DD/YYYY")}`,
					end: moment()
						.set("month", this.getLastMonths[this.getLastMonths.length - 1])
						.endOf('month')
						.format("MM/DD/YYYY"),
					// campaign_id: this.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
					campaign_id: campaign_id
				};
				console.log("Hacer consulta", request)
				// pedir formulas
				const response = await this.$store.dispatch(
					"report_2/GET_FILTERS",
					request,
				);
				this.filters = response.data.data;
				// actualizar cabecera
				this.getDays(this.selectedMonth);
				// construrir cuerpo
				this.$_generateTableBody2();
				// modificar cuerpo
				this.pruebav2();
			} catch (error) {
				console.log(error);
			} finally {
				this.loading = false;
			}

		},

		/**
		 * [selectOtherMonth description]
		 * @return {[type]} [description]
		 */
		selectOtherMonth(month) {
			// actualizar cabecera
			this.getDays(moment.monthsShort().indexOf(month));
			// construrir cuerpo
			this.$_generateTableBody2();
			// modificar cuerpo
			this.pruebav2();
		},

		async getValuesTable() {
			this.getDays(this.currentMonth);
			await this.getDisposition();
			await this.getFormulasMonth();
		}

	},

};
