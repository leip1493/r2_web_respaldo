import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// LAYOUT
import { layout } from './modules/layout/layout';
import { auth } from './modules/Auth/Auth';
import { users } from './modules/Users/Users';
import { report_1 } from './modules/Reports/report_1';
import { report_2 } from './modules/Reports/report_2';
import { report_3 } from './modules/Reports/report_3';
import { report_4 } from './modules/Reports/report_4';
import { campaigns } from './modules/Campaigns/Campaigns';
import { audit } from './modules/Audit/audit';

export default new Vuex.Store({
    modules: {
        layout,
        auth,
        users,
        report_1,
        report_2,
        report_3,
        report_4,
        campaigns,
        audit
    },
    state: {},
    getters: {},
    mutations: {},
    actions: {}
});
