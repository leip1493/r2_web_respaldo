export const layout = {
    namespaced: true,
    state: {
        drawer: true
    },
    getters: {
        DRAWER_VALUE(state) {
            return state.drawer;
        }
    },
    mutations: {
        SET_DRAWER(state, payload) {
            state.drawer = payload;
        }
    },
    actions:{}
};
