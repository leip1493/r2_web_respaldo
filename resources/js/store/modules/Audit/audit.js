import Api from '../../../Services/index';


export const audit = {
    namespaced: true,
    state: {
        total: 0,
        page_count: 0,
        audit: [],
        
    },
    getters: {
        PAGINATION(state) {
            return {
                total: state.total,
                page_count: state.page_count
            };
        },
        GET_AUDIT(state) {
            return state.audit;
        },
        GET_AUDIT_KEY(state) {
            return state.audit;
        },
    },
    mutations: {
        SET_PAGINATE(state, payload) {
            state.total = payload.total;
            state.page_count = payload.page_count;
        },
        SET_AUDIT(state, payload) {
            state.audit = payload;
        }
    },
    actions: {
        async INDEX({ commit }, payload) {
            try {
                const response = await Api.get(
                    `/api/v1/audits?start_date=${payload.start}&end_date=${payload.end}&limit=9&page=${payload.page}`
                );
                commit("SET_PAGINATE", {
                    total: response.data.data.total,
                    page_count: Math.ceil(
                        response.data.data.total / response.data.data.per_page
                    )
                });
                commit("SET_AUDIT", response.data.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async INDEX_DATE({ commit }, payload) {
            try {
                const response = await Api.get(
                    `/api/v1/audits?start_date=${payload.start}&end_date=${payload.end}&campaign_id=${payload.campaign_id}&limit=9&page=${payload.page}`
                );
                commit("SET_PAGINATE", {
                    total: response.data.data.total,
                    page_count: Math.ceil(
                        response.data.data.total / response.data.data.per_page
                    )
                });
                commit("SET_AUDIT", response.data.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
};
