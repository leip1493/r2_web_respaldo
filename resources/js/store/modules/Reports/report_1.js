import ApiFile from '../../../Services/file';
import Api from '../../../Services/index';


export const report_1 = {
  namespaced: true,
  state: {
    agents_general: [],
    itemsAgent: [],
    dateAgents: {},
    agents_general_fecha: []
  },
  getters: {
    GET_AGENTS(state) {
      const agentsHcc = state.agents_general.filter(value => {
        if (value.hcc == 1) {
          return value
        }
      });
      const newArray = agentsHcc.filter((item) => {
        return state.agents_general_fecha.includes(item.name)
      })
      console.log('newArray', newArray)
      return newArray
    },
    GET_AGENTS_GENERAL(state) {
      return state.agents_general;
    },
    GET_DATE_AGENTS(state) {
      return state.dateAgents;
    },
  },
  mutations: {
    SET_PUSH_AGENT(state, payload) {
      state.dateAgents = payload
      console.log('agents push', payload)
    },
    SET_AGENTS(state, payload) {
      state.agents_general = payload;
      console.log('agents agents_general', payload)
    },
    SET_AGENTS_TABLE(state, payload) {
      state.itemsAgent = payload.data;
    },
    SET_AGENTS_DATE(state, payload) {
      console.log('agents_date respuestaaaaaaaaaaaaaaaa', payload)
      state.agents_general_fecha = Object.keys(payload)
    }
  },
  actions: {
    async ADD_FILE({ commit }, payload) {
      try {
        const response = await ApiFile.post('/api/v1/call-log/upload', payload);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },
    async GET_FILE({ commit }, payload) {
      try {
        const id = localStorage.getItem("campaign_id");
        const response = await Api.get(
          `/api/v1/call-log?start_date=${payload.date1}&end_date=${payload.date2}&campaign_id=${payload.campaign_id}&group_by=agents-dates&hcc=1`
          // `/api/v1/call-log?start_date=03/03/2019&end_date=03/10/2020
          //   &campaign_id=${payload.campaign_id}&group_by=agents-dates&hcc=1`
        );
        console.log('respuestaaaaaaaaaa hhjhj', response.data.data)

        commit("SET_AGENTS_DATE", response.data.data);
        commit("SET_PUSH_AGENT", response.data.data);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },
    async GET_AGENTS({ commit }, payload) {
      try {
        console.log('payload.campaign_id',payload.campaign_id)
        const response = await Api.get(`/api/v1/agents?campaigns[0]=${payload.campaign_id}`);
        commit("SET_AGENTS", response.data.data);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },
    async GRAPH({ commit }, payload) {
      try {
        console.log("ID DE LA CAMPANA", payload.campaign_id)
        if (payload.campaign_id == 0 || payload.campaign_id == null) {
          console.log("NULLLLL")
          return null;
        } else {
          const response = await Api.get(`/api/v1/call-log?start_date=${payload.ini}&end_date=${payload.end}&group_by=dates&campaign_id=${payload.campaign_id}`);
          return response.data;

        }
      } catch (error) {
        return Promise.reject(error);
      }
    },
  }
};
