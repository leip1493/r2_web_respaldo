import Api from "../../../Services/index";

export const report_4 = {
    namespaced: true,
    state: {
        total: 0,
        page_count: 0,
        call_log: [],
        call_log_key: [],
        headers: [
            {
                text: "Call ID",
                name: "call_id",
                value: "telephonic_data.call_id",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "Timestamp",
                name: "timestamp",
                value: "telephonic_data.timestamp",
                class: "length_text tr-color tam-header-call-log",
                width: 250,
                sortable: false,
            },
            {
                text: "Call Type",
                name: "call_type",
                value: "telephonic_data.call_type",
                class: "length_text tr-color tam-header-call-log",
                width: 250,
                sortable: false,
            },
            {
                text: "Agent",
                name: "agent_name",
                value: "telephonic_data.agent_name",
                class: "length_text tr-color tam-header-call-log",
                width: 350,
                sortable: false,
            },
            {
                text: "Disposition",
                name: "disposition",
                value: "telephonic_data.disposition",
                class: "length_text tr-color tam-header-call-log",
                width: 250,
                sortable: false,
            },
            // {
            //     text: "Subdisposition",
            //     name: "sub_disposition",
            //     value: "telephonic_data.sub_disposition",
            //     class: "length_text tr-color tam-header-call-log",
            //     width: 130,
            //     sortable: false,
            // },
            // {
            //     text: "BTN",
            //     name: "btn",
            //     value: "telephonic_data.btn",
            //     class: "length_text tr-color tam-header-call-log",
                // width: 130,
            //     sortable: false,
            // },
            {
                text: "ANI",
                name: "ani",
                value: "telephonic_data.ani",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "DNIS",
                name: "dnis",
                value: "telephonic_data.dnis",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "Call Time",
                name: "call_time",
                value: "telephonic_data.call_time",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "Talk Time",
                name: "talk_time",
                value: "telephonic_data.talk_time",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "Hold Time",
                name: "hold_time",
                value: "telephonic_data.hold_time",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "Park Time",
                name: "park_time",
                value: "telephonic_data.park_time",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "ACW",
                name: "acw",
                value: "telephonic_data.acw",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            },
            {
                text: "Campaign",
                name: "campaign",
                value: "telephonic_data.campaign",
                class: "length_text tr-color tam-header-call-log",
                width: 300,
                sortable: false,
            },
            {
                text: "List Name",
                name: "list_name",
                value: "telephonic_data.list_name",
                class: "length_text tr-color tam-header-call-log",
                width: 130,
                sortable: false,
            }
        ]
    },
    getters: {
        PAGINATION(state) {
            return {
                total: state.total,
                page_count: state.page_count
            };
        },
        GET_CALL_LOG(state) {
            return state.call_log;
        },
        GET_CALL_LOG_KEY(state) {
            return state.call_log_key;
        }
    },
    mutations: {
        SET_PAGINATE(state, payload) {
            state.total = payload.total;
            state.page_count = payload.page_count;
        },
        SET_CALL_LOG(state, payload) {
            state.call_log = payload;
            let element = {};
            state.call_log_key = [];

            if (
                payload.length > 0 &&
                payload[0]?.telephonic_data &&
                payload[0].telephonic_data?.id
            ) {
                const object = payload
                .map(index => {
                    let data = {
                        name: "total",
                        ...index.telephonic_data
                    };
                    return data;
                })
                .reduce((accumulator, currentValue) => {
                    const {
                        call_id,
                        timestamp,
                        call_type,
                        agent_name,
                        disposition,
                        sub_disposition,
                        btn,
                        ani,
                        dnis,
                        call_time,
                        talk_time,
                        hold_time,
                        park_time,
                        acw,
                        campaign,
                        list_name
                    } = currentValue;

                    accumulator[currentValue.name] = {
                        call_id:
                            accumulator[currentValue.name]?.call_id +
                                (call_id !== null ? 1 : 0) || 0,
                        timestamp:
                            accumulator[currentValue.name]?.timestamp +
                                (timestamp !== null ? 1 : 0) || 0,
                        call_type:
                            accumulator[currentValue.name]?.call_type +
                                (call_type !== null ? 1 : 0) || 0,
                        agent_name:
                            accumulator[currentValue.name]?.agent_name +
                                (agent_name !== null ? 1 : 0) || 0,
                        disposition:
                            accumulator[currentValue.name]?.disposition +
                                (disposition !== null ? 1 : 0) || 0,
                        sub_disposition:
                            accumulator[currentValue.name]?.sub_disposition +
                                (sub_disposition !== null ? 1 : 0) || 0,
                        btn:
                            accumulator[currentValue.name]?.btn +
                                (btn !== null ? 1 : 0) || 0,
                        ani:
                            accumulator[currentValue.name]?.ani +
                                (ani !== null ? 1 : 0) || 0,
                        dnis:
                            accumulator[currentValue.name]?.dnis +
                                (dnis !== null ? 1 : 0) || 0,
                        call_time:
                            accumulator[currentValue.name]?.call_time +
                                (call_time !== null ? 1 : 0) || 0,
                        talk_time:
                            accumulator[currentValue.name]?.talk_time +
                                (talk_time !== null ? 1 : 0) || 0,
                        hold_time:
                            accumulator[currentValue.name]?.hold_time +
                                (hold_time !== null ? 1 : 0) || 0,
                        park_time:
                            accumulator[currentValue.name]?.park_time +
                                (park_time !== null ? 1 : 0) || 0,
                        acw:
                            accumulator[currentValue.name]?.acw +
                                (acw !== null ? 1 : 0) || 0,
                        campaign:
                            accumulator[currentValue.name]?.campaign +
                                (campaign !== null ? 1 : 0) || 0,
                        list_name:
                            accumulator[currentValue.name]?.list_name +
                                (list_name !== null ? 1 : 0) || 0
                    };
                    return accumulator;
                }, {}).total;

                for (const key in object) {
                    if (object.hasOwnProperty(key) && object[key] > 0) {
                        element = state.headers.find(index => {
                            return index.name == key;
                        });

                        if (element !== undefined) {
                            state.call_log_key.push(element);
                        }
                    }
                }
            }
            // console.log("cabeceras", state.call_log_key);
        }
    },
    actions: {
        async INDEX({ commit }, payload) {
            try {
                const response = await Api.get(
                    `/api/v1/call-log?start_date=${payload.start}&end_date=${payload.end}&campaign_id=${payload.campaign_id}&limit=6&page=1&hcc=1`
                );
                // commit("SET_ALL_DISPOSITION", response.data.data);
                commit("SET_PAGINATE", {
                    total: response.data.data.total,
                    page_count: Math.ceil(
                        response.data.data.total / response.data.data.per_page
                    )
                });
                commit("SET_CALL_LOG", response.data.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async GET_INDEX_CALL_LOG({ commit }, payload) {
            try {
                const response = await Api.get(
                    `/api/v1/call-log?start_date=${payload.start}&end_date=${payload.end}&campaign_id=${payload.campaign_id}&limit=9&page=${payload.page}&hcc=1`
                );
                commit("SET_PAGINATE", {
                    total: response.data.data.total,
                    page_count: Math.ceil(
                        response.data.data.total / response.data.data.per_page
                    )
                });
                commit("SET_CALL_LOG", response.data.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        }
    }
};
