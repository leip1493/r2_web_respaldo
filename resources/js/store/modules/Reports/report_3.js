import Api from '../../../Services/index';


export const report_3 = {
    namespaced: true,
    state: {
    },
    getters: {
    },
    mutations: {
    },
    actions: {
        async INDEX({ commit }, payload) {
            try {
                const response = await Api.get(`/api/v1/call-log?start_date=${payload.start}&end_date=${payload.end}&group_by=dates-agents&campaign_id=${payload.campaign_id}&hcc=1`);
                // commit("SET_ALL_DISPOSITION", response.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
};
