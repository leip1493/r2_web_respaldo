import Api from '../../../Services/index';


export const report_2 = {
    namespaced: true,
    state: {
        allDispositions: []
    },
    getters: {
        GET_DISPOSITIONS(state) {
            return state.allDispositions;
        }
    },
    mutations: {
        SET_ALL_DISPOSITION(state, payload) {
            state.allDispositions = payload;
        },
        SET_DISPOSITION(state, payload) {
            state.allDispositions.push(payload);
        },
        SET_UPDATE_DISPOSITION(state, payload) {
            state.allDispositions.splice(payload.position_array, 1, payload.value);
        },
        SET_DELETED_DISPOSITION(state, payload) {
            console.log("STORE",payload);
            state.allDispositions.splice(payload, 1);
        }
    },
    actions: {
        async GET_LIST_DISPOSITION({ commit }, payload) {
            try {
                console.log("!23123123123")
                const response = await Api.get(`/api/v1/dispositions?campaign_id=${payload.id}`);
                commit("SET_ALL_DISPOSITION", response.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async REGISTER_DISPOSITION({ commit }, payload) {
            try {
                const response = await Api.post(`/api/v1/dispositions`, payload);
                commit("SET_DISPOSITION", response.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async UPDATE_DISPOSITION({ commit }, payload) {
            try {
                const response = await Api.put(`/api/v1/dispositions/${payload.id}`, payload.data);
                commit("SET_UPDATE_DISPOSITION", { position_array: payload.data.position_array, value: response.data.data });
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async DELETE_DISPOSITION({ commit }, payload) {
            try {
                console.log(payload);
                const response = await Api.delete(`/api/v1/dispositions/${payload.id}`);
                commit("SET_DELETED_DISPOSITION", payload.id -1);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async GET_FILTERS({ commit, rootState  }, payload) {
            try {
                // rootState.campaigns.campaign_id
                console.log(payload, 'payload')
                const response = await Api.get(
                    // `/api/v1/call-log?start_date=03/01/2020&end_date=03/31/2020&group_by=dates&campaign_id=${1}&hcc=1`
                    `/api/v1/call-log?start_date=${payload.start}&end_date=${payload.end}&group_by=dates&campaign_id=${payload.campaign_id}&hcc=1`
                );
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async UPDATE_MANY_AGENTS({ commit  }, payload) {
            try {
                const response = await Api.post(`/api/v1/agents/update-many`, payload);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        }
    }
};
