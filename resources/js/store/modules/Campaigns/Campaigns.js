import Api from '../../../Services/index';
import ApiFile from '../../../Services/file';

export const campaigns = {
  namespaced: true,
  state: {
    itemsCampam: [],
    itemsDashboard: [],
    campaign_id: null,
    campaigns: [],
    campaign_name: '',
    selectCampaign: {},
    date_general: {},
    daily_information: [],
    total_call_log: 0,        
    call_log_page_count: 0,
    permission: []
  },
  getters: {
    GET_CAMPAIGNS_ID(state) {
      return state.campaign_id;
    },
    GET_CAMPAIGNS(state) {
      return state.campaigns;
    },
    // GET_PERMISSION(state) {
    //   return state.permission;
    // },
    GET_DATE_DASHBOARD(state) {
      return state.date_general;
    },
    SHOW_CAMPAIGNS(state) {
      return state.campaigns;
    },
    GET_CAMPAIGNS_NAME(state) {
      return state.campaign_name;
    },
    GET_SELECTED_CAMPAIGN(state) {
      return state.selectCampaign;
    },
    GET_DAILY_INFORMATION(state) {
      return state.daily_information;
    },
    PAGINATION(state) {
        return {
            total: state.total_call_log,        
            page_count: state.call_log_page_count,
        };
    },
  },
  mutations: {
    SET_PUSH_CAMPAIGNS(state, payload) {
      state.campaigns.push(payload);
    },
    SET_CAMPAIGNS_ID(state, payload) {
      state.campaign_id = payload.campaign_id
      localStorage.setItem('campaign_id', payload.campaign_id);
    },
    SET_DATE_DASHBOARD_TRY(state, payload) {
      state.date_general = payload.date_general;
    },
    SET_SELECTED_CAMPAIGN_TRY(state, payload) {
      state.selectCampaign = payload.selectCampaign;
    },



    // SET_CAMPAIGNS_NAME(state, payload) {
    //   state.campaign_name = payload;
    // },
    SET_CAMPAIGNS(state, payload) {
      state.campaigns = payload
    },
    SET_UPDATED_CAMPAIGNS(state, payload) {
      state.campaigns.splice(payload.position_array, 1, payload.data);
    },
    SET_DELETED_CAMPAIGNS(state, payload) {
      let position = state.campaigns.indexOf(payload.data)
      state.campaigns.splice(position, 1);
    },
    SET_SELECTED_CAMPAIGN(state, payload) {
      console.log('nombre campala', payload)
      localStorage.setItem('selectCampaign', JSON.stringify(payload));
      state.selectCampaign = payload;
    },
    SET_DATE_DASHBOARD(state, payload) {
      localStorage.setItem('date_general', JSON.stringify(payload));
      state.date_general = payload;
    },
    SET_DAILY_INFORMATION(state, payload) {
      state.daily_information = payload;
    },
    // SET_PERMISSION(state, payload) {
    //   state.permission = payload;
    // },
    SET_PAGINATE(state, payload) {
        state.total_call_log = payload.total;
        state.call_log_page_count = payload.page_count;
    },
  },
  actions: {
    async REGISTER({ commit }, payload) {
      try {
        const response = await Api.post('/api/v1/campaigns', payload);
        console.log(response, 'guardando campala')
        commit("SET_PUSH_CAMPAIGNS", response.data.data);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },
    async UPDATED({ commit }, payload) {
      try {
        const response = await Api.put(`/api/v1/campaigns/${payload.id}`, payload)
        console.log(response.data.data, 'updated fff')
        const data = {
          position_array: payload.position_array,
          data: response.data.data
        }
        commit("SET_UPDATED_CAMPAIGNS", data);
        return response;
      } catch (error) {
        return Promise.reject(error);
      }
    },
    async INDEX({ commit }, payload) {
      try {
        const response = await Api.get('/api/v1/campaigns');
        commit("SET_CAMPAIGNS", response.data.data);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },
    // async PERMISSIONS({ commit }, payload) {
    //   try {
    //     const response = await Api.get('/api/v1/permissions?search=front');
    //     commit("SET_PERMISSION", response.data.message.data);
    //     console.log('response permission', response.data.message.data)
    //     return response.data;
    //   } catch (error) {
    //     return Promise.reject(error);
    //   }
    // },
    async DELETED({ commit }, payload) {
      try {
        const response = await Api.delete(`/api/v1/campaigns/${payload.id}`);
        let data = {
          data: payload
        }
        commit("SET_DELETED_CAMPAIGNS", data);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },
    async DELETE_DATE_REGISTER({ commit }, payload) {
      try {
        console.log('register', payload)
        const response = await Api.delete(`/api/v1/call-log/${payload.id}/by-date`, {data: payload});
        // let data = {
        //   data: payload
        // }
        // commit("SET_DELETED_CAMPAIGNS", data);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },    
    async DAILY_INFORMATION({ commit }, payload) {
      try {
        const response = await Api.get(`/api/v1/logs/file?limit=200&page=${payload.page}&campaign_id=${payload.campaign_id}`);
        commit("SET_PAGINATE", {
                        total: response.data.data.total,
                        page_count: Math.ceil(response.data.data.total / 
                            response.data.data.per_page)
                      });
        commit("SET_DAILY_INFORMATION", response.data.data.data);
        return response.data;
      } catch (error) {
        return Promise.reject(error);
      }
    },
  }
};
