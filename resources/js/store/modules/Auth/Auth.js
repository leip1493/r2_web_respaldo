import Api from "../../../Services/index";
import ApiFile from "../../../Services/file";

export const auth = {
    namespaced: true,
    state: {
        token: null,
        user_name: "",
        auth_user_permissions: [],
        permission_admin: false
    },
    getters: {
        IS_AUTHENTICATED(state) {
            return state.token == null ? false : true;
        },
        USER_NAME(state) {
            return state.user_name;
        },
        GET_USER_PERMISSIONS(state) {
            const permissions = state.auth_user_permissions.map(
                item => item.id
            );
            permissions.push(0);
            permissions.push(1);
            permissions.push(2);
            return permissions;
        },
        GET_PERMISSIONS_ADMIN(state) {
            return state.permission_admin;
        }
    },
    mutations: {
        SET_TOKEN(state, payload) {
            state.token = payload;
        },
        SET_USER_NAME(state, payload) {
            state.user_name = payload;
        },
        SET_USER_PERMISSIONS(state, payload) {
            state.auth_user_permissions = payload;
        },
        SET_PERMISSIONS_ADMIN(state, payload) {
            if (payload == 1 || payload == 2) state.permission_admin = true;
            else state.permission_admin = false;
        }
    },
    actions: {
        async LOGIN_USER({ commit }, payload) {
            try {
                const response = await Api.post("/api/v1/auth/login", payload);
                Api.defaults.headers.common[
                    "Authorization"
                ] = `Bearer ${response.data.data.token}`;
                ApiFile.defaults.headers.common[
                    "Authorization"
                ] = `Bearer ${response.data.data.token}`;
                const response_campaing = await Api.get("/api/v1/campaigns");

                let campaign_id = 0;
                let selectCampaign = {};

                if (response_campaing.data.data.length > 0) {
                    // let data = response_campaing.data.data.find((index) => {
                    //     return index.name == 'YYC'
                    // });

                    // if (data !== undefined) {
                    //     campaign_id = data.id
                    //     selectCampaign = data;
                    // }else {
                    console.log(
                        "parse int campaing",
                        parseInt(localStorage.getItem("campaign_id"))
                    );
                    if (parseInt(localStorage.getItem("campaign_id")) > 0) {
                        campaign_id = parseInt(
                            localStorage.getItem("campaign_id")
                        );
                        let data = response_campaing.data.data.find(index => {
                            return index.id == campaign_id;
                        });

                        if (data !== undefined) {
                            selectCampaign = data;
                        } else {
                            let data_yyc = response_campaing.data.data.find(
                                index => {
                                    return index.name == "YYC";
                                }
                            );

                            if (data_yyc !== undefined) {
                                campaign_id = data_yyc.id;
                                selectCampaign = data_yyc;
                            } else {
                                campaign_id = response_campaing.data.data[0].id;
                                selectCampaign = response_campaing.data.data[0];
                            }
                        }
                    } else {
                        let data_yyc = response_campaing.data.data.find(
                            index => {
                                return index.name == "YYC";
                            }
                        );

                        if (data_yyc !== undefined) {
                            campaign_id = data_yyc.id;
                            selectCampaign = data_yyc;
                        } else {
                            campaign_id = response_campaing.data.data[0].id;
                            selectCampaign = response_campaing.data.data[0];
                        }
                    }
                    // }
                }

                commit("SET_TOKEN", response.data.data.token);
                commit("SET_USER_NAME", response.data.data.user.name);
                commit(
                    "campaigns/SET_CAMPAIGNS_ID",
                    { campaign_id: campaign_id },
                    { root: true }
                );
                commit(
                    "campaigns/SET_SELECTED_CAMPAIGN_TRY",
                    { selectCampaign: selectCampaign },
                    { root: true }
                );

                // LLENAR ARREGLO DE PERMISOS
                const permissions = response.data.data.user.permissions;
                const role = response.data.data.user.roles[0].id;
                commit("SET_PERMISSIONS_ADMIN", role);
                commit("SET_USER_PERMISSIONS", permissions);

                localStorage.setItem(
                    "auth_permission",
                    JSON.stringify(permissions)
                );
                localStorage.setItem("token", response.data.data.token);
                localStorage.setItem(
                    "role",
                    response.data.data.user.roles[0].id
                );
                localStorage.setItem("id", response.data.data.user.id);
                localStorage.setItem("user_name", response.data.data.user.name);
                localStorage.setItem("campaign_id", campaign_id);
                localStorage.setItem(
                    "selectCampaign",
                    JSON.stringify(selectCampaign)
                );
                // localStorage.setItem("id", response.data.data.user.id);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async TRY_AUTO_LOGIN({ commit, dispatch }, payload) {
            try {
                const token = localStorage.getItem("token");
                const role = localStorage.getItem("role");
                const auth_permission = localStorage.getItem("auth_permission");
                const user_name = localStorage.getItem("user_name");
                const campaign_name = localStorage.getItem("campaign_name");
                const campaign_id = parseInt(
                    localStorage.getItem("campaign_id")
                );
                const selectCampaign = JSON.parse(
                    localStorage.getItem("selectCampaign")
                );
                const date_general = JSON.parse(
                    localStorage.getItem("date_general")
                );
                if (token === null) {
                    return;
                } else {
                    Api.defaults.headers.common[
                        "Authorization"
                    ] = `Bearer ${token}`;
                    ApiFile.defaults.headers.common[
                        "Authorization"
                    ] = `Bearer ${token}`;
                    commit("SET_TOKEN", token);
                    commit("SET_PERMISSIONS_ADMIN", role);
                    commit("SET_USER_PERMISSIONS", JSON.parse(auth_permission));
                    commit("SET_USER_NAME", user_name);
                    // commit('campaigns/SET_CAMPAIGNS_NAME', campaign_name, { root: true })
                    commit(
                        "campaigns/SET_CAMPAIGNS_ID",
                        { campaign_id: campaign_id },
                        { root: true }
                    );
                    commit(
                        "campaigns/SET_SELECTED_CAMPAIGN_TRY",
                        { selectCampaign: selectCampaign },
                        { root: true }
                    );
                    commit(
                        "campaigns/SET_DATE_DASHBOARD_TRY",
                        { date_general: date_general },
                        { root: true }
                    );
                    return true;
                }
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async logout({ commit }) {
            try {
                const response = await Api.post("/api/v1/auth/logout");
                commit("SET_TOKEN", null);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        }
    }
};
