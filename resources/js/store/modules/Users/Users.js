import Api from '../../../Services/index';


export const users = {
    namespaced: true,
    state: {
        role: [],
        permission: [],
        users: [],
        // pageCount: 0,
        // itemsPerPage: 0
    },
    getters: {
        GET_ROL(state) {
            return state.role;
        },
        GET_PERMISSION(state) {
            return state.permission;
        },
        GET_USERS(state) {
            const authId = localStorage.getItem('id');
            console.log("***", parseInt(authId));
            // if (parseInt(authId) == 1) {
                return state.users;
            // } else {
            //     const parseData = state.users.filter(value => {
            //         if (value.roles[0].id !== 1) {
            //             console.log("asdas", value)
            //             return value
            //         }
            //     })
            //     return parseData;
            // }
        },
    },
    mutations: {
        SET_ROL(state, payload) {
            state.role = payload;
        },
        SET_PERMISSION(state, payload) {
            state.permission = payload;
            // state.permission = payload.filter((index) => {
            //     return index.id !== 22;
            // });;
        },
        SET_USERS(state, payload) {
            state.users = payload.data;
            state.pageCount = payload.last_page;
            state.itemsPerPage = payload.to;
        },
        SET_PUSH_USER(state, payload) {
            state.users.push(payload);
        },
        SET_UPDATED_USER(state, payload) {
            state.users.splice(payload.position_array, 1, payload.data);
            // state.users = Object.assign({}, item)
        },
        SET_DELETED_USER(state, payload) {
            let position = state.users.indexOf(payload.data)
            state.users.splice(position, 1);
        }
    },
    actions: {
        async REGISTER_USER({ commit }, payload) {
            try {
                const response = await Api.post('/api/v1/users', payload);
                commit("SET_PUSH_USER", response.data.data.user);
                console.log(response.data, 'registro user')
                return response.data;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async UPDATED_USER({ commit }, payload) {
            try {
                const response = await Api.put(`/api/v1/users/${payload.id}`, payload);
                const data = {
                    data: response.data.data.user,
                    position_array: payload.position_array
                }
                commit("SET_UPDATED_USER", data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async INDEX_USER({ commit }, payload) {
            try {
                const response = await Api.get(`/api/v1/users?withCampaigns=1`);
                console.log(response, 'usuarios index')
                commit("SET_USERS", response.data);
                return response.data;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async ROL_USER({ commit }, payload) {
            try {
                const response = await Api.get('/api/v1/roles');
                commit("SET_ROL", response.data.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async PERMISSION_USER({ commit }, payload) {
            try {
                const response = await Api.get('/api/v1/permissions?search=front');
                commit("SET_PERMISSION", response.data.message.data);
                return response;
            } catch (error) {
                return Promise.reject(error);
            }
        },
        async DELETED({ commit }, payload) {
            try {
                const response = await Api.delete(`/api/v1/users/${payload.id}`);
                let data = {
                    data: payload
                }
                commit("SET_DELETED_USER", data);
                return response.data;
            } catch (error) {
                return Promise.reject(error);
            }
        },
    }
};
