<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>Errescuer</title>
  @if ( config('app.url')  == 'https://hccreports.online')
   <link rel="stylesheet" href="https://hccreports.online/css/app.css">
  @else
   <link rel="stylesheet" href="{{ asset('css/app.css') }}">
  @endif
</head>
<body >
  <div id="app">
    <App></App>
  </div>
  @if ( config('app.url')  == 'https://hccreports.online')
    <script defer src="https://hccreports.online/js/app.js"></script>
  @else
    <script defer src="{{ asset('js/app.js') }}"></script>
  @endif

<!--<script src="{{ asset('js/app.js') }}"></script>-->
</body>
</html>
