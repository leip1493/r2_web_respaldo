<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Custom REST Language Response Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | corresponding the API endpoints.
    |
    */

    'error' => 'Error',

    'success' => 'Success',

    'http' => [
        'method_not_allowed' => 'Http method not allowed or not found',
        'not_found' => 'Not found',
    ],

    'model' => [
        'found' => 'Records found successfully.',
        'not_found' => 'Model records not found.',
        'create' => [
            'success' => "Successful :name creation!",
            'failed' => "An error has occurred in the creation of :name",
        ],
        'edit' => [
            'success' => "Successful :name edition!",
            'failed' => "An error has occurred in the edition of :name",
        ],
        'already_exists' => 'Already have a :name created.'
    ],

    'service' => [
        'response' => [
            'not_found' => "No data found in the service",
        ],
        'connection' => [
            'failed' => 'Error trying to connect to the service. Try again later'
        ]
    ]
];
