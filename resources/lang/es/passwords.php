<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Su contraseña ha sido reiniciada',
    'sent' => 'Hemos enviado un correo con su enlace de reinicio de contraseña!',
    'throttled' => 'Por favor espere antes de reintentar.',
    'token' => 'El token para reinicio de contraseña es inválido',
    'user' => "No se ha podido conseguir usuario con el correo suministrado",

];
