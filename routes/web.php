<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('call-log')->group(function () {
    Route::get('/download-csv', 'CallLogController@downloadCSV');
});

// SPA VUE
Route::get('/{any}', function () {
    return view('Master/Master');
})->where('any', '.*');
