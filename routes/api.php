<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix("v1")->group(function () {
    Route::get('test', function () {
        return "test";
    });
    Route::middleware(['auth:api'])->group(function () {

        Route::prefix('users')->group(function () {
            Route::get('/', 'UserController@index');
            Route::post('/', 'UserController@store')->middleware(['role:super-admin|admin']);
            Route::get('/{user}', 'UserController@show')->middleware(['role:super-admin|admin'])->where("user", "[0-9]+");
            Route::put('/{user}', 'UserController@update')->middleware(['role:super-admin|admin'])->where("user", "[0-9]+");
            Route::patch('/{user}', 'UserController@update')->middleware(['role:super-admin|admin'])->where("user", "[0-9]+");
            Route::delete('/{user}', 'UserController@destroy')->middleware(['role:super-admin|admin'])->where("user", "[0-9]+");
        });
        Route::prefix('roles')->group(function () {
            Route::get('/', 'RoleController@index')->middleware('permission:roles.index');
            Route::post('/', 'RoleController@store')->middleware('permission:roles.create');
            Route::get('/{role}', 'RoleController@show')->middleware('permission:roles.show')->where("role", "[0-9]+");
        });
        Route::prefix('permissions')->group(function () {
            Route::get('/', 'PermissionController@index')->middleware('permission:permissions.index');
            Route::post('/', 'PermissionController@store')->middleware('permission:permissions.create');
            Route::get('/{permission}', 'PermissionController@show')->middleware('permission:permissions.show')->where("permission", "[0-9]+");
            Route::put('/{permission}', 'PermissionController@update')->middleware('permission:permissions.update')->where("permission", "[0-9]+");
        });
        Route::prefix('campaigns')->group(function () {
            Route::get('/', 'CampaignController@index');
            Route::post('/', 'CampaignController@store');
            Route::get('/{campaign}', 'CampaignController@show');
            Route::put('/{campaign}', 'CampaignController@update');
            Route::patch('/{campaign}', 'CampaignController@update');
            Route::delete('/{campaign}', 'CampaignController@destroy');
        });

        Route::prefix('call-log')->group(function () {
            Route::get('/', 'CallLogController@index');
            Route::post('/upload', 'CallLogController@upload');
            Route::get('/download-csv', 'CallLogController@downloadCSV');
            Route::delete('/{campaign}/by-date', 'CallLogController@deleteByDate')->middleware('role:super-admin');
        });
        Route::prefix('logs')->group(function () {
            Route::prefix('session')->group(function () {
                Route::get('/', 'LogController@getLogSession');
                Route::get('/{user}', 'LogController@userLogSession');
            });
            Route::prefix('file')->group(function () {
                Route::get('/', 'LogController@getLogFiles');
                Route::get('/{user}', 'LogController@userLogFiles');
            });
        });
        Route::prefix('agents')->group(function () {
            Route::get('/', 'AgentController@index');
            Route::get('/{agent}', 'AgentController@show');
            Route::put('/{agent}', 'AgentController@update');
            Route::patch('/{agent}', 'AgentController@update');
            Route::post('/update-many', 'AgentController@updateMany');
        });
        Route::prefix('dispositions')->group(function () {
            Route::get('/', 'DispositionController@index');
            Route::post('/', 'DispositionController@store');
            Route::get('/{disposition}', 'DispositionController@show');
            Route::put('/{disposition}', 'DispositionController@update');
            Route::patch('/{disposition}', 'DispositionController@update');
            Route::delete('/{disposition}', 'DispositionController@destroy');
        });
        Route::prefix('audits')->group(function () {
            Route::get('/', 'AuditController@index')
                ->middleware('role:super-admin');
            Route::get('/download-csv', 'AuditController@downloadCSV')->middleware('role:super-admin');
        });
    });


    Route::prefix('auth')->group(function () {
        Route::post('register', "AuthController@register");
        Route::post('login', "AuthController@login");
        Route::post('logout', "AuthController@logout");
        Route::get('loggedUserData', "AuthController@loggedUserData");
        Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
        Route::post('password/reset', 'Auth\ResetPasswordController@reset');
        // Register
        // Route::post('register', 'RegisterController@register');
        // Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail');
        // Route::post('password/reset', 'ResetPasswordController@reset');
        // Route::get('verify/email/{code}', 'RegisterController@verifyEmail');
        // // Login
        // Route::post('login', 'LoginController@login');
        // Route::post('logout', 'LoginController@logout');
        // Route::get('login/data', 'LoginController@loggedUserData');
        // // Profile
        // Route::post('profile/edit', 'ProfileController@edit');
    });

    Route::any('/{any}', function () {
        return response()->restResponse([
            "status" => __('rest.error'),
            "statusCode" => 404,
            "message" => trans('rest.http.not_found'),
            "error" => trans('rest.http.not_found'),
        ]);
    })->where('any', '.*');
});
