<?php

namespace App\Imports;

use App\Imports\AImportable;
use App\Models\CallLog;


class CallLogImportFactory
{
    public function getImport($dialing_platform, ...$params): AImportable
    {
        switch ($dialing_platform) {
            case CallLog::XENCALL:
                return new XenCallImport(...$params);
            case CallLog::RINGCENTRAL:
                return new RingCentralImport(...$params);
            case CallLog::THREECX:
                return new ThreeCXImport(...$params);
            case CallLog::NIMBUS:
                return new NimbusImport(...$params);
            default:
                throw new \Exception("$dialing_platform file not implemented.", 400);
        }
    }
}
