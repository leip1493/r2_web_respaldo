<?php

namespace App\Imports;

use App\Imports\AImportable;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
// Models
use App\User;
use App\Models\{Agent, Campaign, CallLog, TelephonicData, CrmData, Disposition};
// Laravel Excel
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Illuminate\Contracts\Queue\ShouldQueue;

use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;

class XenCallImport extends AImportable implements OnEachRow, WithHeadingRow, WithBatchInserts, WithChunkReading, ShouldQueue, SkipsOnFailure, SkipsOnError, WithEvents
{
    use Importable;

    public function __construct(User $creator, String $nameFile,  Campaign $campaign)
    {
        parent::__construct($creator, $nameFile, $campaign);
    }

    public function onRow(Row $row)
    {
        $row = $row->toArray();
        Log::debug("CSV: New row", $row);
        $this->incrementRowCounter();

        // AGENT
        $agentName = preg_replace("/\s+/", "", $row['agent_name']);
        $agent = Agent::firstOrCreate([
            'name' => blank($agentName) ? "No agent" : $row['agent_name']
        ]);
        $this->incrementAgentCounter($agentName);
        $agent->campaigns()->syncWithoutDetaching($this->campaign->id);

        // DISPOSITION
        $dispositionName = is_null($row['sent_to']) ? $row['log_type'] :  $row['log_type'] . " - " . $row['sent_to'];
        $disposition = Disposition::firstOrCreate([
            'name' => $dispositionName
        ]);

        // CALL LOG
        $callLog = new CallLog([
            'upload_file_name' => $this->nameFile,
            'type' => 'xencall'
        ]);
        $callLog->campaign()->associate($this->campaign);
        $callLog->uploadedBy()->associate($this->creator);
        $callLog->agent()->associate($agent);
        $callLog->disposition()->associate($disposition);
        $callLog->save();

        // TELEPHONIC DATA
        $date = Carbon::parse($row['log_time_date'])->format('d/m/Y');
        $end_time = Carbon::parse($row['log_time_time'])
            ->addSeconds($row["recording_length_seconds"])
            ->format('h:i:s A');

        $timestamp = Carbon::parse($row['log_time']);
        $this->verifyAndUpdateRangeTimestamps($timestamp);
        $telephonicData = new TelephonicData([
            'call_id' => $row['call_log_id'],
            'timestamp' => $timestamp,
            'call_type' => $row['call_type'],
            'agent_name' => $row['agent_name'],
            'disposition' => $row['log_type'],
            'sub_disposition' => $row['sent_to'],
            'btn' => $row['phone'],
            // NOT PRESENT 'ani' => $row['ani'],
            // NOT PRESENT 'dnis' => $row['dnis'],
            'call_time' => $row['recording_length_seconds'],
            // NOT PRESENT 'talk_time' => $row['talk_time'],
            // NOT PRESENT 'hold_time' => $row['hold_time'],
            // NOT PRESENT 'park_time' => $row['park_time'],
            // NOT PRESENT 'acw' => $row['acw'],
            'campaign' => $row['current_campaign'],
            'list_name' => $row['original_lead_file'],
            // CUSTOM FIELDS
            'date' => $date,
            'start_time' => $row['log_time_time'],
            'end_time' => $end_time,
            'call_notes' => $row['call_notes']
        ]);
        $telephonicData->callLog()->associate($callLog);
        $telephonicData->save();

        // CRM DATA
        $crmData = new CrmData([
            "first_name" => $row['first_name'],
            "last_name" => $row['last_name'],
            // NOT PRESENT "title" => $row['title'],
            "email" => $row['e_mail_address'],
            "company" => $row['company_name'],
            "address" => $row['address'],
            "state" => $row['state'],
            "zip" => $row['zip_code'],
            "fax" => $row['fax_number'],
            // NOT PRESENT "sic" => $row['sic'],
            // NOT PRESENT "sic_description" => $row['sic_description'],
            // NOT PRESENT "employees" => $row['employees'],
            // NOT PRESENT "sales_volume" => $row['sales_volume']
        ]);
        $crmData->callLog()->associate($callLog);
        $crmData->save();
    }

    // public function rules(): array
    // {
    //     return [
    //         'call_log_id' => Rule::unique('telephonic_data', "call_id")
    //     ];
    // }

    public function onFailure(Failure ...$failures)
    {
        // Handle the failures how you'd like.
        Log::debug("CSV: onFailure", $failures);
    }

    public function onError(\Throwable $e)
    {
        // Handle the exception how you'd like.
        Log::debug("CSV: onError", ["error" => $e]);
    }

    public function batchSize(): int
    {
        return 1000;
    }
    public function chunkSize(): int
    {
        return 1000;
    }

    public function registerEvents(): array
    {
        return [

            BeforeImport::class => function () {
                Log::debug("::::::::: Starting import Xencall :::::::::");
            },
            AfterImport::class => function () {
                Log::debug("::::::::: Finished importing Xencall :::::::::");
                $this->logUploadedFile();
            },
        ];
    }
}
