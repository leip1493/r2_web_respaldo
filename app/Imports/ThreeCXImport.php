<?php

namespace App\Imports;

use App\Imports\AImportable;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
// Models
use App\User;
use App\Models\{Agent, Campaign, CallLog, TelephonicData, CrmData, Disposition};
// Laravel Excel
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;
use Maatwebsite\Excel\Events\ImportFailed;

class ThreeCXImport extends AImportable implements OnEachRow, WithHeadingRow, WithChunkReading, ShouldQueue, WithEvents, WithCustomCsvSettings
{
    use Importable;

    public function __construct(User $creator, String $nameFile,  Campaign $campaign)
    {
        parent::__construct($creator, $nameFile, $campaign);
    }

    public function onRow(Row $row)
    {
        $row = $row->toArray();

        $this->verifyIfFileIsValid($row);

        if (
            is_null($row['btn']) ||
            !$this->validateIfExtnIsInRange($row['extn'])
        ) {
            return;
        }

        $this->incrementRowCounter();

        // AGENT
        $agentName = $row['agent_or_queue'];
        $isEmpty = blank(preg_replace("/\s+/", "", $agentName)); // Se valida si el nombre esta vacío
        $agentName = $isEmpty ? "No agent" : $agentName;
        $agent = Agent::firstOrCreate([
            'name' => $agentName
        ]);

        $this->incrementAgentCounter($agentName);
        // if ($agent->wasRecentlyCreated)
        //     $this->incrementAgentCounter();

        $agent->campaigns()->syncWithoutDetaching($this->campaign->id);

        // DISPOSITION
        $dispositionName = $row['call_disposition'];
        $isEmpty = blank(preg_replace("/\s+/", "", $dispositionName)); // Se valida si el nombre esta vacío
        $dispositionName = $isEmpty ? "No disposition" : $dispositionName;
        $disposition = Disposition::firstOrCreate([
            'name' => $dispositionName
            // 'name' => $row['call_disposition']
        ]);

        // CALL LOG
        $callLog = new CallLog([
            'upload_file_name' => $this->nameFile,
            'type' => '3cx'
        ]);
        $callLog->campaign()->associate($this->campaign);
        $callLog->uploadedBy()->associate($this->creator);
        $callLog->agent()->associate($agent);
        $callLog->disposition()->associate($disposition);
        $callLog->save();

        // TELEPHONIC DATA
        $timestamp = Carbon::parse($row['call_date'] . " " . $row['start']);
        $this->verifyAndUpdateRangeTimestamps($timestamp);

        $date = $timestamp->format('d/m/Y');
        $start_time = $timestamp->format('H:i:s A');
        $end_time = Carbon::createFromFormat("H:i:s", $row["end"])->format("H:i:s A");

        $talk_time = Carbon::createFromFormat("H:i:s", $row['talking'])->secondsSinceMidnight();
        $ringing =  Carbon::createFromFormat("H:i:s", $row['ringing'])
            ->secondsSinceMidnight();
        $call_time = $talk_time + $ringing;
        $telephonicData = new TelephonicData([
            'call_id' => $timestamp->secondsSinceMidnight() . $row['btn'] . $row['extn'], // No posee call_id el csv y en el documento se sugiere usar el call time
            'timestamp' => $timestamp,
            'call_type' => $row['ibob'], // IB/OB
            'agent_name' => $agentName,
            'disposition' => $dispositionName,
            //NOT PRESENT 'sub_disposition' => $row['sent_to'],
            'btn' => $row['btn'],
            // NOT PRESENT 'ani' => $row['ani']
            // NOT PRESENT 'dnis' => $row['dnis'],
            'call_time' => $call_time,
            'talk_time' => $talk_time,
            // NOT PRESENT 'hold_time' => $row['sec_on_hold'],
            // NOT PRESENT 'park_time' => $row['park_time'],
            // NOT PRESENT 'acw' => $row['inbound_duration'] - $row['agent_duration'],
            'campaign' => $row['extn'],
            'list_name' => $row['campaign'],
            // CUSTOM FIELDS
            'date' => $date,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'call_notes' => $row['reason']
        ]);
        $telephonicData->callLog()->associate($callLog);
        $telephonicData->save();

        // Log::debug('timestamps', [
        //     "id" => $callLog->id,
        //     "timestamps" =>  $timestamp->toDateTimeString(),
        //     "call_date" =>  $row['call_date'],
        //     "start" =>  $row['start']
        // ]);
    }

    private function validateIfExtnIsInRange($extn): bool
    {
        $extn = (int) $extn;
        return (($extn >= 3000 && $extn <= 3099) || ($extn >= 7000 && $extn <= 7999));
    }

    private function verifyIfFileIsValid(array $row): void
    {
        if (
            !array_key_exists("call_date", $row) ||
            !array_key_exists("btn", $row)
        ) {
            throw new ImportFileInvalid($this->nameFile, $this->campaign->name);
        }
    }


    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            // 'enclosure' => 'ISO-8859-1',
            // 'delimiter'         => ';',
            // 'enclosure'         => '"',
            // 'escape_character'  => '\\',
            // 'contiguous'        => false,
            // 'input_encoding'    => 'UTF-8',
        ];
    }

    public function registerEvents(): array
    {
        return [

            BeforeImport::class => function () {
                Log::debug("::::::::: Starting import 3CX :::::::::");
            },
            AfterImport::class => function () {
                Log::debug("::::::::: Finished importing 3CX :::::::::");
                $this->logUploadedFile();
            },
            ImportFailed::class => function (ImportFailed $event) {
                // Log::debug("::::::::: 3CX EXPLOTOOO :::::::::");
            },
        ];
    }
}
