<?php

namespace App\Imports;

use App\Models\Audit;
use App\Models\Campaign;
use App\User;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Importable;
use Illuminate\Support\Facades\Cache;

abstract class AImportable
{
    use Importable;

    const MAX_DURATION_CACHE_VARIABLES = 60 * 60 * 6;
    protected $creator;
    protected $nameFile;
    protected $campaign;

    public function __construct(User $creator, String $nameFile,  Campaign $campaign)
    {
        $this->creator = $creator;
        $this->nameFile = $nameFile;
        $this->campaign = $campaign;
        $this->initializeCacheVariables();
    }

    private function initializeCacheVariables(): void
    {
        Cache::put("count-file-rows-" . $this->nameFile, 0);
        Cache::put("count-file-agents-" . $this->nameFile, 0);
        Cache::put("agents-in-file" . $this->nameFile, json_encode([]), self::MAX_DURATION_CACHE_VARIABLES);
        Cache::put("from-timestamp-record" . $this->nameFile, "", self::MAX_DURATION_CACHE_VARIABLES);
        Cache::put("to-timestamp-record" . $this->nameFile, "", self::MAX_DURATION_CACHE_VARIABLES);
    }

    private function rowsCounter(): int
    {
        return (int) Cache::pull("count-file-rows-" . $this->nameFile);
    }
    private function agentsCounter(): int
    {
        return (int) Cache::pull("count-file-agents-" . $this->nameFile);
    }

    protected function incrementRowCounter(): void
    {
        Cache::increment("count-file-rows-" . $this->nameFile);
    }

    protected function incrementAgentCounter(string $agentName): void
    {
        $agentsInFile = json_decode(Cache::get("agents-in-file" . $this->nameFile));
        $exist = in_array($agentName, $agentsInFile);
        if (!$exist) {
            $agentsInFile[] = $agentName;
            Cache::put(
                "agents-in-file" . $this->nameFile,
                json_encode($agentsInFile),
                self::MAX_DURATION_CACHE_VARIABLES
            );
            Cache::increment("count-file-agents-" . $this->nameFile);
        }
    }

    private function getFromTimestamp(): ?string
    {
        $timestamp = Cache::pull("from-timestamp-record" . $this->nameFile);
        return empty($timestamp) ? NULL : $timestamp;
    }

    private function getToTimestamp(): ?string
    {
        $timestamp = Cache::pull("to-timestamp-record" . $this->nameFile);
        return empty($timestamp) ? NULL : $timestamp;
    }
    public function verifyAndUpdateRangeTimestamps(Carbon $timestamp): void
    {
        $from = Cache::get("from-timestamp-record" . $this->nameFile);
        $to = Cache::get("to-timestamp-record" . $this->nameFile);

        if ($timestamp->lessThan($from) || empty($from)) {
            Cache::put(
                "from-timestamp-record" . $this->nameFile,
                $timestamp->toDateTimeString(),
                self::MAX_DURATION_CACHE_VARIABLES
            );
        }
        if ($timestamp->greaterThan($to) || empty($to)) {
            Cache::put(
                "to-timestamp-record" . $this->nameFile,
                $timestamp->toDateTimeString(),
                self::MAX_DURATION_CACHE_VARIABLES
            );
        }
    }

    protected function logUploadedFile(): void
    {
        $logFile = $this->creator->logFiles()->create([
            'name' => $this->nameFile,
            'count_rows' =>  $this->rowsCounter(),
            'count_agents' => $this->agentsCounter(),
            'from_timestamp' => $this->getFromTimestamp(),
            'to_timestamp' => $this->getToTimestamp()
        ]);
        $logFile->campaign()->associate($this->campaign);
        $logFile->save();
        // Audit::success(
        //     "UPLOAD",
        //     "{$logFile->count_rows} Call logs successfully loaded. Check the campaign log for details",
        //     $this->campaign->id,
        //     $this->creator->id
        // );
    }
}
