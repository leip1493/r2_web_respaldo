<?php

namespace App\Imports;

use App\Imports\AImportable;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Cache;
// Models
use App\User;
use App\Models\{Agent, Campaign, CallLog, TelephonicData, Disposition};
use Exception;
// Laravel Excel
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;

use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;

class NimbusImport extends AImportable implements OnEachRow, WithHeadingRow, WithChunkReading, ShouldQueue, SkipsOnFailure, SkipsOnError, WithEvents
{
    use Importable;

    const INBOUND = "INBOUND";
    const OUTBOUND = "OUTBOUND";
    const NO_AGENT_CALL_TIME = 60 * 60;

    public function __construct(User $creator, String $nameFile,  Campaign $campaign)
    {
        parent::__construct($creator, $nameFile, $campaign);
    }

    public function onRow(Row $row)
    {
        $row = $row->toArray();

        $this->verifyIfFileIsValid($row);

        if (!$this->isValidToInsert($row)) return;

        $this->incrementRowCounter();

        // AGENT
        $agentName = $row['agente'];
        $isEmptyAgentName = blank(preg_replace("/\s+/", "", $agentName));
        $agentName = $isEmptyAgentName ? "No agent" : $agentName;
        $agent = Agent::firstOrCreate(['name' => $agentName]);

        $this->incrementAgentCounter($agentName);
        $agent->campaigns()->syncWithoutDetaching($this->campaign->id);

        // DISPOSITION
        $disposition = Disposition::firstOrCreate([
            'name' => $this->getDispositionName($row)
        ]);

        // CALL LOG
        $callLog = new CallLog([
            'upload_file_name' => $this->nameFile,
            'type' => 'Nimbus'
        ]);
        $callLog->campaign()->associate($this->campaign);
        $callLog->uploadedBy()->associate($this->creator);
        $callLog->agent()->associate($agent);
        $callLog->disposition()->associate($disposition);
        $callLog->save();

        // TELEPHONIC DATA
        $timestamp = $this->getTimestamp($row);
        $this->verifyAndUpdateRangeTimestamps($timestamp);
        $date = $timestamp->format('d/m/Y');
        $start_time = $this->getStartTime($row);
        $end_time = $this->getEndTime($row);

        $call_time = $this->getCallTime($row);

        $call_id = $this->getCallId($row);

        $telephonicData = new TelephonicData([
            'call_id' => $call_id,
            'timestamp' => $timestamp->toDateTimeString(),
            'call_type' => $this->getCallType($row),
            'agent_name' => $agentName,
            'disposition' => $disposition->name,
            'sub_disposition' => $this->getSubDisposition($row),
            'btn' => $call_id,
            'ani' => $this->getAni($row),
            'dnis' => $this->getDNI($row),
            'call_time' => $call_time,
            // NOT PRESENT 'talk_time' => $talk_time,
            // NOT PRESENT 'hold_time' => $row['sec_on_hold'],
            // NOT PRESENT 'park_time' => $row['park_time'],
            // NOT PRESENT 'acw' => $row['inbound_duration'] - $row['agent_duration'],
            'campaign' => $row['nombre_aplicacion'],
            // NOT PRESENT 'list_name' => $row['campaign'],
            // CUSTOM FIELDS
            'date' => $date,
            'start_time' => $start_time,
            'end_time' => $end_time,
            // 'call_notes' => $row['reason']
        ]);
        $telephonicData->callLog()->associate($callLog);
        $telephonicData->save();

        if ($isEmptyAgentName && $call_time >= self::NO_AGENT_CALL_TIME) {
            $this->addNoAgentId($callLog->id);
        }
    }

    public function onFailure(Failure ...$failures)
    {
        Log::debug("CSV Nimbus: onFailure", $failures);
    }

    public function onError(\Throwable $e)
    {
        Log::debug("CSV Nimbus: onError", ["error" => $e]);
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function registerEvents(): array
    {
        return [

            BeforeImport::class => function () {
                Log::debug("::::::::: Starting import nimbus :::::::::");
                $this->initializeNimbusTypeFromNameFile();
                $this->initializeNoAgentsIds();
            },
            AfterImport::class => function () {
                Log::debug("::::::::: Finished importing nimbus :::::::::");
                $this->logUploadedFile();
                $noAgentIds = $this->getNoAgentIds();
                $this->assignNoAgentsWithDurationGreaterAllowedCallTime($noAgentIds);
            },
        ];
    }

    private function initializeNimbusTypeFromNameFile(): void
    {
        $type = preg_match('/Outbound/i', $this->nameFile) ? self::OUTBOUND
            : (preg_match('/Inbound/i', $this->nameFile) ? self::INBOUND : NULL);

        if (!$type)
            throw new Exception("The file must end in Inbound.csv or Outbound.csv", 400);

        Cache::put(
            "type-nimbus-file" . $this->nameFile . $this->creator->name,
            $type,
            self::MAX_DURATION_CACHE_VARIABLES
        );
    }

    private function verifyIfFileIsValid(array $row): void
    {
        if (
            !array_key_exists("aplicacion", $row) ||
            !array_key_exists("agente", $row)
        ) {
            throw new ImportFileInvalid($this->nameFile, $this->campaign->name);
        }
    }

    private function getTypeNimbusImport(): string
    {
        return Cache::get("type-nimbus-file" . $this->nameFile . $this->creator->name);
    }

    public function getTimestamp($row): Carbon
    {
        return Carbon::parse($row['fecha'] . " " . $row['hora_inicio']);
    }

    public function getStartTime($row): string
    {
        return $this->getTimestamp($row)->format('h:i:s A');
    }

    public function getEndTime($row): string
    {
        return Carbon::createFromFormat("H:i:s", $row["hora_fin"])->format("h:i:s A");
    }

    public function getCallTime($row): int
    {
        return Carbon::createFromFormat("H:i:s", $row['duracion'])->secondsSinceMidnight();
    }

    public function getCallId($row): string
    {
        return isset($row["caller_id"]) ? $row["caller_id"] : $this->getTimestamp($row)->secondsSinceMidnight() . $row["destino"];
    }

    private function getCallType($row): string
    {
        return $row["aplicacion"];
    }

    private function getDNI($row): int
    {
        return (int) ($this->getTypeNimbusImport() == self::INBOUND ? $row["did"] : $row["destino"]);
    }

    private function getAni($row): ?string
    {
        return $this->getTypeNimbusImport() == self::INBOUND ? $this->getCallId($row) : NULL;
    }

    private function getSubDisposition($row): ?string
    {
        return $this->getTypeNimbusImport() == self::INBOUND ? NULL : $row["resultado"];
    }


    private function existsInDatabase($row): bool
    {
        return CallLog::whereCampaignId($this->campaign->id)
            ->whereHas('telephonicData', function ($query) use ($row) {
                return $query->whereCallType($this->getCallType($row))
                    ->whereStartTime($this->getStartTime($row))
                    ->whereEndTime($this->getEndTime($row))
                    ->whereTimestamp($this->getTimestamp($row)->toDateTimeString())
                    ->whereDnis($this->getDNI($row));
            })
            ->exists();
    }

    private function isValidToInsert($row): bool
    {
        $exists = $this->existsInDatabase($row);

        if ($exists) {
            Log::debug("CSV: Nimbus DUPLICATE row", $row);
        }
        return !$exists && ($this->getTypeNimbusImport() == self::INBOUND
            ? $this->inboundValidations($row)
            : $this->outboundValidations($row));
    }

    private function inboundValidations($row): bool
    {
        $validDidNumbers = [
            5514540231, 5514540233, 5514540239, 5514540243, 5514540244, 5514540232, 5514540240,
            5514540241, 5514540242, 5514540245, 5514540246, 5514540247
        ];

        $validCampaigns = [
            "Sonrics Promociones", "Sonrics Chocolates", "Sonrics Chicles", "Pepsico Socio Estrella", "Pepsico Clientes",
            "Sonrics Dulces", "Gamesa Maizoro", "Gamesa Promociones", "Quaker", "Sabritas Doritos", "Sabritas Botana",
            "Pepsico CFC", "Desvio Ventas", "Desvio Consumidor",
        ];

        return in_array($row['did'], $validDidNumbers) || in_array($row['nombre_aplicacion'], $validCampaigns);
    }
    private function outboundValidations($row): bool
    {
        return preg_match("/pepsico/i", $row['grupo_agente']);
    }

    private function initializeNoAgentsIds(): void
    {
        Cache::put(
            "no-agents-ids" . $this->nameFile . $this->creator->name,
            json_encode([]),
            self::MAX_DURATION_CACHE_VARIABLES
        );
    }

    private function addNoAgentId($id): void
    {
        $noAgentIds = json_decode(Cache::get("no-agents-ids" . $this->nameFile . $this->creator->name));
        $noAgentIds[] = $id;
        Cache::put(
            "no-agents-ids" . $this->nameFile . $this->creator->name,
            json_encode($noAgentIds),
            self::MAX_DURATION_CACHE_VARIABLES
        );
    }

    private function getNoAgentIds(): array
    {
        return json_decode(Cache::pull("no-agents-ids" . $this->nameFile . $this->creator->name));
    }

    private function assignNoAgentsWithDurationGreaterAllowedCallTime(array $ids): void
    {
        $call_logs_to_assign = CallLog::with(['telephonicData', 'agent'])->whereIn('id', $ids)->get();

        foreach ($call_logs_to_assign as $call_log) {
            $agent = Agent::where("name", "<>", "No agent")
                ->whereHas('callLogs', function ($query) use ($call_log) {
                    $query->whereCampaignId($call_log->campaign_id);
                })
                ->whereDoesntHave('callLogs.telephonicData', function ($query) use ($call_log) {
                    $query->where('date', $call_log->telephonicData->date)
                        ->whereTime('start_time', ">=", $call_log->telephonicData->start_time)
                        ->whereTime('end_time', "<=", $call_log->telephonicData->end_time);
                })->inRandomOrder()->first();
            $call_log->agent()->dissociate();
            $call_log->agent()->associate($agent);
            $call_log->telephonicData()->update([
                'agent_name' => $agent->name,
                'timestamp' => $call_log->telephonicData->timestamp,
            ]);
            $call_log->save();
        }
    }

    private function getDispositionName($row): string
    {
        return $this->getTypeNimbusImport() == self::INBOUND
            ? $this->getInboundDisposition($row)
            : $this->getOutboundDisposition($row);
    }

    private function getInboundDisposition($row): string
    {
        $application = $row["aplicacion"];
        $application_name = $row["nombre_aplicacion"];
        $agent = $row["agente"];
        $disposition = "No disposition";

        if ($application == "ANUNCIO")
            $disposition = "Prerecorded Message Played";
        if ($application == "BUZON")
            $disposition = "Sent To Voicemail";
        if ($application == "IVR")
            $disposition = "CX Hung Up";
        if ($application == "IVR_SOLICITUD")
            $disposition = "EXtension Request";
        if ($application == "TIME_CONDITION")
            $disposition = "Time Condition";
        if ($application == "AGENTE" && $application_name == $agent)
            $disposition = "Not available";
        if ($application == "INBOUND" && $application_name != $agent)
            $disposition = "Answered";

        return $disposition;
    }

    private function getOutboundDisposition($row): string
    {
        $application = $row["aplicacion"];
        $result = $row["resultado"];
        $duration = $this->getCallTime($row);
        $agent = $row["agente"];
        $disposition = "No disposition";

        if ($application == "INBOUND" && $result == "ANSWER")
            $disposition = "Answered";
        if ($application == "INBOUND" && $result == "ANSWER")
            $disposition = "Not Answered";
        if ($application == "LOGIN")
            $disposition = "Agent Login";
        if ($application == "MANUAL" && $result == "NO ANSWER" && $duration == 0)
            $disposition = "Disconnected";
        if ($application == "MANUAL" && $result == "NO ANSWER" && $duration > 0)
            $disposition = "Busy Dial";
        if ($application == "MANUAL" && $result == "ANSWERED")
            $disposition = "Answered";
        if ($application == "OUTBOUND" && $result == "ANSWER")
            $disposition = "Answered";
        if ($application == "OUTBOUND" && $result == "NO ANSWER")
            $disposition = "Not Answered";

        return $disposition;
    }
}
