<?php

namespace App\Imports;

use Exception;

class ImportFileInvalid extends Exception
{
    public function __construct(string $filename, string $campaign)
    {
        parent::__construct("File $filename is not valid for $campaign campaign", 400);
    }
}
