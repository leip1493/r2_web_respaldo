<?php

namespace App\Imports;

use App\Imports\AImportable;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Log;
// Models
use App\User;
use App\Models\{Agent, Campaign, CallLog, TelephonicData, CrmData, Disposition};
// Laravel Excel
use Maatwebsite\Excel\Validators\Failure;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithValidation;
use Maatwebsite\Excel\Concerns\SkipsOnFailure;
use Maatwebsite\Excel\Concerns\SkipsOnError;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Row;
use Maatwebsite\Excel\Concerns\OnEachRow;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterImport;
use Maatwebsite\Excel\Events\BeforeImport;

class RingCentralImport extends AImportable implements OnEachRow, WithHeadingRow, WithChunkReading, ShouldQueue, WithCustomCsvSettings, WithEvents
{
    use Importable;

    public function __construct(User $creator, String $nameFile,  Campaign $campaign)
    {
        parent::__construct($creator, $nameFile, $campaign);
    }

    public function onRow(Row $row)
    {
        $row = $row->toArray();

        $this->verifyIfFileIsValid($row);

        $this->incrementRowCounter();

        // AGENT
        $agentName = $row['agent_fname'] . " " . $row['agent_lname'];
        $isEmpty = blank(preg_replace("/\s+/", "", $agentName)); // Se valida si el nombre esta vacío
        $agentName = $isEmpty ? "No agent" : $agentName;
        $agent = Agent::firstOrCreate([
            'name' => $agentName
        ]);
        $this->incrementAgentCounter($agentName);
        $agent->campaigns()->syncWithoutDetaching($this->campaign->id);

        // DISPOSITION
        // $dispositionName = $row['agent_disposition'];
        // $isEmpty = blank(preg_replace("/\s+/", "", $dispositionName)); // Se valida si el nombre esta vacío
        $disposition = Disposition::firstOrCreate([
            //'name' => $isEmpty ? "No disposition" : $dispositionName
            'name' => $row['agent_disposition']
        ]);

        // CALL LOG
        $callLog = new CallLog([
            'upload_file_name' => $this->nameFile,
            'type' => 'ringcentral'
        ]);
        $callLog->campaign()->associate($this->campaign);
        $callLog->uploadedBy()->associate($this->creator);
        $callLog->agent()->associate($agent);
        $callLog->disposition()->associate($disposition);
        $callLog->save();

        // TELEPHONIC DATA
        $date = Carbon::parse($row["enqueue_time"])->format('d/m/Y');
        $start_time = Carbon::parse($row["enqueue_time"])->format('h:i:s A');
        $end_time = Carbon::parse($row["dequeue_time"])->format('h:i:s A');

        $timestamp = Carbon::parse($row['enqueue_time']);
        $this->verifyAndUpdateRangeTimestamps($timestamp);
        //$call_id = $row['uii'] . "-" . $row['agent_duration'] . "-" . $agentName; //Debido a que se repite el uii pero con distintos agentes
        $telephonicData = new TelephonicData([
            'call_id' => $row['uii'],
            'timestamp' => $timestamp,
            'call_type' => $row['dial_type'],
            'agent_name' => $row['agent_fname'] . " " . $row['agent_lname'],
            'disposition' => $row['agent_disposition'],
            //NOT PRESENT 'sub_disposition' => $row['sent_to'],
            'btn' => $row['ani'],
            'ani' => $row['ani'],
            'dnis' => $row['dnis'],
            'call_time' => $row['inbound_duration'],
            'talk_time' => $row['agent_duration'],
            'hold_time' => $row['sec_on_hold'],
            // NOT PRESENT 'park_time' => $row['park_time'],
            'acw' => $row['inbound_duration'] - $row['agent_duration'],
            'campaign' => $row['gate_name'],
            // NOT PRESENT 'list_name' => $row['original_lead_file'],
            // CUSTOM FIELDS
            'date' => $date,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'call_notes' => $row['recording_url']
        ]);
        $telephonicData->callLog()->associate($callLog);
        $telephonicData->save();
    }

    private function verifyIfFileIsValid(array $row): void
    {
        if (
            !array_key_exists("agent_fname", $row) ||
            !array_key_exists("gate_name", $row)
        ) {
            throw new ImportFileInvalid($this->nameFile, $this->campaign->name);
        }
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function getCsvSettings(): array
    {
        return [
            // 'enclosure' => 'ISO-8859-1'
        ];
    }

    public function registerEvents(): array
    {
        return [

            BeforeImport::class => function () {
                Log::debug("::::::::: Starting import RingCentral :::::::::");
            },
            AfterImport::class => function () {
                Log::debug("::::::::: Finished importing RingCentral :::::::::");
                $this->logUploadedFile();
            },
        ];
    }
}
