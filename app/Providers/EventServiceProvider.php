<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\TestEvent' => [
            'App\Listeners\TestListener'
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // Event::listen('TestEvent', function ($foo, $bar) {
        //     // HACER ALGO
        //     Log::debug('DESDE SINGLE EVENT HANDLER');
        // });

        // Event::listen('event.*', function ($eventName, array $data) {
        //     // ESCUCHA VARIOS EVENTOS
        //     Log::debug('DESDE WILDCARD');
        // });
    }
}
