<?php

namespace App\Traits;

use App\Models\CallLog;

trait CallLogExport
{

    public function campaignHeaders(): array
    {

        switch ($this->campaign->dialing_platform) {
            case CallLog::NIMBUS:
                return [
                    // 'id',
                    'Call Id',
                    'Timestamp',
                    'Call Type',
                    'Call Time',
                    'Disposition',
                    'Agent',
                    // 'BTN',
                    'ANI',
                    'DNIS',
                    "Campaign"
                ];
            default:
                return [
                    // 'id',
                    'Call Id',
                    'Timestamp',
                    'Agent',
                    'Call Type',
                    'Call Time',
                    'Talk Time',
                    'Hold Time',
                    // 'Park Time',
                    'Disposition',
                    // 'Subdisposition',
                    'ACW',
                    // 'BTN',
                    'ANI',
                    'DNIS',
                    "Campaign",
                    "List name"
                ];
        }
    }

    public function campaignMap($call_log): array
    {
        $telephonicData = $call_log->telephonicData;

        switch ($this->campaign->dialing_platform) {
            case CallLog::NIMBUS:
                return [
                    // $telephonicData->id,
                    $telephonicData->call_id,
                    $telephonicData->timestamp,
                    $telephonicData->call_type,
                    $this->getTimeFormated($telephonicData->call_time),
                    $telephonicData->disposition,
                    $telephonicData->agent_name,
                    // $telephonicData->btn,
                    $telephonicData->ani,
                    $telephonicData->dnis,
                    $telephonicData->campaign,
                ];
            default:
                return [
                    // $telephonicData->id,
                    $telephonicData->call_id,
                    $telephonicData->timestamp,
                    $telephonicData->agent_name,
                    $telephonicData->call_type,
                    $this->getTimeFormated($telephonicData->call_time),
                    $this->getTimeFormated($telephonicData->talk_time),
                    $this->getTimeFormated($telephonicData->hold_time),
                    // $this->getTimeFormated($telephonicData->park_time),
                    $telephonicData->disposition,
                    // $telephonicData->sub_disposition,
                    $telephonicData->acw,
                    // $telephonicData->btn,
                    $telephonicData->ani,
                    $telephonicData->dnis,
                    $telephonicData->campaign,
                    $telephonicData->list_name,
                ];
        }
    }

    private function getTimeFormated(?int $seconds = 0): string
    {
        if ($seconds == 0) {
            return "";
        }
        return now()->setTime(0, 0, $seconds)->format('H:i:s');
    }
}
