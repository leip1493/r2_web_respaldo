<?php

namespace App\Traits;

use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Models\CallLog as CallLogModel;
use Illuminate\Database\Eloquent\Collection;

trait CallLog
{
    public static function groupByDate(Collection $callLogs, $type)
    {
        return collect($callLogs->groupBy(['telephonicData.date']))->map(function ($rows, $log) use ($type) {
            $calls = $rows->count();
            $total_hours = collect($rows->groupBy('telephonicData.agent_name'))->reduce(function ($total, $agent) {
                $firstRecord = $agent->firstWhere('telephonicData.timestamp', $agent->min('telephonicData.timestamp'));
                $lastRecord = $agent->firstWhere('telephonicData.timestamp', $agent->max('telephonicData.timestamp'));
                $total_seconds = Carbon::parse($firstRecord->telephonicData->timestamp)
                    ->floatDiffInRealSeconds(Carbon::parse($lastRecord->telephonicData->timestamp));
                $total += ($total_seconds + $lastRecord->telephonicData->call_time) / 3600;
                return $total;
            }, 0);

            $dials_per_hour = $calls / ($total_hours == 0 ? 1 : $total_hours);

            $totalContacts = $rows->where('disposition.contact', "contact")->count();
            $contacts_per_hour = $totalContacts / ($total_hours == 0 ? 1 : $total_hours);

            $primary_sales = $rows->where('disposition.primary', true)->count();

            $sales_pipeline = $rows->where('disposition.pipeline', true)->count();

            $applications_received = $rows->where('disposition.name', "Transfer - Applications Received")->count();

            $sales_per_hour = $primary_sales / ($total_hours == 0 ? 1 : $total_hours);

            $applications_sent = $rows->where('disposition.name', "Application Sent")->count();

            $information_on_company = $rows->where('disposition.name', "Info on Company")->count();

            $wood_apps = $rows->where('disposition.name', "Wood App Out")->count();

            $call_backs = $rows->where('disposition.name', "Callback")->count();

            $inbound = $rows->filter(function ($value, $key) use ($type) {
                $pattern = [];
                if ($type == "ringcentral") {
                    $pattern = ['N/A', "IN"];
                } elseif ($type ==  "xencall") {
                    $pattern = ["Inbound"];
                } elseif ($type ==  "3cx") {
                    $pattern = ["IB", "VM"];
                } elseif ($type ==  "nimbus") {
                    $pattern = ["INBOUND", "ANUNCIO", "BUZON", "IVR", "IVR_SOLICITUD", "TIME_CONDITION", "AGENTE"];
                    return in_array($value['telephonicData']['call_type'], $pattern);
                }
                return Str::startsWith($value['telephonicData']['call_type'], $pattern);
            })->count();

            $outbound = $rows->filter(function ($value, $key) use ($type) {
                $pattern = [];
                if ($type == "xencall")
                    $pattern = ["Dialer", "MANUAL", " "];
                else if ($type == "3cx")
                    $pattern = ["OB"];
                else if ($type == "nimbus") {
                    $call_type = $value['telephonicData']['call_type'];
                    $pattern = ["LOGIN", "MANUAL", "OUTBOUND"];
                    return in_array($call_type, $pattern);
                }
                return Str::startsWith($value['telephonicData']['call_type'], $pattern);
            })->count();

            if ($type === "ringcentral") {
                $outbound = $calls - $inbound;
            }

            $recycles = $rows->where('disposition.final', "recycle")->count();

            $not_available = $rows->where('disposition.name', "Not Available")
                ->where('disposition.final', "recycle")
                ->where("telephonicData.call_time", ">", 0)
                ->count();

            $busy = $rows->where('disposition.name', "Not Available")
                ->where('disposition.final', "recycle")
                ->where("telephonicData.call_time", 0)
                ->count();

            $info_on_company = $rows->where('disposition.name', "Info on Company")
                ->where('disposition.final', "recycle")
                ->count();

            $wood_app_out = $rows->where('disposition.name', "Wood App Out")
                ->where('disposition.final', "recycle")
                ->count();

            $scheduled_callback = $rows->where('disposition.name', "Callback")
                ->where('disposition.final', "recycle")
                ->count();

            $finalized_declines = $rows->where('disposition.decline', 1)->count();

            $not_interesed = $rows->where('disposition.name', "Not interested")
                ->where('disposition.final', "final")
                ->count();

            $do_not_call = $rows->where('disposition.name', "Do Not Call")
                ->where('disposition.final', "final")
                ->count();

            $not_a_business = $rows->where('disposition.name', "Not A Business")
                ->where('disposition.final', "final")
                ->count();

            $lead_finalized = $finalized_declines - $not_interesed - $do_not_call - $not_a_business;
            // Se insertan en el objeto de devolucion en formato de variable separada por guion bajo (_) los nombres de los dispositions existentes
            $output = collect($rows->groupBy('disposition.name')->mapWithKeys(function ($disposition, $key) {
                $newKey = preg_replace("/[^a-z]/i", " ", $key);
                $newKey = preg_replace("/\s+/i", "_", trim($newKey));
                $newKey = strtolower($newKey);
                return [$newKey => $disposition->where('disposition.name', $key)->count()];
            }));

            $output = $output->merge([
                "primary_sales" => $primary_sales,
                "sales_pipeline" => $sales_pipeline,
                "calls_log" => $calls,
                "total_hours" => $total_hours,
                "dials_per_hour" => $dials_per_hour,
                "contacts_per_hour" => $contacts_per_hour,
                "transfer_applications_received" => $applications_received,
                "sales_per_hour" => $sales_per_hour,
                "application_sent" => $applications_sent,
                "information_on_company" => $information_on_company,
                "wood_apps" => $wood_apps,
                "callback" => $call_backs,
                "inbound" => $inbound,
                "outbound" => $outbound,
                "recycles" => $recycles,
                "not_available" => $not_available,
                "busy" => $busy,
                "info_on_company" => $info_on_company,
                "wood_app_out" => $wood_app_out,
                "scheduled_callback" => $scheduled_callback,
                "finalized_declines" => $finalized_declines,
                "not_interesed" => $not_interesed,
                "do_not_call" => $do_not_call,
                "not_a_business" => $not_a_business,
                "lead_finalized" => $lead_finalized,
            ]);

            return $output;
        })->all();
    }

    public static function groupByAgents(Collection $callLogs)
    {
        return collect($callLogs->groupBy(['telephonicData.agent_name']))->map(function ($rows, $log) {
            $firstRecord =  $rows->firstWhere('telephonicData.timestamp', $rows->min('telephonicData.timestamp'));
            $lastRecord = $rows->firstWhere('telephonicData.timestamp', $rows->max('telephonicData.timestamp'));
            return [
                "total_hours" => Carbon::parse($firstRecord->telephonicData->timestamp)->floatDiffInRealHours($lastRecord->telephonicData->timestamp),
                "first_record" => $firstRecord,
                "last_record" => $lastRecord
            ];
        })->all();
    }

    public static function groupByAgentsDates(Collection $callLogs)
    {
        return collect($callLogs->groupBy(['telephonicData.agent_name']))->map(function ($rows, $agentName) {
            $groupedByDate = collect($rows)->groupBy('telephonicData.date');
            return collect($groupedByDate)->map(function ($rows, $log) {

                // $total_hours = collect($rows)->reduce(function ($total, $row) {
                //     // return $row;
                //     $total += $row["telephonicData"]["call_time"] / 3600;
                //     return $total;
                // }, 0);
                // return ["total_hours" => $total_hours];

                $firstRecord =  $rows->firstWhere('telephonicData.timestamp', $rows->min('telephonicData.timestamp'));
                $lastRecord = $rows->firstWhere('telephonicData.timestamp', $rows->max('telephonicData.timestamp'));
                $total_seconds =  Carbon::parse($firstRecord->telephonicData->timestamp)
                    ->floatDiffInRealSeconds($lastRecord->telephonicData->timestamp);
                $total_hours = ($total_seconds + $lastRecord->telephonicData->call_time) / 3600;
                return [
                    "total_hours" => $total_hours,
                    // "call_time" => $lastRecord->telephonicData->call_time,
                    "first_record" => $firstRecord["telephonicData"]["timestamp"],
                    "last_record" => $lastRecord["telephonicData"]["timestamp"]
                ];
            });
        })->all();
    }

    public static function groupByDatesAgents(Collection $callLogs)
    {
        return collect($callLogs->groupBy(['telephonicData.date']))->map(function ($rows, $agentName) {
            $callLogs = collect($rows);
            $callLogs = collect($callLogs->groupBy('telephonicData.agent_name'));
            return collect($callLogs)->map(function ($rows, $log) {
                $total_calls = $rows->count();
                $disposed_calls = $rows->whereNotIn('disposition.name', ["No disposition", "No Disposition Found"])->count();
                $firstRecord = (new self)->getFirstRecord($rows);
                $lastRecord = (new self)->getLastRecord($rows);
                $total_seconds =  Carbon::parse($firstRecord->telephonicData->timestamp)
                    ->floatDiffInRealSeconds($lastRecord->telephonicData->timestamp);
                $total_hours = ($total_seconds + $lastRecord->telephonicData->call_time) / 3600;

                $login = $firstRecord->telephonicData->timestamp;
                $logout = Carbon::parse($lastRecord->telephonicData->timestamp)
                    ->addSeconds($lastRecord->telephonicData->call_time)
                    ->toDateTimeString();
                $adherence = $total_hours * 100 / 8;
                $calls_per_hour = $total_calls / ($total_hours == 0 ? 1 : $total_hours);

                $talktime = $rows->reduce(function ($total, $row) {
                    $total += $row->telephonicData->talk_time / 3600;
                    return $total;
                }, 0);

                return [
                    "total_calls" => $total_calls,
                    "total_hours" => $total_hours,
                    "login" => $login,
                    "logout" => $logout,
                    "adherence" => $adherence,
                    "disposed_calls" => $disposed_calls,
                    "calls_per_hour" => $calls_per_hour,
                    "talktime" => $talktime,
                ];
            });
        })->all();
    }

    private function getFirstRecord($rows)
    {
        return $rows->firstWhere('telephonicData.timestamp', $rows->min('telephonicData.timestamp'));
    }

    private function getLastRecord($rows)
    {
        return $rows->firstWhere('telephonicData.timestamp', $rows->max('telephonicData.timestamp'));
    }
}
