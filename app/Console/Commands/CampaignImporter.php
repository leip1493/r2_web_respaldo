<?php

namespace App\Console\Commands;

use App\Imports\CallLogImportFactory;
use App\Imports\ImportFileInvalid;
use App\Models\Audit;
use App\Models\Campaign;
use App\Models\LogFile;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Validators\ValidationException;

class CampaignImporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:campaigns';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to start importing csv files from different campaigns';

    private $baseFolder = "campaigns";

    private $subFolders = ["3cx", "nimbus", "ringcentral"];

    private $importFactory;

    public function __construct(CallLogImportFactory $callLogImportFactory)
    {
        parent::__construct();
        $this->importFactory = $callLogImportFactory;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::first();

        Audit::success(
            "AUTOMATIC UPLOAD - STARTING",
            "The automatic csv import was started.",
            NULL,
            $user->id
        );

        $this->createFolders();

        $campaignFolders = Storage::directories($this->baseFolder);


        foreach ($campaignFolders as $campaignFolder) {
            Log::info("=== $campaignFolder files ===");

            $dialingPlatform = explode("/", $campaignFolder)[1];

            $filesPaths = Storage::allFiles($campaignFolder);
            Log::info("<$campaignFolder>", [
                "files" => count($filesPaths) > 0 ? $filesPaths : "Doesn't have files."
            ]);


            foreach ($filesPaths as $filePath) {
                $campaign = Campaign::whereDialingPlatform($dialingPlatform)->first();

                if (!$campaign) {
                    Log::info("$dialingPlatform campaign not found. Import is ignored");
                    Audit::error(
                        "AUTOMATIC UPLOAD",
                        "There is no campaign for the $dialingPlatform dialing platform.",
                        NULL,
                        $user->id
                    );
                    continue;
                }

                $fileExtension = pathinfo($filePath, PATHINFO_EXTENSION);

                $fileName = pathinfo($filePath, PATHINFO_FILENAME);

                if ($fileExtension !== 'csv') {
                    Log::info("File $filePath deleted for not having extension.");
                    Audit::error(
                        "AUTOMATIC UPLOAD",
                        "The $fileName file extension must be .csv. File deleted",
                        $campaign->id,
                        $user->id
                    );
                    Storage::delete($filePath);
                    continue;
                }

                $fileAlreadyUploaded = LogFile::whereName($fileName)->whereCampaignId($campaign->id)->first();
                if ($fileAlreadyUploaded && $campaign->dialing_platform != "nimbus") {
                    Log::info("The file $fileName is deleted because it has already been uploaded");
                    Storage::delete($filePath);
                    Audit::error(
                        "AUTOMATIC UPLOAD",
                        "The file $fileName is deleted because it has already been uploaded.",
                        $campaign->id,
                        $user->id
                    );
                    continue;
                }

                $import = $this->importFactory->getImport(
                    $campaign->dialing_platform,
                    $user,
                    $fileName,
                    $campaign
                );
                ini_set('max_execution_time', 7200);
                set_time_limit(0);
                try {
                    $import->queue($filePath, null, \Maatwebsite\Excel\Excel::CSV);
                    Storage::delete($filePath);
                    Audit::success(
                        "AUTOMATIC UPLOAD",
                        "Call log successfully loaded. Check the campaign log for details.",
                        $campaign->id,
                        $user->id
                    );
                } catch (ImportFileInvalid $e) {
                    Log::error("=== $campaignFolder ImportFileInvalid ERROR ===");
                    Storage::delete($filePath);
                    Audit::error(
                        "AUTOMATIC UPLOAD",
                        "{$e->getMessage()}",
                        $campaign->id,
                        $user->id
                    );
                } catch (\Exception $e) {
                    Log::error("=== $campaignFolder ERROR ===");
                    Log::error($e->getMessage());
                    Audit::error(
                        "AUTOMATIC UPLOAD",
                        "Unexpected error when importing file. please check the log /storage/logs",
                        $campaign->id,
                        $user->id
                    );
                }
            }
        }

        $this->info("The file importer has finished. Check /storage/logs for issues.");
        Audit::success(
            "AUTOMATIC UPLOAD - FINISHED",
            "The automatic csv import is finished.",
            NULL,
            $user->id
        );
    }

    private function createFolders(): void
    {
        foreach ($this->subFolders as $folder) {
            if (!Storage::exists("$this->baseFolder/$folder")) {
                Storage::makeDirectory("$this->baseFolder/$folder");
            }
        }
    }
}
