<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Http\JsonResponse;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Spatie\Permission\Exceptions\UnauthorizedException) {
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => 403,
                "message" => 'You do not have the required authorization.',
                "error" => 'You do not have the required authorization.',
            ]);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => JsonResponse::HTTP_NOT_FOUND, // 404
                "message" => trans('rest.http.method_not_allowed'),
                "data" => NULL,
                "error" => __('rest.error'),
            ]);
        }

        if ($exception instanceof NotFoundHttpException) {
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => JsonResponse::HTTP_NOT_FOUND, // 404
                "message" => trans('rest.http.not_found'),
                "data" => NULL,
                "error" => __('rest.error'),
            ]);
        }

        if ($exception instanceof ModelNotFoundException) {
            return response()->restResponse([
                "status" => __('rest.error'),
                "statusCode" => JsonResponse::HTTP_NOT_FOUND, // 404
                "message" => trans('rest.model.not_found'),
                "data" => NULL,
                "error" => __('rest.error'),
            ]);
        }

        return parent::render($request, $exception);
    }
}
