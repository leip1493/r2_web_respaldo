<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiBaseController;
use App\User;
use Validator;
use Exception;
use DB;
use App\Events\TestEvent;
use App\Models\Audit;
use App\Models\Campaign;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class UserController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "campaigns" => "array"
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('user.index.failed'), $validator->errors(), 400);
            }

            $users = User::when((int) $request->withCampaigns === 1, function ($query) {
                $query->with('campaigns');
            })->when($request->has('campaigns'), function ($query) use ($request) {
                $query->whereHas('campaigns', function ($query) use ($request) {
                    $query->whereIn('campaign_id', $request->campaigns);
                });
            });
            $users = $request->has('page')
                ? $users->paginate($request->limit)
                : $users->get();

            // event(new TestEvent(User::first())); // TEST DE EMITIR EVENTO
            return $this->sendResponse($users, __('user.index.success'));
        } catch (Exception $exception) {
            $error = __('user.index.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $validator = Validator::make($input, [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|confirmed',
                "roles" => "array|min:1",
                "roles.*" => "numeric|exists:roles,id",
                "permissions" => "array",
                "permissions.*" => "numeric|exists:permissions,id",
                "campaigns" => "array",
                "campaigns.*" => "numeric|exists:campaigns,id",
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('auth.register.failed'), $validator->errors(), 400);
            }
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            if ($request->permissions) {
                $permissions = Permission::find($request->permissions)->pluck("name");
                $user->givePermissionTo($permissions);
            }
            if ($request->roles) {
                $roles = Role::find($request->roles)->pluck("name");
                $user->assignRole($roles);
            }
            if ($request->campaigns) {
                $user->campaigns()->sync($request->campaigns);
                $user->load('campaigns');
            }
            DB::commit();
            Audit::success(
                "CREATE USER",
                "User $user->name created."
            );
            $response["user"] = $user;
            return $this->sendResponse($response, __('rest.success'));
        } catch (Exception $exception) {
            $error = __('auth.register.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, User $user)
    {
        try {
            if ((int) $request->withCampaigns === 1) {
                $user->load('campaigns');
            }
            return $this->sendResponse($user, __('rest.success'));
        } catch (Exception $exception) {
            $error = __('user.show.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        try {
            DB::beginTransaction();
            $input = $request->all();
            $validator = Validator::make($input, [
                'name' => 'sometimes|required',
                'email' => 'sometimes|required|email|unique:users,email,' . $user->id,
                'password' => 'sometimes|required|confirmed',
                "roles" => "sometimes|array|min:1",
                "roles.*" => "sometimes|numeric|exists:roles,id",
                "permissions" => "sometimes|array",
                "permissions.*" => "sometimes|numeric|exists:permissions,id",
                "campaigns" => "sometimes|array",
                "campaigns.*" => "sometimes|numeric|exists:campaigns,id",
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('auth.update.failed'), $validator->errors(), 400);
            }
            $dataToUpdate = $request->all();
            if ($request->has("password")) {
                $dataToUpdate["password"] = bcrypt($dataToUpdate["password"]);
            }
            $user->update($dataToUpdate);
            if ($request->permissions) {
                $permissions = Permission::find($request->permissions)->pluck("name");
                $user->syncPermissions($permissions);
            }
            if ($request->roles) {
                $roles = Role::find($request->roles)->pluck("name");
                $user->syncRoles($roles);
            }
            if ($request->campaigns) {
                $user->campaigns()->sync($request->campaigns);
                $user->load('campaigns');
            }
            DB::commit();
            Audit::success(
                "UPDATE USER",
                "User $user->name updated."
            );
            $response["user"] = $user;
            return $this->sendResponse($response, __('rest.success'));
        } catch (Exception $exception) {
            $error = __('auth.update.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            DB::beginTransaction();
            $deletedUser = $user->delete();
            DB::commit();
            Audit::success(
                "DELETE USER",
                "User $user->name deleted."
            );
            $response["deletedUser"] = $deletedUser;
            return $this->sendResponse($response, __('rest.success'));
        } catch (Exception $exception) {
            $error = __('auth.update.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
}
