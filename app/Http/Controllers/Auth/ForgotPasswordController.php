<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    protected function sendResetLinkResponse(Request $request, $response)
    {
        return response()->restResponse([
            "status" => __('rest.success'),
            "statusCode" => JsonResponse::HTTP_OK, // 200
            "message" => trans($response),
            "data" => [],
            "errors" => null,
        ]);
    }

    protected function sendResetLinkFailedResponse(Request $request, $response, $data = [])
    {
        return response()->restResponse([
            "status" => __('rest.error'),
            "statusCode" => JsonResponse::HTTP_BAD_REQUEST, // 400
            "message" => trans($response),
            "data" => $data,
            "errors" => trans($response),
        ]);
    }

    public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $this->credentials($request)
        );

        return $response == Password::RESET_LINK_SENT
            ? $this->sendResetLinkResponse($request, $response, [
                "token" => "SIN TOKEN"
            ])
            : $this->sendResetLinkFailedResponse($request, $response);
    }

    protected function validateEmail(Request $request)
    {
        $request->validate(['email' => 'required|email']);
    }

    protected function credentials(Request $request)
    {
        return ["email" => $request->email];
    }
}
