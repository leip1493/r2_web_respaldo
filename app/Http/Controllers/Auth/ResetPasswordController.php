<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    protected function sendResetResponse(Request $request, $response)
    {
        return response()->restResponse([
            "status" => __('rest.success'),
            "statusCode" => JsonResponse::HTTP_OK, // 200
            "message" => trans($response),
            "data" => [],
            "errors" => null,
        ]);
    }

    protected function sendResetFailedResponse(Request $request, $response)
    {
        return response()->restResponse([
            "status" => __('rest.error'),
            "statusCode" => JsonResponse::HTTP_FORBIDDEN, // 422
            "message" => trans($response),
            "data" => [],
            "errors" => trans($response),
        ]);
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        $response = $this->broker()->reset(
            $this->credentials($request),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($request, $response)
            : $this->sendResetFailedResponse($request, $response);
    }

    protected function credentials(Request $request)
    {
        return [
            'email' => $request->email,
            'password' => $request->password,
            'password_confirmation' => $request->password_confirmation,
            'token' => $request->token,
        ];
    }

    protected function resetPassword(Authenticatable $user, $password)
    {
        $user->password = bcrypt($password);
        $user->save();

        event(new PasswordReset($user));

        $this->guard()->login($user);
    }
}
