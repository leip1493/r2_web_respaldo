<?php

namespace App\Http\Controllers;

use App\Models\Audit;
use App\Models\Disposition;
use Illuminate\Http\Request;
use Validator;

class DispositionController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {

            $dispositions = Disposition::when($request->has('campaign_id'), function ($query) use ($request) {
                $query->whereHas('callLogs.campaign', function ($query) use ($request) {
                    $query->whereId($request->campaign_id);
                });
            })->orderBy('name', 'asc');

            $response = $request->has('page')
                ? $dispositions->paginate($request->limit)
                : $dispositions->get();

            return $this->sendResponse($response, __('dispositions.list.success'));
        } catch (\Exception $e) {
            $error = __('dispositions.list.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "name" => ["required", "unique:dispositions,name"],
                "final" => ["required", "in:recycle,final"],
                "contact" => ["required", "in:no contact,contact"],
                "effective" => ["required", "in:not effective,effective"],
                "pipeline" => ["boolean"],
                "decline" => ["boolean"],
                "primary" => ["boolean"],
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('dispositions.create.failed'), $validator->errors(), 400);
            }
            $disposition = Disposition::create($request->all());

            Audit::success(
                "CREATE DISPOSITION",
                "Disposition $disposition->name created."
            );

            return $this->sendResponse($disposition, __('dispositions.create.success'));
        } catch (\Exception $e) {
            $error = __('dispositions.create.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Disposition $disposition)
    {
        try {
            return $this->sendResponse($disposition, __('dispositions.show.success'));
        } catch (\Exception $e) {
            $error = __('dispositions.show.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Disposition $disposition)
    {
        try {
            $validator = Validator::make($request->all(), [
                "name" => ["sometimes", "unique:dispositions,name,$disposition->id"],
                "final" => ["sometimes", "in:recycle,final"],
                "contact" => ["sometimes", "in:no contact,contact"],
                "effective" => ["sometimes", "in:not effective,effective"],
                "pipeline" => ["boolean"],
                "decline" => ["boolean"],
                "primary" => ["boolean"],
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('dispositions.update.failed'), $validator->errors(), 400);
            }
            $disposition->update($request->all());
            $disposition->fresh();

            Audit::success(
                "UPDATE DISPOSITION",
                "Disposition $disposition->name updated."
            );
            return $this->sendResponse($disposition, __('dispositions.update.success'));
        } catch (\Exception $e) {
            $error = __('dispositions.update.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Disposition $disposition)
    {
        try {
            $deletedDisposition = $disposition->delete();
            Audit::success(
                "UPDATE DISPOSITION",
                "Disposition $disposition->name deleted."
            );
            return $this->sendResponse($deletedDisposition, __('dispositions.delete.success'));
        } catch (\Exception $e) {
            $error = __('dispositions.delete.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
}
