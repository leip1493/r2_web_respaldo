<?php

namespace App\Http\Controllers;

use App\Exports\CallLogExport;
use Illuminate\Http\Request;
use App\Imports\CallLogImportFactory;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\ApiBaseController;
use Carbon\Carbon;
use Validator;
use App\Models\{Audit, Campaign, CallLog, LogFile};

class CallLogController extends ApiBaseController
{
    private $importFactory;

    public function __construct(CallLogImportFactory $callLogImportFactory)
    {
        $this->importFactory = $callLogImportFactory;
    }

    public function upload(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'csv' => 'required|file',
                "campaign_id" => 'required|exists:campaigns,id,deleted_at,NULL',
                //'type' => 'required|in:xencall,ringcentral,3cx'
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('call_log.upload.failed'), $validator->errors(), 400);
            }
            $file = $request->file('csv');
            $fileExtension = $file->getClientOriginalExtension();
            if ($fileExtension !== 'csv') {
                return $this->sendError(__('call_log.upload.failed'), "File must be .csv", 400);
            }
            $path = $file->storeAs('csv', Str::random(40) . '.' . $fileExtension);

            if (!Storage::exists($path))
                throw new \Exception("File cannot be imported", 400);

            $user = auth()->guard('api')->user();
            $filename = $file->getClientOriginalName();

            $campaign = Campaign::find($request->campaign_id);

            $fileAlreadyUploaded = LogFile::whereName($filename)->whereCampaignId($campaign->id)->first();
            if ($fileAlreadyUploaded && $campaign->dialing_platform != "nimbus") {
                Storage::delete($path);
                Audit::error(
                    "MANUAL UPLOAD",
                    "The file $filename is already uploaded.",
                    $campaign->id
                );
                return $this->sendError(
                    __('call_log.upload.failed'),
                    "The file $filename is already uploaded",
                    400
                );
            }

            $import = $this->importFactory->getImport(
                $campaign->dialing_platform,
                $user,
                $filename,
                $campaign
            );
            ini_set('max_execution_time', 3600);
            set_time_limit(0);
            $import->queue($path, null, \Maatwebsite\Excel\Excel::CSV);
            Storage::delete($path);
            Audit::success(
                "MANUAL UPLOAD",
                "Call log successfully loaded. Check the campaign log for details",
                $campaign->id
            );
            return $this->sendResponse(NULL, "$filename imported successfully.");
        } catch (\Exception $e) {
            $error = __('call_log.upload.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            Audit::error(
                "MANUAL UPLOAD",
                "An error has occurred while uploading a file.",
                $campaign->id
            );
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
    public function index(Request $request)
    {
        ini_set('memory_limit', '-1');
        set_time_limit(0);
        try {
            $validator = Validator::make($request->all(), [
                "campaign_id" => 'required|exists:campaigns,id,deleted_at,NULL',
                'type' => 'in:xencall,ringcentral,3cx,nimbus',
                "group_by" => 'in:agents,dates,dates-agents,agents-dates'
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('get.call_logs.failed'), $validator->errors(), 400);
            }
            $campaign = Campaign::find($request->campaign_id);

            $callLogs = CallLog::with(["telephonicData", "crmData", "disposition"])
                ->requestFilters($request)
                ->whereHas('telephonicData', function ($query) use ($request) {
                    if ($request->has('start_date')) {
                        $query->whereDate('timestamp', '>=', Carbon::parse($request->start_date));
                    }
                    if ($request->has('end_date')) {
                        $query->whereDate('timestamp', '<=', Carbon::parse($request->end_date));
                    }
                });

            if ($request->group_by == "dates") {
                $callLogs = CallLog::groupByDate($callLogs->get(), $campaign->dialing_platform);
            } elseif ($request->group_by == "agents") {
                $callLogs = CallLog::groupByAgents($callLogs->get());
            } elseif ($request->group_by == "agents-dates") {
                $callLogs = CallLog::groupByAgentsDates($callLogs->get());
            } elseif ($request->group_by == "dates-agents") {
                $callLogs = CallLog::groupByDatesAgents($callLogs->get());
            } else {
                $callLogs = $callLogs
                    ->orderByTelephonicDataTimestamp("DESC")
                    ->paginate(request()->limit);
            }

            $callLogs = collect($callLogs);

            $type = $campaign->dialing_platform ? $campaign->dialing_platform : "call logs";

            return $this->sendResponse($callLogs, "List $type data");
        } catch (\Exception $e) {
            $error = __('get.xencall.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function downloadCSV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "campaign_id" => 'required|exists:campaigns,id,deleted_at,NULL',
            'type' => 'in:xencall,ringcentral,3cx,nimbus',
            "start_date" => 'required|date',
            "end_date" => 'required|date',
        ]);

        if ($validator->fails()) {
            return $this->sendError(__('get.call_logs.failed'), $validator->errors(), 400);
        }
        $campaign = Campaign::find($request->campaign_id);
        $start_date = Carbon::parse($request->start_date)->format('Y_m_d');
        $end_date = Carbon::parse($request->end_date)->format('Y_m_d');
        return (new CallLogExport($request, $campaign))->download(
            "Call_logs_$campaign->name" . "_from_$start_date" . "_to_$end_date.csv",
            \Maatwebsite\Excel\Excel::CSV,
            ['Content-Type' => 'text/csv']
        );
    }

    public function deleteByDate(Campaign $campaign, Request $request)
    {
        // return $request->all();
        try {
            $validator = Validator::make($request->all(), [
                'date' => 'required|date'
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('call_logs.delete.failed'), $validator->errors(), 400);
            }

            $callLogs = CallLog::whereCampaignId($campaign->id)
                ->whereHas('telephonicData', function ($query) use ($request) {
                    $query
                        ->whereDate('timestamp', '>=', Carbon::parse($request->date))
                        ->whereDate('timestamp', '<=', Carbon::parse($request->date));
                });

            $deleted = $callLogs->delete();

            if ($deleted) {
                Audit::success(
                    "DELETE",
                    "{$request->date} Call log deleted.",
                    $campaign->id
                );
            }

            return $this->sendResponse(NULL, __('call_logs.delete.success'));
        } catch (\Exception $e) {
            $error = __('call_logs.delete.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
}
