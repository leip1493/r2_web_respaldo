<?php

namespace App\Http\Controllers;

use App\Helpers\HttpStatusCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class ApiBaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
        return response()->restResponse([
            "status" => __('rest.success'),
            "statusCode" => JsonResponse::HTTP_OK, // 200
            "message" => $message,
            "data" => $result
        ]);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessage = [], $statusCode = JsonResponse::HTTP_INTERNAL_SERVER_ERROR)
    {
        return response()->restResponse([
            "status" => __('rest.error'),
            "statusCode" => $statusCode,
            "message" => $errorMessage,
            "error" => $error,
        ]);
    }

    public function guard()
    {
        return Auth::guard("api");
    }
}
