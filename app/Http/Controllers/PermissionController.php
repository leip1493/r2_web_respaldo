<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Permission;

class PermissionController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $permissions =  Permission::when($request->has('search'), function ($query) {
                $query->where("name", "LIKE", request()->search . "%");
            })->paginate($request->limit);

            return $this->sendResponse(__('permissions.index.success'), $permissions);
        } catch (\Exception $e) {
            $error = __('permissions.index.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => "required"
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('permissions.create.failed'), $validator->errors(), 400);
            }

            $permission = Permission::create(['name' => $request->name, 'guard_name' => 'web']);

            return $this->sendResponse(__('permissions.create.success'), $permission);
        } catch (\Exception $e) {
            $error = __('permissions.create.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => "required|unique:permissions,name"
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('permissions.update.failed'), $validator->errors(), 400);
            }

            $permission->update(['name' => $request->name]);
            $permission->touch();

            return $this->sendResponse(__('permissions.update.success'), $permission);
        } catch (\Exception $e) {
            $error = __('permissions.update.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
