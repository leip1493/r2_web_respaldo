<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiBaseController;
use App\Models\Audit;
use Illuminate\Http\Request;
use App\Models\Campaign;
use App\Rules\Uppercase;
use Validator;

class CampaignController extends ApiBaseController
{
    const DIALING_PLATFORM_VALUES = "in:xencall,ringcentral,3cx,nimbus";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $campaigns = auth()->user()->hasRole(['super-admin', 'admin'])
                ? Campaign::get()
                : auth()->user()->campaigns;
            return $this->sendResponse($campaigns, __('campaigns.list.success'));
        } catch (\Exception $e) {
            $error = __('campaigns.list.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                //https://www.akki.io/blog/laravel-soft-delete-unique-validation/ unique with softdeletes
                "name" => ["required", "max:3", new Uppercase, "unique:campaigns,name,NULL,id,deleted_at,NULL"],
                "dialing_platform" => ["required", self::DIALING_PLATFORM_VALUES],
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('campaigns.create.failed'), $validator->errors(), 400);
            }
            $campaign = Campaign::create($request->all());

            Audit::success(
                "CREATE CAMPAIGN",
                "Campaign $campaign->name created.",
                $campaign->id
            );

            return $this->sendResponse($campaign, __('campaigns.create.success'));
        } catch (\Exception $e) {
            $error = __('campaigns.create.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign)
    {
        try {
            return $this->sendResponse($campaign, __('campaigns.show.success'));
        } catch (\Exception $e) {
            $error = __('campaigns.show.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign)
    {
        try {
            $validator = Validator::make($request->all(), [
                "name" => ["required", "max:3", new Uppercase, "unique:campaigns,name," . $campaign->id],
                "dialing_platform" => ["required", self::DIALING_PLATFORM_VALUES],
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('campaigns.update.failed'), $validator->errors(), 400);
            }
            $campaign->update($request->all());
            $campaign->fresh();
            Audit::success(
                "UPDATED CAMPAIGN",
                "Campaign $campaign->name updated.",
                $campaign->id
            );
            return $this->sendResponse($campaign, __('campaigns.update.success'));
        } catch (\Exception $e) {
            $error = __('campaigns.update.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        try {
            $deletedCampaign = $campaign->delete();
            Audit::success(
                "DELETE CAMPAIGN",
                "Campaign $campaign->name deleted.",
                $campaign->id
            );
            return $this->sendResponse($deletedCampaign, __('campaigns.delete.success'));
        } catch (\Exception $e) {
            $error = __('campaigns.delete.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
}
