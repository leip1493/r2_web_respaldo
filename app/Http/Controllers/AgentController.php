<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ApiBaseController;
use App\Models\Agent;
use App\Models\Audit;
use Illuminate\Http\Request;
use Validator;

class AgentController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "campaigns" => "array"
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('agents.list.failed'), $validator->errors(), 400);
            }

            $agents = Agent::when((int) $request->withCampaigns === 1, function ($query) {
                $query->with('campaigns');
            })
                ->when($request->has('campaigns'), function ($query) use ($request) {
                    $query->whereHas('campaigns', function ($query) use ($request) {
                        $query->whereIn('campaign_id', $request->campaigns);
                    });
                })->orderBy('name', 'asc');

            $agents = $request->has('page')
                ? $agents->paginate($request->limit)
                : $agents->get();
            return $this->sendResponse($agents, __('agents.list.success'));
        } catch (\Exception $e) {
            $error = __('agents.list.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        try {
            return $this->sendResponse($agent, __('agents.show.success'));
        } catch (\Exception $e) {
            $error = __('agents.show.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        try {
            $validator = Validator::make($request->all(), [
                "hcc" => "required|boolean",
                "active" => "required|boolean",
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('agents.update.failed'), $validator->errors(), 400);
            }

            $hcc = $request->hcc;
            $active = $request->active;
            $agent->update([
                'hcc' => $hcc,
                'active' => $active,
            ]);

            $agent->fresh();

            Audit::success(
                "AGENT UPDATE",
                "Agent $agent->name updated.",
                $agent->campaigns->first()->id
            );

            return $this->sendResponse($agent, __('agents.update.success'));
        } catch (\Exception $e) {
            $error = __('agents.update.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function updateMany(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "agents" => "required|array",
                "agents.*.id" => "required|exists:agents,id",
                "agents.*.hcc" => "required|boolean",
                "agents.*.active" => "required|boolean",
            ]);

            if ($validator->fails()) {
                return $this->sendError(__('agents.updateMany.failed'), $validator->errors(), 400);
            }

            $updatedAgents = [];
            foreach ($request->agents as $agent) {
                $updatedAgent = Agent::with('campaigns')->find($agent["id"]);
                $updatedAgent->update([
                    'hcc' => $agent["hcc"],
                    'active' => $agent["active"],
                ]);
                $updatedAgent->fresh();
                $updatedAgents[] = $updatedAgent;
                Audit::success(
                    "AGENT UPDATE",
                    "Agent $updatedAgent->name updated.",
                    $updatedAgent->campaigns->first()->id
                );
            }

            return $this->sendResponse($updatedAgents, __('agents.updateMany.success'));
        } catch (\Exception $e) {
            $error = __('agents.updateMany.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
