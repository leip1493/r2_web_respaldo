<?php

namespace App\Http\Controllers;

use App\Exports\AuditExport;
use App\Models\Audit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AuditController extends ApiBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "start_date" => "date",
                "end_date" => "date",
                "user_order" => "in:asc,desc",
                "campaign_id" => "exists:campaigns,id,deleted_at,NULL",
            ]);

            if ($validator->fails()) {
                return $this->sendError(__("audit.index.failed"), $validator->errors(), 400);
            }

            $audits = Audit::with(["user", "campaign"])
                ->when($request->has("start_date"), function ($query) use ($request) {
                    $query->whereDate("created_at", ">=", Carbon::parse($request->start_date));
                })
                ->when($request->has("start_date"), function ($query) use ($request) {
                    $query->whereDate("created_at", ">=", Carbon::parse($request->start_date));
                })
                ->when($request->has("campaign_id"), function ($query) use ($request) {
                    $query->whereCampaignId($request->campaign_id);
                })
                ->orderByRaw("created_at DESC, id DESC")
                // ->when($request->has("user_order"), function ($query) use ($request) {
                //     $query->whereHas("user", function ($query) use ($request) {
                //         $query->orderBy("name", $request->user_order);
                //     });
                // })
                ->paginate($request->limit);
            return $this->sendResponse($audits, __("audit.index.success"));
        } catch (\Exception $e) {
            $error = __("audit.index.failed");
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function downloadCSV(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "start_date" => "required|date",
            "end_date" => "required|date",
            "user_order" => "in:asc,desc",
            "campaign_id" => "exists:campaigns,id,deleted_at,NULL",
        ]);

        if ($validator->fails()) {
            return $this->sendError(__("audit.download_csv.failed"), $validator->errors(), 400);
        }

        $start_date = Carbon::parse($request->start_date)->format("Y_m_d");
        $end_date = Carbon::parse($request->end_date)->format("Y_m_d");

        return (new AuditExport($request))->download(
            "audits_from_{$start_date}_to_{$end_date}.csv",
            \Maatwebsite\Excel\Excel::CSV,
            ["Content-Type" => "text/csv"]
        );
    }
}
