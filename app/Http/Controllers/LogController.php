<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\ApiBaseController;
use App\Models\LogFile;
use App\Models\LogSession;
use App\User;
use Illuminate\Support\Facades\Validator;

class LogController extends ApiBaseController
{
    public function getLogSession(Request $request)
    {
        try {
            $logs = LogSession::paginate($request->limit);
            return $this->sendResponse($logs, __('logSessions.list.success'));
        } catch (\Exception $e) {
            $error = __('logSessions.list.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function userLogSession(Request $request, User $user)
    {
        try {
            $logs = LogSession::where('user_id', $user->id)->paginate($request->limit);
            return $this->sendResponse($logs, __('logSessions.show.success'));
        } catch (\Exception $e) {
            $error = __('logSessions.show.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function getLogFiles(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                "campaign_id" => 'sometimes|exists:campaigns,id,deleted_at,NULL',
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('call_log.upload.failed'), $validator->errors(), 400);
            }
            $logs = LogFile::when($request->has('campaign_id'), function ($query) use ($request) {
                $query
                    ->whereCampaignId($request->campaign_id)
                    ->orWhereNull("campaign_id");
            })
                ->orderBy('created_at', 'desc')
                ->paginate($request->limit);
            return $this->sendResponse($logs, __('logFiles.list.success'));
        } catch (\Exception $e) {
            $error = __('logFiles.list.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function userLogFiles(Request $request, User $user)
    {
        try {
            $logs = LogFile::where('uploaded_by', $user->id)->paginate($request->limit);
            return $this->sendResponse($logs, __('logFiles.show.success'));
        } catch (\Exception $e) {
            $error = __('logFiles.show.failed');
            $errorMessages = $e->getMessage();
            $statusCode = httpStatusCode((int) $e->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
}
