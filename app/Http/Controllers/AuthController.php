<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Validator;
use App\Http\Controllers\ApiBaseController;
use App\Models\LogSession;
use Exception;
use Carbon\Carbon;

class AuthController extends ApiBaseController
{
    public function __construct()
    {
        $this->middleware(['auth:api'])->only(['logout', 'loggedUserData']);
    }
    /**
     * login API
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $input = $request->all();
            $validator = Validator::make($input, [
                'email' => 'required|email',
                'password' => 'required',
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('auth.login.failed'), $validator->errors(), 400);
            }
            $credentials = $request->only(['email', 'password']);
            if (Auth::attempt($credentials)) {
                $user = Auth::user();
                $token = $user->createToken('reports');
                $response = [
                    'user'          => $user,
                    'token'         => $token->accessToken,
                    'token_type'    => 'Bearer',
                    'expires_at'    => Carbon::parse($token->token->expires_at)->toDateTimeString()
                ];
                $user->logSessions()->save(new LogSession);

                return $this->sendResponse($response, __('rest.success'));
            } else {
                return $this->sendError(__('rest.not_found'), __('rest.not_found'), 403);
            }
        } catch (Exception $exception) {
            $error = __('auth.login.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
    /**
     * register API
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        try {
            $input = $request->all();
            $validator = Validator::make($input, [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required',
                'password_confirmation' => 'required|same:password',
            ]);
            if ($validator->fails()) {
                return $this->sendError(__('auth.register.failed'), $validator->errors(), 400);
            }
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ]);
            $token = $user->createToken('reports');
            $response = [
                'user'          => $user,
                'token'         => $token->accessToken,
                'token_type'    => 'Bearer',
                'expires_at'    => Carbon::parse($token->token->expires_at)->toDateTimeString()
            ];
            return $this->sendResponse($response, __('rest.success'));
        } catch (Exception $exception) {
            $error = __('auth.register.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function loggedUserData()
    {
        try {
            $data = $this->guard()->authenticate();
            return $this->sendResponse($data, __('auth.loggedUserData.success'));
        } catch (Exception $exception) {
            $error = __('auth.loggedUserData.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }

    public function logout()
    {
        try {

            $data = $this->guard()->authenticate()->token()->revoke();
            return $this->sendResponse($data, __('auth.logout.success'));
        } catch (Exception $exception) {
            $error = __('auth.logout.failed');
            $errorMessages = $exception->getMessage();
            $statusCode = httpStatusCode((int) $exception->getCode());
            return $this->sendError($error, $errorMessages, $statusCode);
        }
    }
}
