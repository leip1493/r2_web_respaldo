<?php

namespace App\Exports;

use App\Models\CallLog;
use App\Models\Campaign;
use App\Traits\CallLogExport as TraitsCallLogExport;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\FromQuery;


class CallLogExport implements WithHeadings, WithMapping, WithCustomCsvSettings, FromQuery
{
    use Exportable, TraitsCallLogExport;

    private $request;
    private $campaign;

    public function __construct(Request $request, Campaign $campaign)
    {
        $this->request = $request;
        $this->campaign = $campaign;
    }

    public function query()
    {
        return CallLog::with(["telephonicData"])
            ->requestFilters($this->request)
            ->orderByTelephonicDataTimestamp("DESC")
            ->whereHas('telephonicData', function ($query) {
                $query->whereDate('timestamp', '>=', Carbon::parse($this->request->start_date))
                    ->whereDate('timestamp', '<=', Carbon::parse($this->request->end_date));
            });
    }

    public function headings(): array
    {
        return $this->campaignHeaders();
    }

    public function map($call_log): array
    {
        return [$this->campaignMap($call_log)];
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ','
        ];
    }
}
