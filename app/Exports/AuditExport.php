<?php

namespace App\Exports;

use App\Models\Audit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithCustomCsvSettings;
use Maatwebsite\Excel\Concerns\FromQuery;


class AuditExport implements WithHeadings, WithMapping, WithCustomCsvSettings, FromQuery
{
    use Exportable;

    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function query()
    {
        return Audit::with(["user", "campaign"])
            ->when($this->request->has("start_date"), function ($query) {
                $query->whereDate("created_at", ">=", Carbon::parse($this->request->start_date));
            })
            ->when($this->request->has("start_date"), function ($query) {
                $query->whereDate("created_at", ">=", Carbon::parse($this->request->start_date));
            })
            ->when($this->request->has("campaign_id"), function ($query) {
                $query->whereCampaignId($this->request->campaign_id);
            })
            ->orderByRaw("created_at DESC, id DESC");
    }

    public function headings(): array
    {
        return [
            'date',
            'time',
            'user',
            'campaign',
            'action',
            'details',
            'status',
        ];
    }

    public function map($audit): array
    {
        return [
            $audit->created_at->format('m/d/Y'),
            $audit->created_at->format('H:i:s'),
            $audit->user->name,
            $audit->campaign->name,
            $audit->action,
            $audit->details,
            $audit->status,
        ];
    }

    public function getCsvSettings(): array
    {
        return [
            'delimiter' => ','
        ];
    }
}
