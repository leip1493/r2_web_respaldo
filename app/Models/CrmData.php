<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CrmData extends Model
{
    protected $table = "crm_data";

    protected $fillable = [
        "first_name", "last_name", "title", "email", "company", "address",  "state", "zip", "fax", "sic", "sic_description", "employees", "sales_volume"
    ];

    public function callLog()
    {
        return $this->belongsTo(CallLog::class, "call_log_id");
    }
}
