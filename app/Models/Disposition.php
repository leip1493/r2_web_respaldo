<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disposition extends Model
{
    protected $table = "dispositions";

    protected $fillable = [
        "name", "final", "contact", "effective", "pipeline", "decline", "primary"
    ];

    public function callLogs()
    {
        return $this->hasMany(CallLog::class, "disposition_id");
    }
}
