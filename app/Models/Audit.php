<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Audit extends Model
{
    protected $fillable = [
        'id',
        'action',
        'details',
        'status',
        'campaign_id',
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class)
            ->withDefault(['name' => NULL]);
    }

    public static function success($action, $details, $campaign_id = NULL, $user_id = NULL)
    {
        return self::create([
            'status' => 'SUCCESS',
            'user_id' => is_null($user_id) ? auth()->user()->id : $user_id,
            'action' => $action,
            'details' => $details,
            'campaign_id' => $campaign_id,
        ]);
    }

    public static function error($action, $details, $campaign_id, $user_id = NULL)
    {
        return self::create([
            'status' => 'ERROR',
            'user_id' => is_null($user_id) ? auth()->user()->id : $user_id,
            'action' => $action,
            'details' => $details,
            'campaign_id' => $campaign_id,
        ]);
    }
}
