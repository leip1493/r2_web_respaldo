<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\CallLog as CallLogTrait;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class CallLog extends Model
{
    use CallLogTrait;

    const XENCALL = "xencall";
    const RINGCENTRAL = "ringcentral";
    const THREECX = "3cx";
    const NIMBUS = "nimbus";

    protected $table = "call_log";

    protected $fillable = [
        "upload_file_name", "type"
    ];

    public function telephonicData()
    {
        return $this->hasOne(TelephonicData::class, "call_log_id");
    }

    public function crmData()
    {
        return $this->hasOne(CrmData::class, "call_log_id");
    }

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, "campaign_id");
    }

    public function uploadedBy()
    {
        return $this->belongsTo(\App\User::class, "uploaded_by");
    }

    public function agent()
    {
        return $this->belongsTo(Agent::class, "agent_id");
    }

    public function disposition()
    {
        return $this->belongsTo(Disposition::class, "disposition_id");
    }

    public function scopeRequestFilters(Builder $query, $request): Builder
    {
        return $query
            ->when($request->has('type'), function ($query) use ($request) {
                $query->whereType($request->type);
            })
            ->when($request->has('campaign_id'), function ($query) use ($request) {
                $query->whereCampaignId((int) $request->campaign_id);
            })
            ->when($request->has('hcc'), function ($query) use ($request) {
                $query->whereHas('agent', function ($query) use ($request) {
                    $query->whereHcc($request->hcc);
                });
            })
            ->when($request->has('active'), function ($query) use ($request) {
                $query->whereHas('agent', function ($query) use ($request) {
                    $query->whereActive($request->active);
                });
            });
    }

    public function scopeOrderByTelephonicDataTimestamp($query, $order = "DESC")
    {
        return $query->leftjoin('telephonic_data', 'telephonic_data.call_log_id', 'call_log.id')
            ->select(['call_log.*', 'telephonic_data.timestamp as td_timestamp'])
            ->orderBy('telephonic_data.timestamp', $order);
    }
}
