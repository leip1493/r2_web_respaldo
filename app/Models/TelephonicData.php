<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TelephonicData extends Model
{
    protected $table = "telephonic_data";

    protected $fillable = [
        "call_id", "timestamp", "call_type", "agent_name", "disposition", "sub_disposition", "btn", "ani", "dnis", "call_time", "talk_time", "hold_time", "park_time", "acw", "campaign", "list_name",
        // Custom fields
        "date", "start_time", "end_time", "call_notes"
    ];

    protected $hidden = ["btn"];

    public function callLog()
    {
        return $this->belongsTo(CallLog::class, "call_log_id");
    }
}
