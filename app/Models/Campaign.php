<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use SoftDeletes;

    protected $table = "campaigns";

    protected $fillable = [
        "name", "dialing_platform"
    ];

    public function files()
    {
        return $this->hasMany('App\Models\XenCall', "campaign_id");
    }

    public function agents()
    {
        return $this->belongsToMany("App\Models\Agent");
    }

    public function users()
    {
        return $this->belongsToMany("App\User");
    }
}
