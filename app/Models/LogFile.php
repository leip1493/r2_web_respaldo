<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogFile extends Model
{
    protected $table = "log_files";

    protected $fillable = [
        'name', 'count_rows', 'count_agents', 'from_timestamp', 'to_timestamp'
    ];

    protected $with = ['uploadedBy', 'campaign'];

    protected $appends = ['range_timestamp'];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class, "campaign_id")->withDefault(['name' => 'N/A']);
    }
    public function uploadedBy()
    {
        return $this->belongsTo(\App\User::class, "uploaded_by");
    }

    public function getRangeTimestampAttribute()
    {
        return $this->from_timestamp && $this->to_timestamp ? "{$this->from_timestamp} - {$this->to_timestamp}" : "No data added";
    }
}
