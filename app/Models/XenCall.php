<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class XenCall extends Model
{
    protected $table = "xencall";

    protected $fillable = [
        "call_log_id",
        "agent_name",
        "start_time",
        "date",
        "end_time",
        "call_duration",
        "call_notes",
        "uploaded_by",
        "campaign_id",
        "upload_file_name",
        "log_time",
    ];

    // protected $dates = ['log_time'];

    // protected $casts = [
    //     'date' => 'date:d/m/Y',
    // ];

    public function campaign()
    {
        return $this->belongsTo("App\Models\Campaign", "campaign_id");
    }
}
