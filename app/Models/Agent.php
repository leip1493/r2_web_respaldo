<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = "agents";

    protected $fillable = [
        "name", "hcc", "active"
    ];

    public function campaigns()
    {
        return $this->belongsToMany("App\Models\Campaign");
    }

    public function callLogs()
    {
        return $this->hasMany(CallLog::class, "agent_id");
    }
}
