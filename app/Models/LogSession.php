<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LogSession extends Model
{
    protected $table = "log_sessions";

    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo(\App\User::class, "user_id");
    }
}
