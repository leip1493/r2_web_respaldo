(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["user"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/audit/Index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      status: 0,
      status_csv: 0,
      loading: false,
      dense: false,
      fixedHeader: false,
      height: "33rem",
      table_first: [],
      options: {},
      page: 1,
      pageCount: 2,
      itemsPerPage: 20,
      total: 2,
      date_app: "",
      headers: [{
        text: "Date",
        value: "date",
        "class": "length_text tr-color tam-header-call-log",
        sortable: false,
        width: 150
      }, {
        text: "Time",
        value: "time",
        "class": "length_text tr-color tam-header-call-log",
        sortable: false,
        width: 150
      }, {
        text: "User",
        value: "user.name",
        "class": "length_text tr-color tam-header-call-log",
        sortable: false,
        width: 150
      }, {
        text: "Campaign",
        value: "campaign.name",
        "class": "length_text tr-color tam-header-call-log",
        sortable: false,
        width: 150
      }, {
        text: "Action and Details",
        value: "details",
        "class": "length_text tr-color tam-header-call-log",
        width: 600,
        sortable: false
      }]
    };
  },
  filters: {
    parse_decimal: function parse_decimal(value) {
      return parseFloat(value).toFixed(2);
    },
    format_hours: function format_hours(value) {
      var moment = __webpack_require__(/*! moment-timezone */ "./node_modules/moment-timezone/index.js");

      return moment.utc(value).tz("America/Mexico_City").format("HH:mm:ss"); // return moment(value).format("H:mm:ss");
    },
    format_date: function format_date(value) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(value).format("YYYY-MM-DD");
    }
  },
  watch: {
    options: {
      handler: function handler() {
        var _this = this;

        return _asyncToGenerator(
        /*#__PURE__*/
        _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
          var dates, payload;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.prev = 0;
                  _this.loading = true;
                  dates = _this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                  payload = {
                    start: dates.ini,
                    end: dates.end,
                    page: _this.page,
                    campaign_id: _this.$store.getters["campaigns/GET_CAMPAIGNS_ID"],
                    itemsPerPage: _this.itemsPerPage
                  };

                  if (!(_this.status == 1)) {
                    _context.next = 8;
                    break;
                  }

                  _this.quarter();

                  _context.next = 19;
                  break;

                case 8:
                  if (!(_this.status == 2)) {
                    _context.next = 12;
                    break;
                  }

                  _this.year();

                  _context.next = 19;
                  break;

                case 12:
                  if (!(_this.status == 3)) {
                    _context.next = 16;
                    break;
                  }

                  _this.all_campaigns();

                  _context.next = 19;
                  break;

                case 16:
                  _context.next = 18;
                  return _this.$store.dispatch("audit/INDEX_DATE", payload);

                case 18:
                  _this.loading = false;

                case 19:
                  _context.next = 25;
                  break;

                case 21:
                  _context.prev = 21;
                  _context.t0 = _context["catch"](0);
                  console.error(_context.t0);

                  _this.$snotify.error("Error al cargar listado.", "¡Vaya!");

                case 25:
                  _context.prev = 25;
                  _this.loading = false;
                  return _context.finish(25);

                case 28:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, null, [[0, 21, 25, 28]]);
        }))();
      },
      deep: true
    }
  },
  created: function created() {
    this.date_position();
    this.first_fortnight(); // this.scorecard_second_fortnight();
    // this.scorecard_today();
  },
  mounted: function mounted() {
    this.campaign_id = this.$store.getters["campaigns/GET_CAMPAIGNS_ID"];
  },
  computed: {
    Itemscampaigns: function Itemscampaigns() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    },
    DateDashboard: function DateDashboard() {
      return this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
    },
    pagination: function pagination() {
      return this.$store.getters["audit/PAGINATION"];
    },
    ItemsAudit: function ItemsAudit() {
      return this.$store.getters["audit/GET_AUDIT"];
    },
    Itemsheaders: function Itemsheaders() {
      return this.$store.getters["audit/GET_AUDIT_KEY"];
    }
  },
  methods: {
    current_day: function current_day() {
      var day_new = moment__WEBPACK_IMPORTED_MODULE_2___default()().subtract(1, "days").format("MMMM Do, YYYY");
      return day_new;
    },
    first_fortnight: function first_fortnight() {
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
      var day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("MMMM Do");
      var day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("Do, YYYY");
      this.date_app = "".concat(day_1, " - ").concat(day_2);
    },
    second_fortnight: function second_fortnight() {
      var day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");
      var month = moment__WEBPACK_IMPORTED_MODULE_2___default()().subtract(1, "months").format("MM");
      var month2 = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("MM");
      var year = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("YYYY");
      var day_1;
      var day_2;

      if (day > 15) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).format("MMMM Do");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).endOf("month").format("Do, YYYY");
      } else {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).format("MMMM Do");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).endOf("month").format("Do, YYYY");
      }

      return "".concat(day_1, " - ").concat(day_2);
    },
    date_position: function date_position() {
      var day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");

      if (day > 15) {
        this.second_fortnight_section = true;
      } else {
        this.first_fortnight_section = false;
      }
    },
    export_csv: function export_csv() {
      var _this2 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var dates, payload, url;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.loading = true;
                dates = _this2.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                payload = {
                  start: moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("MM/DD/YYYY"),
                  end: moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM/DD/YYYY"),
                  campaign_id: _this2.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                url = '';

                if (_this2.status_csv == 3) {
                  url = "".concat("http://dev.hccreports.online/", "/api/v1/audits/download-csv?start_date=").concat(payload.start, "&end_date=").concat(payload.end);
                } else {
                  url = "".concat("http://dev.hccreports.online/", "/api/v1/audits/download-csv?start_date=").concat(payload.start, "&end_date=").concat(payload.end, "&campaign_id=").concat(payload.campaign_id);
                }

                axios({
                  method: "get",
                  url: url,
                  timeout: 600000,
                  responseType: "arraybuffer",
                  headers: {
                    Authorization: " Bearer ".concat(localStorage.getItem("token"))
                  } // data: request

                }).then(function (response) {
                  _this2.loading = false;

                  _this2.forceFileDownload(response, payload);
                })["catch"](function (error) {
                  _this2.loading = false;
                  console.log("error occured", error);
                });

              case 6:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    forceFileDownload: function forceFileDownload(response, payload) {
      var url = window.URL.createObjectURL(new Blob([response.data]));
      var link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "Audit_Report_".concat(this.NameCampaigns.name, "_from_").concat(payload.start, "_to_").concat(payload.end, ".csv")); //or any other extension

      document.body.appendChild(link);
      link.click();
    },
    quarter: function quarter() {
      var _this3 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var dates, month_ini, year_ini, month_end, year_end, date_end, payload, day_1, day_2;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _this3.loading = true;
                _this3.status_csv = 1;
                dates = _this3.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                month_ini = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).subtract(3, "months").format("MM");
                year_ini = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).subtract(11, "months").format("YYYY");
                month_end = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("MM");
                year_end = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("YYYY"); // const date_end = moment(dates.ini).endOf("month").format("DD");

                date_end = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("DD");
                payload = {
                  start: "".concat(month_ini, "/01/").concat(year_ini),
                  end: "".concat(month_end, "/").concat(date_end, "/").concat(year_end),
                  page: _this3.page,
                  campaign_id: _this3.$store.getters["campaigns/GET_CAMPAIGNS_ID"],
                  itemsPerPage: _this3.itemsPerPage
                };
                console.log("****", payload);
                _context3.next = 13;
                return _this3.$store.dispatch("audit/INDEX_DATE", payload);

              case 13:
                // this.loading = false;
                // this.date_app = 'argelisssssssss'
                day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(month_ini, "/01/").concat(year_ini)).format("MMMM Do");
                day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(month_end, "/").concat(date_end, "/").concat(year_end)).format("MMMM Do, YYYY");
                _this3.status = 1;
                _this3.date_app = "".concat(day_1, " - ").concat(day_2);
                _context3.next = 23;
                break;

              case 19:
                _context3.prev = 19;
                _context3.t0 = _context3["catch"](0);
                console.error(_context3.t0);

                _this3.$snotify.error("Error al cargar listado.", "¡Vaya!");

              case 23:
                _context3.prev = 23;
                _this3.loading = false;
                return _context3.finish(23);

              case 26:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 19, 23, 26]]);
      }))();
    },
    year: function year() {
      var _this4 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var dates, month_ini, year_ini, month_end, year_end, date_end, payload, day_1, day_2;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _this4.loading = true;
                _this4.status_csv = 2;
                dates = _this4.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                month_ini = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).subtract(11, "months").format("MM");
                year_ini = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).subtract(11, "months").format("YYYY");
                month_end = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("MM");
                year_end = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("YYYY");
                date_end = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).endOf("month").format("DD");
                console.log(month_ini);
                payload = {
                  start: "".concat(month_ini, "/01/").concat(year_ini),
                  end: "".concat(month_end, "/").concat(date_end, "/").concat(year_end),
                  page: _this4.page,
                  campaign_id: _this4.$store.getters["campaigns/GET_CAMPAIGNS_ID"],
                  itemsPerPage: _this4.itemsPerPage
                };
                console.log("****", payload);
                _context4.next = 14;
                return _this4.$store.dispatch("audit/INDEX_DATE", payload);

              case 14:
                // this.loading = false;
                // this.date_app = 'argelisssssssss'
                day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(month_ini, "/01/").concat(year_ini)).format("MMMM Do, YYYY");
                day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(month_end, "/").concat(date_end, "/").concat(year_end)).format("MMMM Do, YYYY");
                _this4.status = 2;
                _this4.date_app = "".concat(day_1, " - ").concat(day_2);
                _context4.next = 24;
                break;

              case 20:
                _context4.prev = 20;
                _context4.t0 = _context4["catch"](0);
                console.error(_context4.t0);

                _this4.$snotify.error("Error al cargar listado.", "¡Vaya!");

              case 24:
                _context4.prev = 24;
                _this4.loading = false;
                return _context4.finish(24);

              case 27:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 20, 24, 27]]);
      }))();
    },
    all_campaigns: function all_campaigns() {
      var _this5 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var dates, payload, day_1, day_2;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _this5.loading = true;
                _this5.status_csv = 3;
                dates = _this5.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                payload = {
                  start: dates.ini,
                  end: dates.end,
                  page: _this5.page,
                  itemsPerPage: _this5.itemsPerPage
                };
                console.log("****", payload);
                _context5.next = 8;
                return _this5.$store.dispatch("audit/INDEX", payload);

              case 8:
                // this.loading = false;
                // this.date_app = 'argelisssssssss'
                day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("MMMM Do");
                day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("Do, YYYY");
                _this5.status = 3;
                _this5.date_app = "".concat(day_1, " - ").concat(day_2);
                _context5.next = 18;
                break;

              case 14:
                _context5.prev = 14;
                _context5.t0 = _context5["catch"](0);
                console.error(_context5.t0);

                _this5.$snotify.error("Error al cargar listado.", "¡Vaya!");

              case 18:
                _context5.prev = 18;
                _this5.loading = false;
                return _context5.finish(18);

              case 21:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 14, 18, 21]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/campaigns/Index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      edit: false,
      position_array: "",
      id_campaign: 0,
      itemsPerPage: 10,
      dialog: false,
      headers: [{
        text: "Name",
        align: "left",
        sortable: false,
        value: "name",
        "class": "length_text head-color"
      }, {
        text: "Platform",
        align: "left",
        sortable: false,
        value: "dialing_platform",
        "class": "length_text head-color"
      }, {
        text: "Edit / Delete",
        value: "action",
        sortable: false,
        "class": "length_text head-color"
      }],
      editedIndex: -1,
      editedItem: {
        name: "",
        Pbx: ""
      },
      defaultItem: {
        name: ""
      },
      items: ['xencall', 'ringcentral', '3cx', 'nimbus']
    };
  },
  computed: {
    formTitle: function formTitle() {
      return this.edit === false ? "New Campaign" : "Edit Campaign";
    },
    ItemsCampains: function ItemsCampains() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    }
  },
  watch: {
    dialog: function dialog(val) {
      val || this.close();
    }
  },
  created: function created() {
    this.initialize();
  },
  methods: {
    rowClick: function rowClick(item, row) {
      if (row.isSelected == true) {
        row.select(false);
      } else {
        row.select(true);
        this.editItem(item);
      }
    },
    initialize: function initialize() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _this.$store.dispatch("campaigns/INDEX");

              case 3:
                response = _context.sent;
                _context.next = 9;
                break;

              case 6:
                _context.prev = 6;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 6]]);
      }))();
    },
    editItem: function editItem(item) {
      this.position_array = this.ItemsCampains.indexOf(item);
      this.edit = true;
      this.id_campaign = item.id;
      this.editedItem.name = item.name;
      this.dialog = true;
    },
    deleteItem: function deleteItem(item) {
      var _this2 = this;

      sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        cancelButtonColor: "#E63E59",
        confirmButtonText: "Yes, delete it!"
      }).then(
      /*#__PURE__*/
      function () {
        var _ref = _asyncToGenerator(
        /*#__PURE__*/
        _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(result) {
          var response;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  if (!result.value) {
                    _context2.next = 11;
                    break;
                  }

                  _context2.prev = 1;
                  _context2.next = 4;
                  return _this2.$store.dispatch("campaigns/DELETED", item);

                case 4:
                  response = _context2.sent;
                  // console.log(response);
                  sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: "success",
                    title: "Deleted!",
                    text: "Your campaign has been deleted.",
                    type: "success",
                    confirmButtonColor: "#25A8E0"
                  });
                  _context2.next = 11;
                  break;

                case 8:
                  _context2.prev = 8;
                  _context2.t0 = _context2["catch"](1);
                  console.log(_context2.t0.response);

                case 11:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, null, [[1, 8]]);
        }));

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    },
    close: function close() {
      var _this3 = this;

      this.dialog = false;
      setTimeout(function () {
        _this3.editedItem = Object.assign({}, _this3.defaultItem);
        _this3.editedIndex = -1;
        _this3.edit = false;
      }, 300);
    },
    save: function save() {
      var _this4 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var request, response, _request, _response, e, list, prop;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!(_this4.edit === true)) {
                  _context3.next = 13;
                  break;
                }

                //   Object.assign(this.desserts[this.editedIndex], this.editedItem)
                request = {
                  id: _this4.id_campaign,
                  name: _this4.editedItem.name,
                  dialing_platform: _this4.editedItem.Pbx,
                  position_array: _this4.position_array
                }; // console.log("ENVIO",request)

                _context3.prev = 2;
                _context3.next = 5;
                return _this4.$store.dispatch("campaigns/UPDATED", request);

              case 5:
                response = _context3.sent;
                _context3.next = 11;
                break;

              case 8:
                _context3.prev = 8;
                _context3.t0 = _context3["catch"](2);
                console.log(_context3.t0);

              case 11:
                _context3.next = 27;
                break;

              case 13:
                // guardar
                //   this.desserts.push(this.editedItem)
                _request = {
                  name: _this4.editedItem.name,
                  dialing_platform: _this4.editedItem.Pbx
                };
                _context3.prev = 14;
                _context3.next = 17;
                return _this4.$store.dispatch("campaigns/REGISTER", _request);

              case 17:
                _response = _context3.sent;
                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  icon: "success",
                  title: "Campaign created successfully",
                  text: "",
                  type: "success",
                  confirmButtonColor: "#25A8E0"
                });
                _context3.next = 27;
                break;

              case 21:
                _context3.prev = 21;
                _context3.t1 = _context3["catch"](14);
                // console.log(error.response);
                e = _context3.t1.response.data.message; // console.log(e, "error");

                list = "";

                for (prop in e) {
                  // console.log(prop, "ptop");
                  list = list + "<li>".concat(e[prop][0], "</li>");
                }

                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  icon: "error",
                  title: "Oops...",
                  html: list
                });

              case 27:
                _this4.close();

              case 28:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[2, 8], [14, 21]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Dashboard.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_apexcharts__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-apexcharts */ "./node_modules/vue-apexcharts/dist/vue-apexcharts.js");
/* harmony import */ var vue_apexcharts__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue_apexcharts__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);


function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    apexchart: vue_apexcharts__WEBPACK_IMPORTED_MODULE_1___default.a
  },
  data: function data() {
    return {
      status_graph: false,
      dates: [],
      loadingValues: false,
      loading: false,
      showAlert: false,
      comparison: [],
      loadingModal: false,
      campaign_id: 0,
      campaign_name: "",
      files: [],
      render: false,
      array_comparison_finally: [],
      chartBar: {
        chart: {
          toolbar: {
            show: false
          },
          zoom: {
            enabled: false
          }
        },
        plotOptions: {
          bar: {
            columnWidth: "80%"
          }
        },
        dataLabels: {
          enabled: false
        },
        yaxis: {
          show: false
        },
        xaxis: {
          categories: ["01-01", "02-01", "03-01", "04-01", "05-01", "06-01", "01-01", "02-01", "03-01", "04-01", "05-01", "06-01", "01-01", "02-01", "03-01"],
          labels: {
            rotate: -60,
            style: {
              fontSize: "12px"
            }
          },
          tickPlacement: "on"
        },
        colors: ["#E63E59"],
        fill: {
          colors: ["#fff"]
        },
        stroke: {
          show: true,
          curve: "straight",
          lineCap: "square",
          colors: ["#E63E59"],
          width: 3,
          dashArray: 0
        },
        grid: {
          show: true,
          borderColor: "#90A4AE",
          strokeDashArray: 0,
          position: "back",
          xaxis: {
            lines: {
              show: false
            }
          },
          yaxis: {
            lines: {
              show: false
            }
          }
        }
      },
      serieBar: [{
        name: "calls",
        data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      }]
    };
  },
  mounted: function mounted() {
    this.render = true;
    this.campaign_id = this.$store.getters["campaigns/GET_CAMPAIGNS_ID"];
    var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];

    if (dates === null || dates === void 0 ? void 0 : dates.ini) {
      this.dates = [dates.ini, dates.end];
      this.dateLayout();
    }
  },
  created: function created() {
    var _this$$store$getters$;

    if ((_this$$store$getters$ = this.$store.getters["campaigns/GET_DATE_DASHBOARD"]) === null || _this$$store$getters$ === void 0 ? void 0 : _this$$store$getters$.ini) {
      this.GetCampaign(); // this.DateRangeGraph();
      // this.dateLayout();
    } else {
      this.dateRange();
    }
  },
  computed: {
    Itemscampaigns: function Itemscampaigns() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    },
    DateDashboard: function DateDashboard() {
      return this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
    }
  },
  methods: {
    dataSelectionDelete: function dataSelectionDelete(e, chartContext, config) {
      var _this = this;

      var role = localStorage.getItem('role');
      console.log(role, 'roleeeeeeeee');

      if (role == 1) {
        var position_date = this.array_comparison_finally[config.dataPointIndex];
        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
          title: "Delete all information for",
          html: "".concat(position_date, " <br> Are you really sure?"),
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#4CAF50",
          cancelButtonColor: "#E63E59",
          confirmButtonText: "Yes, delete it!"
        }).then(
        /*#__PURE__*/
        function () {
          var _ref = _asyncToGenerator(
          /*#__PURE__*/
          _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(result) {
            var request, response;
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    if (!result.value) {
                      _context.next = 13;
                      break;
                    }

                    _context.prev = 1;
                    request = {
                      date: position_date,
                      id: _this.campaign_id
                    };
                    _context.next = 5;
                    return _this.$store.dispatch("campaigns/DELETE_DATE_REGISTER", request);

                  case 5:
                    response = _context.sent;

                    _this.dateLayout(); // console.log(response);


                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                      icon: "success",
                      title: "Success!",
                      text: "The data has been deleted.",
                      type: "success",
                      confirmButtonColor: "#25A8E0"
                    });
                    _context.next = 13;
                    break;

                  case 10:
                    _context.prev = 10;
                    _context.t0 = _context["catch"](1);
                    console.log(_context.t0.response);

                  case 13:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee, null, [[1, 10]]);
          }));

          return function (_x) {
            return _ref.apply(this, arguments);
          };
        }());
      }
    },
    fileDoc: function fileDoc() {
      var _this2 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var idC;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                try {
                  idC = localStorage.getItem("campaign_id");

                  if (_this2.files.length === 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                      icon: "warning",
                      title: "Please select a file",
                      text: "",
                      confirmButtonColor: "#25A8E0"
                    });
                  } else if (idC == null) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                      icon: "warning",
                      title: "Please select a campaign",
                      text: "",
                      confirmButtonColor: "#25A8E0"
                    });
                  } else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                      title: "Are you sure you want to do this?",
                      text: "This action cannot be undone",
                      icon: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#EC687D",
                      cancelButtonColor: "#25A8E0",
                      confirmButtonText: "Agree"
                    }).then(
                    /*#__PURE__*/
                    function () {
                      var _ref2 = _asyncToGenerator(
                      /*#__PURE__*/
                      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(result) {
                        var id, formData;
                        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
                          while (1) {
                            switch (_context2.prev = _context2.next) {
                              case 0:
                                if (result.value) {
                                  _this2.loadingModal = true;
                                  id = localStorage.getItem("campaign_id");
                                  formData = new FormData();
                                  formData.append("csv", _this2.files);
                                  formData.append("campaign_id", id);

                                  _this2.$store.dispatch("report_1/ADD_FILE", formData).then(function (response) {
                                    var id = localStorage.getItem("campaign_id");
                                    _this2.campaign_id = id;
                                    _this2.files = [];
                                    _this2.loadingModal = false;

                                    _this2.graph();

                                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                      icon: "success",
                                      title: "File uploaded successfully",
                                      text: "",
                                      type: "success",
                                      confirmButtonColor: "#25A8E0"
                                    });
                                  })["catch"](function (e) {
                                    _this2.loadingModal = false;
                                    var message = "The format of the uploaded file does not belong to the dialing platform associated with the campaign";
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                                      icon: "error",
                                      title: "Error!",
                                      html: message,
                                      type: "error",
                                      customClass: {
                                        content: "text-xs-left px-3"
                                      },
                                      confirmButtonText: "Ok",
                                      confirmButtonColor: "#25A8E0"
                                    });
                                  });
                                }

                              case 1:
                              case "end":
                                return _context2.stop();
                            }
                          }
                        }, _callee2);
                      }));

                      return function (_x2) {
                        return _ref2.apply(this, arguments);
                      };
                    }());
                  }
                } catch (e) {
                  _this2.loadingModal = false;
                  sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                    icon: "error",
                    title: "Error!",
                    html: e.response.data.message,
                    type: "error",
                    customClass: {
                      content: "text-xs-left px-3"
                    },
                    confirmButtonText: "Ok",
                    confirmButtonColor: "#25A8E0"
                  });
                }

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },
    GetCampaign: function GetCampaign() {
      var _this3 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _this3.$store.dispatch("campaigns/INDEX");

              case 3:
                _context4.next = 8;
                break;

              case 5:
                _context4.prev = 5;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 8:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 5]]);
      }))();
    },
    DateRangeGraph: function DateRangeGraph() {
      this.status_graph = true;
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
      var ini = moment__WEBPACK_IMPORTED_MODULE_3___default()(dates.ini); // .subtract(15, "days")
      // .format("DD");

      var end_date = moment__WEBPACK_IMPORTED_MODULE_3___default()(dates.end);
      var date = moment__WEBPACK_IMPORTED_MODULE_3___default()(dates.ini).subtract(1, "day"); // .subtract(15, "days")
      // .format("YYYY-MM-DD");

      var day_1;
      var array_categories = [];
      var array_comparison = [];
      var array_serie_date = [];
      var serieBar = [];
      var i = 0;
      var diff_date = end_date.diff(ini, "days");

      if (diff_date > 30) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
          icon: "error",
          title: "The range must not be greater than 30 days",
          text: "",
          confirmButtonColor: "#25A8E0"
        });
        return;
      }

      do {
        // if (i == 0) {
        //     date = moment(date).format("YYYY-MM-DD");
        // } else {
        date = moment__WEBPACK_IMPORTED_MODULE_3___default()(date).add(1, "days").format("YYYY-MM-DD"); // }

        day_1 = moment__WEBPACK_IMPORTED_MODULE_3___default()(date).format("MMM Do");
        array_categories.push(day_1);
        array_comparison.push(moment__WEBPACK_IMPORTED_MODULE_3___default()(date).format("DD/MM/YYYY"));
        array_serie_date.push(moment__WEBPACK_IMPORTED_MODULE_3___default()(date).format("MM/DD/YYYY"));
        serieBar.push(0);
        i++;
      } while (i <= diff_date);

      this.array_comparison_finally = array_serie_date;
      this.barArea(array_categories, serieBar); // this.chartBar.xaxis.categories = array_categories;

      this.comparison = array_comparison;
      this.graph();
    },
    barArea: function barArea(array_categories, diff_date) {
      var _this4 = this;

      this.chartBar = {
        chart: {
          events: {
            dataPointSelection: function dataPointSelection(e, chart, opts) {
              // you can call Vue methods now as "this" will point to the Vue instance when you use ES6 arrow function
              _this4.VueDemoMethod();
            }
          },
          toolbar: {
            show: false
          },
          zoom: {
            enabled: false
          }
        },
        plotOptions: {
          bar: {
            columnWidth: "80%"
          }
        },
        dataLabels: {
          enabled: false
        },
        yaxis: {
          show: false
        },
        xaxis: {
          categories: array_categories,
          labels: {
            rotate: -60,
            style: {
              fontSize: "12px"
            }
          },
          tickPlacement: "on"
        },
        colors: ["#E63E59"],
        fill: {
          colors: ["#fff"]
        },
        stroke: {
          show: true,
          curve: "straight",
          lineCap: "square",
          colors: ["#E63E59"],
          width: 3,
          dashArray: 0
        },
        grid: {
          show: true,
          borderColor: "#90A4AE",
          strokeDashArray: 0,
          position: "back",
          xaxis: {
            lines: {
              show: false
            }
          },
          yaxis: {
            lines: {
              show: false
            }
          }
        }
      };
      this.serieBar = [{
        name: "calls",
        data: diff_date
      }];
    },
    graph: function graph() {
      var _this5 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var dates, request, response, date_log, array_calls, date_total, _i, _Object$entries, _Object$entries$_i, key, value, data, _iteratorNormalCompletion, _didIteratorError, _iteratorError, _loop, _iterator, _step;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                dates = _this5.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                _this5.loading = true;
                request = {
                  ini: moment__WEBPACK_IMPORTED_MODULE_3___default()(dates.ini).format("MM/DD/YYYY"),
                  end: moment__WEBPACK_IMPORTED_MODULE_3___default()(dates.end).format("MM/DD/YYYY"),
                  campaign_id: _this5.campaign_id
                };
                _context5.next = 6;
                return _this5.$store.dispatch("report_1/GRAPH", request);

              case 6:
                response = _context5.sent;
                _this5.loading = false;

                if (!(response !== null)) {
                  _context5.next = 38;
                  break;
                }

                _this5.loading = false;
                date_log = response.data;
                array_calls = [];
                date_total = [];

                for (_i = 0, _Object$entries = Object.entries(date_log); _i < _Object$entries.length; _i++) {
                  _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2), key = _Object$entries$_i[0], value = _Object$entries$_i[1];
                  data = {
                    date: key,
                    calls: value.calls_log
                  };
                  date_total.push(data);
                }

                _iteratorNormalCompletion = true;
                _didIteratorError = false;
                _iteratorError = undefined;
                _context5.prev = 17;

                _loop = function _loop() {
                  var iterator = _step.value;
                  var count = date_total.filter(function (element) {
                    return element.date === iterator;
                  });

                  if (count.length === 0) {
                    array_calls.push(0);
                  } else {
                    array_calls.push(count[0].calls);
                  }
                };

                for (_iterator = _this5.comparison[Symbol.iterator](); !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                  _loop();
                }

                _context5.next = 26;
                break;

              case 22:
                _context5.prev = 22;
                _context5.t0 = _context5["catch"](17);
                _didIteratorError = true;
                _iteratorError = _context5.t0;

              case 26:
                _context5.prev = 26;
                _context5.prev = 27;

                if (!_iteratorNormalCompletion && _iterator["return"] != null) {
                  _iterator["return"]();
                }

              case 29:
                _context5.prev = 29;

                if (!_didIteratorError) {
                  _context5.next = 32;
                  break;
                }

                throw _iteratorError;

              case 32:
                return _context5.finish(29);

              case 33:
                return _context5.finish(26);

              case 34:
                _this5.serieBar = [{
                  name: "calls",
                  data: array_calls
                }];
                _this5.status_graph = false;
                _context5.next = 40;
                break;

              case 38:
                _this5.loading = false;
                _this5.showAlert = true;

              case 40:
                _context5.next = 46;
                break;

              case 42:
                _context5.prev = 42;
                _context5.t1 = _context5["catch"](0);
                // statements
                _this5.loading = false;
                console.log(_context5.t1);

              case 46:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 42], [17, 22, 26, 34], [27,, 29, 33]]);
      }))();
    },
    CampaignsId: function CampaignsId() {
      var _this6 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        var campaigns, request;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                campaigns = _this6.Itemscampaigns.find(function (element) {
                  return element.id == _this6.campaign_id;
                });
                request = {
                  campaign_id: _this6.campaign_id
                };
                _context6.next = 5;
                return _this6.$store.commit("campaigns/SET_CAMPAIGNS_ID", request);

              case 5:
                _context6.next = 7;
                return _this6.$store.commit("campaigns/SET_SELECTED_CAMPAIGN", campaigns);

              case 7:
                _this6.get_agents();

                _context6.next = 13;
                break;

              case 10:
                _context6.prev = 10;
                _context6.t0 = _context6["catch"](0);
                console.log(_context6.t0);

              case 13:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 10]]);
      }))();
    },
    get_agents: function get_agents() {
      var _this7 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
        var request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                request = {
                  campaign_id: _this7.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context7.next = 4;
                return _this7.$store.dispatch("report_1/GET_AGENTS", request);

              case 4:
                response = _context7.sent;
                _context7.next = 10;
                break;

              case 7:
                _context7.prev = 7;
                _context7.t0 = _context7["catch"](0);
                console.log(_context7.t0, "ERROR ARCHIVO");

              case 10:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 7]]);
      }))();
    },
    dateLayout: function dateLayout() {
      var _this8 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
        var ini, end, request;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
          while (1) {
            switch (_context8.prev = _context8.next) {
              case 0:
                _context8.prev = 0;
                _this8.loadingValues = true;
                ini = "";
                end = "";

                if (_this8.dates[0] > _this8.dates[1]) {
                  ini = _this8.dates[1];
                  end = _this8.dates[0];
                } else {
                  ini = _this8.dates[0];
                  end = _this8.dates[1];
                }

                request = {
                  ini: ini,
                  end: end
                };
                _context8.next = 8;
                return _this8.$store.commit("campaigns/SET_DATE_DASHBOARD", request);

              case 8:
                // const response = await this.$store.dispatch(
                //   "campaigns/GET_FILE",
                //   request
                // );
                _this8.loadingValues = false; // let date_header = this.table_headers_1.find(element => element.name == data.name)
                // if (Object.keys(response.data).length > 0) {
                //   this.table_headers_1 = [
                //     {
                //       name: "",
                //       month1: "00-00-0000",
                //       month2: "00-00-0000"
                //     }
                //   ];
                //   // this.status_table = true;
                //   // this.selectedAgent = this.ItemsAgents;
                //   // this.agentAdd(this.selectedAgent);
                //   // this.addRange();
                // }
                // this.total();

                _this8.DateRangeGraph();

                _context8.next = 15;
                break;

              case 12:
                _context8.prev = 12;
                _context8.t0 = _context8["catch"](0);
                _this8.loadingValues = false; // console.log(error, "ERROR ARCHIVO");

              case 15:
              case "end":
                return _context8.stop();
            }
          }
        }, _callee8, null, [[0, 12]]);
      }))();
    },
    dateRange: function dateRange() {
      var day = moment__WEBPACK_IMPORTED_MODULE_3___default()().format("DD");
      var month = moment__WEBPACK_IMPORTED_MODULE_3___default()().subtract(1, "months").format("MM");
      var month2 = moment__WEBPACK_IMPORTED_MODULE_3___default()().format("MM");
      var year = moment__WEBPACK_IMPORTED_MODULE_3___default()().format("YYYY");
      var day_1;
      var day_2;

      if (day >= 2 && day <= 15) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month2, "-01")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month2, "-").concat(day)).format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      } else if (day >= 17) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month2, "-16")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month2, "-").concat(day)).format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      } else if (day == 1) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month, "-16")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month, "-16")).endOf("month").format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      } else if (day == 16) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month2, "-01")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_3___default()("".concat(year, "-").concat(month2, "-15")).format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      }

      this.dateLayout();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);


function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var _ref;

    return _ref = {
      showAgents: false,
      loadingValues: false,
      selectA: false,
      name_campaigns: "",
      tam_st: 0,
      settings: [],
      dates: [],
      month: "",
      singleSelect: false,
      select_agent_comparison: [],
      selectedAgent: [],
      dense: false,
      fixedHeader: false,
      height: 300
    }, _defineProperty(_ref, "singleSelect", false), _defineProperty(_ref, "selected", []), _defineProperty(_ref, "headersAgent", [{
      text: "",
      value: "action",
      "class": "length_text head-color"
    }, {
      text: "Agent",
      value: "name",
      "class": "length_text head-color"
    } // { text: 'HCC ', value: 'hcc', class: 'length_text head-color' },
    ]), _defineProperty(_ref, "table_1", [{
      name: "Agent Billable Hours",
      status: true
    }, {
      name: "Operational Hours",
      status: true
    }, {
      name: "Billable Hours",
      status: true
    }, {
      name: "Target Hours",
      status: true
    }, {
      name: "Reach",
      status: true
    }, {
      name: "Agent Total",
      status: true
    }, {
      name: "Agent Hours",
      status: true
    }]), _defineProperty(_ref, "table_headers_1", [{
      name: "",
      month1: "00-00-0000",
      month2: "00-00-0000"
    }]), _ref;
  },
  filters: {
    date1: function date1(value) {
      var date1 = "";

      if (value.month1 !== "00-00-0000") {
        var name_year1 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month1).format("MMM Do, YYYY");
        var name_year2 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month2).format("MMM Do, YYYY");

        if (name_year1 === name_year2) {
          date1 = name_year1;
        } else {
          date1 = name_year1; // date1 = name_year1 name_year2
        }
      } else {
        date1 = value.name;
      }

      return date1;
    },
    date2: function date2(value) {
      var date2 = "";

      if (value.month1 !== "00-00-0000") {
        var name_year1 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month1).format("MMM Do, YYYY");
        var name_year2 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month2).format("MMM Do, YYYY");

        if (name_year1 !== name_year2) {
          date2 = name_year2;
        }
      }

      return date2;
    }
  },
  computed: {
    dateRangeText: function dateRangeText() {
      return this.dates.join(" - ");
    },
    ItemsAgents: function ItemsAgents() {
      return this.$store.getters["report_1/GET_AGENTS"];
    },
    ItemsDateAgents: function ItemsDateAgents() {
      return this.$store.getters["report_1/GET_DATE_AGENTS"];
    },
    Itemscampaigns: function Itemscampaigns() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    }
  },
  watch: {
    selectedAgent: function selectedAgent(item) {
      var _this = this;

      // console.log(item,'dato')
      var prueba;
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        var _loop = function _loop() {
          var iterator = _step.value;
          prueba = _this.table_1.some(function (e, i) {
            return e.name === iterator.name;
          }); // prueba = this.table_1.indexOf(iterator);

          if (prueba === false) {
            iterator.status = true;

            _this.table_1.push(iterator); // console.log("agregando elemento primera vez");

          } else {
            iterator.status = true;
          }
        };

        for (var _iterator = item[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          _loop();
        } // console.log(item,'kkk')
        // if (item.length > 0) {

      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      this.trueFalse(item); // }
      // console.log(this.table_1)
    }
  },
  created: function created() {
    this.dateRange();
  },
  methods: {
    jsexport: function jsexport() {
      var _this2 = this;

      // URL Falsa para descargar
      var csvContent = "data:text/csv;charset=utf-8,"; // formatos

      var delimiter = ",";
      var fixedDecimal = 2; // cabecera

      var csvHeader = [[this.NameCampaigns.name + "-Agent Hours Custom"]];
      this.table_headers_1.forEach(function (index, key) {
        if (key !== 0) {
          csvHeader[0].push(index.name);
        }
      });
      console.log("00000"); // cuerpo

      var csvBody = [];
      var name = "";
      this.table_1.forEach(function (element, key) {
        // columna padre
        var dispFatherRowArray = [];

        if (key > 6) {
          name = element.active == false ? "* ".concat(element.name) : element.name;
        } else {
          name = element.name;
        }

        if (element.name !== "Agent Billable Hours") {
          dispFatherRowArray.push(name);
          dispFatherRowArray.push("");
        }

        _this2.table_headers_1.forEach(function (index, key2) {
          if (element["total_".concat(key2 + 1).concat(key)] == undefined) {
            dispFatherRowArray.push("0.00");
          } else {
            dispFatherRowArray.push(element["total_".concat(key2 + 1).concat(key)]);
          }
        });

        dispFatherRowArray.pop();
        dispFatherRowArray.splice(1, 1); // inserta columna padre

        if (key > 0) {
          csvBody.push(dispFatherRowArray);
        }
      }); // añadir resultados url falsa

      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvBody.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      var nameCSV = "".concat(this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"].name, "-Agent-Hours-Custom-").concat(moment__WEBPACK_IMPORTED_MODULE_1___default()().format("YYYY-MM-DD"), ".csv"); // generar elemento del dom para poner el enlace

      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", nameCSV);
      document.body.appendChild(link); // Required for FF
      // darle click

      link.click();
    },
    addRange: function addRange() {
      var _this3 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var data, request, response, date_header;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this3.showAgents = true;
                data = {
                  name: "".concat(_this3.dates[0], " - ").concat(_this3.dates[1]),
                  month1: _this3.dates[0],
                  month2: _this3.dates[1]
                };
                _context.prev = 2;
                _this3.loadingValues = true;
                request = {
                  date1: moment__WEBPACK_IMPORTED_MODULE_1___default()(_this3.dates[0]).format("MM/DD/YYYY"),
                  date2: moment__WEBPACK_IMPORTED_MODULE_1___default()(_this3.dates[1]).format("MM/DD/YYYY"),
                  campaign_id: _this3.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context.next = 7;
                return _this3.$store.dispatch("report_1/GET_FILE", request);

              case 7:
                response = _context.sent;
                _this3.loadingValues = false;
                date_header = _this3.table_headers_1.find(function (element) {
                  return element.name == data.name;
                });

                if (Object.keys(response.data).length > 0 && date_header == undefined) {
                  _this3.table_headers_1.push(data);

                  _this3.select_agent_comparison = _this3.selectedAgent; // this.trueFalse(this.selectedAgent)
                  // console.log('this.selectedAgent.length', this.selectedAgent.length)
                  // console.log('this.ItemsAgents.length', this.ItemsAgents.length)

                  if (_this3.selectedAgent.length === _this3.ItemsAgents.length) {
                    _this3.total();

                    _this3.selectA = true;
                  }
                } // this.total();


                _context.next = 17;
                break;

              case 13:
                _context.prev = 13;
                _context.t0 = _context["catch"](2);
                _this3.loadingValues = false;
                console.log(_context.t0, "ERROR ARCHIVO");

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[2, 13]]);
      }))();
    },
    dateRange: function dateRange() {
      var day = moment__WEBPACK_IMPORTED_MODULE_1___default()().format("DD");
      var month = moment__WEBPACK_IMPORTED_MODULE_1___default()().subtract(1, "months").format("MM");
      var month2 = moment__WEBPACK_IMPORTED_MODULE_1___default()().format("MM");
      var year = moment__WEBPACK_IMPORTED_MODULE_1___default()().format("YYYY");
      var day_1;
      var day_2;

      if (day <= 15) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month, "-16")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month, "-16")).endOf("month").format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      } else {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-01")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-15")).format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      }
    },
    total: function total() {
      var num = this.table_headers_1.length - 1;
      var prueba;
      var date2;

      for (var _i = 0, _Object$entries = Object.entries(this.table_1); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            key = _Object$entries$_i[0],
            value = _Object$entries$_i[1];

        // statement
        if (key == 0) {
          this.table_1[key]["total_" + num + key] = "Total";
        } else {
          var m = moment__WEBPACK_IMPORTED_MODULE_1___default()(new Date(2011, 2, 12, 0, 0, 0));
          var call_duration = void 0;
          var cont = 0;

          if (this.selectedAgent.length > 0 && key > 6) {
            for (var _i2 = 0, _Object$entries2 = Object.entries(this.ItemsDateAgents); _i2 < _Object$entries2.length; _i2++) {
              var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i2], 2),
                  _key = _Object$entries2$_i[0],
                  iterator = _Object$entries2$_i[1];

              if (_key == value.name) {
                call_duration = Object.values(iterator).reduce(function (accumulator, currentValue) {
                  return accumulator + currentValue.total_hours;
                }, 0);
                cont = cont + parseFloat(call_duration);
              }
            }

            this.table_1[key]["total_" + num + key] = cont.toFixed(2);
          } // console.log(this.table_1,'tabla')
          else {
              switch (key) {
                case "1":
                  this.table_1[key]["total_" + num + key] = this.addHours();
                  break;

                case "2":
                  this.table_1[key]["total_" + num + key] = this.addHours();
                  break;

                case "3":
                  this.table_1[key]["total_" + num + key] = this.countAgentDay() * 8;
                  break;

                case "4":
                  var num_2 = this.table_1[2]["total_" + num + 2];
                  var num_3 = this.table_1[3]["total_" + num + 3];
                  var result = parseFloat(num_2) - parseFloat(num_3);
                  this.table_1[key]["total_" + num + key] = result.toFixed(2);
                  break;

                case "5":
                  this.table_1[key]["total_" + num + key] = this.countAgentDay();
                  break;

                case "6":
                  this.table_1[key]["total_" + num + key] = this.table_1[2]["total_" + num + 2];
                  break;

                default:
                  this.table_1[key]["total_" + num + key] = 0;
                  break;
              }
            }
        }
      }
    },
    addHours: function addHours() {
      var call_duration;
      var cont = 0;
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = this.selectedAgent[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var iterator = _step2.value;

          for (var _i3 = 0, _Object$entries3 = Object.entries(this.ItemsDateAgents); _i3 < _Object$entries3.length; _i3++) {
            var _Object$entries3$_i = _slicedToArray(_Object$entries3[_i3], 2),
                key = _Object$entries3$_i[0],
                iterator2 = _Object$entries3$_i[1];

            if (key == iterator.name) {
              call_duration = Object.values(iterator2).reduce(function (accumulator, currentValue) {
                return accumulator + currentValue.total_hours;
              }, 0);
              cont = cont + parseFloat(call_duration);
            }
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
            _iterator2["return"]();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      return cont.toFixed(2);
    },
    countAgentDay: function countAgentDay() {
      var call_duration;
      var cont = 0;
      var array_B = [];
      var arrayAgentDate = [];
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        for (var _iterator3 = this.selectedAgent[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
          var iterator = _step3.value;

          for (var _i4 = 0, _Object$entries4 = Object.entries(this.ItemsDateAgents); _i4 < _Object$entries4.length; _i4++) {
            var _Object$entries4$_i = _slicedToArray(_Object$entries4[_i4], 2),
                name = _Object$entries4$_i[0],
                iterator2 = _Object$entries4$_i[1];

            for (var _i5 = 0, _Object$entries5 = Object.entries(iterator2); _i5 < _Object$entries5.length; _i5++) {
              var _Object$entries5$_i = _slicedToArray(_Object$entries5[_i5], 2),
                  key2 = _Object$entries5$_i[0],
                  iterator3 = _Object$entries5$_i[1];

              if (name == iterator.name) {
                var array = Object.keys(iterator2);
                array_B.push(array.length);
              }
            }
          }
        } // arrayAgentDate.push(array_B.length);

      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
            _iterator3["return"]();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }

      console.log("++++++", array_B);
      return array_B.length;
    },
    trueFalse: function trueFalse(item) {
      var _this4 = this;

      // console.log('estoy en true y false')
      var prueba2;
      var num = -1; // if(item.length > 0)
      // {

      var _loop2 = function _loop2() {
        var _Object$entries6$_i = _slicedToArray(_Object$entries6[_i6], 2),
            key = _Object$entries6$_i[0],
            value = _Object$entries6$_i[1];

        num++;

        if (num > 6) {
          // console.log(num,'soy num 7')
          prueba2 = item.some(function (e, i) {
            return e.name === value.name;
          }); // prueba2 = item.indexOf(value);
          // console.log(prueba2,'prueba2')

          if (prueba2 === false) {
            value.status = false;

            _this4.table_1.splice(num, 1, value);
          } else {
            value.status = true;

            _this4.table_1.splice(num, 1, value);
          }
        }
      };

      for (var _i6 = 0, _Object$entries6 = Object.entries(this.table_1); _i6 < _Object$entries6.length; _i6++) {
        _loop2();
      }

      this.total(); // }
    },
    prueba: function prueba(value) {
      console.log("Valor de prueba", value);
    },
    agents_active: function agents_active(name) {
      var agent = this.ItemsAgents.filter(function (index) {
        return index.name === name;
      }); // console.log('active',agent)

      if (agent.length === 0) {
        return name;
      } else if (agent[0].active == false) {
        return "* " + name;
      } else {
        return name;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1_d.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);


function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    var _ref;

    return _ref = {
      showAgents: false,
      loadingValues: false,
      status_table: false,
      settings: [],
      dates: [],
      month: "",
      singleSelect: false,
      selectedAgent: [],
      itemsMonth: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
      dense: false,
      fixedHeader: false,
      height: 300
    }, _defineProperty(_ref, "singleSelect", false), _defineProperty(_ref, "selected", []), _defineProperty(_ref, "headersAgent", [{
      text: "Agent",
      value: "name",
      "class": "length_text head-color"
    } // { text: 'Active', value: 'active', class: 'length_text head-color'}
    ]), _defineProperty(_ref, "table_1", [{
      name: "Agent Billable Hours"
    }, {
      name: "Operational Hours"
    }, {
      name: "Billable Hours"
    }, {
      name: "Target Hours"
    }, {
      name: "Reach"
    }, {
      name: "Agent Total"
    }, {
      name: "Agent Hours"
    }]), _defineProperty(_ref, "table_headers_1", [{
      name: "",
      month1: "00-00-0000",
      month2: "00-00-0000",
      "class": 'headers-agent'
    }]), _ref;
  },
  filters: {
    date1: function date1(value) {
      var date1 = "";

      if (value.month1 !== "00-00-0000") {
        var name_year1 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month1).format("MMM Do, YYYY");
        var name_year2 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month2).format("MMM Do, YYYY");

        if (name_year1 !== name_year2) {
          date1 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month1).format("MMM Do -");
        } else {
          date1 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month2).format("Do"); // date1 = name_year1 name_year2
        }
      } else {
        date1 = value.name;
      }

      return date1;
    },
    date2: function date2(value) {
      var date2 = "";

      if (value.month1 !== "00-00-0000") {
        var name_year1 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month1).format("MMM Do, YYYY");
        var name_year2 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month2).format("MMM Do, YYYY");

        if (name_year1 !== name_year2) {
          date2 = moment__WEBPACK_IMPORTED_MODULE_1___default()(value.month2).format("Do, YYYY");
        }
      }

      return date2;
    }
  },
  created: function created() {
    var _this = this;

    return _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              // this.dateRange();
              _this.calendar_get();

              _this.get_agents();

            case 2:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  computed: {
    dateRangeText: function dateRangeText() {
      return this.dates.join(" - ");
    },
    ItemsAgents: function ItemsAgents() {
      return this.$store.getters["report_1/GET_AGENTS"];
    },
    ItemsDateAgents: function ItemsDateAgents() {
      return this.$store.getters["report_1/GET_DATE_AGENTS"];
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    },
    total1: function total1() {
      var _this2 = this;

      var num = 0; // let prueba_table = [];

      for (var _i = 0, _Object$entries = Object.entries(this.table_headers_1); _i < _Object$entries.length; _i++) {
        var _Object$entries$_i = _slicedToArray(_Object$entries[_i], 2),
            key1 = _Object$entries$_i[0],
            value1 = _Object$entries$_i[1];

        num++;

        for (var _i2 = 0, _Object$entries2 = Object.entries(this.table_1); _i2 < _Object$entries2.length; _i2++) {
          var _Object$entries2$_i = _slicedToArray(_Object$entries2[_i2], 2),
              key = _Object$entries2$_i[0],
              value = _Object$entries2$_i[1];

          if (key > 0 && num > 2) {
            (function () {
              var m = moment__WEBPACK_IMPORTED_MODULE_1___default()(new Date(2011, 2, 12, 0, 0, 0));
              var call_duration = void 0;
              var cont = 0;
              var pos = 0;
              var pos1 = 0; // let day;
              // let month;
              // let year;
              // let date;

              var day = moment__WEBPACK_IMPORTED_MODULE_1___default()(value1.month1).format("DD");
              var month = moment__WEBPACK_IMPORTED_MODULE_1___default()(value1.month1).format("MM");
              var year = moment__WEBPACK_IMPORTED_MODULE_1___default()(value1.month1).format("YYYY");
              var date = "".concat(day, "/").concat(month, "/").concat(year);
              var num_1 = num - 1;

              if (_this2.selectedAgent.length > 0 && key > 6) {
                for (var _i3 = 0, _Object$entries3 = Object.entries(_this2.ItemsDateAgents); _i3 < _Object$entries3.length; _i3++) {
                  var _Object$entries3$_i = _slicedToArray(_Object$entries3[_i3], 2),
                      name = _Object$entries3$_i[0],
                      iterator = _Object$entries3$_i[1];

                  if (name == value.name) {
                    var _loop = function _loop() {
                      var _Object$entries4$_i = _slicedToArray(_Object$entries4[_i4], 2),
                          key_2 = _Object$entries4$_i[0],
                          iterator2 = _Object$entries4$_i[1];

                      if (moment__WEBPACK_IMPORTED_MODULE_1___default()(value1.month1).format("DD/MM/YYYY") == key_2) {
                        pos1 = key;
                        pos = _this2.table_headers_1.findIndex(function (element) {
                          return moment__WEBPACK_IMPORTED_MODULE_1___default()(element.month1).format("DD/MM/YYYY") == key_2;
                        });
                        call_duration = iterator2.total_hours;
                      }

                      _this2.table_1[pos1]["total_" + num_1 + key] = parseFloat(call_duration).toFixed(2);
                    };

                    for (var _i4 = 0, _Object$entries4 = Object.entries(iterator); _i4 < _Object$entries4.length; _i4++) {
                      _loop();
                    }
                  }
                }
              } else {
                switch (key) {
                  case "1":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == "".concat(year, " ").concat(month, " ").concat(day);
                    });
                    _this2.table_1[key]["total_" + num_1 + key] = _this2.addHours1(date);
                    break;

                  case "2":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == "".concat(year, " ").concat(month, " ").concat(day);
                    });
                    _this2.table_1[key]["total_" + num_1 + key] = _this2.addHours1(date);
                    break;

                  case "3":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == "".concat(year, " ").concat(month, " ").concat(day);
                    }); // this.table_1[key]["total_"+num_1 +key] = this.selectedAgent.length * 8

                    _this2.table_1[key]["total_" + num_1 + key] = _this2.countAgentDay1(date) * 8;
                    break;

                  case "4":
                    var pos4 = 0;
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == "".concat(year, " ").concat(month, " ").concat(day);
                    });
                    var num_2 = _this2.table_1[2]["total_" + num_1 + 2];
                    var num_3 = _this2.table_1[3]["total_" + num_1 + 3];
                    var result = parseFloat(num_2) - parseFloat(num_3);
                    _this2.table_1[key]["total_" + num_1 + key] = result.toFixed(2);
                    break;

                  case "5":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == "".concat(year, " ").concat(month, " ").concat(day);
                    }); // this.table_1[key]["total_"+num+key] = this.selectedAgent.length

                    _this2.table_1[key]["total_" + num_1 + key] = _this2.countAgentDay1(date);
                    break;

                  case "6":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == "".concat(year, " ").concat(month, " ").concat(day);
                    });
                    _this2.table_1[key]["total_" + num_1 + key] = _this2.table_1[2]["total_" + num_1 + 2];
                    break;
                }
              }
            })();
          } else if (key > 0 && num == 1) {
            var m = moment__WEBPACK_IMPORTED_MODULE_1___default()(new Date(2011, 2, 12, 0, 0, 0));
            var call_duration = void 0;
            var cont = 0;

            if (this.selectedAgent.length > 0 && key > 6) {
              for (var _i5 = 0, _Object$entries5 = Object.entries(this.ItemsDateAgents); _i5 < _Object$entries5.length; _i5++) {
                var _Object$entries5$_i = _slicedToArray(_Object$entries5[_i5], 2),
                    _key = _Object$entries5$_i[0],
                    iterator = _Object$entries5$_i[1];

                if (_key == value.name) {
                  call_duration = Object.values(iterator).reduce(function (accumulator, currentValue) {
                    return accumulator + currentValue.total_hours;
                  }, 0);
                  cont = cont + parseFloat(call_duration);
                }
              }

              this.table_1[key]["total_" + num + key] = cont.toFixed(2);
            } else {
              switch (key) {
                case "1":
                  this.table_1[key]["total_" + num + key] = this.addHours();
                  break;

                case "2":
                  this.table_1[key]["total_" + num + key] = this.addHours();
                  break;

                case "3":
                  this.table_1[key]["total_" + num + key] = this.countAgentDay() * 8;
                  break;

                case "4":
                  var num_2 = this.table_1[2]["total_" + num + 2];
                  var num_3 = this.table_1[3]["total_" + num + 3];
                  var result = parseFloat(num_2) - parseFloat(num_3);
                  this.table_1[key]["total_" + num + key] = result.toFixed(2);
                  break;

                case "5":
                  this.table_1[key]["total_" + num + key] = this.countAgentDay();
                  break;

                case "6":
                  this.table_1[key]["total_" + num + key] = this.table_1[2]["total_" + num + 2];
                  break;

                default:
                  this.table_1[key]["total_" + num + key] = 0;
                  break;
              }
            }
          } else {
            (function () {
              var m = moment__WEBPACK_IMPORTED_MODULE_1___default()(new Date(2011, 2, 12, 0, 0, 0));
              var call_duration = void 0;
              var cont = 0;
              var pos = 0;
              var pos1 = 0; // let day;
              // let month;
              // let year;
              // let date;

              var day = moment__WEBPACK_IMPORTED_MODULE_1___default()(value1.month1).format("DD");
              var month = moment__WEBPACK_IMPORTED_MODULE_1___default()(value1.month1).format("MM");
              var year = moment__WEBPACK_IMPORTED_MODULE_1___default()(value1.month1).format("YYYY");
              var date = "".concat(day, "/").concat(month, "/").concat(year);

              if (_this2.selectedAgent.length > 0 && key > 6) {
                for (var _i6 = 0, _Object$entries6 = Object.entries(_this2.ItemsDateAgents); _i6 < _Object$entries6.length; _i6++) {
                  var _Object$entries6$_i = _slicedToArray(_Object$entries6[_i6], 2),
                      name = _Object$entries6$_i[0],
                      _iterator = _Object$entries6$_i[1];

                  if (name == value.name) {
                    var headers = _this2.table_headers_1;

                    var _loop2 = function _loop2() {
                      var _Object$entries7$_i = _slicedToArray(_Object$entries7[_i7], 2),
                          key_2 = _Object$entries7$_i[0],
                          iterator2 = _Object$entries7$_i[1];

                      var status = headers.some(function (e, i) {
                        return e.month1 === key_2;
                      });

                      if (status == true) {
                        pos1 = key;
                        pos = headers.findIndex(function (element) {
                          return element.month1 == key_2;
                        });
                        call_duration = iterator2.total_hours;
                      } // console.log("pos", pos);


                      _this2.table_1[pos1]["total_" + pos + pos1] = call_duration;
                    };

                    for (var _i7 = 0, _Object$entries7 = Object.entries(_iterator); _i7 < _Object$entries7.length; _i7++) {
                      _loop2();
                    }
                  }
                } // cont = cont / 3600;
                // this.table_1[key]["total_"+num+key] = cont.toFixed(2)

              } else {
                switch (key) {
                  case "1":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == date;
                    });
                    _this2.table_1[pos1]["total_" + pos + pos1] = _this2.addHours1(date);
                    break;

                  case "2":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == date;
                    });
                    _this2.table_1[pos1]["total_" + pos + pos1] = _this2.addHours1(date);
                    break;

                  case "3":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == date;
                    });
                    _this2.table_1[pos1]["total_" + pos + pos1] = _this2.countAgentDay1(date) * 8;
                    break;

                  case "4":
                    var pos4 = 0;
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == date;
                    });
                    var _num_ = _this2.table_1[2]["total_" + pos + 2];
                    var _num_2 = _this2.table_1[3]["total_" + pos + 3];

                    var _result = parseFloat(_num_) - parseFloat(_num_2);

                    _this2.table_1[pos1]["total_" + pos + pos1] = _result.toFixed(2);
                    break;

                  case "5":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == date;
                    }); // this.table_1[pos1]["total_"+pos+pos1] = this.selectedAgent.length

                    _this2.table_1[pos1]["total_" + pos + pos1] = _this2.countAgentDay1(date);
                    break;

                  case "6":
                    pos1 = key;
                    pos = _this2.table_headers_1.findIndex(function (element) {
                      return element.month1 == date;
                    });
                    _this2.table_1[pos1]["total_" + pos + pos1] = _this2.table_1[2]["total_" + pos + 2];
                    break;
                }
              }
            })();
          }
        }
      } // for (const [key, value] of Object.entries(this.table_1)) {
      // let prueba_table = this.table_1;


      return this.table_1;
    }
  },
  methods: {
    jsexport: function jsexport() {
      var _this3 = this;

      // URL Falsa para descargar
      var csvContent = "data:text/csv;charset=utf-8,"; // formatos

      var delimiter = ",";
      var fixedDecimal = 2; // cabecera

      var csvHeader = [[this.NameCampaigns.name + "-Agent Hours"]];
      this.table_headers_1.forEach(function (index, key) {
        if (key === 1) {
          csvHeader[0].push("".concat(index.month1, " ~ ").concat(index.month2));
        } else if (key > 1) {
          csvHeader[0].push(moment__WEBPACK_IMPORTED_MODULE_1___default()(index.month1).format("Do"));
        }
      }); // cuerpo

      var csvBody = [];
      var name = "";
      this.table_1.forEach(function (element, key) {
        // columna padre
        var dispFatherRowArray = [];

        if (key > 6) {
          name = element.active == false ? "* ".concat(element.name) : element.name;
        } else {
          name = element.name;
        }

        if (element.name !== "Agent Billable Hours") {
          dispFatherRowArray.push(name);
          dispFatherRowArray.push("");
        }

        _this3.table_headers_1.forEach(function (index, key2) {
          if (key > 0) {
            if (element["total_".concat(key2 + 1).concat(key)] == undefined) {
              dispFatherRowArray.push("0.00");
            } else {
              dispFatherRowArray.push(element["total_".concat(key2 + 1).concat(key)]);
            }
          }
        });

        dispFatherRowArray.splice(1, 1);
        dispFatherRowArray.pop(); // inserta columna padre

        if (key > 0) {
          csvBody.push(dispFatherRowArray);
        }
      }); // añadir resultados url falsa

      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvBody.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      var nameCSV = "".concat(this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"].name, "-Agent-Hours-").concat(moment__WEBPACK_IMPORTED_MODULE_1___default()().format("YYYY-MM-DD"), ".csv"); // generar elemento del dom para poner el enlace

      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", nameCSV);
      document.body.appendChild(link); // Required for FF
      // darle click

      link.click();
    },
    get_agents: function get_agents() {
      var _this4 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                request = {
                  campaign_id: _this4.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context2.next = 4;
                return _this4.$store.dispatch("report_1/GET_AGENTS", request);

              case 4:
                response = _context2.sent;
                _context2.next = 10;
                break;

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0, "ERROR ARCHIVO");

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 7]]);
      }))();
    },
    calendar_get: function calendar_get() {
      var _this5 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var dates, request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _this5.showAgents = true; // console.log('this.dates[0]',this.dates[0])
                // console.log('this.dates[0]',this.$store.getters["campaigns/GET_DATE_DASHBOARD"].ini)
                // let data = {
                //   name: `${this.dates[0]} - ${this.dates[1]}`,
                //   month1: this.dates[0],
                //   month2: this.dates[1]
                // };
                // console.log('data',data)

                _context3.prev = 1;
                dates = _this5.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                _this5.loadingValues = true;
                request = {
                  date1: moment__WEBPACK_IMPORTED_MODULE_1___default()(dates.ini).format("MM/DD/YYYY"),
                  date2: moment__WEBPACK_IMPORTED_MODULE_1___default()(dates.end).format("MM/DD/YYYY"),
                  campaign_id: _this5.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context3.next = 7;
                return _this5.$store.dispatch("report_1/GET_FILE", request);

              case 7:
                response = _context3.sent;
                _this5.loadingValues = false; // let date_header = this.table_headers_1.find(element => element.name == data.name)

                if (Object.keys(response.data).length > 0) {
                  _this5.table_headers_1 = [{
                    name: "",
                    month1: "00-00-0000",
                    month2: "00-00-0000"
                  }]; // this.table_1 = [
                  //   {
                  //     name: "Agent Billable Hours"
                  //   },
                  //   {
                  //     name: "Operational Hours"
                  //   },
                  //   {
                  //     name: "Billable Hours"
                  //   },
                  //   {
                  //     name: "Target Hours"
                  //   },
                  //   {
                  //     name: "Reach"
                  //   },
                  //   {
                  //     name: "Agent Total"
                  //   },
                  //   {
                  //     name: "Agent Hours"
                  //   }
                  // ];

                  _this5.status_table = true;
                  _this5.selectedAgent = _this5.ItemsAgents;

                  _this5.agentAdd(_this5.selectedAgent);

                  _this5.addRange();
                } // this.total();


                _context3.next = 15;
                break;

              case 12:
                _context3.prev = 12;
                _context3.t0 = _context3["catch"](1);
                _this5.loadingValues = false; // console.log(error, "ERROR ARCHIVO");

              case 15:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[1, 12]]);
      }))();
    },
    addRange: function addRange() {
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"]; // console.log(moment(this.dates[0]).add(1,'d').format('YYYY-MM-DD'),'llll')
      // console.log(moment(this.dates[0]).format('DD'),'llll')

      var ini = moment__WEBPACK_IMPORTED_MODULE_1___default()(dates.ini).format("DD");
      var end = moment__WEBPACK_IMPORTED_MODULE_1___default()(dates.end).format("DD");
      var data = {};
      var day = dates.ini; // if(moment().format('DD') <= 15){
      //   ini = 16
      //   end = moment(dates.end).endOf("month").format('DD')
      // }else {
      //   ini = 1
      //   end = 15
      // }

      for (var i = ini; i <= end; i++) {
        if (i === ini) {
          data = {
            month1: dates.ini,
            month2: dates.end
          };
          this.table_headers_1.push(data);
          this.createdTable();
          data = {
            month1: moment__WEBPACK_IMPORTED_MODULE_1___default()(day).format("MM/DD/YYYY"),
            month2: moment__WEBPACK_IMPORTED_MODULE_1___default()(day).format("MM/DD/YYYY")
          };
          this.table_headers_1.push(data);
          this.createdTable();
        } else {
          day = moment__WEBPACK_IMPORTED_MODULE_1___default()(day).add(1, "d").format("MM/DD/YYYY");
          data = {
            month1: day,
            month2: day
          };
          this.table_headers_1.push(data);
          this.createdTable();
        }
      } // console.log(this.table_1)

    },
    createdTable: function createdTable() {
      var num = this.table_headers_1.length - 1;

      for (var _i8 = 0, _Object$entries8 = Object.entries(this.table_1); _i8 < _Object$entries8.length; _i8++) {
        var _Object$entries8$_i = _slicedToArray(_Object$entries8[_i8], 2),
            key = _Object$entries8$_i[0],
            value = _Object$entries8$_i[1];

        // statement
        if (key == 0) {
          this.table_1[key]["total_" + num + key] = "Total";
        } else {
          // this.table_1[key]["total_"+num+key] = this.table_headers_1.length -1
          var cont = 0;

          if (key == 3 || key == 5) {
            this.table_1[key]["total_" + num + key] = cont;
          } else {
            this.table_1[key]["total_" + num + key] = cont.toFixed(2);
          }
        }
      }
    },
    dateRange: function dateRange() {
      var day = moment__WEBPACK_IMPORTED_MODULE_1___default()().format("DD");
      var month = moment__WEBPACK_IMPORTED_MODULE_1___default()().subtract(1, "months").format("MM");
      var month2 = moment__WEBPACK_IMPORTED_MODULE_1___default()().format("MM");
      var year = moment__WEBPACK_IMPORTED_MODULE_1___default()().format("YYYY");
      var day_1;
      var day_2;

      if (day >= 2 && day <= 15) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-01")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-").concat(day)).format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      } else if (day >= 17) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-16")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-").concat(day)).format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      } else if (day == 1) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month, "-16")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month, "-16")).endOf("month").format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      } else if (day == 16) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-01")).format("YYYY-MM-DD");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_1___default()("".concat(year, "-").concat(month2, "-15")).format("YYYY-MM-DD");
        this.dates = [day_1, day_2];
      }
    },
    total: function total() {
      var num = this.table_headers_1.length - 1;
      var prueba;
      var date2;

      for (var _i9 = 0, _Object$entries9 = Object.entries(this.table_1); _i9 < _Object$entries9.length; _i9++) {
        var _Object$entries9$_i = _slicedToArray(_Object$entries9[_i9], 2),
            key = _Object$entries9$_i[0],
            value = _Object$entries9$_i[1];

        // statement
        if (key == 0) {
          this.table_1[key]["total_" + num + key] = "Total";
        }
      }
    },
    addHours: function addHours() {
      var call_duration;
      var cont = 0;
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator2 = this.selectedAgent[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator2.next()).done); _iteratorNormalCompletion = true) {
          var iterator = _step.value;

          for (var _i10 = 0, _Object$entries10 = Object.entries(this.ItemsDateAgents); _i10 < _Object$entries10.length; _i10++) {
            var _Object$entries10$_i = _slicedToArray(_Object$entries10[_i10], 2),
                key = _Object$entries10$_i[0],
                iterator2 = _Object$entries10$_i[1];

            if (key == iterator.name) {
              call_duration = Object.values(iterator2).reduce(function (accumulator, currentValue) {
                return accumulator + currentValue.total_hours;
              }, 0);
              cont = cont + parseFloat(call_duration);
            }
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator2["return"] != null) {
            _iterator2["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return cont.toFixed(2);
    },
    countAgentDay: function countAgentDay() {
      var call_duration;
      var cont = 0;
      var array_B = [];
      var arrayAgentDate = [];
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator3 = this.selectedAgent[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator3.next()).done); _iteratorNormalCompletion2 = true) {
          var iterator = _step2.value;

          for (var _i11 = 0, _Object$entries11 = Object.entries(this.ItemsDateAgents); _i11 < _Object$entries11.length; _i11++) {
            var _Object$entries11$_i = _slicedToArray(_Object$entries11[_i11], 2),
                name = _Object$entries11$_i[0],
                iterator2 = _Object$entries11$_i[1];

            for (var _i12 = 0, _Object$entries12 = Object.entries(iterator2); _i12 < _Object$entries12.length; _i12++) {
              var _Object$entries12$_i = _slicedToArray(_Object$entries12[_i12], 2),
                  key2 = _Object$entries12$_i[0],
                  iterator3 = _Object$entries12$_i[1];

              if (name == iterator.name) {
                var array = Object.keys(iterator2);
                array_B.push(array.length);
              }
            }
          }
        } // arrayAgentDate.push(array_B.length);
        // console.log("++++++", array_B);

      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator3["return"] != null) {
            _iterator3["return"]();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      return array_B.length;
    },
    agentAdd: function agentAdd(item) {
      var _this6 = this;

      var prueba;
      var _iteratorNormalCompletion3 = true;
      var _didIteratorError3 = false;
      var _iteratorError3 = undefined;

      try {
        var _loop3 = function _loop3() {
          var iterator = _step3.value;
          prueba = _this6.table_1.some(function (e, i) {
            return e.name === iterator.name;
          }); // prueba = this.table_1.indexOf(iterator);

          if (prueba === false) {
            iterator.status = true;

            _this6.table_1.push(iterator); // console.log("agregando elemento primera vez");

          } else {
            iterator.status = true;
          }
        };

        for (var _iterator4 = item[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator4.next()).done); _iteratorNormalCompletion3 = true) {
          _loop3();
        } // console.log(999, this.table_1);

      } catch (err) {
        _didIteratorError3 = true;
        _iteratorError3 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion3 && _iterator4["return"] != null) {
            _iterator4["return"]();
          }
        } finally {
          if (_didIteratorError3) {
            throw _iteratorError3;
          }
        }
      }
    },
    addHours1: function addHours1(date) {
      var call_duration;
      var cont = 0;
      var _iteratorNormalCompletion4 = true;
      var _didIteratorError4 = false;
      var _iteratorError4 = undefined;

      try {
        for (var _iterator5 = this.selectedAgent[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator5.next()).done); _iteratorNormalCompletion4 = true) {
          var iterator = _step4.value;

          for (var _i13 = 0, _Object$entries13 = Object.entries(this.ItemsDateAgents); _i13 < _Object$entries13.length; _i13++) {
            var _Object$entries13$_i = _slicedToArray(_Object$entries13[_i13], 2),
                name = _Object$entries13$_i[0],
                iterator2 = _Object$entries13$_i[1];

            for (var _i14 = 0, _Object$entries14 = Object.entries(iterator2); _i14 < _Object$entries14.length; _i14++) {
              var _Object$entries14$_i = _slicedToArray(_Object$entries14[_i14], 2),
                  key2 = _Object$entries14$_i[0],
                  iterator3 = _Object$entries14$_i[1];

              if (name == iterator.name && key2 == date) {
                call_duration = iterator3.total_hours;
                cont = cont + parseFloat(call_duration);
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError4 = true;
        _iteratorError4 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion4 && _iterator5["return"] != null) {
            _iterator5["return"]();
          }
        } finally {
          if (_didIteratorError4) {
            throw _iteratorError4;
          }
        }
      }

      return cont.toFixed(2);
    },
    countAgentDay1: function countAgentDay1(date) {
      var call_duration;
      var cont = 0;
      var array_B = [];
      var _iteratorNormalCompletion5 = true;
      var _didIteratorError5 = false;
      var _iteratorError5 = undefined;

      try {
        for (var _iterator6 = this.selectedAgent[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator6.next()).done); _iteratorNormalCompletion5 = true) {
          var iterator = _step5.value;

          for (var _i15 = 0, _Object$entries15 = Object.entries(this.ItemsDateAgents); _i15 < _Object$entries15.length; _i15++) {
            var _Object$entries15$_i = _slicedToArray(_Object$entries15[_i15], 2),
                name = _Object$entries15$_i[0],
                iterator2 = _Object$entries15$_i[1];

            for (var _i16 = 0, _Object$entries16 = Object.entries(iterator2); _i16 < _Object$entries16.length; _i16++) {
              var _Object$entries16$_i = _slicedToArray(_Object$entries16[_i16], 2),
                  key2 = _Object$entries16$_i[0],
                  iterator3 = _Object$entries16$_i[1];

              if (name == iterator.name && key2 == date) {
                array_B.push(name);
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError5 = true;
        _iteratorError5 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion5 && _iterator6["return"] != null) {
            _iterator6["return"]();
          }
        } finally {
          if (_didIteratorError5) {
            throw _iteratorError5;
          }
        }
      }

      return array_B.length;
    },
    agentCantidad: function agentCantidad(date) {
      var call_duration;
      var cont = 0;
      var _iteratorNormalCompletion6 = true;
      var _didIteratorError6 = false;
      var _iteratorError6 = undefined;

      try {
        for (var _iterator7 = this.selectedAgent[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator7.next()).done); _iteratorNormalCompletion6 = true) {
          var iterator = _step6.value;

          for (var _i17 = 0, _Object$entries17 = Object.entries(this.ItemsDateAgents); _i17 < _Object$entries17.length; _i17++) {
            var _Object$entries17$_i = _slicedToArray(_Object$entries17[_i17], 2),
                name = _Object$entries17$_i[0],
                iterator2 = _Object$entries17$_i[1];

            for (var _i18 = 0, _Object$entries18 = Object.entries(iterator2); _i18 < _Object$entries18.length; _i18++) {
              var _Object$entries18$_i = _slicedToArray(_Object$entries18[_i18], 2),
                  key2 = _Object$entries18$_i[0],
                  iterator3 = _Object$entries18$_i[1];

              if (name == iterator.name && key2 == date) {
                call_duration = iterator3.total_hours;
                cont = cont + parseFloat(call_duration);
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError6 = true;
        _iteratorError6 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion6 && _iterator7["return"] != null) {
            _iterator7["return"]();
          }
        } finally {
          if (_didIteratorError6) {
            throw _iteratorError6;
          }
        }
      }

      return cont.toFixed(2);
    },
    agents_active: function agents_active(name) {
      var agent = this.ItemsAgents.filter(function (index) {
        return index.name === name;
      });

      if (agent.length === 0) {
        return name;
      } else if (agent[0].active == false) {
        return "* " + name;
      } else {
        return name;
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      currentMonth: "",
      dayOfMonth: "",
      showDays: false,
      days: [],
      length: 3,
      onboarding: 0,
      Total_Hours: [{
        name: "Dials per Hour",
        calories: 159
      }, {
        name: "Contacts (Sales, Refusals, Calbacks) per Hours",
        calories: 237
      }],
      Primary_Sales: [{
        name: "Applications Received",
        calories: 159
      }, {
        name: "Apps/Hours",
        calories: 237
      }],
      Other_Commissionable_Hours: [{
        name: "",
        calories: ""
      }, {
        name: "",
        calories: ""
      }],
      sales_pipeline: [{
        name: "Applications Sent",
        calories: "345"
      }, {
        name: "Information On Company",
        calories: "345"
      }, {
        name: "Wood Apps",
        calories: "345"
      }, {
        name: "Call Backs",
        calories: "345"
      }],
      Load_Analysis: [],
      calls_log: [{
        name: "Inbound",
        calories: "345"
      }, {
        name: "Outbound",
        calories: "345"
      }],
      recycles: [{
        name: "Not Avalaible",
        calories: "345"
      }, {
        name: "Busy",
        calories: "345"
      }, {
        name: "Info On Company",
        calories: "345"
      }, {
        name: "Wood App Out",
        calories: "345"
      }, {
        name: "Sheduled Callback",
        calories: "345"
      }],
      finalized_declines: [{
        name: "Not Interested",
        calories: "345"
      }, {
        name: "Do Not Call",
        calories: "345"
      }, {
        name: "Not A Business",
        calories: "345"
      }, {
        name: "Lead Finalized (Other Reasons)",
        calories: "345"
      }]
    };
  },
  filters: {},
  created: function created() {
    // this.getValues();
    this.getDays(moment__WEBPACK_IMPORTED_MODULE_1___default()(new Date(), "YYY-MM-DD").format("MMM"));
  },
  computed: {
    getLastMonths: function getLastMonths() {
      var months = moment__WEBPACK_IMPORTED_MODULE_1___default.a.monthsShort();
      var currentMonth = moment__WEBPACK_IMPORTED_MODULE_1___default()().month();
      var lastMonths = [months[currentMonth - 2], months[currentMonth - 1], months[currentMonth]];
      return lastMonths;
    },
    getBody: function getBody() {
      var arrayData = [];
      var currentMonth = this.currentMonth;
      var dayOfMonth = this.dayOfMonth;

      for (var index = 0; index < dayOfMonth; index++) {
        arrayData[index] = {
          name: "Principal name",
          value: Math.floor(Math.random() * 101)
        };
      }

      return arrayData;
    },
    getAgents: function getAgents() {
      return this.$store.dispatch("report_1/GET_AGENTS");
    },
    getCampaign: function getCampaign() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    }
  },
  methods: {
    getValues: function getValues() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var request, response, selectAgents;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                request = {
                  date1: moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", _this.getLastMonths[0]).format("MM/DD/YYYY"),
                  date2: moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", _this.getLastMonths[2]).format("MM/DD/YYYY"),
                  campaign_id: _this.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context.next = 4;
                return _this.$store.dispatch("report_1/GET_FILE", request);

              case 4:
                response = _context.sent;
                selectAgents = response.data.filter(function (agents) {
                  if (agents.telephonic_data.id == _this.getAgents.id) {
                    return;
                  }
                });
                _context.next = 11;
                break;

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }))();
    },
    getDays: function getDays(month) {
      this.showDays = false;
      this.currentMonth = month;
      var dayOfMonth = moment__WEBPACK_IMPORTED_MODULE_1___default()().month(month).daysInMonth();
      this.dayOfMonth = dayOfMonth;
      var currentDay = moment__WEBPACK_IMPORTED_MODULE_1___default()().month(month).format("MM/DD/YYYY");
      var arrayOfDays = [];

      for (var index = 1; index < dayOfMonth; index++) {
        arrayOfDays[index] = {
          day: "".concat(moment__WEBPACK_IMPORTED_MODULE_1___default.a.monthsShort().indexOf(month) + 1, "/").concat(index, "/").concat(moment__WEBPACK_IMPORTED_MODULE_1___default()().year()),
          day_name: moment__WEBPACK_IMPORTED_MODULE_1___default()().year(2020).month(month).date(index).format("ddd")
        }; // arrayOfDays.push(`${moment.months().indexOf(month)+1}/${index}/${moment().year()}`);
      }

      this.days = arrayOfDays; // console.log("antes de eliminar", this.days);

      var value = this.days.shift(); // console.log(value);
      // console.log(this.days);
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_agents.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      originalAgents: [],
      json: {},
      headers: [{
        text: "Name",
        value: "name",
        "class": "length_text head-color"
      }, {
        text: "HCC ",
        value: "hcc",
        "class": "length_text head-color"
      }, {
        text: "Active",
        value: "active",
        "class": "length_text head-color"
      }]
    };
  },
  mounted: function mounted() {
    this.index_agents();
  },
  computed: {
    ItemsAgents: function ItemsAgents() {
      var parse = this.$store.getters["report_1/GET_AGENTS_GENERAL"].filter(function (value) {
        value.hcc == 0 ? value.hcc = false : value.hcc = true;
        value.active == 0 ? value.active = false : value.active = true;
        return value;
      });
      this.originalAgents = _toConsumableArray(parse);
      return parse;
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    }
  },
  methods: {
    index_agents: function index_agents() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                console.log('*****');
                _context.prev = 1;
                request = {
                  campaign_id: localStorage.getItem("campaign_id")
                };
                _context.next = 5;
                return _this.$store.dispatch("report_1/GET_AGENTS", request);

              case 5:
                response = _context.sent;
                _context.next = 11;
                break;

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](1);
                // statements
                console.log(_context.t0);

              case 11:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 8]]);
      }))();
    },
    add_table_agents: function add_table_agents() {
      var _this2 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                request = {
                  data: _this2.ItemsAgents.filter(function (index) {
                    return index.hcc == true;
                  })
                };
                _this2.json = request;
                _context2.next = 4;
                return _this2.$store.commit("report_1/SET_AGENTS_TABLE", request);

              case 4:
                response = _context2.sent;

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    updateManyAgents: function updateManyAgents() {
      var _this3 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var agents, enviar, index, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                agents = [];
                enviar = {
                  agents: agents
                };

                for (index = 0; index < _this3.ItemsAgents.length; index++) {
                  agents[index] = {
                    id: _this3.ItemsAgents[index].id,
                    hcc: _this3.ItemsAgents[index].hcc,
                    active: _this3.ItemsAgents[index].active
                  };
                }

                _context3.next = 5;
                return _this3.$store.dispatch("report_2/UPDATE_MANY_AGENTS", enviar);

              case 5:
                response = _context3.sent;
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  icon: "success",
                  title: "Agents updated successfully",
                  confirmButtonColor: "#25A8E0"
                });

              case 7:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: ["active", "data", "value"],
  data: function data() {
    return {
      name: "",
      "final": "",
      contact: "",
      effective: "",
      primary: false,
      pipeline: false,
      decline: false
    };
  },
  methods: {
    registerDisposition: function registerDisposition() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var enviar, response, _response, e, list, prop;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                enviar = {
                  name: _this.data.name,
                  "final": _this.data["final"],
                  contact: _this.data.contact,
                  effective: _this.data.effective,
                  pipeline: _this.data.pipeline,
                  decline: _this.data.decline,
                  primary: _this.data.primary
                };

                if (!(_this.value == 0)) {
                  _context.next = 9;
                  break;
                }

                _context.next = 5;
                return _this.$store.dispatch("report_2/REGISTER_DISPOSITION", enviar);

              case 5:
                response = _context.sent;

                if (response.status == 200) {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: "success",
                    title: "Disposition created successfully",
                    confirmButtonColor: "#25A8E0"
                  });

                  _this.$emit("dialog:change", "cerrar");
                }

                _context.next = 13;
                break;

              case 9:
                _context.next = 11;
                return _this.$store.dispatch("report_2/UPDATE_DISPOSITION", {
                  data: _this.data,
                  id: _this.data.id,
                  position_array: _this.data.position_array
                });

              case 11:
                _response = _context.sent;

                if (_response.status == 200) {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: "success",
                    title: "Disposition updated successfully",
                    confirmButtonColor: "#25A8E0"
                  });

                  _this.$emit("dialog:change", "cerrar");
                }

              case 13:
                _context.next = 21;
                break;

              case 15:
                _context.prev = 15;
                _context.t0 = _context["catch"](0);
                e = _context.t0.response.data.message;
                list = "";

                for (prop in e) {
                  list = list + "<li>".concat(e[prop][0], "</li>");
                }

                sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                  icon: "error",
                  title: "Oops...",
                  html: list,
                  confirmButtonColor: "#25A8E0"
                });

              case 21:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 15]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _R2_create_disposition__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./R2_create_disposition */ "./resources/js/components/reports/R2/R2_create_disposition.vue");


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    CreateDisposition: _R2_create_disposition__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  data: function data() {
    return {
      loading: false,
      dispositionValue: {},
      active: false,
      value: 0,
      headers: [{
        text: "Disposition",
        align: "end",
        sortable: true,
        value: "name",
        "class": "text-right length_text head-color"
      }, {
        text: "Final",
        align: "start",
        sortable: true,
        value: "final",
        "class": " length_text head-color"
      }, {
        text: "Contact",
        align: "start",
        sortable: true,
        value: "contact",
        "class": " length_text head-color"
      }, {
        text: "Effective",
        align: "start",
        sortable: true,
        value: "effective",
        "class": " length_text head-color"
      }, {
        text: "Primary",
        align: "start",
        sortable: true,
        value: "primary",
        "class": " length_text head-color"
      }, {
        text: "Pipeline",
        align: "start",
        sortable: true,
        value: "pipeline",
        "class": " length_text head-color"
      }, {
        text: "Decline",
        align: "start",
        sortable: true,
        value: "decline",
        "class": " length_text head-color"
      }, {
        text: "Edit / Delete",
        value: "action",
        align: "start",
        sortable: false,
        "class": " length_text head-color"
      }],
      disposition: []
    };
  },
  created: function created() {
    this.getDisposition();
  },
  filters: {
    primary: function primary(value) {
      if (value == 1) {
        return "primary";
      } else {
        return "---";
      }
    },
    pipeline: function pipeline(value) {
      if (value == true) {
        return "pipeline";
      } else {
        return "---";
      }
    },
    decline: function decline(value) {
      if (value == true) {
        return "decline";
      } else {
        return "---";
      }
    }
  },
  computed: {
    formTitle: function formTitle() {
      return this.edit === false ? "New" : "Edit";
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    },
    DispositionValues: function DispositionValues() {
      return this.$store.getters["report_2/GET_DISPOSITIONS"];
    }
  },
  methods: {
    getDisposition: function getDisposition() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _this.loading = true;
                _context.prev = 1;
                request = {
                  id: _this.NameCampaigns.id === undefined ? 0 : _this.NameCampaigns.id
                };
                _context.next = 5;
                return _this.$store.dispatch("report_2/GET_LIST_DISPOSITION", request);

              case 5:
                response = _context.sent;
                _this.disposition = response.data.data;
                _this.loading = false;
                _context.next = 14;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](1);
                _this.loading = false;
                console.log(_context.t0);

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 10]]);
      }))();
    },
    HandlerRegister: function HandlerRegister() {
      this.active = true;
      this.value = 0;
      this.dispositionValue = {};
    },
    HandlerEdit: function HandlerEdit(item) {
      this.active = true;
      this.value = 1;
      this.dispositionValue = _objectSpread({}, item, {
        position_array: this.DispositionValues.indexOf(item)
      });
    },
    HandlerDelete: function HandlerDelete(item) {
      var _this2 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _this2.$store.dispatch("report_2/DELETE_DISPOSITION", {
                  id: item.id
                });

              case 3:
                response = _context2.sent;

                if (response.status == 200) {
                  sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: "success",
                    title: "Disposition deleted successfully",
                    confirmButtonColor: "#25A8E0"
                  });
                }

                _context2.next = 10;
                break;

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 7]]);
      }))();
    },
    eventdialog: function eventdialog(message) {
      if (message !== "cerrar") {
        this.active = false;
        this.dispositionValue = {};
      }

      this.active = false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R3/Scorecard.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);


function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      principal_section: true,
      first_fortnight_section: true,
      second_fortnight_section: true,
      dense: false,
      fixedHeader: false,
      height: 300,
      table_first: [],
      table_second: [],
      table_principal: [],
      table_first_total: {
        total: {
          adherence: 0,
          calls_per_hour: 0,
          disposed_calls: 0,
          talktime: 0,
          total_calls: 0,
          total_hours: 0
        }
      },
      table_second_total: {
        total: {
          adherence: 0,
          calls_per_hour: 0,
          disposed_calls: 0,
          talktime: 0,
          total_calls: 0,
          total_hours: 0
        }
      },
      table_today_total: {
        total: {
          adherence: 0,
          calls_per_hour: 0,
          disposed_calls: 0,
          talktime: 0,
          total_calls: 0,
          total_hours: 0,
          target_time: 0
        }
      }
    };
  },
  filters: {
    parse_decimal: function parse_decimal(value) {
      return parseFloat(value).toFixed(2);
    },
    parse_int: function parse_int(value) {
      return parseInt(value);
    },
    hours: function hours(value) {
      var seconds = value * 3600;
      var hour = Math.floor(seconds / 3600);
      hour = hour < 10 ? '0' + hour : hour;
      var minute = Math.floor(seconds / 60 % 60);
      minute = minute < 10 ? '0' + minute : minute;
      var second = seconds % 60;
      second = second < 10 ? '0' + second : second;
      return hour + ':' + minute; // return moment.utc().startOf('day').add(minutes, 'minutes').format('HH:mm')
    },
    hours_log: function hours_log(value) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()(value).format("HH:mm");
    }
  },
  created: function created() {
    // this.date_position();
    this.scorecard_first_fortnight();
    this.scorecard_second_fortnight();
    this.scorecard_today();
  },
  mounted: function mounted() {
    this.campaign_id = this.$store.getters["campaigns/GET_CAMPAIGNS_ID"];
  },
  computed: {
    Itemscampaigns: function Itemscampaigns() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    },
    DateDashboard: function DateDashboard() {
      return this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
    }
  },
  methods: {
    hours_export: function hours_export(value) {
      var seconds = value * 3600;
      var hour = Math.floor(seconds / 3600);
      hour = hour < 10 ? '0' + hour : hour;
      var minute = Math.floor(seconds / 60 % 60);
      minute = minute < 10 ? '0' + minute : minute;
      var second = seconds % 60;
      second = second < 10 ? '0' + second : second;
      return hour + ':' + minute;
    },
    current_day: function current_day() {
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
      var day_new = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end) // .subtract(1, "days")
      .format('MMMM Do, YYYY'); // .format('dddd, Do MMMM YYYY');

      return day_new;
    },
    first_fortnight: function first_fortnight() {
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
      var month = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM");
      var day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");
      var month2 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM");
      var year = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("YYYY");
      var day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-01")).format("MMMM Do");
      var day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-15")).format("Do, YYYY");
      return "".concat(day_1, " - ").concat(day_2);
    },
    second_fortnight: function second_fortnight() {
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
      var month = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM");
      var day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");
      var month2 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM");
      var year = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("YYYY");
      var day_1;
      var day_2;

      if (day > 15) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).format("MMMM Do");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).endOf("month").format("Do, YYYY");
      } else {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).format("MMMM Do");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).endOf("month").format("Do, YYYY");
      }

      return "".concat(day_1, " - ").concat(day_2);
    },
    date_position: function date_position() {
      var day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");

      if (day > 15) {
        this.second_fortnight_section = true;
      } else {
        this.first_fortnight_section = false;
      }
    },
    scorecard_first_fortnight: function scorecard_first_fortnight() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var dates, month, request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                dates = _this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                month = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format('MM');
                request = {
                  start: moment__WEBPACK_IMPORTED_MODULE_2___default()().format(month + "/01/YYYY"),
                  end: moment__WEBPACK_IMPORTED_MODULE_2___default()().format(month + "/15/YYYY"),
                  campaign_id: _this.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context.next = 6;
                return _this.$store.dispatch("report_3/INDEX", request);

              case 6:
                response = _context.sent;

                _this.table_first_fortnight(response.data.data);

                _context.next = 13;
                break;

              case 10:
                _context.prev = 10;
                _context.t0 = _context["catch"](0);
                // statements
                console.log(_context.t0);

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 10]]);
      }))();
    },
    scorecard_second_fortnight: function scorecard_second_fortnight() {
      var _this2 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var dates, day, month, month2, year, day_1, day_2, request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                dates = _this2.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");
                month = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format('MM');
                month2 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM");
                year = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("YYYY");

                if (day > 15) {
                  day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).format("MM/DD/YYYY");
                  day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).endOf("month").format("MM/DD/YYYY");
                } else {
                  day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).format("MM/DD/YYYY");
                  day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).endOf("month").format("MM/DD/YYYY");
                }

                request = {
                  start: day_1,
                  end: day_2,
                  campaign_id: _this2.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context2.next = 10;
                return _this2.$store.dispatch("report_3/INDEX", request);

              case 10:
                response = _context2.sent;

                _this2.table_second_fortnight(response.data.data);

                _context2.next = 17;
                break;

              case 14:
                _context2.prev = 14;
                _context2.t0 = _context2["catch"](0);
                // statements
                console.log(_context2.t0);

              case 17:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 14]]);
      }))();
    },
    scorecard_today: function scorecard_today() {
      var _this3 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var dates, request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                dates = _this3.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                request = {
                  start: moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM/DD/YYYY"),
                  end: moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM/DD/YYYY"),
                  campaign_id: _this3.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                _context3.next = 5;
                return _this3.$store.dispatch("report_3/INDEX", request);

              case 5:
                response = _context3.sent;

                _this3.table_today(response.data.data);

                _context3.next = 12;
                break;

              case 9:
                _context3.prev = 9;
                _context3.t0 = _context3["catch"](0);
                // statements
                console.log(_context3.t0);

              case 12:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 9]]);
      }))();
    },
    table_first_fortnight: function table_first_fortnight(data_dates) {
      var array_agents = [];

      for (var _i = 0, _Object$values = Object.values(data_dates); _i < _Object$values.length; _i++) {
        var agents = _Object$values[_i];

        for (var agent in agents) {
          array_agents.push(_objectSpread({
            name: agent
          }, agents[agent]));
        }
      }

      var total_agents = array_agents.reduce(function (accumulator, currentValue) {
        var _accumulator$currentV, _accumulator$currentV2, _accumulator$currentV3, _accumulator$currentV4, _accumulator$currentV5, _accumulator$currentV6, _accumulator$currentV7, _accumulator$currentV8, _accumulator$currentV9, _accumulator$currentV10;

        // accumulator[currentValue.name] = (accumulator[currentValue.name]?.adherence) ? 
        //   {
        //     adherence: accumulator[currentValue.name].adherence + currentValue.adherence,
        //     calls_per_hour: accumulator[currentValue.name].calls_per_hour + currentValue.calls_per_hour
        //   }
        //   : currentValue
        var adherence = currentValue.adherence,
            calls_per_hour = currentValue.calls_per_hour,
            disposed_calls = currentValue.disposed_calls,
            talktime = currentValue.talktime,
            total_calls = currentValue.total_calls,
            total_hours = currentValue.total_hours,
            name = currentValue.name;
        accumulator[currentValue.name] = {
          adherence: ((_accumulator$currentV = accumulator[currentValue.name]) === null || _accumulator$currentV === void 0 ? void 0 : _accumulator$currentV.adherence) + adherence || adherence,
          adherence1: ((_accumulator$currentV2 = accumulator[currentValue.name]) === null || _accumulator$currentV2 === void 0 ? void 0 : _accumulator$currentV2.adherence) == adherence || adherence,
          calls_per_hour: ((_accumulator$currentV3 = accumulator[currentValue.name]) === null || _accumulator$currentV3 === void 0 ? void 0 : _accumulator$currentV3.calls_per_hour) + calls_per_hour || calls_per_hour,
          disposed_calls: ((_accumulator$currentV4 = accumulator[currentValue.name]) === null || _accumulator$currentV4 === void 0 ? void 0 : _accumulator$currentV4.disposed_calls) + disposed_calls || disposed_calls,
          talktime: ((_accumulator$currentV5 = accumulator[currentValue.name]) === null || _accumulator$currentV5 === void 0 ? void 0 : _accumulator$currentV5.talktime) + talktime || talktime,
          total_calls: ((_accumulator$currentV6 = accumulator[currentValue.name]) === null || _accumulator$currentV6 === void 0 ? void 0 : _accumulator$currentV6.total_calls) + total_calls || total_calls,
          total_hours: ((_accumulator$currentV7 = accumulator[currentValue.name]) === null || _accumulator$currentV7 === void 0 ? void 0 : _accumulator$currentV7.total_hours) + total_hours || total_hours,
          session: ((_accumulator$currentV8 = accumulator[currentValue.name]) === null || _accumulator$currentV8 === void 0 ? void 0 : _accumulator$currentV8.session) + 1 || 1,
          target_time: (((_accumulator$currentV9 = accumulator[currentValue.name]) === null || _accumulator$currentV9 === void 0 ? void 0 : _accumulator$currentV9.target_time) / 8 + 1) * 8 || 8,
          name: ((_accumulator$currentV10 = accumulator[currentValue.name]) === null || _accumulator$currentV10 === void 0 ? void 0 : _accumulator$currentV10.name) || name,
          total: 'total'
        };

        if (accumulator[currentValue.name].name == 'Ana Guadalupe Carvajal Morales') {
          console.log(accumulator[currentValue.name]);
        }

        return accumulator;
      }, {});
      var total_value = Object.values(total_agents).reduce(function (accumulator, currentValue) {
        var _accumulator$currentV11, _accumulator$currentV12, _accumulator$currentV13, _accumulator$currentV14, _accumulator$currentV15, _accumulator$currentV16, _accumulator$currentV17, _accumulator$currentV18;

        var adherence = currentValue.adherence,
            calls_per_hour = currentValue.calls_per_hour,
            disposed_calls = currentValue.disposed_calls,
            talktime = currentValue.talktime,
            total_calls = currentValue.total_calls,
            total_hours = currentValue.total_hours,
            session = currentValue.session,
            target_time = currentValue.target_time,
            total = currentValue.total;
        accumulator[currentValue.total] = {
          adherence: ((_accumulator$currentV11 = accumulator[currentValue.total]) === null || _accumulator$currentV11 === void 0 ? void 0 : _accumulator$currentV11.adherence) + adherence || adherence,
          calls_per_hour: ((_accumulator$currentV12 = accumulator[currentValue.total]) === null || _accumulator$currentV12 === void 0 ? void 0 : _accumulator$currentV12.calls_per_hour) + calls_per_hour || calls_per_hour,
          disposed_calls: ((_accumulator$currentV13 = accumulator[currentValue.total]) === null || _accumulator$currentV13 === void 0 ? void 0 : _accumulator$currentV13.disposed_calls) + disposed_calls || disposed_calls,
          talktime: ((_accumulator$currentV14 = accumulator[currentValue.total]) === null || _accumulator$currentV14 === void 0 ? void 0 : _accumulator$currentV14.talktime) + talktime || talktime,
          total_calls: ((_accumulator$currentV15 = accumulator[currentValue.total]) === null || _accumulator$currentV15 === void 0 ? void 0 : _accumulator$currentV15.total_calls) + total_calls || total_calls,
          total_hours: ((_accumulator$currentV16 = accumulator[currentValue.total]) === null || _accumulator$currentV16 === void 0 ? void 0 : _accumulator$currentV16.total_hours) + total_hours || total_hours,
          session: ((_accumulator$currentV17 = accumulator[currentValue.total]) === null || _accumulator$currentV17 === void 0 ? void 0 : _accumulator$currentV17.session) + session || session,
          target_time: ((_accumulator$currentV18 = accumulator[currentValue.total]) === null || _accumulator$currentV18 === void 0 ? void 0 : _accumulator$currentV18.target_time) + target_time || target_time
        };
        return accumulator;
      }, {});
      this.table_first = total_agents;
      this.table_first_total = (total_value === null || total_value === void 0 ? void 0 : total_value.total) ? total_value : this.table_first_total;
    },
    table_second_fortnight: function table_second_fortnight(data_dates) {
      var array_agents = [];

      for (var _i2 = 0, _Object$values2 = Object.values(data_dates); _i2 < _Object$values2.length; _i2++) {
        var agents = _Object$values2[_i2];

        for (var agent in agents) {
          array_agents.push(_objectSpread({
            name: agent
          }, agents[agent]));
        }
      }

      var total_agents = array_agents.reduce(function (accumulator, currentValue) {
        var _accumulator$currentV19, _accumulator$currentV20, _accumulator$currentV21, _accumulator$currentV22, _accumulator$currentV23, _accumulator$currentV24, _accumulator$currentV25, _accumulator$currentV26, _accumulator$currentV27;

        // accumulator[currentValue.name] = (accumulator[currentValue.name]?.adherence) ? 
        //   {
        //     adherence: accumulator[currentValue.name].adherence + currentValue.adherence,
        //     calls_per_hour: accumulator[currentValue.name].calls_per_hour + currentValue.calls_per_hour
        //   }
        //   : currentValue
        var adherence = currentValue.adherence,
            calls_per_hour = currentValue.calls_per_hour,
            disposed_calls = currentValue.disposed_calls,
            talktime = currentValue.talktime,
            total_calls = currentValue.total_calls,
            total_hours = currentValue.total_hours,
            name = currentValue.name;
        accumulator[currentValue.name] = {
          adherence: ((_accumulator$currentV19 = accumulator[currentValue.name]) === null || _accumulator$currentV19 === void 0 ? void 0 : _accumulator$currentV19.adherence) + adherence || adherence,
          calls_per_hour: ((_accumulator$currentV20 = accumulator[currentValue.name]) === null || _accumulator$currentV20 === void 0 ? void 0 : _accumulator$currentV20.calls_per_hour) + calls_per_hour || calls_per_hour,
          disposed_calls: ((_accumulator$currentV21 = accumulator[currentValue.name]) === null || _accumulator$currentV21 === void 0 ? void 0 : _accumulator$currentV21.disposed_calls) + disposed_calls || disposed_calls,
          talktime: ((_accumulator$currentV22 = accumulator[currentValue.name]) === null || _accumulator$currentV22 === void 0 ? void 0 : _accumulator$currentV22.talktime) + talktime || talktime,
          total_calls: ((_accumulator$currentV23 = accumulator[currentValue.name]) === null || _accumulator$currentV23 === void 0 ? void 0 : _accumulator$currentV23.total_calls) + total_calls || total_calls,
          total_hours: ((_accumulator$currentV24 = accumulator[currentValue.name]) === null || _accumulator$currentV24 === void 0 ? void 0 : _accumulator$currentV24.total_hours) + total_hours || total_hours,
          session: ((_accumulator$currentV25 = accumulator[currentValue.name]) === null || _accumulator$currentV25 === void 0 ? void 0 : _accumulator$currentV25.session) + 1 || 1,
          target_time: (((_accumulator$currentV26 = accumulator[currentValue.name]) === null || _accumulator$currentV26 === void 0 ? void 0 : _accumulator$currentV26.target_time) / 8 + 1) * 8 || 8,
          name: ((_accumulator$currentV27 = accumulator[currentValue.name]) === null || _accumulator$currentV27 === void 0 ? void 0 : _accumulator$currentV27.name) || name,
          total: 'total'
        };
        return accumulator;
      }, {});
      var total_value = Object.values(total_agents).reduce(function (accumulator, currentValue) {
        var _accumulator$currentV28, _accumulator$currentV29, _accumulator$currentV30, _accumulator$currentV31, _accumulator$currentV32, _accumulator$currentV33, _accumulator$currentV34, _accumulator$currentV35;

        var adherence = currentValue.adherence,
            calls_per_hour = currentValue.calls_per_hour,
            disposed_calls = currentValue.disposed_calls,
            talktime = currentValue.talktime,
            total_calls = currentValue.total_calls,
            total_hours = currentValue.total_hours,
            session = currentValue.session,
            target_time = currentValue.target_time,
            total = currentValue.total;
        accumulator[currentValue.total] = {
          adherence: ((_accumulator$currentV28 = accumulator[currentValue.total]) === null || _accumulator$currentV28 === void 0 ? void 0 : _accumulator$currentV28.adherence) + adherence || adherence,
          calls_per_hour: ((_accumulator$currentV29 = accumulator[currentValue.total]) === null || _accumulator$currentV29 === void 0 ? void 0 : _accumulator$currentV29.calls_per_hour) + calls_per_hour || calls_per_hour,
          disposed_calls: ((_accumulator$currentV30 = accumulator[currentValue.total]) === null || _accumulator$currentV30 === void 0 ? void 0 : _accumulator$currentV30.disposed_calls) + disposed_calls || disposed_calls,
          talktime: ((_accumulator$currentV31 = accumulator[currentValue.total]) === null || _accumulator$currentV31 === void 0 ? void 0 : _accumulator$currentV31.talktime) + talktime || talktime,
          total_calls: ((_accumulator$currentV32 = accumulator[currentValue.total]) === null || _accumulator$currentV32 === void 0 ? void 0 : _accumulator$currentV32.total_calls) + total_calls || total_calls,
          total_hours: ((_accumulator$currentV33 = accumulator[currentValue.total]) === null || _accumulator$currentV33 === void 0 ? void 0 : _accumulator$currentV33.total_hours) + total_hours || total_hours,
          session: ((_accumulator$currentV34 = accumulator[currentValue.total]) === null || _accumulator$currentV34 === void 0 ? void 0 : _accumulator$currentV34.session) + session || session,
          target_time: ((_accumulator$currentV35 = accumulator[currentValue.total]) === null || _accumulator$currentV35 === void 0 ? void 0 : _accumulator$currentV35.target_time) + target_time || target_time
        };
        return accumulator;
      }, {});
      console.log('table_second', total_value);
      this.table_second = total_agents;
      this.table_second_total = (total_value === null || total_value === void 0 ? void 0 : total_value.total) ? total_value : this.table_second_total;
    },
    table_today: function table_today(data_dates) {
      var array_agents = [];

      for (var _i3 = 0, _Object$values3 = Object.values(data_dates); _i3 < _Object$values3.length; _i3++) {
        var agents = _Object$values3[_i3];

        for (var agent in agents) {
          array_agents.push(_objectSpread({
            name: agent
          }, agents[agent], {
            total: 'total'
          }));
        }
      }

      var total_value = Object.values(array_agents).reduce(function (accumulator, currentValue) {
        var _accumulator$currentV36, _accumulator$currentV37, _accumulator$currentV38, _accumulator$currentV39, _accumulator$currentV40, _accumulator$currentV41, _accumulator$currentV42;

        var adherence = currentValue.adherence,
            calls_per_hour = currentValue.calls_per_hour,
            disposed_calls = currentValue.disposed_calls,
            talktime = currentValue.talktime,
            total_calls = currentValue.total_calls,
            target_time = currentValue.target_time,
            total_hours = currentValue.total_hours,
            total = currentValue.total;
        accumulator[currentValue.total] = {
          adherence: ((_accumulator$currentV36 = accumulator[currentValue.total]) === null || _accumulator$currentV36 === void 0 ? void 0 : _accumulator$currentV36.adherence) + adherence || adherence,
          calls_per_hour: ((_accumulator$currentV37 = accumulator[currentValue.total]) === null || _accumulator$currentV37 === void 0 ? void 0 : _accumulator$currentV37.calls_per_hour) + calls_per_hour || calls_per_hour,
          disposed_calls: ((_accumulator$currentV38 = accumulator[currentValue.total]) === null || _accumulator$currentV38 === void 0 ? void 0 : _accumulator$currentV38.disposed_calls) + disposed_calls || disposed_calls,
          talktime: ((_accumulator$currentV39 = accumulator[currentValue.total]) === null || _accumulator$currentV39 === void 0 ? void 0 : _accumulator$currentV39.talktime) + talktime || talktime,
          total_calls: ((_accumulator$currentV40 = accumulator[currentValue.total]) === null || _accumulator$currentV40 === void 0 ? void 0 : _accumulator$currentV40.total_calls) + total_calls || total_calls,
          total_hours: ((_accumulator$currentV41 = accumulator[currentValue.total]) === null || _accumulator$currentV41 === void 0 ? void 0 : _accumulator$currentV41.total_hours) + total_hours || total_hours,
          target_time: ((_accumulator$currentV42 = accumulator[currentValue.total]) === null || _accumulator$currentV42 === void 0 ? void 0 : _accumulator$currentV42.target_time) + 8 || 8
        };
        return accumulator;
      }, {});
      console.log('array_agents', array_agents);
      this.table_principal = array_agents;
      this.table_today_total = (total_value === null || total_value === void 0 ? void 0 : total_value.total) ? total_value : this.table_today_total;
    },
    adherence_session: function adherence_session(value) {
      if (value === null || value === void 0 ? void 0 : value.adherence) {
        var total = value.total_hours * 100 / value.target_time;
        return parseInt(total);
      } else {
        return 0;
      }
    },
    jsexport: function jsexport() {
      var _this4 = this;

      // URL Falsa para descargar
      var csvContent = "data:text/csv;charset=utf-8,"; // formatos

      var delimiter = ",";
      var fixedDecimal = 2; // cabecera

      var csvHeader = [[this.NameCampaigns.name + "- Scorecard " + this.current_day()]];
      ["Log In", "Log Out", "Session Time", "Adherence", "Dispo'ed Calls", "Calls per Hour", "Talktime"].forEach(function (index, key) {
        csvHeader[0].push(index);
      }); // cuerpo

      var csvBody = [];
      var name = "";
      Object.values(this.table_principal).forEach(function (element, key) {
        // columna padre
        var dispFatherRowArray = []; // let hours = this.hours_export(element.total_hours)
        // console.log('hours', hours)

        dispFatherRowArray.push(element.name);
        dispFatherRowArray.push(moment__WEBPACK_IMPORTED_MODULE_2___default()(element.login).format('HH:mm'));
        dispFatherRowArray.push(moment__WEBPACK_IMPORTED_MODULE_2___default()(element.logout).format('HH:mm'));
        dispFatherRowArray.push(_this4.hours_export(element.total_hours));
        dispFatherRowArray.push(parseFloat(element.adherence).toFixed(2));
        dispFatherRowArray.push(element.disposed_calls);
        dispFatherRowArray.push(parseFloat(element.calls_per_hour).toFixed(2));
        dispFatherRowArray.push(_this4.hours_export(element.talktime)); // inserta columna padre
        // if (key > 0) {

        csvBody.push(dispFatherRowArray); // }
      }); // añadir resultados url falsa

      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvBody.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvHeader[0] = [];
      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      }); // primera *****************************************
      // cabecera

      csvHeader = [[this.NameCampaigns.name + "- Scorecard " + this.first_fortnight()]];
      ["Sessions", "Target Time", "Session Time", "Adherence", "Dispo'ed Calls", "Calls per Hour", "Talktime"].forEach(function (index, key) {
        csvHeader[0].push(index);
      }); // cuerpo

      csvBody = [];
      name = "";
      Object.values(this.table_first).forEach(function (element, key) {
        // columna padre
        var dispFatherRowArray = [];
        dispFatherRowArray.push(element.name);
        dispFatherRowArray.push(element.session);
        dispFatherRowArray.push(element.target_time);
        dispFatherRowArray.push(_this4.hours_export(element.total_hours));
        dispFatherRowArray.push(_this4.adherence_session(element));
        dispFatherRowArray.push(element.disposed_calls);
        dispFatherRowArray.push(parseFloat(element.calls_per_hour).toFixed(2));
        dispFatherRowArray.push(_this4.hours_export(element.talktime)); // inserta columna padre
        // if (key > 0) {

        csvBody.push(dispFatherRowArray); // }
      }); // añadir resultados url falsa

      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvBody.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvHeader[0] = [];
      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      }); // segunda ******************************************
      // cabecera

      csvHeader = [[this.NameCampaigns.name + "- Scorecard " + this.second_fortnight()]];
      ["Sessions", "Target Time", "Session Time", "Adherence", "Dispo'ed Calls", "Calls per Hour", "Talktime"].forEach(function (index, key) {
        csvHeader[0].push(index);
      }); // cuerpo

      csvBody = [];
      name = "";
      Object.values(this.table_second).forEach(function (element, key) {
        // columna padre
        var dispFatherRowArray = [];
        dispFatherRowArray.push(element.name);
        dispFatherRowArray.push(element.session);
        dispFatherRowArray.push(element.target_time);
        dispFatherRowArray.push(_this4.hours_export(element.total_hours));
        dispFatherRowArray.push(_this4.adherence_session(element));
        dispFatherRowArray.push(element.disposed_calls);
        dispFatherRowArray.push(parseFloat(element.calls_per_hour).toFixed(2));
        dispFatherRowArray.push(_this4.hours_export(element.talktime)); // inserta columna padre
        // if (key > 0) {

        csvBody.push(dispFatherRowArray); // }
      }); // añadir resultados url falsa

      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvBody.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
      var nameCSV = "".concat(this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"].name, "-Scorecard-").concat(moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("YYYY-MM-DD"), ".csv"); // generar elemento del dom para poner el enlace

      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", nameCSV);
      document.body.appendChild(link); // Required for FF
      // darle click

      link.click();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R4/CallLog.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      loading: false,
      dense: false,
      fixedHeader: false,
      height: "33rem",
      table_first: [],
      options: {},
      page: 1,
      pageCount: 2,
      itemsPerPage: 20,
      total: 2
    };
  },
  filters: {
    parse_decimal: function parse_decimal(value) {
      return parseFloat(value).toFixed(2);
    },
    format_hours: function format_hours(seconds) {
      return moment__WEBPACK_IMPORTED_MODULE_2___default()("2015-01-01").startOf("day").seconds(parseInt(seconds)).format("H:mm:ss");
    }
  },
  watch: {
    options: {
      handler: function handler() {
        var _this = this;

        return _asyncToGenerator(
        /*#__PURE__*/
        _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
          var dates, payload;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.prev = 0;
                  _this.loading = true;
                  dates = _this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                  payload = {
                    start: dates.ini,
                    end: dates.end,
                    page: _this.page,
                    campaign_id: _this.$store.getters["campaigns/GET_CAMPAIGNS_ID"],
                    itemsPerPage: _this.itemsPerPage
                  };
                  _context.next = 6;
                  return _this.$store.dispatch("report_4/GET_INDEX_CALL_LOG", payload);

                case 6:
                  _this.loading = false;
                  _context.next = 13;
                  break;

                case 9:
                  _context.prev = 9;
                  _context.t0 = _context["catch"](0);
                  console.error(_context.t0);

                  _this.$snotify.error("Error al cargar listado.", "¡Vaya!");

                case 13:
                  _context.prev = 13;
                  _this.loading = false;
                  return _context.finish(13);

                case 16:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, null, [[0, 9, 13, 16]]);
        }))();
      },
      deep: true
    }
  },
  created: function created() {
    this.date_position(); // this.scorecard_second_fortnight();
    // this.scorecard_today();
  },
  mounted: function mounted() {
    this.campaign_id = this.$store.getters["campaigns/GET_CAMPAIGNS_ID"];
  },
  computed: {
    Itemscampaigns: function Itemscampaigns() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    },
    DateDashboard: function DateDashboard() {
      return this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
    },
    pagination: function pagination() {
      return this.$store.getters["report_4/PAGINATION"];
    },
    ItemsCallLog: function ItemsCallLog() {
      return this.$store.getters["report_4/GET_CALL_LOG"];
    },
    Itemsheaders: function Itemsheaders() {
      return this.$store.getters["report_4/GET_CALL_LOG_KEY"];
    }
  },
  methods: {
    current_day: function current_day() {
      var day_new = moment__WEBPACK_IMPORTED_MODULE_2___default()().subtract(1, "days").format("MMMM Do, YYYY");
      return day_new;
    },
    first_fortnight: function first_fortnight() {
      var dates = this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
      var day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("MMMM Do");
      var day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("Do, YYYY");
      return "".concat(day_1, " - ").concat(day_2);
    },
    second_fortnight: function second_fortnight() {
      var day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");
      var month = moment__WEBPACK_IMPORTED_MODULE_2___default()().subtract(1, "months").format("MM");
      var month2 = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("MM");
      var year = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("YYYY");
      var day_1;
      var day_2;

      if (day > 15) {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).format("MMMM Do");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month2, "-16")).endOf("month").format("Do, YYYY");
      } else {
        day_1 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).format("MMMM Do");
        day_2 = moment__WEBPACK_IMPORTED_MODULE_2___default()("".concat(year, "-").concat(month, "-16")).endOf("month").format("Do, YYYY");
      }

      return "".concat(day_1, " - ").concat(day_2);
    },
    date_position: function date_position() {
      var day = moment__WEBPACK_IMPORTED_MODULE_2___default()().format("DD");

      if (day > 15) {
        this.second_fortnight_section = true;
      } else {
        this.first_fortnight_section = false;
      }
    },
    export_csv: function export_csv() {
      var _this2 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var dates, payload;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.loading = true; // let export_type = ''
                // if (name == 'pdf') {
                //   this.loading_pdf = true
                //   export_type = 1
                // }else {
                //   this.loading_excel = true
                //   export_type = 2
                // }
                // if (this.dates[0] > this.dates[1] && this.menu == true)
                //   {
                //     let num_1 = this.dates[0]
                //     let num_2 = this.dates[1]
                //     this.dates[0] = num_2
                //     this.dates[1] = num_1
                //   }
                //   let ini = 0;
                //   let end = 0;
                //   if (this.dates.length > 0) {
                //     ini = this.dates[0];
                //     end = this.dates[1];
                //   }

                dates = _this2.$store.getters["campaigns/GET_DATE_DASHBOARD"];
                payload = {
                  start: moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.ini).format("MM/DD/YYYY"),
                  end: moment__WEBPACK_IMPORTED_MODULE_2___default()(dates.end).format("MM/DD/YYYY"),
                  campaign_id: _this2.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                axios({
                  method: "get",
                  url: "".concat("http://dev.hccreports.online/", "/call-log/download-csv?campaign_id=").concat(payload.campaign_id, "&start_date=").concat(payload.start, "&end_date=").concat(payload.end),
                  timeout: 600000,
                  responseType: "arraybuffer" // headers: {
                  //   Authorization: ` Bearer ${localStorage.getItem("token")}`
                  // },
                  // data: request

                }).then(function (response) {
                  _this2.loading = false;

                  _this2.forceFileDownload(response, payload);
                })["catch"](function (error) {
                  _this2.loading = false;
                  console.log("error occured", error);
                });

              case 4:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    forceFileDownload: function forceFileDownload(response, payload) {
      var url = window.URL.createObjectURL(new Blob([response.data]));
      var link = document.createElement("a");
      link.href = url;
      link.setAttribute("download", "Call_log_".concat(this.NameCampaigns.name, "_from_").concat(payload.start, "_to_").concat(payload.end, ".csv")); //or any other extension

      document.body.appendChild(link);
      link.click();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/upload/upload_call_log.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_2__);


function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      status_actual: '',
      options: {},
      dates: [],
      loadingValues: false,
      loading: false,
      showAlert: false,
      comparison: [],
      loadingModal: false,
      campaign_id: "",
      campaign_name: "",
      files: [],
      dense: false,
      fixedHeader: false,
      height: 300,
      page: 1,
      pageCount: 0,
      itemsPerPage: 10,
      headers: [{
        text: "Date range of calls",
        value: "range_timestamp",
        "class": "tr-color tam-header",
        sortable: false
      }, {
        text: "Number of calls",
        value: "count_rows",
        "class": "tr-color tam-header",
        sortable: false
      }, {
        text: "Number of agents",
        value: "count_agents",
        "class": "tr-color tam-header",
        sortable: false
      }, {
        text: "Upload date stamp",
        value: "created_at",
        "class": "tr-color tam-header",
        sortable: false
      }, {
        text: "Uploaded by",
        value: "uploaded_by.name",
        "class": "tr-color tam-header",
        sortable: false
      }]
    };
  },
  filters: {
    hours_central: function hours_central(date) {
      var moment = __webpack_require__(/*! moment-timezone */ "./node_modules/moment-timezone/index.js");

      return moment.utc(date).tz("America/Mexico_City").format("YYYY-MM-DD HH:mm:ss");
    }
  },
  watch: {
    options: {
      handler: function handler() {
        var _this = this;

        return _asyncToGenerator(
        /*#__PURE__*/
        _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
          var payload, response;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.prev = 0;
                  _this.loading = true;
                  console.log("qwertyuiqwert");
                  payload = {
                    page: _this.page,
                    itemsPerPage: _this.itemsPerPage,
                    campaign_id: _this.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                  };
                  console.log('payload', payload);
                  _context.next = 7;
                  return _this.$store.dispatch("campaigns/DAILY_INFORMATION", payload);

                case 7:
                  response = _context.sent;
                  _this.loading = false;
                  _context.next = 15;
                  break;

                case 11:
                  _context.prev = 11;
                  _context.t0 = _context["catch"](0);
                  console.error(_context.t0);

                  _this.$snotify.error("Error al cargar listado.", "¡Vaya!");

                case 15:
                  _context.prev = 15;
                  _this.loading = false;
                  return _context.finish(15);

                case 18:
                case "end":
                  return _context.stop();
              }
            }
          }, _callee, null, [[0, 11, 15, 18]]);
        }))();
      },
      deep: true
    }
  },
  created: function created() {
    var _this2 = this;

    this.GetCampaign();
    this.GetCallLog();
    setInterval(function () {
      _this2.hours_actual();
    }, 1000);
  },
  mounted: function mounted() {
    this.campaign_id = this.$store.getters["campaigns/GET_CAMPAIGNS_ID"];
  },
  computed: {
    Itemscampaigns: function Itemscampaigns() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    },
    NameCampaigns: function NameCampaigns() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    },
    DateDashboard: function DateDashboard() {
      return this.$store.getters["campaigns/GET_DATE_DASHBOARD"];
    },
    ItemsAgents: function ItemsAgents() {
      var parse = this.$store.getters["report_1/GET_AGENTS_GENERAL"].filter(function (value) {
        value.hcc == 0 ? value.hcc = false : value.hcc = true;
        value.active == 0 ? value.active = false : value.active = true;
        return value;
      });
      this.originalAgents = _toConsumableArray(parse);
      return parse;
    },
    ItemsDailyInformation: function ItemsDailyInformation() {
      return this.$store.getters["campaigns/GET_DAILY_INFORMATION"];
    },
    pagination: function pagination() {
      return this.$store.getters["campaigns/PAGINATION"];
    }
  },
  methods: {
    hours_actual: function hours_actual() {
      var moment = __webpack_require__(/*! moment-timezone */ "./node_modules/moment-timezone/index.js");

      this.status_actual = moment.utc().tz("America/Mexico_City").format("YYYY-MM-DD HH:mm:ss");
    },
    jsexport: function jsexport() {
      this.loading = true; // URL Falsa para descargar

      var csvContent = "data:text/csv;charset=utf-8,"; // formatos

      var delimiter = ",";
      var fixedDecimal = 2; // cabecera

      var csvHeader = [[this.NameCampaigns.name + "-  Upload Call Log "]];
      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvHeader[0] = [];
      this.headers.forEach(function (index, key) {
        csvHeader[0].push(index.text);
      }); // cuerpo

      var csvBody = [];
      var name = "";
      this.ItemsDailyInformation.forEach(function (element, key) {
        // columna padre
        var dispFatherRowArray = [];
        dispFatherRowArray.push(element.created_at);
        dispFatherRowArray.push(element.count_rows);
        dispFatherRowArray.push(element.id);
        dispFatherRowArray.push(element.created_at);
        dispFatherRowArray.push(element.uploaded_by.name); // inserta columna padre
        // if (key > 0) {

        csvBody.push(dispFatherRowArray); // }
      }); // añadir resultados url falsa

      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvBody.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      this.loading = false;
      var nameCSV = "".concat(this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"].name, "-Upload-Call-Log-").concat(moment__WEBPACK_IMPORTED_MODULE_2___default()().format("YYYY-MM-DD"), ".csv"); // generar elemento del dom para poner el enlace

      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", nameCSV);
      document.body.appendChild(link); // Required for FF
      // darle click

      link.click();
    },
    GetCampaign: function GetCampaign() {
      var _this3 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _this3.$store.dispatch("campaigns/INDEX");

              case 3:
                response = _context2.sent;
                _context2.next = 9;
                break;

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }))();
    },
    GetCallLog: function GetCallLog() {
      var _this4 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var payload, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                payload = {
                  page: _this4.page,
                  itemsPerPage: _this4.itemsPerPage,
                  campaign_id: _this4.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                };
                console.log('payload 2', payload);
                _context3.next = 5;
                return _this4.$store.dispatch("campaigns/DAILY_INFORMATION", payload);

              case 5:
                response = _context3.sent;
                _context3.next = 11;
                break;

              case 8:
                _context3.prev = 8;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 11:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 8]]);
      }))();
    },
    fileDoc: function fileDoc() {
      var _this5 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var idC;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                try {
                  idC = localStorage.getItem("campaign_id");

                  if (_this5.files.length === 0) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                      icon: "warning",
                      title: "Please select a file",
                      text: "",
                      confirmButtonColor: "#25A8E0"
                    });
                  } else if (idC == null) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                      icon: "warning",
                      title: "Please select a campaign",
                      text: "",
                      confirmButtonColor: "#25A8E0"
                    });
                  } else {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                      title: "Are you sure you want to do this?",
                      text: "This action cannot be undone",
                      icon: "warning",
                      showCancelButton: true,
                      confirmButtonColor: "#4CAF50",
                      cancelButtonColor: "#E63E59",
                      confirmButtonText: "Agree"
                    }).then(
                    /*#__PURE__*/
                    function () {
                      var _ref = _asyncToGenerator(
                      /*#__PURE__*/
                      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(result) {
                        var id, formData;
                        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
                          while (1) {
                            switch (_context4.prev = _context4.next) {
                              case 0:
                                if (result.value) {
                                  _this5.loadingModal = true;
                                  id = localStorage.getItem("campaign_id");
                                  formData = new FormData();
                                  formData.append("csv", _this5.files);
                                  formData.append("campaign_id", id);

                                  _this5.$store.dispatch("report_1/ADD_FILE", formData).then(function (response) {
                                    var id = localStorage.getItem("campaign_id");
                                    _this5.campaign_id = id;
                                    _this5.files = [];
                                    _this5.loadingModal = false;

                                    _this5.GetCallLog();

                                    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                                      icon: "success",
                                      title: "File uploaded successfully",
                                      text: "",
                                      confirmButtonColor: "#25A8E0"
                                    });
                                  })["catch"](function (e) {
                                    _this5.loadingModal = false;
                                    console.log("error", e.response);
                                    var message = e.response.data.message;
                                    sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                                      icon: "error",
                                      title: "Error!",
                                      html: message,
                                      customClass: {
                                        content: "text-xs-left px-3"
                                      },
                                      confirmButtonText: "Ok",
                                      confirmButtonColor: "#25A8E0"
                                    });
                                  });
                                }

                              case 1:
                              case "end":
                                return _context4.stop();
                            }
                          }
                        }, _callee4);
                      }));

                      return function (_x) {
                        return _ref.apply(this, arguments);
                      };
                    }());
                  }
                } catch (e) {
                  _this5.loadingModal = false;
                  sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: "error",
                    title: "Error!",
                    html: e.response.data.message,
                    type: "error",
                    customClass: {
                      content: "text-xs-left px-3"
                    },
                    confirmButtonText: "Ok",
                    confirmButtonColor: "#25A8E0"
                  });
                }

              case 1:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/users/Index.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_1__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      selectedRow: null,
      userId: parseInt(localStorage.getItem("id")),
      edit: false,
      position_array: "",
      id_user: 0,
      itemsPerPage: 10,
      dialog: false,
      headers: [{
        text: "Name",
        align: "left",
        sortable: false,
        value: "name",
        "class": "length_text head-color"
      }, {
        text: "Email",
        value: "email",
        "class": "length_text head-color"
      }, {
        text: "Role",
        value: "roles[0].name",
        "class": "length_text head-color"
      }, {
        text: "Edit / Delete",
        value: "action",
        sortable: false,
        "class": "length_text head-color"
      }],
      desserts: [],
      editedIndex: -1,
      password: "",
      confirm_password: "",
      editedItem: {
        name: "",
        email: "",
        rol: "",
        campaign_id: []
      },
      defaultItem: {
        name: "",
        email: "",
        rol: "",
        campaign_id: []
      }
    };
  },
  filters: {
    permission_name: function permission_name(name) {
      switch (name) {
        case 'front.upload.index':
          return 'Upload Call Log';
          break;

        case 'front.agents.index':
          return 'Agents';
          break;

        case 'front.dispositions.index':
          return 'Dispositions';
          break;

        case 'front.agents_hours.index':
          return 'Agent Hours';
          break;

        case 'front.agents_hours_custom.index':
          return 'Agent Hours - Custom';
          break;

        case 'front.daily_activity.index':
          return 'Daily Activity';
          break;

        case 'front.scorecard.index':
          return 'Scorecard';
          break;

        case 'front.call_log.index':
          return 'Call Log';
          break;
      }
    }
  },
  computed: {
    formTitle: function formTitle() {
      return this.edit === false ? "New User" : "Edit User";
    },
    ItemsRol: function ItemsRol() {
      return this.$store.getters["users/GET_ROL"];
    },
    ItemsPermission: function ItemsPermission() {
      return this.$store.getters["users/GET_PERMISSION"];
    },
    ItemsUsers: function ItemsUsers() {
      return this.$store.getters["users/GET_USERS"];
    },
    Itemscampaigns: function Itemscampaigns() {
      return this.$store.getters["campaigns/GET_CAMPAIGNS"];
    }
  },
  watch: {
    dialog: function dialog(val) {
      val || this.close();
    }
  },
  created: function created() {
    this.initialize();
    this.role();
    this.permission();
  },
  methods: {
    rowClick: function rowClick(item, row) {
      if (row.isSelected == true) {
        row.select(false);
      } else {
        row.select(true);
        this.editItem(item);
      }
    },
    initialize: function initialize() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _this.$store.dispatch("users/INDEX_USER");

              case 3:
                response = _context.sent;
                console.log(response.data, "PAGINADOS");
                _context.next = 10;
                break;

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 7]]);
      }))();
    },
    editItem: function editItem(item) {
      console.log(item, "editar"); // let data = state.users.indexOf(payload)

      this.position_array = this.ItemsUsers.indexOf(item);
      console.log(this.position_array, "updated position");
      this.edit = true;
      this.id_user = item.id; // this.editedIndex = this.desserts.indexOf(item)
      // this.editedItem = Object.assign({}, item)

      this.editedItem.name = item.name;
      this.editedItem.email = item.email;
      this.editedItem.rol = item.roles[0].id;
      this.editedItem.permission_id = item.permissions.map(function (index, elem) {
        return index.id;
      });
      console.log("this.editedItem.rol");
      this.editedItem.campaign_id = item.campaigns.map(function (index, elem) {
        return index.id;
      });
      this.dialog = true;
    },
    deleteItem: function deleteItem(item) {
      var _this2 = this;

      // this.edit = false;
      console.log('this.edit ', this.edit);
      sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#4CAF50",
        cancelButtonColor: "#E63E59",
        confirmButtonText: "Yes, delete it!"
      }).then(
      /*#__PURE__*/
      function () {
        var _ref = _asyncToGenerator(
        /*#__PURE__*/
        _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2(result) {
          var response;
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
            while (1) {
              switch (_context2.prev = _context2.next) {
                case 0:
                  if (!result.value) {
                    _context2.next = 12;
                    break;
                  }

                  _context2.prev = 1;
                  _context2.next = 4;
                  return _this2.$store.dispatch("users/DELETED", item);

                case 4:
                  response = _context2.sent;
                  console.log(response);
                  sweetalert2__WEBPACK_IMPORTED_MODULE_1___default.a.fire({
                    icon: "success",
                    title: "Deleted!",
                    text: "Your user has been deleted.",
                    type: "success",
                    confirmButtonColor: "#25A8E0"
                  });
                  _context2.next = 12;
                  break;

                case 9:
                  _context2.prev = 9;
                  _context2.t0 = _context2["catch"](1);
                  console.log(_context2.t0.response);

                case 12:
                case "end":
                  return _context2.stop();
              }
            }
          }, _callee2, null, [[1, 9]]);
        }));

        return function (_x) {
          return _ref.apply(this, arguments);
        };
      }());
    },
    close: function close() {
      var _this3 = this;

      this.dialog = false;
      setTimeout(function () {
        _this3.editedItem = Object.assign({}, _this3.defaultItem);
        _this3.editedIndex = -1;
        _this3.edit = false;
      }, 300);
    },
    save: function save() {
      var _this4 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var request, response, _request, _response;

        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!(_this4.edit === true)) {
                  _context3.next = 15;
                  break;
                }

                //   Object.assign(this.desserts[this.editedIndex], this.editedItem)
                request = {
                  id: _this4.id_user,
                  name: _this4.editedItem.name,
                  email: _this4.editedItem.email,
                  rol: _this4.editedItem.rol,
                  campaigns: _this4.editedItem.campaign_id,
                  permissions: _this4.editedItem.permission_id,
                  position_array: _this4.position_array
                };
                console.log(request, "editar");
                _context3.prev = 3;
                _context3.next = 6;
                return _this4.$store.dispatch("users/UPDATED_USER", request);

              case 6:
                response = _context3.sent;
                console.log(response);
                _context3.next = 13;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](3);
                console.log(_context3.t0);

              case 13:
                _context3.next = 27;
                break;

              case 15:
                // guardar
                //   this.desserts.push(this.editedItem)
                _request = {
                  password: _this4.password,
                  password_confirmation: _this4.confirm_password,
                  name: _this4.editedItem.name,
                  email: _this4.editedItem.email,
                  roles: [_this4.editedItem.rol],
                  campaigns: _this4.editedItem.campaign_id,
                  permissions: _this4.editedItem.permission_id
                };
                console.log(_request, "guardar");
                _context3.prev = 17;
                _context3.next = 20;
                return _this4.$store.dispatch("users/REGISTER_USER", _request);

              case 20:
                _response = _context3.sent;
                console.log(_response, "registro con exito");
                _context3.next = 27;
                break;

              case 24:
                _context3.prev = 24;
                _context3.t1 = _context3["catch"](17);
                console.log(_context3.t1);

              case 27:
                _this4.close();

              case 28:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[3, 10], [17, 24]]);
      }))();
    },
    role: function role() {
      var _this5 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _this5.$store.dispatch("users/ROL_USER");

              case 3:
                response = _context4.sent;
                _context4.next = 9;
                break;

              case 6:
                _context4.prev = 6;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 9:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 6]]);
      }))();
    },
    permission: function permission() {
      var _this6 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _this6.$store.dispatch("users/PERMISSION_USER");

              case 3:
                response = _context5.sent;
                _context5.next = 9;
                break;

              case 6:
                _context5.prev = 6;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 9:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 6]]);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.js?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./resources/js/components/reports/R2/R2TableReport/R2TableReport.js?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ "./node_modules/sweetalert2/dist/sweetalert2.all.js");
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _tests___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./__tests__ */ "./resources/js/components/reports/R2/R2TableReport/__tests__/index.js");
/* harmony import */ var _mixins__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./mixins */ "./resources/js/components/reports/R2/R2TableReport/mixins/index.js");


function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }





/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'R2TableReport',
  mixins: [_mixins__WEBPACK_IMPORTED_MODULE_4__["tableBodyMixin"], _mixins__WEBPACK_IMPORTED_MODULE_4__["tableFilterMixin"]],
  data: function data() {
    return {
      // meses
      currentSelectMonth: moment__WEBPACK_IMPORTED_MODULE_1___default()().month(),
      currentMonth: moment__WEBPACK_IMPORTED_MODULE_1___default()().month(),
      // no mutar
      selectedMonth: moment__WEBPACK_IMPORTED_MODULE_1___default()().month(),
      dayOfMonth: "",
      showDays: false,
      days: [],
      length: 3,
      onboarding: 0,
      filters: {},
      // evitar la acumulacion de ciclos de actualizacion DOM
      loading: false
    };
  },
  computed: {
    /**
     * [getLastMonths description]
     * @return {[type]} [description]
     */
    getLastMonths: function getLastMonths() {
      var months = moment__WEBPACK_IMPORTED_MODULE_1___default.a.monthsShort(); // cantidad de elementos añadir

      var quantityMonth = 3;
      var lastMonths = [];

      if (this.selectedMonth < 2) {
        quantityMonth = this.selectedMonth + 1;
      }

      for (var index = 0; index < quantityMonth; index++) {
        lastMonths.unshift(months[this.selectedMonth - index]);
      } // let lastMonths = [
      //   months[this.currentMonth - 2],
      //   months[this.currentMonth - 1],
      //   months[this.currentMonth]
      // ];


      return lastMonths;
    },

    /**
     * [getAgents description]
     * @return {[type]} [description]
     */
    getAgents: function getAgents() {
      return this.$store.getters["report_1/GET_AGENTS"];
    },
    getCampaign: function getCampaign() {
      return this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"];
    }
  },
  created: function created() {
    var _this = this;

    return _asyncToGenerator(
    /*#__PURE__*/
    _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _this.getDays(_this.currentMonth);

              _context.next = 3;
              return _this.getDisposition();

            case 3:
              _context.next = 5;
              return _this.getFormulasMonth();

            case 5:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  methods: {
    /**
    		* [jsexport description]
    		* Arreglo de arreglos
    		* cada columna del csv debe es un arreglo de valores primitivos
    		* nota: el csv es una arreglo de arreglos
    		*
    		* referencia:
    		* https://stackoverflow.com/questions/14964035/how-to-export-javascript-array-info-to-csv-on-client-side
    		* @return {[type]} [description]
    	*/
    jsexport: function jsexport() {
      // URL Falsa para descargar
      var csvContent = "data:text/csv;charset=utf-8,"; // formatos 

      var delimiter = ',';
      var fixedDecimal = 2;
      var dayFormat = 'day'; // cabecera

      var csvHeader = [["Name", "YTD"]];
      this.getLastMonths.forEach(function (month) {
        csvHeader[0].push(month);
      });
      this.days.forEach(function (day) {
        csvHeader[0].push(day[dayFormat]);
      }); // cuerpo

      var csvBody = [];
      this.tableBodyTest.forEach(function (dispFather) {
        // columna padre
        var dispFatherRowArray = [];
        dispFatherRowArray.push(dispFather.fatherName);

        for (var nameFormula in dispFather.formulas) {
          if (typeof dispFather.formulas[nameFormula] === 'number') {
            dispFatherRowArray.push(dispFather.formulas[nameFormula].toFixed(fixedDecimal));
          } else {
            dispFatherRowArray.push(dispFather.formulas[nameFormula]);
          }
        } // inserta columna padre


        csvBody.push(dispFatherRowArray); // guardar hijos

        var _iteratorNormalCompletion = true;
        var _didIteratorError = false;
        var _iteratorError = undefined;

        try {
          for (var _iterator = dispFather.children[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var dispSon = _step.value;
            // columna hijo
            var dispSonRowArray = [];
            dispSonRowArray.push(dispSon.name);

            for (var nameSonFormula in dispSon.formulas) {
              if (typeof dispSon.formulas[nameSonFormula] === 'number') {
                dispSonRowArray.push(dispSon.formulas[nameSonFormula].toFixed(fixedDecimal));
              } else {
                dispSonRowArray.push(dispSon.formulas[nameSonFormula]);
              }
            } // inserta columna hijo


            csvBody.push(dispSonRowArray);
          }
        } catch (err) {
          _didIteratorError = true;
          _iteratorError = err;
        } finally {
          try {
            if (!_iteratorNormalCompletion && _iterator["return"] != null) {
              _iterator["return"]();
            }
          } finally {
            if (_didIteratorError) {
              throw _iteratorError;
            }
          }
        }
      }); // añadir resultados url falsa

      csvHeader.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      csvBody.forEach(function (rowArray) {
        var row = rowArray.join(delimiter);
        csvContent += row + "\r\n";
      });
      var nameCSV = "".concat(this.$store.getters["campaigns/GET_SELECTED_CAMPAIGN"].name, "-Daily-Activity-").concat(moment__WEBPACK_IMPORTED_MODULE_1___default()().format('YYYY-MM-DD'), ".csv"); // generar elemento del dom para poner el enlace

      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", nameCSV);
      document.body.appendChild(link); // Required for FF
      // darle click 

      link.click();
    },

    /**
     * [rotateCondition description]
     * dada una direccion, indice actual, longitud del arreglo
     * determinar si el elemento es el ultimo o primero, en que direccion
     * añadir boton
     *
     * @param  {string} direction 'left' | 'right' [description]
     * @param  {number} index     [description]
     * @param  {number} length    [description]
     * @return {boolean}           [description]
     */
    rotateCondition: function rotateCondition(direction, index, length) {
      if (index === length - 1 && direction === 'left') {
        return true;
      }

      if (index === 0 && direction === 'right') {
        return true;
      }

      return false;
    },

    /**
     * [rotateMonths description]
     * @param  {number} rotate [1: incrementar , -1: dismincuir] 
     * 
     */
    rotateMonths: function rotateMonths(rotate) {
      var _this2 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;

                if (!(_this2.selectedMonth === 2 && rotate !== 1)) {
                  _context2.next = 4;
                  break;
                }

                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  icon: "warning",
                  title: "Lower limit reached",
                  confirmButtonColor: "#25A8E0"
                });
                return _context2.abrupt("return");

              case 4:
                if (!(_this2.selectedMonth === moment__WEBPACK_IMPORTED_MODULE_1___default()().month() && rotate !== -1)) {
                  _context2.next = 7;
                  break;
                }

                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default.a.fire({
                  icon: "warning",
                  title: "Upper limit reached",
                  confirmButtonColor: "#25A8E0"
                });
                return _context2.abrupt("return");

              case 7:
                // rotar meses
                _this2.selectedMonth = _this2.selectedMonth + rotate;

                _this2.getFormulasMonth();

                _context2.next = 14;
                break;

              case 11:
                _context2.prev = 11;
                _context2.t0 = _context2["catch"](0);
                console.log(_context2.t0);

              case 14:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 11]]);
      }))();
    },

    /**
     * [getDays description]
     * retornar una lista con todos los dias de un determinado mes
     * interface R2ColumnTableDay {
     * 		day: string;
     * 		day_resp: string;
     * 		day_name: string;
     * }
     * @param  {number} month [description]
     */
    getDays: function getDays(month) {
      this.showDays = false;
      this.currentMonth = month;
      var dayOfMonth = moment__WEBPACK_IMPORTED_MODULE_1___default()().month(month).daysInMonth();
      this.dayOfMonth = dayOfMonth;
      var currentDay = moment__WEBPACK_IMPORTED_MODULE_1___default()().month(month).format("MM/DD/YYYY");
      var arrayOfDays = [];

      for (var index = 0; index <= dayOfMonth; index++) {
        ;
        arrayOfDays[index] = {
          num: index + 1,
          day: moment__WEBPACK_IMPORTED_MODULE_1___default()().month(month).startOf('month').add(index - 1, 'days').format('MM/DD'),
          day_resp: moment__WEBPACK_IMPORTED_MODULE_1___default()().month(month).startOf('month').add(index - 1, 'days').format('DD/MM/YYYY'),
          day_name: moment__WEBPACK_IMPORTED_MODULE_1___default()().year(2020).month(month).date(index).format("ddd")
        }; // arrayOfDays.push(`${moment.months().indexOf(month)+1}/${index}/${moment().year()}`);
      }

      this.days = arrayOfDays;
      var value = this.days.shift();
      return arrayOfDays;
    },

    /**
     * [getValues description]
     * @return {[type]} [description]
     */
    getValues: function getValues() {
      var _this3 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var request, response, csvData;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                request = {
                  date1: moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", _this3.getLastMonths[0]).format("MM/DD/YYYY"),
                  date2: moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", _this3.getLastMonths[2]).format("MM/DD/YYYY"),
                  // campaign_id: this.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                  campaign_id: 1
                };
                _context3.next = 4;
                return _this3.$store.dispatch("report_1/GET_FILE", request);

              case 4:
                response = _context3.sent;
                csvData = response.data;
                _context3.next = 11;
                break;

              case 8:
                _context3.prev = 8;
                _context3.t0 = _context3["catch"](0);
                console.log(_context3.t0);

              case 11:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 8]]);
      }))();
    },
    getFilters: function getFilters() {
      var _this4 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _this4.$store.dispatch("report_2/GET_FILTERS");

              case 3:
                response = _context4.sent;
                _this4.filters = response.data.data;
                _context4.next = 10;
                break;

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                console.log(_context4.t0);

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 7]]);
      }))();
    },

    /**
     * [pruebav2 description]
     *
     * Añade en el prototipo del arreglo tableBodyTest una propiedad
     * con nombre: formulas
     *
     * formulas en objecto clave - valor
     * {
     * 	  yt2: 0
     * 	  mes_name: 0,
     * 	  otros meses...
     * 	  mm_dd_yy: 0
     * 	  otros dias...
     * }
     *
     * Motivos:
     * Cada dispostion posee una formula y un resultado en la tabla
     * sea padre o hijo, entonces añadirle una propiedad con el resultado
     * preciso para cada caso posible y asignar valores por defecto en
     * caso de que eso valores no existan, permite encerrar toda la generacion
     * del cuerpo de la tabla en una o varias funciones
     * (tortugas hasta el fondo)
     *
     * Disculpas: Por la poca depuracion, y el consumo excesivo de memoria
     *
     */
    pruebav2: function pruebav2() {
      var _this5 = this;

      var daysResponse = Object.keys(this.filters); // **** convertir en estructura mas facil de mapear ****
      // arreglo de objetos
      // { date: 'dd/mm/yy', report: 'respuest api' }

      var arrayResponse = Object.entries(this.filters).map(function (values) {
        var ob = {};
        ob.date = values[0];
        ob.report = values[1];
        return ob;
      }); // console.log(arrayResponse)
      // **** Json Formateado Filters ****
      // evitar mutar directamente la tabla renderizada
      // Recordar asignar valores de clave en propiedad formulas
      // que se crean en este clon, con el orden que se quiere en la tabla
      // para evitar tener claves en el objeto desordenadas

      var cloneArr = _toConsumableArray(this.tableBodyTest); // QUE SIGNIFICA YTD ?
      // SUMATORIA DE LOS 3 MESES QUE APARECEN EN LA TABLA
      // siempre se agrega como primera clave en formulas
      // **** NO USAR toFixed, puede alterar y truncar los resultados ****
      // en su lugar utilizar los filtros
      // iterar padre disposition


      var key_dispSon = 0;
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        var _loop = function _loop() {
          var dispFather = _step2.value;

          // revisar que el nombre del padre este correcto
          var cleanFatherName = _this5.cleanDispositionName(dispFather.fatherName); // iteracion añadir formulas a los hijos


          var _iteratorNormalCompletion4 = true;
          var _didIteratorError4 = false;
          var _iteratorError4 = undefined;

          try {
            var _loop2 = function _loop2() {
              var dispSon = _step4.value;
              // agregar propiedad formulas
              dispSon.formulas = {};
              dispSon.formulas.ytd = 0; // PARSEAR NOMBRES DEL DISPOSITION

              var cleanName = _this5.cleanDispositionName(dispSon.name); // agregar resultados meses (nombre de propiedades son los meses en string)
              // para asignar puede volver a iterarse de esta forma 
              // se debe llamar este antes que los dias para evitar que esten despues en las 
              // claves de diccionario (objeto)


              var _iteratorNormalCompletion8 = true;
              var _didIteratorError8 = false;
              var _iteratorError8 = undefined;

              try {
                for (var _iterator8 = _this5.getLastMonths[Symbol.iterator](), _step8; !(_iteratorNormalCompletion8 = (_step8 = _iterator8.next()).done); _iteratorNormalCompletion8 = true) {
                  var month = _step8.value;
                  dispSon.formulas[month] = 0; // function
                } // days properties (nombre de propiedade son los valores dias en propiedad day)

              } catch (err) {
                _didIteratorError8 = true;
                _iteratorError8 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion8 && _iterator8["return"] != null) {
                    _iterator8["return"]();
                  }
                } finally {
                  if (_didIteratorError8) {
                    throw _iteratorError8;
                  }
                }
              }

              var _iteratorNormalCompletion9 = true;
              var _didIteratorError9 = false;
              var _iteratorError9 = undefined;

              try {
                for (var _iterator9 = _this5.days[Symbol.iterator](), _step9; !(_iteratorNormalCompletion9 = (_step9 = _iterator9.next()).done); _iteratorNormalCompletion9 = true) {
                  var dayob = _step9.value;
                  // console.log('d', dayob)
                  // por defecto colocar 0 en el resultado de la formula
                  dispSon.formulas[dayob.day] = 0;

                  if (daysResponse.includes(dayob.day_resp)) {
                    // undefined (los nombre pueden no ser los mismos)
                    // se deben eliminar espacios en blanco y colocar piso
                    // colocar todas en miniscula
                    var valueFormula = _this5.filters[dayob.day_resp][cleanName];

                    if (valueFormula !== undefined) {
                      dispSon.formulas[dayob.day] = valueFormula;
                    }
                  }
                } // ejemplo de volver a iterar los meses para asignar valor

              } catch (err) {
                _didIteratorError9 = true;
                _iteratorError9 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion9 && _iterator9["return"] != null) {
                    _iterator9["return"]();
                  }
                } finally {
                  if (_didIteratorError9) {
                    throw _iteratorError9;
                  }
                }
              }

              var _iteratorNormalCompletion10 = true;
              var _didIteratorError10 = false;
              var _iteratorError10 = undefined;

              try {
                var _loop3 = function _loop3() {
                  var month = _step10.value;
                  var monthstr = moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", month).format("MM"); // const daysOfMonth = moment().month(month).daysInMonth();
                  // dispSon.formulas[month] = 10;

                  dispSon.formulas[month] = arrayResponse.map(function (dayReport) {
                    var datesplit = dayReport.date.split('/'); // ej = [d, m, y] => [30, 03, 2020]

                    if (datesplit[1] === monthstr) {
                      if (dayReport.report[cleanName] !== undefined) {
                        return dayReport.report[cleanName];
                      } else {
                        return 0;
                      }
                    } else {
                      return 0;
                    }
                  }).reduce(function (a, b) {
                    return a + b;
                  }, 0);

                  if (cleanFatherName === 'total_hours') {
                    console.log('hijo', cleanName, 'padre', cleanFatherName); // dispSon.formulas[month] = dispSon.formulas[month] / daysOfMonth;

                    console.log('+++++++ moment', daysResponse);
                    var count_day = daysResponse.reduce(function (obj, current) {
                      var _current$split = current.split("/"),
                          _current$split2 = _slicedToArray(_current$split, 2),
                          month = _current$split2[1];

                      obj[month] = obj[month] ? obj[month] + 1 : 1;
                      return obj;
                    }, {});
                    var daysInMonth = 1; // moment().month(month).daysInMonth()

                    for (var mes in count_day) {
                      // statement
                      var month_now = moment__WEBPACK_IMPORTED_MODULE_1___default()().month(month).format('MM');
                      console.log('messssssss', mes);
                      console.log('monthhhhhhh', month_now);

                      if (mes == month_now) {
                        daysInMonth = count_day[mes];
                      }

                      console.log('+++++++ count_day', daysInMonth);
                    }

                    dispSon.formulas[month] = dispSon.formulas[month] / daysInMonth; // dispSon.formulas[month] = `${daysInMonth}` ;
                  } // agregar resultado en ytd (sumatoria de meses)


                  dispSon.formulas.ytd = dispSon.formulas.ytd + dispSon.formulas[month];
                };

                for (var _iterator10 = _this5.getLastMonths[Symbol.iterator](), _step10; !(_iteratorNormalCompletion10 = (_step10 = _iterator10.next()).done); _iteratorNormalCompletion10 = true) {
                  _loop3();
                }
              } catch (err) {
                _didIteratorError10 = true;
                _iteratorError10 = err;
              } finally {
                try {
                  if (!_iteratorNormalCompletion10 && _iterator10["return"] != null) {
                    _iterator10["return"]();
                  }
                } finally {
                  if (_didIteratorError10) {
                    throw _iteratorError10;
                  }
                }
              }

              key_dispSon++;

              if (key_dispSon > 0 && key_dispSon < 4) {
                dispSon.formulas.ytd = dispSon.formulas.ytd / 3;
              } // dispSon.formulas.ytd = key_dispSon;

            };

            for (var _iterator4 = dispFather.children[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
              _loop2();
            } // NOTA EN ESTE CASO SE ITERAN PRIMERO LOS HIJOS
            // POR LOS SIGUIENTES MOTIVOS:
            // - que se hace con los padres sin formula? ()
            // - el resultado de los padres es la suma de sus hijos? ()
            // agregar propiedad formulas

          } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion4 && _iterator4["return"] != null) {
                _iterator4["return"]();
              }
            } finally {
              if (_didIteratorError4) {
                throw _iteratorError4;
              }
            }
          }

          dispFather.formulas = {};
          dispFather.formulas.ytd = 0; // agregar resultados meses (nombre de propiedades son los meses en string)
          // para asignar puede volver a iterarse de esta forma 
          // se debe llamar este antes que los dias para evitar que esten despues en las 
          // claves de diccionario (objeto)

          var _iteratorNormalCompletion5 = true;
          var _didIteratorError5 = false;
          var _iteratorError5 = undefined;

          try {
            for (var _iterator5 = _this5.getLastMonths[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
              var month = _step5.value;
              dispFather.formulas[month] = 0; // function
            }
          } catch (err) {
            _didIteratorError5 = true;
            _iteratorError5 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion5 && _iterator5["return"] != null) {
                _iterator5["return"]();
              }
            } finally {
              if (_didIteratorError5) {
                throw _iteratorError5;
              }
            }
          }

          var _iteratorNormalCompletion6 = true;
          var _didIteratorError6 = false;
          var _iteratorError6 = undefined;

          try {
            for (var _iterator6 = _this5.days[Symbol.iterator](), _step6; !(_iteratorNormalCompletion6 = (_step6 = _iterator6.next()).done); _iteratorNormalCompletion6 = true) {
              var dayob = _step6.value;
              // console.log('d', dayob)
              dispFather.formulas[dayob.day] = 0; // function

              if (daysResponse.includes(dayob.day_resp)) {
                // console.log('formula', this.filters[dayob.day_resp][dispFather.fatherName]); // no coinide
                var valuef = _this5.filters[dayob.day_resp][cleanFatherName]; // asignar el valor

                if (valuef !== undefined) {
                  dispFather.formulas[dayob.day] = valuef;
                }
              }
            } // ejemplo de volver a iterar los meses para asignar valor

          } catch (err) {
            _didIteratorError6 = true;
            _iteratorError6 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion6 && _iterator6["return"] != null) {
                _iterator6["return"]();
              }
            } finally {
              if (_didIteratorError6) {
                throw _iteratorError6;
              }
            }
          }

          var _iteratorNormalCompletion7 = true;
          var _didIteratorError7 = false;
          var _iteratorError7 = undefined;

          try {
            var _loop4 = function _loop4() {
              var month = _step7.value;
              // console.log('m', month);
              var monthstr = moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", month).format("MM"); // console.log(monthstr);
              // const daysOfMonth = moment().month(month).daysInMonth();
              // console.log(dayOfMonth);
              // dispFather.formulas[month] = 10;

              dispFather.formulas[month] = arrayResponse.map(function (dayReport) {
                var datesplit = dayReport.date.split('/'); // ej = [d, m, y] => [30, 03, 2020]

                if (datesplit[1] === monthstr) {
                  if (dayReport.report[cleanFatherName] !== undefined) {
                    return dayReport.report[cleanFatherName];
                  } else {
                    return 0;
                  }
                } else {
                  return 0;
                }
              }).reduce(function (a, b) {
                return a + b;
              }, 0); // dispFather.formulas[month] = dispFather.formulas[month] / daysOfMonth;
              // dispFather.formulas[month] = dispFather.formulas[month] / moment().month(month).daysInMonth();
              // agregar resultado en ytd (sumatoria de meses)

              dispFather.formulas.ytd = dispFather.formulas.ytd + dispFather.formulas[month];
            };

            for (var _iterator7 = _this5.getLastMonths[Symbol.iterator](), _step7; !(_iteratorNormalCompletion7 = (_step7 = _iterator7.next()).done); _iteratorNormalCompletion7 = true) {
              _loop4();
            } // dispFather.formulas.ytd = dispFather.formulas.ytd / 3
            // NO BORRAR TODAVIA USADO COMO REFERENCIA
            // days properties (nombre de propiedade son los valores dias en propiedad day)
            // for (const dayob of this.days) {
            // 	// console.log('d', dayob)
            // 	dispFather.formulas[dayob.day] = 0; // function
            // 	// total hours valor que no depende los hijos
            // 	// revisar que el nombre del padre este correcto
            // 	// ETAPA DE PRUEBA ESTA CASADO CON = total hours
            // 	const FtotalHours = "total_hours"; // requerido este nombre
            // 	// se hacer las transformaciones
            // 	const originName = dispFather.fatherName;
            // 	// const processName = dispFather.fatherName.toLowerCase().trim();
            // 	// const t2 = processName.split(' ');
            // 	const t3 = this.cleanDispositionName(dispFather.fatherName);
            // 	if (FtotalHours === t3 && daysResponse.includes(dayob.day_resp)) {
            // 		console.log(originName, 'si es total_hours')
            // 		console.log('day', dayob.day_resp);
            // 		console.log('response', this.filters[dayob.day_resp]);
            // 		console.log('formula', this.filters[dayob.day_resp][dispFather.fatherName]); // no coinide
            // 		console.log('formula', this.filters[dayob.day_resp][t3]);
            // 		const valuef = this.filters[dayob.day_resp][t3];
            // 		// asigno el valor
            // 		dispFather.formulas[dayob.day] = valuef;
            // 		// ejemplo de volver a iterar los meses para asignar valor
            // 		for (const month of this.getLastMonths) {
            // 			// console.log('m', month);
            // 			const monthstr = moment().set("month", month).format("MM");
            // 			console.log(monthstr);
            // 			// dispFather.formulas[month] = 10;
            // 			dispFather.formulas[month] = arrayResponse.map((dayReport) => {
            // 				const datesplit = dayReport.date.split('/');
            // 				// ej = [d, m, y] => [30, 03, 2020]
            // 				if (datesplit[1] === monthstr) {
            // 					return dayReport.report.total_hours;
            // 				} else {
            // 					return 0;
            // 				}
            // 			}).reduce((a, b) => {
            // 				return a + b;
            // 			}, 0);
            // 			// agregar resultado en ytd (sumatoria de meses)
            // 			dispFather.formulas.ytd = dispFather.formulas.ytd + dispFather.formulas[month];
            // 		}
            // 	} else {
            // 		console.log(originName, 'no es total_hours')
            // 	}
            // }

          } catch (err) {
            _didIteratorError7 = true;
            _iteratorError7 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion7 && _iterator7["return"] != null) {
                _iterator7["return"]();
              }
            } finally {
              if (_didIteratorError7) {
                throw _iteratorError7;
              }
            }
          }
        };

        for (var _iterator2 = cloneArr[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          _loop();
        } // AÑADIENDO DAY RESP EN LOS DIAS GENERADOS POR GET_DAYS
        // PARA QUE POSEAN EL MISMO FORMATO QUE EL DE LA RESPUETA
        // DE BACKEND 
        // resultados formulas por dia
        // for (const dayob of this.days) {
        // 	console.log(dayob.day_resp);
        // 	if (daysResponse.includes(dayob.day_resp)) {
        // 		console.log('d', dayob.day_resp);
        // 		console.log('r', this.filters[dayob.day_resp]);
        // 	} else {
        // 		console.log(0)
        // 	}
        // }
        // reasignar la tabla

      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
            _iterator2["return"]();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      this.tableBodyTest = cloneArr;
      var arrayMonths = [this.tableBodyTest[0]];
      var currentMoment2 = moment__WEBPACK_IMPORTED_MODULE_1___default.a.monthsShort(moment__WEBPACK_IMPORTED_MODULE_1___default()().month());
      var arrayOfMonth = moment__WEBPACK_IMPORTED_MODULE_1___default.a.monthsShort();
      console.log(currentMoment2);
      arrayOfMonth.splice(arrayOfMonth.indexOf(currentMoment2) + 1, arrayOfMonth.length);
      console.log("Array", arrayOfMonth);
      console.log(this.tableBodyTest);

      if (this.tableBodyTest.length !== 0) {
        var newValues = Object.entries(this.tableBodyTest[0].formulas);
        var filter = newValues.filter(function (value) {
          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = value[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var iterator = _step3.value;

              // console.log("iterator", iterator)
              if (iterator == 'Feb') {
                console.log(iterator[value]);
              }
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
                _iterator3["return"]();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }
        });
      }
    },

    /**
     * [cleanDispositionName description]
     * cambiar (" ") -> ("_")
     * cambiar a minuscula
     * @return {[type]} [description]
     */
    cleanDispositionName: function cleanDispositionName(name) {
      var originName = name.trim(); // PARSEAR TODOS LOS DISPOSITION SOLO LETRAS

      var processName = originName.replace(/[^a-zA-Z]+/g, " ").toLowerCase().trim();
      var t2 = processName.split(' ');
      return t2.join('_');
    },

    /**
     * [getFormulasMonth description]
     * @return {[type]} [description]
     */
    getFormulasMonth: function getFormulasMonth() {
      var _this6 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
        var campaign_id, request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _this6.loading = true;
                campaign_id = localStorage.getItem('campaign_id');
                request = {
                  start: "".concat(moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", _this6.getLastMonths[0]).startOf('month').format("MM/DD/YYYY")),
                  // start: `${moment()
                  // 	.set("month", 'Jan')
                  // 	.startOf('month')
                  // 	.format("MM/DD/YYYY")}`,
                  end: moment__WEBPACK_IMPORTED_MODULE_1___default()().set("month", _this6.getLastMonths[_this6.getLastMonths.length - 1]).endOf('month').format("MM/DD/YYYY"),
                  // campaign_id: this.$store.getters["campaigns/GET_CAMPAIGNS_ID"]
                  campaign_id: campaign_id
                };
                console.log("Hacer consulta", request); // pedir formulas

                _context5.next = 7;
                return _this6.$store.dispatch("report_2/GET_FILTERS", request);

              case 7:
                response = _context5.sent;
                _this6.filters = response.data.data; // actualizar cabecera

                _this6.getDays(_this6.selectedMonth); // construrir cuerpo


                _this6.$_generateTableBody2(); // modificar cuerpo


                _this6.pruebav2();

                _context5.next = 17;
                break;

              case 14:
                _context5.prev = 14;
                _context5.t0 = _context5["catch"](0);
                console.log(_context5.t0);

              case 17:
                _context5.prev = 17;
                _this6.loading = false;
                return _context5.finish(17);

              case 20:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 14, 17, 20]]);
      }))();
    },

    /**
     * [selectOtherMonth description]
     * @return {[type]} [description]
     */
    selectOtherMonth: function selectOtherMonth(month) {
      // actualizar cabecera
      this.getDays(moment__WEBPACK_IMPORTED_MODULE_1___default.a.monthsShort().indexOf(month)); // construrir cuerpo

      this.$_generateTableBody2(); // modificar cuerpo

      this.pruebav2();
    },
    getValuesTable: function getValuesTable() {
      var _this7 = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _this7.getDays(_this7.currentMonth);

                _context6.next = 3;
                return _this7.getDisposition();

              case 3:
                _context6.next = 5;
                return _this7.getFormulasMonth();

              case 5:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.tr-color {\r\n  background: #6cc1e6 !important;\r\n  color: white !important;\r\n  font-weight: 400;\n}\n.tam-header-call-log {\r\n  /* min-width: 400px !important;\r\n  max-width: 400px !important;  */\r\n  font-size: 1rem !important;\n}\r\n/* .tam-header-agent {\r\n  min-width: 300px !important;\r\n  max-width: 300px !important; \r\n} */\n.tam-header-timestamp {\r\n  min-width: 500px !important;\r\n  max-width: 500px !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.apexcharts-menu-item.exportSVG {\r\n    display: none;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\n.tam-header {\r\n  /* min-width: 160px !important; */\r\n  /* max-width: 160px !important; */\r\n  font-size: 1rem !important;\n}\n.head-color {\r\n  color: white !important;\n}\n.tr-color {\r\n  background: #6cc1e6;\r\n  color: white;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\n.tam-header {\r\n  /* min-width: 160px !important;\r\n  max-width: 160px !important; */\r\n  font-size: 1rem !important;\n}\n.tr-color {\r\n  background: #6cc1e6;\r\n  color: white;\n}\n.headers-agent {\r\n  min-width: 300px !important;\r\n  max-width: 300px !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.length_text_1 {\r\n  font-size: 1.1rem !important;\n}\n.length_text {\r\n  font-size: 1rem !important;\n}\n.tam-header {\r\n  min-width: 160px !important;\r\n  max-width: 160px !important;\r\n  font-size: 1rem !important;\n}\n.custom_color {\r\n  color: white !important;\n}\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\n.tam-header {\r\n  min-width: 160px !important;\r\n  max-width: 160px !important;\r\n  font-size: 1rem !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\n.theme--light.v-data-table .v-data-table-header th.sortable.active, .theme--light.v-data-table .v-data-table-header th.sortable.active .v-data-table-header__icon, .theme--light.v-data-table .v-data-table-header th.sortable:hover {\r\n    color: rgb(250 247 247 / 87%) !important;\n}\n.theme--light.v-data-table .v-data-table-header th.sortable .v-data-table-header__icon {\r\n    color: rgb(250 247 247 / 87%) !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.tr-color {\r\n  background: #6cc1e6 !important;\r\n  color: white !important;\r\n  font-weight: 400;\n}\n.tam-header {\r\n  /* min-width: 160px !important;\r\n  max-width: 160px !important; */\r\n  font-size: 1rem !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.tr-color {\r\n  background: #6cc1e6 !important;\r\n  color: white !important;\r\n  font-weight: 400;\n}\n.tam-header-call-log {\r\n  /* min-width: 400px !important;\r\n  max-width: 400px !important;  */\r\n  font-size: 1rem !important;\n}\n.tam-header-agent {\r\n  min-width: 300px !important;\r\n  max-width: 300px !important;\n}\n.tam-header-timestamp {\r\n  min-width: 500px !important;\r\n  max-width: 500px !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.tr-color {\r\n  background: #6cc1e6 !important;\r\n  color: white !important;\n}\n.tam-header {\r\n  /* min-width: 160px !important;\r\n  max-width: 160px !important; */\r\n  font-size: 1rem !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\n.custom-highlight-row{\r\n  background-color: pink\n}\n.theme--light.v-data-table .v-data-table-header th.sortable.active, .theme--light.v-data-table .v-data-table-header th.sortable.active .v-data-table-header__icon, .theme--light.v-data-table .v-data-table-header th.sortable:hover {\r\n    color: #f8f9fa !important;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".length_text_1 {\r\n  font-size: 1.1rem !important;\n}\n.length_text {\r\n  font-size: 1rem !important;\n}\n.tam-header {\r\n  min-width: 160px !important;\r\n  max-width: 160px !important;\r\n  font-size: 1rem !important;\n}\n.custom_color {\r\n  color: white !important;\n}\n.length_text {\r\n  font-size: 1rem !important;\r\n  color: white !important;\n}\n.custom_select{\r\n  cursor: pointer;\n}\r\n", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-SG": "./node_modules/moment/locale/en-SG.js",
	"./en-SG.js": "./node_modules/moment/locale/en-SG.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1_d.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_agents.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_dispositions.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Scorecard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CallLog.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./upload_call_log.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader??ref--6-1!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--6-2!./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../../../../node_modules/css-loader??ref--6-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--6-2!./R2TableReport.css?vue&type=style&index=0&lang=css& */ "./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=template&id=58015d83&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/audit/Index.vue?vue&type=template&id=58015d83& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "pt-0", attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        { staticClass: "py-0" },
        [
          _c(
            "v-col",
            { staticClass: "py-0", attrs: { cols: "12" } },
            [
              _c(
                "v-card",
                { attrs: { flat: "" } },
                [
                  _c(
                    "v-toolbar",
                    {
                      staticClass: "shadow-large round",
                      attrs: { flat: "", color: "white" }
                    },
                    [
                      _c("v-toolbar-title", { staticClass: "text-accent" }, [
                        _vm._v(
                          "\n            " +
                            _vm._s(
                              _vm.NameCampaigns.name == undefined
                                ? "Select campaign"
                                : _vm.NameCampaigns.name
                            ) +
                            "\n            - Audit Report\n          "
                        )
                      ]),
                      _vm._v(" "),
                      _c("v-divider", {
                        staticClass: "mx-4",
                        attrs: { inset: "", vertical: "" }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "text-capitalize",
                          attrs: {
                            width: "150",
                            color: "#E63E59",
                            outlined: ""
                          },
                          on: { click: _vm.quarter }
                        },
                        [_vm._v("Quarter")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "text-capitalize mx-5",
                          attrs: {
                            width: "150",
                            color: "#E63E59",
                            outlined: ""
                          },
                          on: { click: _vm.year }
                        },
                        [_vm._v("Year")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-btn",
                        {
                          staticClass: "text-capitalize",
                          attrs: {
                            width: "150",
                            color: "#E63E59",
                            outlined: ""
                          },
                          on: { click: _vm.all_campaigns }
                        },
                        [_vm._v("All Campaigns")]
                      ),
                      _vm._v(" "),
                      _c(
                        "v-tooltip",
                        {
                          attrs: { bottom: "" },
                          scopedSlots: _vm._u([
                            {
                              key: "activator",
                              fn: function(ref) {
                                var on = ref.on
                                return [
                                  _c(
                                    "v-btn",
                                    _vm._g(
                                      {
                                        attrs: {
                                          absolute: "",
                                          dark: "",
                                          fab: "",
                                          top: "",
                                          right: "",
                                          color: "#E63E59",
                                          loading: _vm.loading
                                        },
                                        on: { click: _vm.export_csv }
                                      },
                                      on
                                    ),
                                    [_c("v-icon", [_vm._v("mdi-download")])],
                                    1
                                  )
                                ]
                              }
                            }
                          ])
                        },
                        [_vm._v(" "), _c("span", [_vm._v("Download")])]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        { staticClass: "pt-0" },
        [
          _c(
            "v-col",
            { staticClass: "pt-0", attrs: { cols: "12" } },
            [
              _c(
                "v-app-bar",
                { attrs: { color: "#104961", short: "", flat: "" } },
                [
                  _c(
                    "span",
                    {
                      staticClass: "font-weight-bold white--text",
                      staticStyle: {
                        "font-size": "16px",
                        "margin-left": "210px"
                      }
                    },
                    [_vm._v(_vm._s(_vm.date_app))]
                  ),
                  _vm._v(" "),
                  _c("v-spacer")
                ],
                1
              ),
              _vm._v(" "),
              _c("v-data-table", {
                attrs: {
                  headers: _vm.headers,
                  items: _vm.ItemsAudit,
                  page: _vm.page,
                  "items-per-page": _vm.itemsPerPage,
                  "server-items-length": _vm.pagination.total,
                  options: _vm.options,
                  "hide-default-footer": "",
                  loading: _vm.loading,
                  "loading-text": "Loading... Please wait"
                },
                on: {
                  "update:page": function($event) {
                    _vm.page = $event
                  },
                  "update:options": function($event) {
                    _vm.options = $event
                  },
                  "page-count": function($event) {
                    _vm.pageCount = $event
                  }
                },
                scopedSlots: _vm._u([
                  {
                    key: "item.date",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(_vm._s(_vm._f("format_date")(item.created_at)))
                      ]
                    }
                  },
                  {
                    key: "item.time",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(_vm._s(_vm._f("format_hours")(item.created_at)))
                      ]
                    }
                  },
                  {
                    key: "item.details",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(
                          _vm._s(item.action) + " - " + _vm._s(item.details)
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "text-center pt-2" },
                [
                  _c("v-pagination", {
                    attrs: {
                      circle: "",
                      length: _vm.pagination.page_count,
                      "total-visible": 7
                    },
                    model: {
                      value: _vm.page,
                      callback: function($$v) {
                        _vm.page = $$v
                      },
                      expression: "page"
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=template&id=69bf9a8b&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/campaigns/Index.vue?vue&type=template&id=69bf9a8b& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            [
              _c(
                "v-card",
                { staticClass: "shadow-large round" },
                [
                  _c("v-data-table", {
                    staticClass: "elevation-0",
                    attrs: {
                      headers: _vm.headers,
                      items: _vm.ItemsCampains,
                      "sort-by": "calories",
                      "hide-default-footer": "",
                      "disable-pagination": ""
                    },
                    on: { "click:row": _vm.rowClick },
                    scopedSlots: _vm._u([
                      {
                        key: "top",
                        fn: function() {
                          return [
                            _c(
                              "v-toolbar",
                              { attrs: { flat: "", color: "transparent" } },
                              [
                                _c(
                                  "v-toolbar-title",
                                  { staticClass: "text-accent" },
                                  [_vm._v("Campaigns")]
                                ),
                                _vm._v(" "),
                                _c("v-divider", {
                                  staticClass: "mx-4",
                                  attrs: { inset: "", vertical: "" }
                                }),
                                _vm._v(" "),
                                _c("v-spacer"),
                                _vm._v(" "),
                                _c(
                                  "v-dialog",
                                  {
                                    attrs: { "max-width": "300px" },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "activator",
                                        fn: function(ref) {
                                          var on = ref.on
                                          return [
                                            _c(
                                              "v-btn",
                                              _vm._g(
                                                {
                                                  staticClass: "mb-2",
                                                  attrs: {
                                                    outlined: "",
                                                    color: "#E63E59",
                                                    dark: ""
                                                  }
                                                },
                                                on
                                              ),
                                              [
                                                _c(
                                                  "v-icon",
                                                  { staticClass: "mr-3" },
                                                  [_vm._v("mdi-account-plus")]
                                                ),
                                                _vm._v(
                                                  "New Campaign\n                  "
                                                )
                                              ],
                                              1
                                            )
                                          ]
                                        }
                                      }
                                    ]),
                                    model: {
                                      value: _vm.dialog,
                                      callback: function($$v) {
                                        _vm.dialog = $$v
                                      },
                                      expression: "dialog"
                                    }
                                  },
                                  [
                                    _vm._v(" "),
                                    _c("ValidationObserver", {
                                      attrs: { tag: "form" },
                                      scopedSlots: _vm._u([
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var passes = ref.passes
                                            return [
                                              _c(
                                                "v-card",
                                                [
                                                  _c("v-card-title", [
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass: "headline"
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(_vm.formTitle)
                                                        )
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-card-text",
                                                    [
                                                      _c(
                                                        "v-container",
                                                        [
                                                          _c(
                                                            "v-row",
                                                            [
                                                              _c(
                                                                "v-col",
                                                                {
                                                                  attrs: {
                                                                    cols: "12",
                                                                    sm: "12"
                                                                  }
                                                                },
                                                                [
                                                                  _c("p", [
                                                                    _vm._v(
                                                                      "Name must be 3 uppercase characters"
                                                                    )
                                                                  ]),
                                                                  _vm._v(" "),
                                                                  _c(
                                                                    "ValidationProvider",
                                                                    {
                                                                      attrs: {
                                                                        name:
                                                                          "name",
                                                                        rules: {
                                                                          regex: /^[A-Z]+$/,
                                                                          required: true,
                                                                          max: 3
                                                                        }
                                                                      },
                                                                      scopedSlots: _vm._u(
                                                                        [
                                                                          {
                                                                            key:
                                                                              "default",
                                                                            fn: function(
                                                                              ref
                                                                            ) {
                                                                              var errors =
                                                                                ref.errors
                                                                              return [
                                                                                _c(
                                                                                  "v-text-field",
                                                                                  {
                                                                                    attrs: {
                                                                                      label:
                                                                                        "Name",
                                                                                      "error-messages": errors
                                                                                    },
                                                                                    model: {
                                                                                      value:
                                                                                        _vm
                                                                                          .editedItem
                                                                                          .name,
                                                                                      callback: function(
                                                                                        $$v
                                                                                      ) {
                                                                                        _vm.$set(
                                                                                          _vm.editedItem,
                                                                                          "name",
                                                                                          $$v
                                                                                        )
                                                                                      },
                                                                                      expression:
                                                                                        "editedItem.name"
                                                                                    }
                                                                                  }
                                                                                )
                                                                              ]
                                                                            }
                                                                          }
                                                                        ],
                                                                        null,
                                                                        true
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              ),
                                                              _vm._v(" "),
                                                              _c(
                                                                "v-col",
                                                                {
                                                                  attrs: {
                                                                    cols: "12",
                                                                    sm: "12"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "ValidationProvider",
                                                                    {
                                                                      attrs: {
                                                                        name:
                                                                          "Platform",
                                                                        rules: {
                                                                          required: true
                                                                        }
                                                                      },
                                                                      scopedSlots: _vm._u(
                                                                        [
                                                                          {
                                                                            key:
                                                                              "default",
                                                                            fn: function(
                                                                              ref
                                                                            ) {
                                                                              var errors =
                                                                                ref.errors
                                                                              return [
                                                                                _c(
                                                                                  "v-select",
                                                                                  {
                                                                                    attrs: {
                                                                                      items:
                                                                                        _vm.items,
                                                                                      label:
                                                                                        "Platform",
                                                                                      "error-messages": errors
                                                                                    },
                                                                                    model: {
                                                                                      value:
                                                                                        _vm
                                                                                          .editedItem
                                                                                          .Pbx,
                                                                                      callback: function(
                                                                                        $$v
                                                                                      ) {
                                                                                        _vm.$set(
                                                                                          _vm.editedItem,
                                                                                          "Pbx",
                                                                                          $$v
                                                                                        )
                                                                                      },
                                                                                      expression:
                                                                                        "editedItem.Pbx"
                                                                                    }
                                                                                  }
                                                                                )
                                                                              ]
                                                                            }
                                                                          }
                                                                        ],
                                                                        null,
                                                                        true
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-card-actions",
                                                    [
                                                      _c("v-spacer"),
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-btn",
                                                        {
                                                          attrs: {
                                                            outlined: ""
                                                          },
                                                          on: {
                                                            click: _vm.close
                                                          }
                                                        },
                                                        [_vm._v("Cancel")]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-btn",
                                                        {
                                                          attrs: {
                                                            color: "#E63E59",
                                                            outlined: ""
                                                          },
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return passes(
                                                                _vm.save
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Save")]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          }
                                        }
                                      ])
                                    })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "item.action",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c(
                              "v-icon",
                              {
                                staticClass: "mr-8",
                                on: {
                                  click: function($event) {
                                    return _vm.editItem(item)
                                  }
                                }
                              },
                              [_vm._v("mdi-pencil")]
                            ),
                            _vm._v(" "),
                            _c(
                              "v-icon",
                              {
                                on: {
                                  click: function($event) {
                                    return _vm.deleteItem(item)
                                  }
                                }
                              },
                              [_vm._v("mdi-delete")]
                            )
                          ]
                        }
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=template&id=0a813c9a&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/layout/Dashboard.vue?vue&type=template&id=0a813c9a& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "pt-0", attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        { staticClass: "pt-0" },
        [
          _c(
            "v-col",
            { attrs: { cols: "12" } },
            [
              _c(
                "v-card",
                { attrs: { color: "transparent", flat: "" } },
                [
                  _c(
                    "v-toolbar",
                    {
                      staticClass: "shadow-large round",
                      attrs: { flat: "", color: "white" }
                    },
                    [
                      _c("v-toolbar-title", { staticClass: "text-accent" }, [
                        _vm._v(
                          "\n                        " +
                            _vm._s(
                              _vm.NameCampaigns.name == undefined
                                ? "Select campaign"
                                : _vm.NameCampaigns.name
                            ) +
                            "\n                        - Dashboard"
                        )
                      ]),
                      _vm._v(" "),
                      _c("v-divider", {
                        staticClass: "mx-4",
                        attrs: { inset: "", vertical: "" }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-toolbar", {
                    staticClass: "elevation-0",
                    staticStyle: { "border-radius": "0px" },
                    attrs: { color: "#104961" }
                  }),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "6", md: "4" } },
                        [
                          _c(
                            "v-card",
                            {
                              staticClass: "mx-auto",
                              staticStyle: {
                                "min-width": "290px",
                                "max-width": "290px"
                              }
                            },
                            [
                              _c(
                                "v-toolbar",
                                {
                                  attrs: {
                                    dense: "",
                                    color: "#25A8E0",
                                    dark: "",
                                    flat: ""
                                  }
                                },
                                [_c("v-toolbar-title", [_vm._v("Campaign")])],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "v-card-text",
                                [
                                  _c("v-select", {
                                    staticClass: "mt-4",
                                    attrs: {
                                      dense: "",
                                      items: _vm.Itemscampaigns,
                                      "item-text": "name",
                                      "item-value": "id",
                                      label: "Campaign",
                                      outlined: ""
                                    },
                                    on: { change: _vm.CampaignsId },
                                    model: {
                                      value: _vm.campaign_id,
                                      callback: function($$v) {
                                        _vm.campaign_id = $$v
                                      },
                                      expression: "campaign_id"
                                    }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c(
                            "v-card",
                            {
                              staticClass:
                                "mx-auto text-center mt-3 elevation-0 round",
                              attrs: { color: "transparent" }
                            },
                            [
                              _c("v-date-picker", {
                                staticClass: "round",
                                attrs: { color: "#25A8E0", range: "" },
                                model: {
                                  value: _vm.dates,
                                  callback: function($$v) {
                                    _vm.dates = $$v
                                  },
                                  expression: "dates"
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "v-card-actions",
                                [
                                  _c("v-spacer"),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: {
                                        outlined: "",
                                        loading: _vm.loadingValues,
                                        color: "#25A8E0",
                                        dark: "",
                                        width: "280px"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.dateLayout()
                                        }
                                      }
                                    },
                                    [_vm._v("Select")]
                                  ),
                                  _vm._v(" "),
                                  _c("v-spacer")
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "8" } },
                        [
                          _vm.status_graph == true && _vm.render
                            ? _c(
                                "v-card",
                                {
                                  staticClass: "d-flex",
                                  attrs: { height: "300" }
                                },
                                [
                                  _c("v-progress-circular", {
                                    staticClass: "mx-auto my-auto",
                                    attrs: {
                                      size: 70,
                                      width: 7,
                                      color: "pink",
                                      indeterminate: ""
                                    }
                                  })
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.status_graph == false && _vm.render
                            ? _c(
                                "v-card",
                                [
                                  _c("apexchart", {
                                    attrs: {
                                      type: "bar",
                                      height: "300",
                                      options: _vm.chartBar,
                                      series: _vm.serieBar
                                    },
                                    on: {
                                      dataPointSelection:
                                        _vm.dataSelectionDelete
                                    }
                                  })
                                ],
                                1
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=template&id=4361f804&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1.vue?vue&type=template&id=4361f804& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            {
              staticClass: "py-0",
              attrs: { cols: "12", sm: "12", md: "12", lg: "12" }
            },
            [
              _c(
                "v-card",
                {
                  staticClass: "elevation-0 py-0",
                  attrs: { color: "transparent" }
                },
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "6", lg: "6" } },
                        [
                          _c(
                            "v-card",
                            {
                              staticClass:
                                "mx-auto text-center elevation-0 round",
                              attrs: { color: "transparent" }
                            },
                            [
                              _c("v-date-picker", {
                                attrs: { color: "#25A8E0", range: "" },
                                model: {
                                  value: _vm.dates,
                                  callback: function($$v) {
                                    _vm.dates = $$v
                                  },
                                  expression: "dates"
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "v-card-actions",
                                [
                                  _c("v-spacer"),
                                  _vm._v(" "),
                                  _c(
                                    "v-btn",
                                    {
                                      attrs: {
                                        outlined: "",
                                        loading: _vm.loadingValues,
                                        color: "#25A8E0",
                                        dark: "",
                                        width: "280px"
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.addRange()
                                        }
                                      }
                                    },
                                    [
                                      _c("v-icon", [_vm._v("mdi-plus")]),
                                      _vm._v("Add\n                ")
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c("v-spacer")
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticClass: "px-0",
                          attrs: { cols: "12", sm: "12", md: "6", lg: "6" }
                        },
                        [
                          _vm.showAgents
                            ? _c(
                                "v-card",
                                { staticClass: "shadow-large round" },
                                [
                                  _c("v-data-table", {
                                    staticClass: "elevation-0 agentes",
                                    attrs: {
                                      headers: _vm.headersAgent,
                                      items: _vm.ItemsAgents,
                                      "single-select": _vm.singleSelect,
                                      "item-key": "name",
                                      "show-select":
                                        _vm.selectA === false ? true : false,
                                      "hide-default-footer": "",
                                      "disable-pagination": ""
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "item.data-table-select",
                                          fn: function(ref) {
                                            var item = ref.item
                                            var isSelected = ref.isSelected
                                            var select = ref.select
                                            return [
                                              _vm.select_agent_comparison.includes(
                                                item
                                              )
                                                ? _c("v-simple-checkbox", {
                                                    attrs: {
                                                      value: isSelected,
                                                      disabled: ""
                                                    },
                                                    on: {
                                                      input: function($event) {
                                                        return select($event)
                                                      }
                                                    }
                                                  })
                                                : _c("v-simple-checkbox", {
                                                    attrs: {
                                                      value: isSelected
                                                    },
                                                    on: {
                                                      input: function($event) {
                                                        return select($event)
                                                      }
                                                    }
                                                  })
                                            ]
                                          }
                                        },
                                        {
                                          key: "item.name",
                                          fn: function(ref) {
                                            var item = ref.item
                                            return [
                                              item.active === true ||
                                              item.active == 1
                                                ? _c("p", [
                                                    _vm._v(_vm._s(item.name))
                                                  ])
                                                : _c("p", [
                                                    _vm._v(
                                                      "* " + _vm._s(item.name)
                                                    )
                                                  ])
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      false,
                                      1408291635
                                    ),
                                    model: {
                                      value: _vm.selectedAgent,
                                      callback: function($$v) {
                                        _vm.selectedAgent = $$v
                                      },
                                      expression: "selectedAgent"
                                    }
                                  })
                                ],
                                1
                              )
                            : _c(
                                "v-card",
                                { attrs: { color: "#104961", light: "" } },
                                [
                                  _c("v-card-text", [
                                    _c(
                                      "div",
                                      { staticStyle: { color: "white" } },
                                      [_vm._v("Please select a date")]
                                    )
                                  ])
                                ],
                                1
                              )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { staticClass: "pr-0", attrs: { cols: "12" } },
            [
              _c(
                "v-toolbar",
                {
                  staticClass: "shadow-large round",
                  attrs: { flat: "", color: "white" }
                },
                [
                  _c(
                    "v-tooltip",
                    {
                      attrs: { bottom: "" },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            return [
                              _c(
                                "v-btn",
                                _vm._g(
                                  {
                                    attrs: {
                                      absolute: "",
                                      dark: "",
                                      fab: "",
                                      top: "",
                                      right: "",
                                      color: "#E63E59"
                                    },
                                    on: { click: _vm.jsexport }
                                  },
                                  on
                                ),
                                [_c("v-icon", [_vm._v("mdi-download")])],
                                1
                              )
                            ]
                          }
                        }
                      ])
                    },
                    [_vm._v(" "), _c("span", [_vm._v("Download")])]
                  ),
                  _vm._v(" "),
                  _c("v-toolbar-title", { staticClass: "text-accent" }, [
                    _vm._v(
                      _vm._s(
                        _vm.NameCampaigns.name == undefined
                          ? "Select campaign"
                          : _vm.NameCampaigns.name
                      ) + " - Agent Hours - Custom"
                    )
                  ]),
                  _vm._v(" "),
                  _c("v-divider", {
                    staticClass: "mx-4",
                    attrs: { inset: "", vertical: "" }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card",
                { staticClass: "shadow-large round" },
                [
                  _c("v-simple-table", {
                    attrs: { "fixed-header": "", height: "33rem" },
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function() {
                          return [
                            _c("thead", [
                              _c(
                                "tr",
                                {
                                  staticClass: "head-color",
                                  staticStyle: { "white-space": "nowrap" }
                                },
                                _vm._l(_vm.table_headers_1, function(item) {
                                  return _c(
                                    "th",
                                    {
                                      key: item.name,
                                      staticClass: "tam-header white--text",
                                      staticStyle: { background: "#104961" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(_vm._f("date1")(item)) +
                                          "\n                  "
                                      ),
                                      _c("br"),
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(_vm._f("date2")(item)) +
                                          "\n                "
                                      )
                                    ]
                                  )
                                }),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              _vm._l(_vm.table_1, function(item, key) {
                                return _c(
                                  "tr",
                                  {
                                    directives: [
                                      {
                                        name: "show",
                                        rawName: "v-show",
                                        value: item.status === true,
                                        expression: "item.status === true"
                                      }
                                    ],
                                    key: item.name,
                                    class:
                                      item.name == "Agent Hours"
                                        ? "tr-color"
                                        : "p"
                                  },
                                  [
                                    item.name !== "Agent Billable Hours"
                                      ? _c("td", [
                                          _vm._v(
                                            _vm._s(_vm.agents_active(item.name))
                                          )
                                        ])
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _vm._l(_vm.table_headers_1, function(
                                      sub_item,
                                      key2
                                    ) {
                                      return _c(
                                        "td",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.table_headers_1.length >
                                                  1 &&
                                                key2 + 1 <=
                                                  _vm.table_headers_1.length -
                                                    1 &&
                                                key !== 0,
                                              expression:
                                                "table_headers_1.length > 1 && ((key2 + 1) <= (table_headers_1.length - 1)) && key!==0"
                                            }
                                          ],
                                          class:
                                            item["total_" + (key2 + 1) + key] <
                                            0
                                              ? "red--text"
                                              : "p",
                                          attrs: { key2: sub_item.name }
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              item["total_" + (key2 + 1) + key]
                                            )
                                          )
                                        ]
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              0
                            )
                          ]
                        },
                        proxy: true
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=template&id=4543de69&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R1/R1_d.vue?vue&type=template&id=4543de69& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { staticClass: "pr-0", attrs: { cols: "12" } },
            [
              _c(
                "v-toolbar",
                {
                  staticClass: "shadow-large round",
                  attrs: { flat: "", color: "white" }
                },
                [
                  _c(
                    "v-tooltip",
                    {
                      attrs: { bottom: "" },
                      scopedSlots: _vm._u([
                        {
                          key: "activator",
                          fn: function(ref) {
                            var on = ref.on
                            return [
                              _c(
                                "v-btn",
                                _vm._g(
                                  {
                                    attrs: {
                                      absolute: "",
                                      dark: "",
                                      fab: "",
                                      top: "",
                                      right: "",
                                      color: "#E63E59"
                                    },
                                    on: { click: _vm.jsexport }
                                  },
                                  on
                                ),
                                [_c("v-icon", [_vm._v("mdi-download")])],
                                1
                              )
                            ]
                          }
                        }
                      ])
                    },
                    [_vm._v(" "), _c("span", [_vm._v("Download")])]
                  ),
                  _vm._v(" "),
                  _c("v-toolbar-title", { staticClass: "text-accent" }, [
                    _vm._v(
                      _vm._s(
                        _vm.NameCampaigns.name == undefined
                          ? "Select campaign"
                          : _vm.NameCampaigns.name
                      ) + " - Agent Hours"
                    )
                  ]),
                  _vm._v(" "),
                  _c("v-divider", {
                    staticClass: "mx-4",
                    attrs: { inset: "", vertical: "" }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card",
                { staticClass: "shadow-large round" },
                [
                  _c("v-simple-table", {
                    attrs: { "fixed-header": "", height: "33rem" },
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function() {
                          return [
                            _c("thead", [
                              _c(
                                "tr",
                                {
                                  staticClass: "head-color",
                                  staticStyle: { "white-space": "nowrap" }
                                },
                                _vm._l(_vm.table_headers_1, function(item) {
                                  return _c(
                                    "th",
                                    {
                                      key: item.name,
                                      class:
                                        item.name == ""
                                          ? "headers-agent tam-header white--text header"
                                          : "tam-header white--text",
                                      staticStyle: { background: "#104961" }
                                    },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(_vm._f("date1")(item)) +
                                          "\n                  "
                                      ),
                                      _c("br"),
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(_vm._f("date2")(item)) +
                                          "\n                "
                                      )
                                    ]
                                  )
                                }),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              _vm._l(_vm.total1, function(item, key) {
                                return _c(
                                  "tr",
                                  {
                                    key: item.name,
                                    class:
                                      item.name == "Agent Hours"
                                        ? "tr-color"
                                        : "p"
                                  },
                                  [
                                    item.name !== "Agent Billable Hours"
                                      ? _c("td", [
                                          _vm._v(
                                            _vm._s(_vm.agents_active(item.name))
                                          )
                                        ])
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _vm._l(_vm.table_headers_1, function(
                                      sub_item,
                                      key2
                                    ) {
                                      return _c(
                                        "td",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value:
                                                _vm.table_headers_1.length >
                                                  1 &&
                                                key2 + 1 <=
                                                  _vm.table_headers_1.length -
                                                    1 &&
                                                key !== 0,
                                              expression:
                                                "table_headers_1.length > 1 && ((key2 + 1) <= (table_headers_1.length - 1)) && key!==0"
                                            }
                                          ],
                                          class:
                                            item["total_" + (key2 + 1) + key] <
                                            0
                                              ? "red--text"
                                              : "p",
                                          attrs: { key2: sub_item.name }
                                        },
                                        [
                                          key2 + 1 == 1
                                            ? _c(
                                                "v-chip",
                                                { staticClass: "ma-2" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      item[
                                                        "total_" +
                                                          (key2 + 1) +
                                                          key
                                                      ]
                                                    )
                                                  )
                                                ]
                                              )
                                            : item[
                                                "total_" + (key2 + 1) + key
                                              ] == 0
                                            ? _c("span")
                                            : _c("span", [
                                                _vm._v(
                                                  _vm._s(
                                                    item[
                                                      "total_" +
                                                        (key2 + 1) +
                                                        key
                                                    ]
                                                  )
                                                )
                                              ])
                                        ],
                                        1
                                      )
                                    })
                                  ],
                                  2
                                )
                              }),
                              0
                            )
                          ]
                        },
                        proxy: true
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=template&id=a95d8738&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2.vue?vue&type=template&id=a95d8738& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            {
              staticClass: "py-0",
              attrs: { cols: "12", sm: "12", md: "12", lg: "12" }
            },
            [
              _c(
                "v-tooltip",
                {
                  attrs: { bottom: "" },
                  scopedSlots: _vm._u([
                    {
                      key: "activator",
                      fn: function(ref) {
                        var on = ref.on
                        return [
                          _c(
                            "v-btn",
                            _vm._g(
                              {
                                attrs: {
                                  absolute: "",
                                  dark: "",
                                  fab: "",
                                  top: "",
                                  right: "",
                                  small: "",
                                  color: "#E63E59"
                                },
                                on: { click: _vm.getBody }
                              },
                              on
                            ),
                            [_c("v-icon", [_vm._v("mdi-download")])],
                            1
                          )
                        ]
                      }
                    }
                  ])
                },
                [_vm._v(" "), _c("span", [_vm._v("Download")])]
              ),
              _vm._v(" "),
              _c(
                "v-responsive",
                [
                  _c("v-simple-table", {
                    scopedSlots: _vm._u([
                      {
                        key: "top",
                        fn: function() {
                          return [
                            _c(
                              "v-toolbar",
                              { attrs: { flat: "", color: "transparent" } },
                              [
                                _c(
                                  "v-toolbar-title",
                                  { staticClass: "text-accent" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.getCampaign.name == undefined
                                          ? "Select campaign"
                                          : _vm.getCampaign.name
                                      ) + " - Daily Activity"
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("v-divider", {
                                  staticClass: "mx-4",
                                  attrs: { inset: "", vertical: "" }
                                })
                              ],
                              1
                            )
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "default",
                        fn: function() {
                          return [
                            _c(
                              "thead",
                              { staticClass: "length_text head-color" },
                              [
                                _c(
                                  "tr",
                                  [
                                    _c("th", {
                                      staticClass: "text-left length_text"
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "th",
                                      { staticClass: "text-left length_text" },
                                      [_vm._v("YTD")]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(_vm.getLastMonths, function(
                                      month,
                                      index
                                    ) {
                                      return _c(
                                        "th",
                                        {
                                          key: index,
                                          staticClass: "text-center length_text"
                                        },
                                        [
                                          _c(
                                            "span",
                                            {
                                              staticClass: "custom_color",
                                              on: {
                                                click: function($event) {
                                                  return _vm.getDays(month)
                                                }
                                              }
                                            },
                                            [_vm._v(_vm._s(month))]
                                          ),
                                          _vm._v(" "),
                                          _c("v-divider", {
                                            attrs: { color: "white" }
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "span",
                                            { staticClass: "custom_color" },
                                            [_vm._v("Total")]
                                          )
                                        ],
                                        1
                                      )
                                    }),
                                    _vm._v(" "),
                                    _vm._l(_vm.days, function(item, index) {
                                      return _c(
                                        "th",
                                        {
                                          key: index + "n",
                                          staticClass: "text-center length_text"
                                        },
                                        [
                                          _vm._v(
                                            "\n                  " +
                                              _vm._s(item.day_name) +
                                              "\n                  "
                                          ),
                                          _c("v-divider", {
                                            attrs: { color: "white" }
                                          }),
                                          _vm._v(" "),
                                          _c(
                                            "span",
                                            { staticClass: "custom_color" },
                                            [_vm._v(_vm._s(item.day))]
                                          )
                                        ],
                                        1
                                      )
                                    })
                                  ],
                                  2
                                )
                              ]
                            ),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              [
                                _c(
                                  "tr",
                                  { staticStyle: { background: "#6CC1E6" } },
                                  [
                                    _c(
                                      "td",
                                      {
                                        staticClass: "length_text head-color",
                                        staticStyle: {
                                          background: "#6CC1E6",
                                          "white-space": "nowrap"
                                        }
                                      },
                                      [_vm._v("Total Hours")]
                                    ),
                                    _vm._v(" "),
                                    _vm._l(_vm.getBody, function(body, index) {
                                      return _c(
                                        "td",
                                        {
                                          key: index + "a",
                                          staticClass: "text-center"
                                        },
                                        [
                                          _vm._v(
                                            _vm._s(
                                              body.value +
                                                Math.floor(Math.random() * 10)
                                            )
                                          )
                                        ]
                                      )
                                    }),
                                    _vm._v(" "),
                                    _c("td", [_vm._v("21212")]),
                                    _vm._v(" "),
                                    _c("td", [_vm._v("21212")]),
                                    _vm._v(" "),
                                    _c("td", [_vm._v("21212")])
                                  ],
                                  2
                                ),
                                _vm._v(" "),
                                _c("tr", [
                                  _c(
                                    "td",
                                    {
                                      staticClass: "text-right",
                                      staticStyle: { "white-space": "nowrap" }
                                    },
                                    [_vm._v("asd")]
                                  )
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.Total_Hours, function(item, index) {
                                  return _c(
                                    "tr",
                                    { key: index + "b" },
                                    [
                                      _c(
                                        "td",
                                        {
                                          staticClass: "text-right",
                                          staticStyle: {
                                            "white-space": "nowrap"
                                          }
                                        },
                                        [_vm._v(_vm._s(item.name))]
                                      ),
                                      _vm._v(" "),
                                      _vm._l(_vm.getBody, function(
                                        body,
                                        index
                                      ) {
                                        return _c(
                                          "td",
                                          {
                                            key: index + "aa",
                                            staticClass: "text-center"
                                          },
                                          [
                                            _vm._v(
                                              _vm._s(
                                                body.value +
                                                  Math.floor(Math.random() * 10)
                                              )
                                            )
                                          ]
                                        )
                                      }),
                                      _vm._v(" "),
                                      _c("td", [_vm._v("21212")])
                                    ],
                                    2
                                  )
                                })
                              ],
                              2
                            )
                          ]
                        },
                        proxy: true
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue?vue&type=template&id=3412993e&":
/*!*****************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue?vue&type=template&id=3412993e& ***!
  \*****************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            {
              staticClass: "py-0",
              attrs: { cols: "12", sm: "12", md: "12", lg: "12" }
            },
            [
              _c(
                "v-tooltip",
                {
                  attrs: { bottom: "" },
                  scopedSlots: _vm._u([
                    {
                      key: "activator",
                      fn: function(ref) {
                        var on = ref.on
                        return [
                          _c(
                            "v-btn",
                            _vm._g(
                              {
                                attrs: {
                                  loading: _vm.loading,
                                  absolute: "",
                                  dark: "",
                                  fab: "",
                                  top: "",
                                  right: "",
                                  color: "#E63E59"
                                },
                                on: { click: _vm.jsexport }
                              },
                              on
                            ),
                            [_c("v-icon", [_vm._v("mdi-download")])],
                            1
                          )
                        ]
                      }
                    }
                  ])
                },
                [_vm._v(" "), _c("span", [_vm._v("Download")])]
              ),
              _vm._v(" "),
              _c(
                "v-toolbar",
                {
                  staticClass: "shadow-large round",
                  attrs: { flat: "", color: "white" }
                },
                [
                  _c("v-toolbar-title", { staticClass: "text-accent" }, [
                    _vm._v(
                      "\n          " +
                        _vm._s(
                          _vm.getCampaign.name == undefined
                            ? "Select campaign"
                            : _vm.getCampaign.name
                        ) +
                        " -\n          Daily Activity\n        "
                    )
                  ]),
                  _vm._v(" "),
                  _c("v-divider", {
                    staticClass: "mx-4",
                    attrs: { inset: "", vertical: "" }
                  }),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "mx-2",
                      attrs: {
                        outlined: "",
                        loading: _vm.loading,
                        dark: "",
                        color: "#E84D66"
                      },
                      on: { click: _vm.getValuesTable }
                    },
                    [_c("v-icon", [_vm._v("mdi-sync")])],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      attrs: {
                        outlined: "",
                        loading: _vm.loading,
                        dark: "",
                        color: "#E84D66"
                      },
                      on: {
                        click: function($event) {
                          return _vm.rotateMonths(-1)
                        }
                      }
                    },
                    [_c("v-icon", [_vm._v("mdi-chevron-left")])],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-btn",
                    {
                      staticClass: "mr-5 ml-2",
                      attrs: {
                        outlined: "",
                        loading: _vm.loading,
                        dark: "",
                        color: "#E84D66"
                      },
                      on: {
                        click: function($event) {
                          return _vm.rotateMonths(1)
                        }
                      }
                    },
                    [_c("v-icon", [_vm._v("mdi-chevron-right")])],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "v-card",
                { staticClass: "shadow-large round" },
                [
                  _c("v-simple-table", {
                    attrs: { "fixed-header": "", height: "37rem" },
                    scopedSlots: _vm._u([
                      {
                        key: "default",
                        fn: function() {
                          return [
                            _c("thead", [
                              _c(
                                "tr",
                                [
                                  _c(
                                    "th",
                                    {
                                      staticClass: "text-left length_text px-5",
                                      staticStyle: { background: "#104961" }
                                    },
                                    [
                                      _c("span", { staticClass: "title" }, [
                                        _vm._v("2020")
                                      ])
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass: "text-left length_text",
                                      staticStyle: { background: "#104961" }
                                    },
                                    [_vm._v("QTR")]
                                  ),
                                  _vm._v(" "),
                                  _vm._l(_vm.getLastMonths, function(
                                    month,
                                    index
                                  ) {
                                    return _c(
                                      "th",
                                      {
                                        key: index,
                                        staticClass: "text-center length_text",
                                        staticStyle: { background: "#104961" }
                                      },
                                      [
                                        _c(
                                          "span",
                                          {
                                            staticClass: "custom_color",
                                            on: {
                                              click: function($event) {
                                                return _vm.selectOtherMonth(
                                                  month
                                                )
                                              }
                                            }
                                          },
                                          [
                                            _c(
                                              "v-chip",
                                              {
                                                staticClass:
                                                  "mt-2 custom_select"
                                              },
                                              [_vm._v(_vm._s(month))]
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c("v-divider", {
                                          attrs: { color: "white" }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "span",
                                          { staticClass: "custom_color" },
                                          [_vm._v("Total")]
                                        )
                                      ],
                                      1
                                    )
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.days, function(item, index) {
                                    return _c(
                                      "th",
                                      {
                                        key: index + "n",
                                        staticClass: "text-center length_text",
                                        staticStyle: { background: "#104961" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                  " +
                                            _vm._s(item.day_name) +
                                            "\n                  "
                                        ),
                                        _c("v-divider", {
                                          attrs: { color: "white" }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "span",
                                          { staticClass: "custom_color" },
                                          [_vm._v(_vm._s(item.day))]
                                        )
                                      ],
                                      1
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "tbody",
                              [
                                _vm._l(_vm.tableBodyTest, function(row, index) {
                                  return [
                                    row.fatherName
                                      ? _c(
                                          "tr",
                                          {
                                            key: index + "z",
                                            staticClass: "white--text",
                                            staticStyle: {
                                              "background-color": "#6CC1E6",
                                              "white-space": "nowrap"
                                            }
                                          },
                                          [
                                            _c(
                                              "td",
                                              {
                                                staticClass: "text-left",
                                                staticStyle: {
                                                  "background-color": "#6CC1E6",
                                                  "white-space": "nowrap"
                                                }
                                              },
                                              [_vm._v(_vm._s(row.fatherName))]
                                            ),
                                            _vm._v(" "),
                                            _vm._l(row.formulas, function(
                                              value,
                                              key,
                                              keyfindex
                                            ) {
                                              return _c(
                                                "td",
                                                {
                                                  directives: [
                                                    {
                                                      name: "show",
                                                      rawName: "v-show",
                                                      value: row.hasOwnProperty(
                                                        "formulas"
                                                      ),
                                                      expression:
                                                        "row.hasOwnProperty('formulas')"
                                                    }
                                                  ],
                                                  key: keyfindex + "f_father",
                                                  class:
                                                    keyfindex == 0 ||
                                                    keyfindex == 1 ||
                                                    keyfindex == 2 ||
                                                    keyfindex == 3
                                                      ? "text-right"
                                                      : "text-center",
                                                  staticStyle: {
                                                    "background-color":
                                                      "#6CC1E6",
                                                    "white-space": "nowrap"
                                                  }
                                                },
                                                [
                                                  (keyfindex == 1 &&
                                                    value !== 0) ||
                                                  (keyfindex == 2 &&
                                                    value !== 0) ||
                                                  (keyfindex == 3 &&
                                                    value !== 0)
                                                    ? _c("v-chip", [
                                                        _vm._v(
                                                          _vm._s(
                                                            _vm._f("toFixed")(
                                                              value,
                                                              0,
                                                              row.fatherName
                                                            )
                                                          )
                                                        )
                                                      ])
                                                    : _c(
                                                        "span",
                                                        {
                                                          class:
                                                            keyfindex == 0
                                                              ? "font-weight-bold"
                                                              : ""
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(
                                                              _vm._f("toFixed")(
                                                                value,
                                                                0,
                                                                row.fatherName
                                                              )
                                                            )
                                                          )
                                                        ]
                                                      )
                                                ],
                                                1
                                              )
                                            })
                                          ],
                                          2
                                        )
                                      : _vm._e(),
                                    _vm._v(" "),
                                    _vm._l(row.children, function(
                                      subrow,
                                      subindex
                                    ) {
                                      return _c(
                                        "tr",
                                        {
                                          directives: [
                                            {
                                              name: "show",
                                              rawName: "v-show",
                                              value: row.hasOwnProperty(
                                                "children"
                                              ),
                                              expression:
                                                "row.hasOwnProperty('children')"
                                            }
                                          ],
                                          key: index + "-" + subindex
                                        },
                                        [
                                          _c(
                                            "td",
                                            {
                                              staticClass: "text-right",
                                              staticStyle: {
                                                "white-space": "nowrap"
                                              }
                                            },
                                            [_vm._v(_vm._s(subrow.name))]
                                          ),
                                          _vm._v(" "),
                                          _vm._l(subrow.formulas, function(
                                            subvalue,
                                            subkey,
                                            keysindex
                                          ) {
                                            return _c(
                                              "td",
                                              {
                                                directives: [
                                                  {
                                                    name: "show",
                                                    rawName: "v-show",
                                                    value: row.hasOwnProperty(
                                                      "formulas"
                                                    ),
                                                    expression:
                                                      "row.hasOwnProperty('formulas')"
                                                  }
                                                ],
                                                key: keysindex + "f_son",
                                                class:
                                                  keysindex == 0 ||
                                                  keysindex == 1 ||
                                                  keysindex == 2 ||
                                                  keysindex == 3
                                                    ? "text-right"
                                                    : "text-center"
                                              },
                                              [
                                                (keysindex == 1 &&
                                                  subvalue !== 0) ||
                                                (keysindex == 2 &&
                                                  subvalue !== 0) ||
                                                (keysindex == 3 &&
                                                  subvalue !== 0)
                                                  ? _c("v-chip", [
                                                      _vm._v(
                                                        _vm._s(
                                                          _vm._f("toFixed")(
                                                            subvalue,
                                                            0,
                                                            row.fatherName
                                                          )
                                                        )
                                                      )
                                                    ])
                                                  : _c(
                                                      "span",
                                                      {
                                                        class:
                                                          keysindex == 0
                                                            ? "font-weight-bold"
                                                            : ""
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(
                                                            _vm._f("toFixed")(
                                                              subvalue,
                                                              0,
                                                              row.fatherName
                                                            )
                                                          )
                                                        )
                                                      ]
                                                    )
                                              ],
                                              1
                                            )
                                          })
                                        ],
                                        2
                                      )
                                    })
                                  ]
                                })
                              ],
                              2
                            )
                          ]
                        },
                        proxy: true
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=template&id=635587f9&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_agents.vue?vue&type=template&id=635587f9& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            { attrs: { cols: "12" } },
            [
              _c(
                "v-card",
                { staticClass: "shadow-large round" },
                [
                  _c("v-data-table", {
                    staticClass: "elevation-0",
                    attrs: {
                      headers: _vm.headers,
                      items: _vm.ItemsAgents,
                      "hide-default-footer": "",
                      "disable-pagination": ""
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "top",
                        fn: function() {
                          return [
                            _c(
                              "v-toolbar",
                              { attrs: { flat: "", color: "transparent" } },
                              [
                                _c(
                                  "v-toolbar-title",
                                  { staticClass: "text-accent" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.NameCampaigns.name == undefined
                                          ? "Select campaign"
                                          : _vm.NameCampaigns.name
                                      ) + " - Agents"
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("v-divider", {
                                  staticClass: "mx-4",
                                  attrs: { inset: "", vertical: "" }
                                }),
                                _vm._v(" "),
                                _c(
                                  "v-btn",
                                  {
                                    attrs: {
                                      outlined: "",
                                      dark: "",
                                      color: "#E84D66"
                                    },
                                    on: { click: _vm.updateManyAgents }
                                  },
                                  [_vm._v("Save")]
                                )
                              ],
                              1
                            )
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "item.hcc",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c("v-simple-checkbox", {
                              on: { input: _vm.add_table_agents },
                              model: {
                                value: item.hcc,
                                callback: function($$v) {
                                  _vm.$set(item, "hcc", $$v)
                                },
                                expression: "item.hcc"
                              }
                            })
                          ]
                        }
                      },
                      {
                        key: "item.active",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c("v-simple-checkbox", {
                              model: {
                                value: item.active,
                                callback: function($$v) {
                                  _vm.$set(item, "active", $$v)
                                },
                                expression: "item.active"
                              }
                            })
                          ]
                        }
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=template&id=309ab75f&":
/*!***********************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=template&id=309ab75f& ***!
  \***********************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-dialog",
    {
      attrs: { "max-width": "700px", persistent: "" },
      model: {
        value: _vm.active,
        callback: function($$v) {
          _vm.active = $$v
        },
        expression: "active"
      }
    },
    [
      _c(
        "v-card",
        [
          _c("v-card-title", [
            _c("span", { staticClass: "headline" }, [
              _vm._v(_vm._s(_vm.value == 0 ? "New" : "Update"))
            ])
          ]),
          _vm._v(" "),
          _c(
            "v-card-text",
            [
              _c(
                "v-container",
                [
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "4" } },
                        [
                          _c("v-text-field", {
                            attrs: { label: "Name" },
                            model: {
                              value: _vm.data.name,
                              callback: function($$v) {
                                _vm.$set(_vm.data, "name", $$v)
                              },
                              expression: "data.name"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "4" } },
                        [
                          _c(
                            "v-radio-group",
                            {
                              attrs: { mandatory: false },
                              model: {
                                value: _vm.data.final,
                                callback: function($$v) {
                                  _vm.$set(_vm.data, "final", $$v)
                                },
                                expression: "data.final"
                              }
                            },
                            [
                              _c("v-radio", {
                                attrs: { label: "Recycle", value: "recycle" }
                              }),
                              _vm._v(" "),
                              _c("v-radio", {
                                attrs: { label: "Final", value: "final" }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "4" } },
                        [
                          _c(
                            "v-radio-group",
                            {
                              attrs: { mandatory: false },
                              model: {
                                value: _vm.data.contact,
                                callback: function($$v) {
                                  _vm.$set(_vm.data, "contact", $$v)
                                },
                                expression: "data.contact"
                              }
                            },
                            [
                              _c("v-radio", {
                                attrs: {
                                  label: "No Contact",
                                  value: "no contact"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-radio", {
                                attrs: { label: "Contact", value: "contact" }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "4" } },
                        [
                          _c(
                            "v-radio-group",
                            {
                              attrs: { mandatory: false },
                              model: {
                                value: _vm.data.effective,
                                callback: function($$v) {
                                  _vm.$set(_vm.data, "effective", $$v)
                                },
                                expression: "data.effective"
                              }
                            },
                            [
                              _c("v-radio", {
                                attrs: {
                                  label: "Not Effective",
                                  value: "not effective"
                                }
                              }),
                              _vm._v(" "),
                              _c("v-radio", {
                                attrs: {
                                  label: "Effective",
                                  value: "effective"
                                }
                              })
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "4" } },
                        [
                          _c("v-checkbox", {
                            attrs: { label: "Pipeline" },
                            model: {
                              value: _vm.data.pipeline,
                              callback: function($$v) {
                                _vm.$set(_vm.data, "pipeline", $$v)
                              },
                              expression: "data.pipeline"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "4" } },
                        [
                          _c("v-checkbox", {
                            attrs: { label: "Decline" },
                            model: {
                              value: _vm.data.decline,
                              callback: function($$v) {
                                _vm.$set(_vm.data, "decline", $$v)
                              },
                              expression: "data.decline"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        { attrs: { cols: "12", sm: "12", md: "4" } },
                        [
                          _c("v-checkbox", {
                            attrs: { label: "Primary" },
                            model: {
                              value: _vm.data.primary,
                              callback: function($$v) {
                                _vm.$set(_vm.data, "primary", $$v)
                              },
                              expression: "data.primary"
                            }
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-card-actions",
            [
              _c("v-spacer"),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { outlined: "" },
                  on: {
                    click: function($event) {
                      return _vm.$emit("dialog:change", "cerrar")
                    }
                  }
                },
                [_vm._v("Cancel")]
              ),
              _vm._v(" "),
              _c(
                "v-btn",
                {
                  attrs: { color: "#E63E59", outlined: "" },
                  on: { click: _vm.registerDisposition }
                },
                [_vm._v("Save")]
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=template&id=2ed3f047&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=template&id=2ed3f047& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            [
              _c(
                "v-card",
                { staticClass: "shadow-large round" },
                [
                  _c("v-data-table", {
                    staticClass: "elevation-0",
                    attrs: {
                      headers: _vm.headers,
                      items: _vm.DispositionValues,
                      "sort-by": "calories",
                      "hide-default-footer": "",
                      "disable-pagination": ""
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "top",
                        fn: function() {
                          return [
                            _c(
                              "v-toolbar",
                              { attrs: { flat: "", color: "transparent" } },
                              [
                                _c(
                                  "v-toolbar-title",
                                  { staticClass: "text-accent" },
                                  [
                                    _vm._v(
                                      _vm._s(
                                        _vm.NameCampaigns.name == undefined
                                          ? "Select campaign"
                                          : _vm.NameCampaigns.name
                                      ) + " - Dispositions"
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c("v-divider", {
                                  staticClass: "mx-4",
                                  attrs: { inset: "", vertical: "" }
                                }),
                                _vm._v(" "),
                                _c(
                                  "v-btn",
                                  {
                                    staticClass: "mx-2",
                                    attrs: {
                                      outlined: "",
                                      loading: _vm.loading,
                                      dark: "",
                                      color: "#E84D66"
                                    },
                                    on: { click: _vm.getDisposition }
                                  },
                                  [_c("v-icon", [_vm._v("mdi-sync")])],
                                  1
                                ),
                                _vm._v(" "),
                                _c("v-spacer"),
                                _vm._v(" "),
                                _c(
                                  "v-btn",
                                  {
                                    staticClass: "mb-2",
                                    attrs: {
                                      outlined: "",
                                      color: "#E63E59",
                                      dark: ""
                                    },
                                    on: { click: _vm.HandlerRegister }
                                  },
                                  [
                                    _c("v-icon", { staticClass: "mr-3" }, [
                                      _vm._v("mdi-account-plus")
                                    ]),
                                    _vm._v("New Disposition\n              ")
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c("CreateDisposition", {
                                  attrs: {
                                    active: _vm.active,
                                    data: _vm.dispositionValue,
                                    value: _vm.value
                                  },
                                  on: { "dialog:change": _vm.eventdialog }
                                })
                              ],
                              1
                            )
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "item.name",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c("span", { staticClass: "text-right" }, [
                              _vm._v(_vm._s(item.name))
                            ])
                          ]
                        }
                      },
                      {
                        key: "item.primary",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _vm._v(_vm._s(_vm._f("primary")(item.primary)))
                          ]
                        }
                      },
                      {
                        key: "item.pipeline",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _vm._v(_vm._s(_vm._f("pipeline")(item.pipeline)))
                          ]
                        }
                      },
                      {
                        key: "item.decline",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _vm._v(_vm._s(_vm._f("decline")(item.decline)))
                          ]
                        }
                      },
                      {
                        key: "item.action",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c(
                              "v-icon",
                              {
                                staticClass: "mr-12",
                                on: {
                                  click: function($event) {
                                    return _vm.HandlerEdit(item)
                                  }
                                }
                              },
                              [_vm._v("mdi-pencil")]
                            ),
                            _vm._v(" "),
                            _c(
                              "v-icon",
                              {
                                on: {
                                  click: function($event) {
                                    return _vm.HandlerDelete(item)
                                  }
                                }
                              },
                              [_vm._v("mdi-delete")]
                            )
                          ]
                        }
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=template&id=00b1f9a2&":
/*!***********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R3/Scorecard.vue?vue&type=template&id=00b1f9a2& ***!
  \***********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "pt-0", attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        { staticClass: "py-0" },
        [
          _c(
            "v-col",
            { staticClass: "py-0", attrs: { cols: "12" } },
            [
              _c(
                "v-card",
                { attrs: { flat: "" } },
                [
                  _c(
                    "v-toolbar",
                    {
                      staticClass: "shadow-large round",
                      attrs: { flat: "", color: "white" }
                    },
                    [
                      _c("v-toolbar-title", { staticClass: "text-accent" }, [
                        _vm._v(
                          " " +
                            _vm._s(
                              _vm.NameCampaigns.name == undefined
                                ? "Select campaign"
                                : _vm.NameCampaigns.name
                            ) +
                            " - Scorecard"
                        )
                      ]),
                      _vm._v(" "),
                      _c("v-divider", {
                        staticClass: "mx-4",
                        attrs: { inset: "", vertical: "" }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-tooltip",
                        {
                          attrs: { bottom: "" },
                          scopedSlots: _vm._u([
                            {
                              key: "activator",
                              fn: function(ref) {
                                var on = ref.on
                                return [
                                  _c(
                                    "v-btn",
                                    _vm._g(
                                      {
                                        attrs: {
                                          absolute: "",
                                          dark: "",
                                          fab: "",
                                          top: "",
                                          right: "",
                                          color: "#E63E59"
                                        },
                                        on: { click: _vm.jsexport }
                                      },
                                      on
                                    ),
                                    [_c("v-icon", [_vm._v("mdi-download")])],
                                    1
                                  )
                                ]
                              }
                            }
                          ])
                        },
                        [_vm._v(" "), _c("span", [_vm._v("Download")])]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        { staticClass: "pt-0" },
        [
          _c(
            "v-col",
            { staticClass: "pt-0", attrs: { cols: "12" } },
            [
              _c(
                "v-app-bar",
                {
                  attrs: {
                    collapse: _vm.principal_section,
                    "collapse-on-scroll": "",
                    color: "#104961",
                    "scroll-target": "#scrolling-techniques-6",
                    short: ""
                  }
                },
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "font-weight-bold white--text text-center",
                      staticStyle: { "font-size": "16px" }
                    },
                    [
                      _vm._v(
                        "\n              " +
                          _vm._s(_vm.current_day()) +
                          "             \n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c("v-checkbox", {
                    attrs: { color: "white", "hide-details": "" },
                    model: {
                      value: _vm.principal_section,
                      callback: function($$v) {
                        _vm.principal_section = $$v
                      },
                      expression: "principal_section"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.principal_section
                ? _c("v-simple-table", {
                    attrs: { "fixed-header": "", height: "500" },
                    scopedSlots: _vm._u(
                      [
                        {
                          key: "default",
                          fn: function() {
                            return [
                              _c("thead", [
                                _c("tr", [
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [_vm._v("Total")]
                                  ),
                                  _vm._v(" "),
                                  _c("th", {
                                    staticClass: "text-left tam-header"
                                  }),
                                  _vm._v(" "),
                                  _c("th", {
                                    staticClass: "text-left tam-header"
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(
                                            _vm.table_today_total.total
                                              .total_hours
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        "\n                " +
                                          _vm._s(
                                            _vm.adherence_session(
                                              _vm.table_today_total.total
                                            )
                                          ) +
                                          " %\n                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.table_today_total.total
                                            .disposed_calls
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("parse_decimal")(
                                            _vm.table_today_total.total
                                              .calls_per_hour
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(
                                            _vm.table_today_total.total.talktime
                                          )
                                        )
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header",
                                      staticStyle: { width: "350px" }
                                    },
                                    [_vm._v("Agent")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Log In")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Log Out")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Session Time")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Adherence")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Dispo'ed Calls")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Calls per Hour")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Talktime")]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "tbody",
                                _vm._l(_vm.table_principal, function(item) {
                                  return _c("tr", { key: item.name }, [
                                    _c("td", [_vm._v(_vm._s(item.name))]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(_vm._f("hours_log")(item.login))
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(_vm._f("hours_log")(item.logout))
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(item.total_hours)
                                        )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("parse_int")(item.adherence)
                                        ) + " %"
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(_vm._s(item.disposed_calls))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("parse_decimal")(
                                            item.calls_per_hour
                                          )
                                        )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(_vm._f("hours")(item.talktime))
                                      )
                                    ])
                                  ])
                                }),
                                0
                              )
                            ]
                          },
                          proxy: true
                        }
                      ],
                      null,
                      false,
                      4192624771
                    )
                  })
                : _vm._e()
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        { staticClass: "pt-0" },
        [
          _c(
            "v-col",
            { staticClass: "pt-0", attrs: { cols: "12" } },
            [
              _c(
                "v-app-bar",
                {
                  attrs: {
                    collapse: _vm.first_fortnight_section,
                    "collapse-on-scroll": "",
                    color: "#104961",
                    "scroll-target": "#scrolling-techniques-6",
                    short: ""
                  }
                },
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "font-weight-bold white--text",
                      staticStyle: { "font-size": "16px" }
                    },
                    [
                      _vm._v(
                        "\n              " +
                          _vm._s(_vm.first_fortnight()) +
                          "      \n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c("v-checkbox", {
                    attrs: { color: "white", "hide-details": "" },
                    model: {
                      value: _vm.first_fortnight_section,
                      callback: function($$v) {
                        _vm.first_fortnight_section = $$v
                      },
                      expression: "first_fortnight_section"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.first_fortnight_section
                ? _c("v-simple-table", {
                    attrs: { "fixed-header": "", height: "500" },
                    scopedSlots: _vm._u(
                      [
                        {
                          key: "default",
                          fn: function() {
                            return [
                              _c("thead", [
                                _c("tr", [
                                  _c(
                                    "th",
                                    {
                                      staticClass: "text-left tam-header",
                                      staticStyle: { width: "300px" }
                                    },
                                    [_vm._v("Total")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(
                                            _vm.table_first_total.total
                                              .session > 0
                                              ? _vm.table_first_total.total
                                                  .session
                                              : 0
                                          ) +
                                          "\n                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(
                                            _vm.table_first_total.total
                                              .target_time > 0
                                              ? _vm.table_first_total.total
                                                  .target_time
                                              : 0
                                          ) +
                                          "\n                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(
                                            _vm.table_first_total.total
                                              .total_hours
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(
                                            _vm.adherence_session(
                                              _vm.table_first_total.total
                                            )
                                          ) +
                                          " %\n                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.table_first_total.total
                                            .disposed_calls
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("parse_decimal")(
                                            _vm.table_first_total.total
                                              .calls_per_hour
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(
                                            _vm.table_first_total.total.talktime
                                          )
                                        )
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header",
                                      staticStyle: { width: "350px" }
                                    },
                                    [_vm._v("Agent")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Sessions")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Target Time")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Session Time")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Adherence")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Dispo'ed Calls")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Calls per Hour")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Talktime")]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "tbody",
                                _vm._l(_vm.table_first, function(item) {
                                  return _c("tr", { key: item.name }, [
                                    _c("td", [_vm._v(_vm._s(item.name))]),
                                    _vm._v(" "),
                                    _c("td", [_vm._v(_vm._s(item.session))]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(_vm._s(item.target_time))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(item.total_hours)
                                        )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(_vm.adherence_session(item)) +
                                          " %"
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(_vm._s(item.disposed_calls))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("parse_decimal")(
                                            item.calls_per_hour
                                          )
                                        )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(_vm._f("hours")(item.talktime))
                                      )
                                    ])
                                  ])
                                }),
                                0
                              )
                            ]
                          },
                          proxy: true
                        }
                      ],
                      null,
                      false,
                      2053913196
                    )
                  })
                : _vm._e()
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "v-col",
            { staticClass: "pt-0", attrs: { cols: "12" } },
            [
              _c(
                "v-app-bar",
                {
                  attrs: {
                    collapse: _vm.second_fortnight_section,
                    "collapse-on-scroll": "",
                    color: "#104961",
                    "scroll-target": "#scrolling-techniques-6",
                    short: ""
                  }
                },
                [
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c(
                    "span",
                    {
                      staticClass: "font-weight-bold white--text",
                      staticStyle: { "font-size": "16px" }
                    },
                    [
                      _vm._v(
                        "\n              " +
                          _vm._s(_vm.second_fortnight()) +
                          "        \n            "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c("v-spacer"),
                  _vm._v(" "),
                  _c("v-checkbox", {
                    attrs: { color: "white", "hide-details": "" },
                    model: {
                      value: _vm.second_fortnight_section,
                      callback: function($$v) {
                        _vm.second_fortnight_section = $$v
                      },
                      expression: "second_fortnight_section"
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _vm.second_fortnight_section
                ? _c("v-simple-table", {
                    attrs: { "fixed-header": "", height: "500" },
                    scopedSlots: _vm._u(
                      [
                        {
                          key: "default",
                          fn: function() {
                            return [
                              _c("thead", [
                                _c("tr", [
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [_vm._v("Total")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(
                                            _vm.table_second_total.total
                                              .session > 0
                                              ? _vm.table_second_total.total
                                                  .session
                                              : 0
                                          ) +
                                          "\n                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(
                                            _vm.table_second_total.total
                                              .target_time > 0
                                              ? _vm.table_second_total.total
                                                  .target_time
                                              : 0
                                          ) +
                                          "\n                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(
                                            _vm.table_second_total.total
                                              .total_hours
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        "\n                  " +
                                          _vm._s(
                                            _vm.adherence_session(
                                              _vm.table_second_total.total
                                            )
                                          ) +
                                          " %\n                "
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm.table_second_total.total
                                            .disposed_calls
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("parse_decimal")(
                                            _vm.table_second_total.total
                                              .calls_per_hour
                                          )
                                        )
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    { staticClass: "text-left tam-header" },
                                    [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(
                                            _vm.table_second_total.total
                                              .talktime
                                          )
                                        )
                                      )
                                    ]
                                  )
                                ]),
                                _vm._v(" "),
                                _c("tr", [
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header",
                                      staticStyle: { width: "350px" }
                                    },
                                    [_vm._v("Agent")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Sessions")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Target Time")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Session Time")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Adherence")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Dispo'ed Calls")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Calls per Hour")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "th",
                                    {
                                      staticClass:
                                        "text-left tr-color tam-header"
                                    },
                                    [_vm._v("Talktime")]
                                  )
                                ])
                              ]),
                              _vm._v(" "),
                              _c(
                                "tbody",
                                _vm._l(_vm.table_second, function(item) {
                                  return _c("tr", { key: item.name }, [
                                    _c("td", [_vm._v(_vm._s(item.name))]),
                                    _vm._v(" "),
                                    _c("td", [_vm._v(_vm._s(item.session))]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(_vm._s(item.target_time))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("hours")(item.total_hours)
                                        )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(_vm.adherence_session(item)) +
                                          " %"
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(_vm._s(item.disposed_calls))
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(
                                          _vm._f("parse_decimal")(
                                            item.calls_per_hour
                                          )
                                        )
                                      )
                                    ]),
                                    _vm._v(" "),
                                    _c("td", [
                                      _vm._v(
                                        _vm._s(_vm._f("hours")(item.talktime))
                                      )
                                    ])
                                  ])
                                }),
                                0
                              )
                            ]
                          },
                          proxy: true
                        }
                      ],
                      null,
                      false,
                      4220857908
                    )
                  })
                : _vm._e()
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=template&id=418e7c54&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/reports/R4/CallLog.vue?vue&type=template&id=418e7c54& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "pt-0", attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        { staticClass: "py-0" },
        [
          _c(
            "v-col",
            { staticClass: "py-0", attrs: { cols: "12" } },
            [
              _c(
                "v-card",
                { attrs: { flat: "" } },
                [
                  _c(
                    "v-toolbar",
                    {
                      staticClass: "shadow-large round",
                      attrs: { flat: "", color: "white" }
                    },
                    [
                      _c("v-toolbar-title", { staticClass: "text-accent" }, [
                        _vm._v(
                          "\n            " +
                            _vm._s(
                              _vm.NameCampaigns.name == undefined
                                ? "Select campaign"
                                : _vm.NameCampaigns.name
                            ) +
                            "\n            - Call Log\n          "
                        )
                      ]),
                      _vm._v(" "),
                      _c("v-divider", {
                        staticClass: "mx-4",
                        attrs: { inset: "", vertical: "" }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-tooltip",
                        {
                          attrs: { bottom: "" },
                          scopedSlots: _vm._u([
                            {
                              key: "activator",
                              fn: function(ref) {
                                var on = ref.on
                                return [
                                  _c(
                                    "v-btn",
                                    _vm._g(
                                      {
                                        attrs: {
                                          absolute: "",
                                          dark: "",
                                          fab: "",
                                          top: "",
                                          right: "",
                                          color: "#E63E59",
                                          loading: _vm.loading
                                        },
                                        on: { click: _vm.export_csv }
                                      },
                                      on
                                    ),
                                    [_c("v-icon", [_vm._v("mdi-download")])],
                                    1
                                  )
                                ]
                              }
                            }
                          ])
                        },
                        [_vm._v(" "), _c("span", [_vm._v("Download")])]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        { staticClass: "pt-0" },
        [
          _c(
            "v-col",
            { staticClass: "pt-0", attrs: { cols: "12" } },
            [
              _c(
                "v-app-bar",
                { attrs: { color: "#104961", short: "", flat: "" } },
                [
                  _c(
                    "span",
                    {
                      staticClass: "font-weight-bold white--text",
                      staticStyle: {
                        "font-size": "16px",
                        "margin-left": "210px"
                      }
                    },
                    [_vm._v(_vm._s(_vm.first_fortnight()))]
                  ),
                  _vm._v(" "),
                  _c("v-spacer")
                ],
                1
              ),
              _vm._v(" "),
              _c("v-data-table", {
                attrs: {
                  headers: _vm.Itemsheaders,
                  items: _vm.ItemsCallLog,
                  page: _vm.page,
                  "items-per-page": _vm.itemsPerPage,
                  "server-items-length": _vm.pagination.total,
                  options: _vm.options,
                  "hide-default-footer": "",
                  loading: _vm.loading,
                  "loading-text": "Loading... Please wait"
                },
                on: {
                  "update:page": function($event) {
                    _vm.page = $event
                  },
                  "update:options": function($event) {
                    _vm.options = $event
                  },
                  "page-count": function($event) {
                    _vm.pageCount = $event
                  }
                },
                scopedSlots: _vm._u([
                  {
                    key: "item.telephonic_data.call_time",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(
                          _vm._s(
                            _vm._f("format_hours")(
                              item.telephonic_data.call_time
                            )
                          )
                        )
                      ]
                    }
                  },
                  {
                    key: "item.telephonic_data.hold_time",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(
                          _vm._s(
                            _vm._f("format_hours")(
                              item.telephonic_data.hold_time
                            )
                          )
                        )
                      ]
                    }
                  },
                  {
                    key: "item.telephonic_data.talk_time",
                    fn: function(ref) {
                      var item = ref.item
                      return [
                        _vm._v(
                          _vm._s(
                            _vm._f("format_hours")(
                              item.telephonic_data.talk_time
                            )
                          )
                        )
                      ]
                    }
                  }
                ])
              }),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "text-center pt-2" },
                [
                  _c("v-pagination", {
                    attrs: {
                      circle: "",
                      length: _vm.pagination.page_count,
                      "total-visible": 7
                    },
                    model: {
                      value: _vm.page,
                      callback: function($$v) {
                        _vm.page = $$v
                      },
                      expression: "page"
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=template&id=47c214de&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/upload/upload_call_log.vue?vue&type=template&id=47c214de& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { staticClass: "pt-0", attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        { staticClass: "pt-0" },
        [
          _c(
            "v-col",
            { attrs: { cols: "12" } },
            [
              _c(
                "v-card",
                { attrs: { flat: "" } },
                [
                  _c(
                    "v-toolbar",
                    {
                      staticClass: "shadow-large round",
                      attrs: { flat: "", color: "white" }
                    },
                    [
                      _c("v-toolbar-title", { staticClass: "text-accent" }, [
                        _vm._v(
                          "\n            " +
                            _vm._s(
                              _vm.NameCampaigns.name == undefined
                                ? "Select campaign"
                                : _vm.NameCampaigns.name
                            ) +
                            "\n            - Upload Call Log\n          "
                        )
                      ]),
                      _vm._v(" "),
                      _c("v-divider", {
                        staticClass: "mx-4",
                        attrs: { inset: "", vertical: "" }
                      }),
                      _vm._v(" "),
                      _c(
                        "v-tooltip",
                        {
                          attrs: { bottom: "" },
                          scopedSlots: _vm._u([
                            {
                              key: "activator",
                              fn: function(ref) {
                                var on = ref.on
                                return [
                                  _c(
                                    "v-btn",
                                    _vm._g(
                                      {
                                        attrs: {
                                          loading: _vm.loading,
                                          absolute: "",
                                          dark: "",
                                          fab: "",
                                          top: "",
                                          right: "",
                                          color: "#E63E59"
                                        },
                                        on: { click: _vm.jsexport }
                                      },
                                      on
                                    ),
                                    [_c("v-icon", [_vm._v("mdi-download")])],
                                    1
                                  )
                                ]
                              }
                            }
                          ])
                        },
                        [_vm._v(" "), _c("span", [_vm._v("Download")])]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c("v-toolbar", {
                    staticClass: "elevation-0",
                    staticStyle: { "border-radius": "0px" },
                    attrs: { color: "#104961" }
                  }),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    { staticClass: "px-2" },
                    [
                      _c(
                        "v-col",
                        {
                          staticClass: "pr-0 pb-0",
                          attrs: { cols: "12", sm: "8", md: "10" }
                        },
                        [
                          _c("v-file-input", {
                            staticClass: "mx-3",
                            attrs: {
                              "prepend-icon": "",
                              chips: "",
                              outlined: "",
                              placeholder: "Select CSV file"
                            },
                            model: {
                              value: _vm.files,
                              callback: function($$v) {
                                _vm.files = $$v
                              },
                              expression: "files"
                            }
                          })
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-col",
                        {
                          staticClass: "pl-0 pt-4 pb-0",
                          attrs: { cols: "12", sm: "4", md: "2" }
                        },
                        [
                          _c(
                            "v-btn",
                            {
                              staticClass: "text-capitalize",
                              attrs: {
                                outlined: "",
                                color: "pink",
                                dark: "",
                                large: ""
                              },
                              on: { click: _vm.fileDoc }
                            },
                            [
                              _c("v-icon", { attrs: { color: "#104961" } }, [
                                _vm._v("mdi-upload")
                              ]),
                              _vm._v("Upload File\n            ")
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "v-dialog",
                        {
                          attrs: {
                            persistent: "",
                            "max-width": "300px",
                            transition: "dialog-transition"
                          },
                          model: {
                            value: _vm.loadingModal,
                            callback: function($$v) {
                              _vm.loadingModal = $$v
                            },
                            expression: "loadingModal"
                          }
                        },
                        [
                          _c(
                            "v-card",
                            {
                              attrs: { color: "#25A8E0", loading: "", dark: "" }
                            },
                            [
                              _c(
                                "v-card-title",
                                [
                                  _vm._v(
                                    "\n                Loading ...\n                "
                                  ),
                                  _c("v-progress-linear", {
                                    staticClass: "mb-3",
                                    attrs: { indeterminate: "", color: "white" }
                                  })
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        { staticClass: "pt-0" },
        [
          _c(
            "v-col",
            { attrs: { cols: "12" } },
            [
              _c(
                "v-card",
                { attrs: { flat: "" } },
                [
                  _c(
                    "v-toolbar",
                    {
                      staticClass: "elevation-0",
                      staticStyle: { "border-radius": "0px" },
                      attrs: { color: "#104961" }
                    },
                    [
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c(
                        "span",
                        {
                          staticClass: "font-weight-bold white--text",
                          staticStyle: { "font-size": "16px" }
                        },
                        [_vm._v("Daily information loaded into database")]
                      ),
                      _vm._v(" "),
                      _c("v-spacer"),
                      _vm._v(" "),
                      _c("v-spacer")
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "v-row",
                    [
                      _c(
                        "v-col",
                        { staticClass: "pt-0", attrs: { cols: "12" } },
                        [
                          _c("v-data-table", {
                            staticClass: "elevation-0",
                            attrs: {
                              headers: _vm.headers,
                              items: _vm.ItemsDailyInformation,
                              page: _vm.page,
                              "items-per-page": _vm.itemsPerPage,
                              "server-items-length": _vm.pagination.total,
                              options: _vm.options,
                              "hide-default-footer": ""
                            },
                            on: {
                              "update:page": function($event) {
                                _vm.page = $event
                              },
                              "update:options": function($event) {
                                _vm.options = $event
                              },
                              "page-count": function($event) {
                                _vm.pageCount = $event
                              }
                            },
                            scopedSlots: _vm._u([
                              {
                                key: "item.created_at",
                                fn: function(ref) {
                                  var item = ref.item
                                  return [
                                    _vm._v(
                                      _vm._s(
                                        _vm._f("hours_central")(item.created_at)
                                      )
                                    )
                                  ]
                                }
                              }
                            ])
                          })
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=template&id=38473b70&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/users/Index.vue?vue&type=template&id=38473b70& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            [
              _c(
                "v-card",
                { staticClass: "shadow-large round" },
                [
                  _c("v-data-table", {
                    staticClass: "elevation-0",
                    attrs: {
                      headers: _vm.headers,
                      items: _vm.ItemsUsers,
                      "sort-by": "calories",
                      "hide-default-footer": "",
                      "disable-pagination": ""
                    },
                    on: { "click:row": _vm.rowClick },
                    scopedSlots: _vm._u([
                      {
                        key: "top",
                        fn: function() {
                          return [
                            _c(
                              "v-toolbar",
                              { attrs: { flat: "", color: "transparent" } },
                              [
                                _c(
                                  "v-toolbar-title",
                                  { staticClass: "text-accent" },
                                  [_vm._v("Users")]
                                ),
                                _vm._v(" "),
                                _c("v-divider", {
                                  staticClass: "mx-4",
                                  attrs: { inset: "", vertical: "" }
                                }),
                                _vm._v(" "),
                                _c("v-spacer"),
                                _vm._v(" "),
                                _c(
                                  "v-dialog",
                                  {
                                    attrs: { "max-width": "700px" },
                                    scopedSlots: _vm._u([
                                      {
                                        key: "activator",
                                        fn: function(ref) {
                                          var on = ref.on
                                          return [
                                            _c(
                                              "v-btn",
                                              _vm._g(
                                                {
                                                  staticClass: "mb-2",
                                                  attrs: {
                                                    outlined: "",
                                                    color: "#E63E59",
                                                    dark: ""
                                                  }
                                                },
                                                on
                                              ),
                                              [
                                                _c(
                                                  "v-icon",
                                                  { staticClass: "mr-3" },
                                                  [_vm._v("mdi-account-plus")]
                                                ),
                                                _vm._v(
                                                  "New User\n                  "
                                                )
                                              ],
                                              1
                                            )
                                          ]
                                        }
                                      }
                                    ]),
                                    model: {
                                      value: _vm.dialog,
                                      callback: function($$v) {
                                        _vm.dialog = $$v
                                      },
                                      expression: "dialog"
                                    }
                                  },
                                  [
                                    _vm._v(" "),
                                    _c("ValidationObserver", {
                                      attrs: { tag: "form" },
                                      scopedSlots: _vm._u([
                                        {
                                          key: "default",
                                          fn: function(ref) {
                                            var passes = ref.passes
                                            return [
                                              _c(
                                                "v-card",
                                                [
                                                  _c("v-card-title", [
                                                    _c(
                                                      "span",
                                                      {
                                                        staticClass: "headline"
                                                      },
                                                      [
                                                        _vm._v(
                                                          _vm._s(_vm.formTitle)
                                                        )
                                                      ]
                                                    )
                                                  ]),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-card-text",
                                                    [
                                                      _c(
                                                        "v-container",
                                                        [
                                                          _c(
                                                            "v-row",
                                                            [
                                                              _c(
                                                                "v-col",
                                                                {
                                                                  attrs: {
                                                                    cols: "12",
                                                                    sm: "12",
                                                                    md: "4"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "ValidationProvider",
                                                                    {
                                                                      attrs: {
                                                                        name:
                                                                          "name",
                                                                        rules:
                                                                          "required"
                                                                      },
                                                                      scopedSlots: _vm._u(
                                                                        [
                                                                          {
                                                                            key:
                                                                              "default",
                                                                            fn: function(
                                                                              ref
                                                                            ) {
                                                                              var errors =
                                                                                ref.errors
                                                                              return [
                                                                                _c(
                                                                                  "v-text-field",
                                                                                  {
                                                                                    attrs: {
                                                                                      label:
                                                                                        "Name",
                                                                                      "error-messages": errors
                                                                                    },
                                                                                    model: {
                                                                                      value:
                                                                                        _vm
                                                                                          .editedItem
                                                                                          .name,
                                                                                      callback: function(
                                                                                        $$v
                                                                                      ) {
                                                                                        _vm.$set(
                                                                                          _vm.editedItem,
                                                                                          "name",
                                                                                          $$v
                                                                                        )
                                                                                      },
                                                                                      expression:
                                                                                        "editedItem.name"
                                                                                    }
                                                                                  }
                                                                                )
                                                                              ]
                                                                            }
                                                                          }
                                                                        ],
                                                                        null,
                                                                        true
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              ),
                                                              _vm._v(" "),
                                                              _c(
                                                                "v-col",
                                                                {
                                                                  attrs: {
                                                                    cols: "12",
                                                                    sm: "12",
                                                                    md: "8"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "ValidationProvider",
                                                                    {
                                                                      attrs: {
                                                                        name:
                                                                          "email",
                                                                        rules:
                                                                          "email|required"
                                                                      },
                                                                      scopedSlots: _vm._u(
                                                                        [
                                                                          {
                                                                            key:
                                                                              "default",
                                                                            fn: function(
                                                                              ref
                                                                            ) {
                                                                              var errors =
                                                                                ref.errors
                                                                              return [
                                                                                _c(
                                                                                  "v-text-field",
                                                                                  {
                                                                                    attrs: {
                                                                                      label:
                                                                                        "Email",
                                                                                      "error-messages": errors
                                                                                    },
                                                                                    model: {
                                                                                      value:
                                                                                        _vm
                                                                                          .editedItem
                                                                                          .email,
                                                                                      callback: function(
                                                                                        $$v
                                                                                      ) {
                                                                                        _vm.$set(
                                                                                          _vm.editedItem,
                                                                                          "email",
                                                                                          $$v
                                                                                        )
                                                                                      },
                                                                                      expression:
                                                                                        "editedItem.email"
                                                                                    }
                                                                                  }
                                                                                )
                                                                              ]
                                                                            }
                                                                          }
                                                                        ],
                                                                        null,
                                                                        true
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              ),
                                                              _vm._v(" "),
                                                              _vm.edit === false
                                                                ? _c(
                                                                    "v-col",
                                                                    {
                                                                      attrs: {
                                                                        cols:
                                                                          "12",
                                                                        sm:
                                                                          "12",
                                                                        md: "4"
                                                                      }
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "ValidationProvider",
                                                                        {
                                                                          attrs: {
                                                                            name:
                                                                              "password",
                                                                            rules:
                                                                              "confirmed:confirmation|required"
                                                                          },
                                                                          scopedSlots: _vm._u(
                                                                            [
                                                                              {
                                                                                key:
                                                                                  "default",
                                                                                fn: function(
                                                                                  ref
                                                                                ) {
                                                                                  var errors =
                                                                                    ref.errors
                                                                                  return [
                                                                                    _c(
                                                                                      "v-text-field",
                                                                                      {
                                                                                        attrs: {
                                                                                          label:
                                                                                            "Password",
                                                                                          type:
                                                                                            "password",
                                                                                          "error-messages": errors
                                                                                        },
                                                                                        model: {
                                                                                          value:
                                                                                            _vm.password,
                                                                                          callback: function(
                                                                                            $$v
                                                                                          ) {
                                                                                            _vm.password = $$v
                                                                                          },
                                                                                          expression:
                                                                                            "password"
                                                                                        }
                                                                                      }
                                                                                    )
                                                                                  ]
                                                                                }
                                                                              }
                                                                            ],
                                                                            null,
                                                                            true
                                                                          )
                                                                        }
                                                                      )
                                                                    ],
                                                                    1
                                                                  )
                                                                : _vm._e(),
                                                              _vm._v(" "),
                                                              _vm.edit === false
                                                                ? _c(
                                                                    "v-col",
                                                                    {
                                                                      attrs: {
                                                                        cols:
                                                                          "12",
                                                                        sm:
                                                                          "12",
                                                                        md: "4"
                                                                      }
                                                                    },
                                                                    [
                                                                      _c(
                                                                        "ValidationProvider",
                                                                        {
                                                                          attrs: {
                                                                            name:
                                                                              "confirm password",
                                                                            rules:
                                                                              "required",
                                                                            vid:
                                                                              "confirmation"
                                                                          },
                                                                          scopedSlots: _vm._u(
                                                                            [
                                                                              {
                                                                                key:
                                                                                  "default",
                                                                                fn: function(
                                                                                  ref
                                                                                ) {
                                                                                  var errors =
                                                                                    ref.errors
                                                                                  return [
                                                                                    _c(
                                                                                      "v-text-field",
                                                                                      {
                                                                                        attrs: {
                                                                                          label:
                                                                                            "Confirm Password",
                                                                                          type:
                                                                                            "password",
                                                                                          "error-messages": errors
                                                                                        },
                                                                                        model: {
                                                                                          value:
                                                                                            _vm.confirm_password,
                                                                                          callback: function(
                                                                                            $$v
                                                                                          ) {
                                                                                            _vm.confirm_password = $$v
                                                                                          },
                                                                                          expression:
                                                                                            "confirm_password"
                                                                                        }
                                                                                      }
                                                                                    )
                                                                                  ]
                                                                                }
                                                                              }
                                                                            ],
                                                                            null,
                                                                            true
                                                                          )
                                                                        }
                                                                      )
                                                                    ],
                                                                    1
                                                                  )
                                                                : _vm._e(),
                                                              _vm._v(" "),
                                                              _c(
                                                                "v-col",
                                                                {
                                                                  attrs: {
                                                                    cols: "12",
                                                                    sm: "12",
                                                                    md: "4"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "ValidationProvider",
                                                                    {
                                                                      attrs: {
                                                                        name:
                                                                          "role",
                                                                        rules:
                                                                          "required"
                                                                      },
                                                                      scopedSlots: _vm._u(
                                                                        [
                                                                          {
                                                                            key:
                                                                              "default",
                                                                            fn: function(
                                                                              ref
                                                                            ) {
                                                                              var errors =
                                                                                ref.errors
                                                                              return [
                                                                                _c(
                                                                                  "v-select",
                                                                                  {
                                                                                    attrs: {
                                                                                      label:
                                                                                        "Role",
                                                                                      items:
                                                                                        _vm.ItemsRol,
                                                                                      "item-text":
                                                                                        "name",
                                                                                      "item-value":
                                                                                        "id",
                                                                                      "error-messages": errors
                                                                                    },
                                                                                    model: {
                                                                                      value:
                                                                                        _vm
                                                                                          .editedItem
                                                                                          .rol,
                                                                                      callback: function(
                                                                                        $$v
                                                                                      ) {
                                                                                        _vm.$set(
                                                                                          _vm.editedItem,
                                                                                          "rol",
                                                                                          $$v
                                                                                        )
                                                                                      },
                                                                                      expression:
                                                                                        "editedItem.rol"
                                                                                    }
                                                                                  }
                                                                                )
                                                                              ]
                                                                            }
                                                                          }
                                                                        ],
                                                                        null,
                                                                        true
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              ),
                                                              _vm._v(" "),
                                                              _c(
                                                                "v-col",
                                                                {
                                                                  attrs: {
                                                                    cols: "12",
                                                                    sm: "12",
                                                                    md: "6"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "ValidationProvider",
                                                                    {
                                                                      attrs: {
                                                                        name:
                                                                          "campaigns",
                                                                        rules: {
                                                                          required:
                                                                            (_vm.edit ===
                                                                              false &&
                                                                              _vm
                                                                                .editedItem
                                                                                .rol >
                                                                                1) ||
                                                                            (_vm.edit ===
                                                                              true &&
                                                                              _vm
                                                                                .editedItem
                                                                                .rol ==
                                                                                3)
                                                                              ? true
                                                                              : false
                                                                        }
                                                                      },
                                                                      scopedSlots: _vm._u(
                                                                        [
                                                                          {
                                                                            key:
                                                                              "default",
                                                                            fn: function(
                                                                              ref
                                                                            ) {
                                                                              var errors =
                                                                                ref.errors
                                                                              return [
                                                                                (_vm.edit ===
                                                                                  false &&
                                                                                  _vm
                                                                                    .editedItem
                                                                                    .rol >
                                                                                    1) ||
                                                                                (_vm.edit ===
                                                                                  true &&
                                                                                  _vm
                                                                                    .editedItem
                                                                                    .rol ==
                                                                                    2) ||
                                                                                (_vm.edit ===
                                                                                  true &&
                                                                                  _vm
                                                                                    .editedItem
                                                                                    .rol ==
                                                                                    3)
                                                                                  ? _c(
                                                                                      "v-select",
                                                                                      {
                                                                                        attrs: {
                                                                                          multiple:
                                                                                            "",
                                                                                          label:
                                                                                            "Campaigns",
                                                                                          "small-chips":
                                                                                            "",
                                                                                          items:
                                                                                            _vm.Itemscampaigns,
                                                                                          "item-text":
                                                                                            "name",
                                                                                          "item-value":
                                                                                            "id",
                                                                                          "error-messages": errors
                                                                                        },
                                                                                        model: {
                                                                                          value:
                                                                                            _vm
                                                                                              .editedItem
                                                                                              .campaign_id,
                                                                                          callback: function(
                                                                                            $$v
                                                                                          ) {
                                                                                            _vm.$set(
                                                                                              _vm.editedItem,
                                                                                              "campaign_id",
                                                                                              $$v
                                                                                            )
                                                                                          },
                                                                                          expression:
                                                                                            "editedItem.campaign_id"
                                                                                        }
                                                                                      }
                                                                                    )
                                                                                  : _vm._e()
                                                                              ]
                                                                            }
                                                                          }
                                                                        ],
                                                                        null,
                                                                        true
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              ),
                                                              _vm._v(" "),
                                                              _c(
                                                                "v-col",
                                                                {
                                                                  attrs: {
                                                                    cols: "12",
                                                                    sm: "12",
                                                                    md: "6"
                                                                  }
                                                                },
                                                                [
                                                                  _c(
                                                                    "ValidationProvider",
                                                                    {
                                                                      attrs: {
                                                                        name:
                                                                          "permissions",
                                                                        rules: {
                                                                          required:
                                                                            (_vm.edit ===
                                                                              false &&
                                                                              _vm
                                                                                .editedItem
                                                                                .rol >
                                                                                2) ||
                                                                            (_vm.edit ===
                                                                              true &&
                                                                              _vm
                                                                                .editedItem
                                                                                .rol ==
                                                                                3)
                                                                              ? true
                                                                              : false
                                                                        }
                                                                      },
                                                                      scopedSlots: _vm._u(
                                                                        [
                                                                          {
                                                                            key:
                                                                              "default",
                                                                            fn: function(
                                                                              ref
                                                                            ) {
                                                                              var errors =
                                                                                ref.errors
                                                                              return [
                                                                                (_vm.edit ===
                                                                                  false &&
                                                                                  _vm
                                                                                    .editedItem
                                                                                    .rol >
                                                                                    2) ||
                                                                                (_vm.edit ===
                                                                                  true &&
                                                                                  _vm
                                                                                    .editedItem
                                                                                    .rol ==
                                                                                    3)
                                                                                  ? _c(
                                                                                      "v-select",
                                                                                      {
                                                                                        attrs: {
                                                                                          multiple:
                                                                                            "",
                                                                                          label:
                                                                                            "Permissions",
                                                                                          "small-chips":
                                                                                            "",
                                                                                          "menu-props":
                                                                                            "auto",
                                                                                          "single-line":
                                                                                            "",
                                                                                          items:
                                                                                            _vm.ItemsPermission,
                                                                                          "item-text":
                                                                                            "name",
                                                                                          "item-value":
                                                                                            "id",
                                                                                          "error-messages": errors
                                                                                        },
                                                                                        scopedSlots: _vm._u(
                                                                                          [
                                                                                            {
                                                                                              key:
                                                                                                "selection",
                                                                                              fn: function(
                                                                                                ref
                                                                                              ) {
                                                                                                var item =
                                                                                                  ref.item
                                                                                                var index =
                                                                                                  ref.index
                                                                                                return [
                                                                                                  _c(
                                                                                                    "v-chip",
                                                                                                    {
                                                                                                      attrs: {
                                                                                                        small:
                                                                                                          ""
                                                                                                      }
                                                                                                    },
                                                                                                    [
                                                                                                      _c(
                                                                                                        "span",
                                                                                                        [
                                                                                                          _vm._v(
                                                                                                            _vm._s(
                                                                                                              _vm._f(
                                                                                                                "permission_name"
                                                                                                              )(
                                                                                                                item.name
                                                                                                              )
                                                                                                            )
                                                                                                          )
                                                                                                        ]
                                                                                                      )
                                                                                                    ]
                                                                                                  )
                                                                                                ]
                                                                                              }
                                                                                            },
                                                                                            {
                                                                                              key:
                                                                                                "item",
                                                                                              fn: function(
                                                                                                ref
                                                                                              ) {
                                                                                                var item =
                                                                                                  ref.item
                                                                                                var index =
                                                                                                  ref.index
                                                                                                return [
                                                                                                  _c(
                                                                                                    "span",
                                                                                                    [
                                                                                                      _vm._v(
                                                                                                        _vm._s(
                                                                                                          _vm._f(
                                                                                                            "permission_name"
                                                                                                          )(
                                                                                                            item.name
                                                                                                          )
                                                                                                        )
                                                                                                      )
                                                                                                    ]
                                                                                                  )
                                                                                                ]
                                                                                              }
                                                                                            }
                                                                                          ],
                                                                                          null,
                                                                                          true
                                                                                        ),
                                                                                        model: {
                                                                                          value:
                                                                                            _vm
                                                                                              .editedItem
                                                                                              .permission_id,
                                                                                          callback: function(
                                                                                            $$v
                                                                                          ) {
                                                                                            _vm.$set(
                                                                                              _vm.editedItem,
                                                                                              "permission_id",
                                                                                              $$v
                                                                                            )
                                                                                          },
                                                                                          expression:
                                                                                            "editedItem.permission_id"
                                                                                        }
                                                                                      }
                                                                                    )
                                                                                  : _vm._e()
                                                                              ]
                                                                            }
                                                                          }
                                                                        ],
                                                                        null,
                                                                        true
                                                                      )
                                                                    }
                                                                  )
                                                                ],
                                                                1
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  ),
                                                  _vm._v(" "),
                                                  _c(
                                                    "v-card-actions",
                                                    [
                                                      _c("v-spacer"),
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-btn",
                                                        {
                                                          attrs: {
                                                            outlined: ""
                                                          },
                                                          on: {
                                                            click: _vm.close
                                                          }
                                                        },
                                                        [_vm._v("Cancel")]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-btn",
                                                        {
                                                          attrs: {
                                                            color: "#E63E59",
                                                            outlined: ""
                                                          },
                                                          on: {
                                                            click: function(
                                                              $event
                                                            ) {
                                                              return passes(
                                                                _vm.save
                                                              )
                                                            }
                                                          }
                                                        },
                                                        [_vm._v("Save")]
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              )
                                            ]
                                          }
                                        }
                                      ])
                                    })
                                  ],
                                  1
                                )
                              ],
                              1
                            )
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "item.action",
                        fn: function(ref) {
                          var item = ref.item
                          return [
                            _c(
                              "v-icon",
                              {
                                staticClass: "mr-12",
                                on: {
                                  click: function($event) {
                                    return _vm.editItem(item)
                                  }
                                }
                              },
                              [_vm._v("mdi-pencil")]
                            ),
                            _vm._v(" "),
                            _vm.userId != item.id
                              ? _c(
                                  "v-icon",
                                  {
                                    on: {
                                      click: function($event) {
                                        return _vm.deleteItem(item)
                                      }
                                    }
                                  },
                                  [_vm._v("mdi-delete")]
                                )
                              : _vm._e()
                          ]
                        }
                      }
                    ])
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/audit/Index.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/audit/Index.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_58015d83___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=58015d83& */ "./resources/js/components/audit/Index.vue?vue&type=template&id=58015d83&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/components/audit/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_58015d83___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_58015d83___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/audit/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/audit/Index.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/audit/Index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/audit/Index.vue?vue&type=template&id=58015d83&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/audit/Index.vue?vue&type=template&id=58015d83& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_58015d83___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=58015d83& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/audit/Index.vue?vue&type=template&id=58015d83&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_58015d83___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_58015d83___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/campaigns/Index.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/campaigns/Index.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_69bf9a8b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=69bf9a8b& */ "./resources/js/components/campaigns/Index.vue?vue&type=template&id=69bf9a8b&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/components/campaigns/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_69bf9a8b___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_69bf9a8b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/campaigns/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/campaigns/Index.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/campaigns/Index.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/campaigns/Index.vue?vue&type=template&id=69bf9a8b&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/campaigns/Index.vue?vue&type=template&id=69bf9a8b& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_69bf9a8b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=69bf9a8b& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/campaigns/Index.vue?vue&type=template&id=69bf9a8b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_69bf9a8b___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_69bf9a8b___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/layout/Dashboard.vue":
/*!******************************************************!*\
  !*** ./resources/js/components/layout/Dashboard.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_0a813c9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=0a813c9a& */ "./resources/js/components/layout/Dashboard.vue?vue&type=template&id=0a813c9a&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/components/layout/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_0a813c9a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_0a813c9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/layout/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/layout/Dashboard.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/layout/Dashboard.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/layout/Dashboard.vue?vue&type=template&id=0a813c9a&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/layout/Dashboard.vue?vue&type=template&id=0a813c9a& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_0a813c9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=0a813c9a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/layout/Dashboard.vue?vue&type=template&id=0a813c9a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_0a813c9a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_0a813c9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R1/R1.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/reports/R1/R1.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R1_vue_vue_type_template_id_4361f804___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R1.vue?vue&type=template&id=4361f804& */ "./resources/js/components/reports/R1/R1.vue?vue&type=template&id=4361f804&");
/* harmony import */ var _R1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./R1.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R1/R1.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _R1_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./R1.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _R1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _R1_vue_vue_type_template_id_4361f804___WEBPACK_IMPORTED_MODULE_0__["render"],
  _R1_vue_vue_type_template_id_4361f804___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R1/R1.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R1/R1.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/reports/R1/R1.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R1/R1.vue?vue&type=template&id=4361f804&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/reports/R1/R1.vue?vue&type=template&id=4361f804& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_template_id_4361f804___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1.vue?vue&type=template&id=4361f804& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1.vue?vue&type=template&id=4361f804&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_template_id_4361f804___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_vue_vue_type_template_id_4361f804___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R1/R1_d.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/reports/R1/R1_d.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R1_d_vue_vue_type_template_id_4543de69___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R1_d.vue?vue&type=template&id=4543de69& */ "./resources/js/components/reports/R1/R1_d.vue?vue&type=template&id=4543de69&");
/* harmony import */ var _R1_d_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./R1_d.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R1/R1_d.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _R1_d_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./R1_d.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _R1_d_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _R1_d_vue_vue_type_template_id_4543de69___WEBPACK_IMPORTED_MODULE_0__["render"],
  _R1_d_vue_vue_type_template_id_4543de69___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R1/R1_d.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R1/R1_d.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/reports/R1/R1_d.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1_d.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css& ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1_d.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R1/R1_d.vue?vue&type=template&id=4543de69&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/reports/R1/R1_d.vue?vue&type=template&id=4543de69& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_template_id_4543de69___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R1_d.vue?vue&type=template&id=4543de69& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R1/R1_d.vue?vue&type=template&id=4543de69&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_template_id_4543de69___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R1_d_vue_vue_type_template_id_4543de69___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R2/R2.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/reports/R2/R2.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R2_vue_vue_type_template_id_a95d8738___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R2.vue?vue&type=template&id=a95d8738& */ "./resources/js/components/reports/R2/R2.vue?vue&type=template&id=a95d8738&");
/* harmony import */ var _R2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./R2.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R2/R2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _R2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./R2.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _R2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _R2_vue_vue_type_template_id_a95d8738___WEBPACK_IMPORTED_MODULE_0__["render"],
  _R2_vue_vue_type_template_id_a95d8738___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R2/R2.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R2/R2.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css& ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2.vue?vue&type=template&id=a95d8738&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2.vue?vue&type=template&id=a95d8738& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_template_id_a95d8738___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2.vue?vue&type=template&id=a95d8738& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2.vue?vue&type=template&id=a95d8738&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_template_id_a95d8738___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_vue_vue_type_template_id_a95d8738___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_R2TableReport_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/style-loader!../../../../../../node_modules/css-loader??ref--6-1!../../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../../node_modules/postcss-loader/src??ref--6-2!./R2TableReport.css?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_R2TableReport_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_R2TableReport_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_R2TableReport_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_R2TableReport_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_R2TableReport_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.js?vue&type=script&lang=js&":
/*!****************************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/R2TableReport.js?vue&type=script&lang=js& ***!
  \****************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_R2TableReport_js_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/babel-loader/lib??ref--4-0!./R2TableReport.js?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.js?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_R2TableReport_js_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue":
/*!****************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R2TableReport_vue_vue_type_template_id_3412993e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R2TableReport.vue?vue&type=template&id=3412993e& */ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue?vue&type=template&id=3412993e&");
/* harmony import */ var _R2TableReport_js_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./R2TableReport.js?vue&type=script&lang=js& */ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.js?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _R2TableReport_css_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./R2TableReport.css?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.css?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _R2TableReport_js_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _R2TableReport_vue_vue_type_template_id_3412993e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _R2TableReport_vue_vue_type_template_id_3412993e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R2/R2TableReport/R2TableReport.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue?vue&type=template&id=3412993e&":
/*!***********************************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue?vue&type=template&id=3412993e& ***!
  \***********************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2TableReport_vue_vue_type_template_id_3412993e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2TableReport.vue?vue&type=template&id=3412993e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue?vue&type=template&id=3412993e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2TableReport_vue_vue_type_template_id_3412993e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2TableReport_vue_vue_type_template_id_3412993e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/__tests__/agents.js":
/*!******************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/__tests__/agents.js ***!
  \******************************************************************************/
/*! exports provided: fakeAgents */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fakeAgents", function() { return fakeAgents; });
var fakeAgents = [{
  "id": 1,
  "name": "Manny Landeros",
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45"
}, {
  "id": 2,
  "name": "Louis Smith",
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45"
}, {
  "id": 3,
  "name": "Kevin West",
  "created_at": "2020-04-02 18:17:46",
  "updated_at": "2020-04-02 18:17:46"
}, {
  "id": 4,
  "name": "Arthur Martinez",
  "created_at": "2020-04-02 18:17:52",
  "updated_at": "2020-04-02 18:17:52"
}, {
  "id": 5,
  "name": "Jake Wolf",
  "created_at": "2020-04-02 18:17:56",
  "updated_at": "2020-04-02 18:17:56"
}];

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/__tests__/dispositions.js":
/*!************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/__tests__/dispositions.js ***!
  \************************************************************************************/
/*! exports provided: fakeDispositions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fakeDispositions", function() { return fakeDispositions; });
var fakeDispositions = [{
  "id": 1,
  "name": "Total Hours" // "children": [] poseer o añadirse desde frontend

}, {
  "id": 2,
  "name": "Dials per Hour"
}];

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/__tests__/index.js":
/*!*****************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/__tests__/index.js ***!
  \*****************************************************************************/
/*! exports provided: fakeAgents, fakeCsv, fakeDispositions */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _agents__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./agents */ "./resources/js/components/reports/R2/R2TableReport/__tests__/agents.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fakeAgents", function() { return _agents__WEBPACK_IMPORTED_MODULE_0__["fakeAgents"]; });

/* harmony import */ var _report_data__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./report_data */ "./resources/js/components/reports/R2/R2TableReport/__tests__/report_data.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fakeCsv", function() { return _report_data__WEBPACK_IMPORTED_MODULE_1__["fakeCsv"]; });

/* harmony import */ var _dispositions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dispositions */ "./resources/js/components/reports/R2/R2TableReport/__tests__/dispositions.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "fakeDispositions", function() { return _dispositions__WEBPACK_IMPORTED_MODULE_2__["fakeDispositions"]; });

/**
 * temporal evitar consulta al servidor
 * eliminar despues
 */




/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/__tests__/report_data.js":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/__tests__/report_data.js ***!
  \***********************************************************************************/
/*! exports provided: fakeCsv */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fakeCsv", function() { return fakeCsv; });
var fakeCsv = [{
  "id": 1,
  "type": "xencall",
  "upload_file_name": "HCC Call Log_Mar 4 2020.csv",
  "campaign_id": 1,
  "uploaded_by": 1,
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45",
  "telephonic_data": {
    "id": 1,
    "call_log_id": 1,
    "call_id": "6581034",
    "timestamp": "2020-03-03 08:33:27",
    "call_type": "Dialer",
    "agent_name": "Manny Landeros",
    "disposition": "Contact",
    // original es Callback
    "sub_disposition": null,
    "btn": "5402066406",
    "ani": null,
    "dnis": null,
    "call_time": "13",
    "talk_time": null,
    "hold_time": null,
    "park_time": null,
    "acw": null,
    "campaign": "Campaign 1",
    "list_name": "02-21-20 UCCs 18920",
    "date": "03/03/2020",
    "start_time": "8:33:27 AM",
    "end_time": "08:33:40 AM",
    "call_notes": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  },
  "crm_data": {
    "id": 1,
    "call_log_id": 1,
    "first_name": "Corey",
    "last_name": "Prunty",
    "title": null,
    "email": null,
    "company": "COREY PRUNTY",
    "address": "30 REMUS ST",
    "state": "VA",
    "zip": "24112",
    "fax": "0",
    "sic": null,
    "sic_description": null,
    "employees": null,
    "sales_volume": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  }
}, {
  "id": 2,
  "type": "xencall",
  "upload_file_name": "HCC Call Log_Mar 4 2020.csv",
  "campaign_id": 1,
  "uploaded_by": 1,
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45",
  "telephonic_data": {
    "id": 2,
    "call_log_id": 2,
    "call_id": "6581055",
    "timestamp": "2020-03-03 08:34:47",
    "call_type": "Manual",
    "agent_name": "Manny Landeros",
    "disposition": "Callback",
    "sub_disposition": null,
    "btn": "2813328688",
    "ani": null,
    "dnis": null,
    "call_time": "33",
    "talk_time": null,
    "hold_time": null,
    "park_time": null,
    "acw": null,
    "campaign": "Campaign 1",
    "list_name": "11-11-19-UCCs-8188",
    "date": "03/03/2020",
    "start_time": "8:34:47 AM",
    "end_time": "08:35:20 AM",
    "call_notes": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  },
  "crm_data": {
    "id": 2,
    "call_log_id": 2,
    "first_name": "Davorin",
    "last_name": "Salamunovic",
    "title": null,
    "email": null,
    "company": "PRESTIGE FLOORING, L.L.C.",
    "address": "110 GULF FREEWAY NORTH",
    "state": "TX",
    "zip": "77573",
    "fax": "0",
    "sic": null,
    "sic_description": null,
    "employees": null,
    "sales_volume": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  }
}, {
  "id": 3,
  "type": "xencall",
  "upload_file_name": "HCC Call Log_Mar 4 2020.csv",
  "campaign_id": 1,
  "uploaded_by": 1,
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45",
  "telephonic_data": {
    "id": 3,
    "call_log_id": 3,
    "call_id": "6581076",
    "timestamp": "2020-03-03 08:37:48",
    "call_type": "Dialer",
    "agent_name": "Manny Landeros",
    "disposition": "Info on Company",
    "sub_disposition": null,
    "btn": "3139122939",
    "ani": null,
    "dnis": null,
    "call_time": "39",
    "talk_time": null,
    "hold_time": null,
    "park_time": null,
    "acw": null,
    "campaign": "Campaign 1",
    "list_name": "01-24- 20-Fire",
    "date": "03/03/2020",
    "start_time": "8:37:48 AM",
    "end_time": "08:38:27 AM",
    "call_notes": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  },
  "crm_data": {
    "id": 3,
    "call_log_id": 3,
    "first_name": "Marque",
    "last_name": "Gentry",
    "title": null,
    "email": null,
    "company": "Amg Cleaning",
    "address": null,
    "state": "MI",
    "zip": null,
    "fax": "0",
    "sic": null,
    "sic_description": null,
    "employees": null,
    "sales_volume": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  }
}, {
  "id": 4,
  "type": "xencall",
  "upload_file_name": "HCC Call Log_Mar 4 2020.csv",
  "campaign_id": 1,
  "uploaded_by": 1,
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45",
  "telephonic_data": {
    "id": 4,
    "call_log_id": 4,
    "call_id": "6581097",
    "timestamp": "2020-03-03 08:40:37",
    "call_type": "Dialer",
    "agent_name": "Manny Landeros",
    "disposition": "Not Available",
    "sub_disposition": null,
    "btn": "3474399490",
    "ani": null,
    "dnis": null,
    "call_time": "62",
    "talk_time": null,
    "hold_time": null,
    "park_time": null,
    "acw": null,
    "campaign": "Campaign 1",
    "list_name": "Fire 1-2-20",
    "date": "03/03/2020",
    "start_time": "8:40:37 AM",
    "end_time": "08:41:39 AM",
    "call_notes": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  },
  "crm_data": {
    "id": 4,
    "call_log_id": 4,
    "first_name": "Robert",
    "last_name": "Thomas-Walker",
    "title": null,
    "email": null,
    "company": "Bossvizion",
    "address": null,
    "state": "NY",
    "zip": null,
    "fax": "0",
    "sic": null,
    "sic_description": null,
    "employees": null,
    "sales_volume": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  }
}, {
  "id": 5,
  "type": "xencall",
  "upload_file_name": "HCC Call Log_Mar 4 2020.csv",
  "campaign_id": 1,
  "uploaded_by": 1,
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45",
  "telephonic_data": {
    "id": 5,
    "call_log_id": 5,
    "call_id": "6581118",
    "timestamp": "2020-03-03 08:43:05",
    "call_type": "Dialer",
    "agent_name": "Manny Landeros",
    "disposition": "Not Available",
    "sub_disposition": null,
    "btn": "2393320015",
    "ani": null,
    "dnis": null,
    "call_time": "30",
    "talk_time": null,
    "hold_time": null,
    "park_time": null,
    "acw": null,
    "campaign": "Campaign 1",
    "list_name": "01-24- 20-Fire",
    "date": "03/03/2020",
    "start_time": "8:43:05 AM",
    "end_time": "08:43:35 AM",
    "call_notes": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  },
  "crm_data": {
    "id": 5,
    "call_log_id": 5,
    "first_name": "Lance",
    "last_name": "Allen",
    "title": null,
    "email": null,
    "company": "Club Care Inc",
    "address": null,
    "state": "FL",
    "zip": null,
    "fax": "0",
    "sic": null,
    "sic_description": null,
    "employees": null,
    "sales_volume": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  }
}, {
  "id": 6,
  "type": "xencall",
  "upload_file_name": "HCC Call Log_Mar 4 2020.csv",
  "campaign_id": 1,
  "uploaded_by": 1,
  "created_at": "2020-04-02 18:17:45",
  "updated_at": "2020-04-02 18:17:45",
  "telephonic_data": {
    "id": 6,
    "call_log_id": 6,
    "call_id": "6581139",
    "timestamp": "2020-03-03 08:44:34",
    "call_type": "Dialer",
    "agent_name": "Manny Landeros",
    "disposition": "Do Not Call",
    "sub_disposition": null,
    "btn": "8044792086",
    "ani": null,
    "dnis": null,
    "call_time": "26",
    "talk_time": null,
    "hold_time": null,
    "park_time": null,
    "acw": null,
    "campaign": "Campaign 1",
    "list_name": "01-24- 20-Fire",
    "date": "03/03/2020",
    "start_time": "8:44:34 AM",
    "end_time": "08:45:00 AM",
    "call_notes": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  },
  "crm_data": {
    "id": 6,
    "call_log_id": 6,
    "first_name": "Warren",
    "last_name": "Robinson",
    "title": null,
    "email": null,
    "company": "Absolutely Anything",
    "address": null,
    "state": "VA",
    "zip": null,
    "fax": "0",
    "sic": null,
    "sic_description": null,
    "employees": null,
    "sales_volume": null,
    "created_at": "2020-04-02 18:17:45",
    "updated_at": "2020-04-02 18:17:45"
  }
}];

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/index.js":
/*!*******************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/index.js ***!
  \*******************************************************************/
/*! exports provided: R2TableReport, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R2TableReport_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R2TableReport.vue */ "./resources/js/components/reports/R2/R2TableReport/R2TableReport.vue");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "R2TableReport", function() { return _R2TableReport_vue__WEBPACK_IMPORTED_MODULE_0__["default"]; });



/* harmony default export */ __webpack_exports__["default"] = (_R2TableReport_vue__WEBPACK_IMPORTED_MODULE_0__["default"]);

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/mixins/index.js":
/*!**************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/mixins/index.js ***!
  \**************************************************************************/
/*! exports provided: tableBodyMixin, tableFilterMixin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _tableBodyMixin__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tableBodyMixin */ "./resources/js/components/reports/R2/R2TableReport/mixins/tableBodyMixin.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tableBodyMixin", function() { return _tableBodyMixin__WEBPACK_IMPORTED_MODULE_0__["tableBodyMixin"]; });

/* harmony import */ var _tableFilterMixin__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tableFilterMixin */ "./resources/js/components/reports/R2/R2TableReport/mixins/tableFilterMixin.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "tableFilterMixin", function() { return _tableFilterMixin__WEBPACK_IMPORTED_MODULE_1__["tableFilterMixin"]; });




/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/mixins/tableBodyMixin.js":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/mixins/tableBodyMixin.js ***!
  \***********************************************************************************/
/*! exports provided: tableBodyMixin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tableBodyMixin", function() { return tableBodyMixin; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _tests___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../__tests__ */ "./resources/js/components/reports/R2/R2TableReport/__tests__/index.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }


/**
 * [tableBodyMixin description]
 * Generar cuerpo de tabla
 * @type {Object}
 */

var tableBodyMixin = {
  data: function data() {
    return {
      disposition: [],
      contentTable: [],
      fatherNames: ['Total Hours', 'Primary Sales', 'Other Commissionable Sales', 'Sales Pipeline', 'Lead Analysis', 'Calls (from log)', 'Recycles', 'Finalized Declines'],
      tableBodyTest: []
    };
  },
  computed: {
    SelectCampaign: function SelectCampaign() {
      return this.$store.getters['campaigns/GET_SELECTED_CAMPAIGN'];
    }
  },
  methods: {
    /**
       * [generateTableBody description]
       * solo prototipo
       *
       * ejemplo estructura
       * {
       *		name: 'content 1',
      *     children: [
      *     // pendiente confirmacion de esta propiedad
      *     // segunda opcion añadirla desde front end
      *     // tercera opcion modificar esta propiedad por otro metodo de iteracion
      *      {
      *        name: 'content 1.1',
      *      },
      *      {
      *        name: 'content 1.2',
      *      }
      *    ]
       * }
       * cambiar a generador por la posible sobrecarga
       * quitar referencias a propiedades computadas
       * definir como pasar enviar funciones de calculos
       * @param  {any[]} targetArr [dispositions]
       * @return {any[]}        [description]
       */
    $_generateTableBody: function $_generateTableBody(targetArr) {
      var tableBody = [];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = targetArr[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var target = _step.value;
          var ob = {};
          ob.name = target.name;
          ob.ytd = 0; // months properties

          var _iteratorNormalCompletion2 = true;
          var _didIteratorError2 = false;
          var _iteratorError2 = undefined;

          try {
            for (var _iterator2 = this.getLastMonths[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
              var month = _step2.value;
              ob[month] = 0; // function
            } // days properties

          } catch (err) {
            _didIteratorError2 = true;
            _iteratorError2 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion2 && _iterator2["return"] != null) {
                _iterator2["return"]();
              }
            } finally {
              if (_didIteratorError2) {
                throw _iteratorError2;
              }
            }
          }

          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = this.days[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var dayob = _step3.value;
              ob[dayob.day] = 0; // function
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3["return"] != null) {
                _iterator3["return"]();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }

          tableBody.push(ob);
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator["return"] != null) {
            _iterator["return"]();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return tableBody;
    },
    $_generateTableBody2: function $_generateTableBody2(dispositionValues) {
      var tableBody = [];
      var primaryArray = [];
      var pipelineArray = [];
      var declineArray = [];
      var recycleArray = [];
      var test = this.disposition.filter(function (value) {
        if (value["final"] == 'recycle') {
          recycleArray.push(value);
        }

        if (value.primary == 1) {
          primaryArray.push(value);
        }

        if (value.pipeline == 1) {
          pipelineArray.push(value);
        }

        if (value.decline == 1) {
          declineArray.push(value);
        }

        tableBody[0] = {
          fatherName: 'Total Hours',
          children: [{
            name: 'Dials per Hour'
          }, {
            name: 'Contacts per Hour'
          }, {
            name: 'Sales per Hour'
          }]
        };
        tableBody[1] = {
          fatherName: 'Primary Sales',
          children: primaryArray
        };
        tableBody[2] = {
          fatherName: 'Sales Pipeline',
          children: pipelineArray
        };
        tableBody[3] = {
          fatherName: 'Calls Log',
          children: [{
            name: 'Inbound'
          }, {
            name: 'Outbound'
          }]
        };
        tableBody[4] = {
          fatherName: 'Recycles',
          children: recycleArray
        };
        tableBody[5] = {
          fatherName: 'Finalized Declines',
          children: declineArray
        };
        return value;
      });
      this.tableBodyTest = tableBody;
    },
    getDisposition: function getDisposition() {
      var _this = this;

      return _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var request, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                console.log("ENTRANDO");
                request = {
                  id: _this.SelectCampaign.id === undefined ? 0 : _this.SelectCampaign.id
                };
                _context.next = 5;
                return _this.$store.dispatch("report_2/GET_LIST_DISPOSITION", request);

              case 5:
                response = _context.sent;
                _this.disposition = response.data.data;
                _context.next = 12;
                break;

              case 9:
                _context.prev = 9;
                _context.t0 = _context["catch"](0);
                console.log(_context.t0);

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 9]]);
      }))();
    }
  }
};

/***/ }),

/***/ "./resources/js/components/reports/R2/R2TableReport/mixins/tableFilterMixin.js":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2TableReport/mixins/tableFilterMixin.js ***!
  \*************************************************************************************/
/*! exports provided: tableFilterMixin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tableFilterMixin", function() { return tableFilterMixin; });
/**
 * [cleanDispName description]
 *
 * Usado para acceder al clousure de los filters
 * @param  {[type]} name [description]
 * @return {[type]}      [description]
 */
function cleanDispName(name) {
  var originName = name.trim(); // PARSEAR TODOS LOS DISPOSITION SOLO LETRAS

  var processName = originName.replace(/[^a-zA-Z]+/g, " ").toLowerCase().trim();
  var t2 = processName.split(' ');
  return t2.join('_');
}
/**
 * Filtros de la tabla
 * @type {Object}
 */


var tableFilterMixin = {
  filters: {
    /**
     * [toFixed description]
     *
     * Verificar que el valor sea numerico, en caso de no ser
     * numerico devolver el valor actual y permitir seguir
     * renderizando la tabla
     *
     * En caso de que valor sea igual devuelve una cadena vacia
     *
     * fatherDisposition, ignora el valor de numberOfDecimals y establece
     * uno por defecto, si coincide con un de los siguiente valores, en caso
     * de no coincidir continua la ejecucion con numberOfDecimals
     *
     * - total_hours: establece 2 decimales
     * 
     * 
     * @param  {number} value            [description]
     * @param  {number} numberOfDecimals [description]
     * @param  {string | null} fatherDisposition [description]
     * @return {string | any}                  [description]
     */
    toFixed: function toFixed(value) {
      var numberOfDecimals = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var fatherDisposition = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

      if (typeof value !== 'number') {
        return value;
      }

      if (value === 0) {
        return "";
      } // reducir consumo memoria
      // if (typeof numberOfDecimals !== 'number') {
      //     return value;
      // }


      if (fatherDisposition !== null) {
        switch (cleanDispName(fatherDisposition)) {
          case 'total_hours':
            return value.toFixed(2);
            break;

          default:
            break;
        }
      }

      return value.toFixed(numberOfDecimals);
    }
  }
};

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_agents.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_agents.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R2_agents_vue_vue_type_template_id_635587f9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R2_agents.vue?vue&type=template&id=635587f9& */ "./resources/js/components/reports/R2/R2_agents.vue?vue&type=template&id=635587f9&");
/* harmony import */ var _R2_agents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./R2_agents.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R2/R2_agents.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _R2_agents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./R2_agents.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _R2_agents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _R2_agents_vue_vue_type_template_id_635587f9___WEBPACK_IMPORTED_MODULE_0__["render"],
  _R2_agents_vue_vue_type_template_id_635587f9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R2/R2_agents.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_agents.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_agents.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_agents.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_agents.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_agents.vue?vue&type=template&id=635587f9&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_agents.vue?vue&type=template&id=635587f9& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_template_id_635587f9___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_agents.vue?vue&type=template&id=635587f9& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_agents.vue?vue&type=template&id=635587f9&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_template_id_635587f9___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_agents_vue_vue_type_template_id_635587f9___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R2/R2_create_disposition.vue":
/*!**********************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_create_disposition.vue ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R2_create_disposition_vue_vue_type_template_id_309ab75f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R2_create_disposition.vue?vue&type=template&id=309ab75f& */ "./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=template&id=309ab75f&");
/* harmony import */ var _R2_create_disposition_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./R2_create_disposition.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _R2_create_disposition_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _R2_create_disposition_vue_vue_type_template_id_309ab75f___WEBPACK_IMPORTED_MODULE_0__["render"],
  _R2_create_disposition_vue_vue_type_template_id_309ab75f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R2/R2_create_disposition.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_create_disposition_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_create_disposition.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_create_disposition_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=template&id=309ab75f&":
/*!*****************************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=template&id=309ab75f& ***!
  \*****************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_create_disposition_vue_vue_type_template_id_309ab75f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_create_disposition.vue?vue&type=template&id=309ab75f& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_create_disposition.vue?vue&type=template&id=309ab75f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_create_disposition_vue_vue_type_template_id_309ab75f___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_create_disposition_vue_vue_type_template_id_309ab75f___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R2/R2_dispositions.vue":
/*!****************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_dispositions.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _R2_dispositions_vue_vue_type_template_id_2ed3f047___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./R2_dispositions.vue?vue&type=template&id=2ed3f047& */ "./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=template&id=2ed3f047&");
/* harmony import */ var _R2_dispositions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./R2_dispositions.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _R2_dispositions_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./R2_dispositions.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _R2_dispositions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _R2_dispositions_vue_vue_type_template_id_2ed3f047___WEBPACK_IMPORTED_MODULE_0__["render"],
  _R2_dispositions_vue_vue_type_template_id_2ed3f047___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R2/R2_dispositions.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_dispositions.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css&":
/*!*************************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css& ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_dispositions.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=template&id=2ed3f047&":
/*!***********************************************************************************************!*\
  !*** ./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=template&id=2ed3f047& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_template_id_2ed3f047___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./R2_dispositions.vue?vue&type=template&id=2ed3f047& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R2/R2_dispositions.vue?vue&type=template&id=2ed3f047&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_template_id_2ed3f047___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_R2_dispositions_vue_vue_type_template_id_2ed3f047___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R3/Scorecard.vue":
/*!**********************************************************!*\
  !*** ./resources/js/components/reports/R3/Scorecard.vue ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Scorecard_vue_vue_type_template_id_00b1f9a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Scorecard.vue?vue&type=template&id=00b1f9a2& */ "./resources/js/components/reports/R3/Scorecard.vue?vue&type=template&id=00b1f9a2&");
/* harmony import */ var _Scorecard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Scorecard.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R3/Scorecard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Scorecard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Scorecard.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Scorecard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Scorecard_vue_vue_type_template_id_00b1f9a2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Scorecard_vue_vue_type_template_id_00b1f9a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R3/Scorecard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R3/Scorecard.vue?vue&type=script&lang=js&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/reports/R3/Scorecard.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Scorecard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css& ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Scorecard.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R3/Scorecard.vue?vue&type=template&id=00b1f9a2&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/reports/R3/Scorecard.vue?vue&type=template&id=00b1f9a2& ***!
  \*****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_template_id_00b1f9a2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./Scorecard.vue?vue&type=template&id=00b1f9a2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R3/Scorecard.vue?vue&type=template&id=00b1f9a2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_template_id_00b1f9a2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Scorecard_vue_vue_type_template_id_00b1f9a2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/reports/R4/CallLog.vue":
/*!********************************************************!*\
  !*** ./resources/js/components/reports/R4/CallLog.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CallLog_vue_vue_type_template_id_418e7c54___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CallLog.vue?vue&type=template&id=418e7c54& */ "./resources/js/components/reports/R4/CallLog.vue?vue&type=template&id=418e7c54&");
/* harmony import */ var _CallLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CallLog.vue?vue&type=script&lang=js& */ "./resources/js/components/reports/R4/CallLog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _CallLog_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./CallLog.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _CallLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CallLog_vue_vue_type_template_id_418e7c54___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CallLog_vue_vue_type_template_id_418e7c54___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/reports/R4/CallLog.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/reports/R4/CallLog.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/reports/R4/CallLog.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CallLog.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css&":
/*!*****************************************************************************************!*\
  !*** ./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css& ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/style-loader!../../../../../node_modules/css-loader??ref--6-1!../../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../../node_modules/postcss-loader/src??ref--6-2!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CallLog.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/reports/R4/CallLog.vue?vue&type=template&id=418e7c54&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/reports/R4/CallLog.vue?vue&type=template&id=418e7c54& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_template_id_418e7c54___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./CallLog.vue?vue&type=template&id=418e7c54& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/reports/R4/CallLog.vue?vue&type=template&id=418e7c54&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_template_id_418e7c54___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_CallLog_vue_vue_type_template_id_418e7c54___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/upload/upload_call_log.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/upload/upload_call_log.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _upload_call_log_vue_vue_type_template_id_47c214de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./upload_call_log.vue?vue&type=template&id=47c214de& */ "./resources/js/components/upload/upload_call_log.vue?vue&type=template&id=47c214de&");
/* harmony import */ var _upload_call_log_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./upload_call_log.vue?vue&type=script&lang=js& */ "./resources/js/components/upload/upload_call_log.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _upload_call_log_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./upload_call_log.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _upload_call_log_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _upload_call_log_vue_vue_type_template_id_47c214de___WEBPACK_IMPORTED_MODULE_0__["render"],
  _upload_call_log_vue_vue_type_template_id_47c214de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/upload/upload_call_log.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/upload/upload_call_log.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/upload/upload_call_log.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./upload_call_log.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css&":
/*!*********************************************************************************************!*\
  !*** ./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css& ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./upload_call_log.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/upload/upload_call_log.vue?vue&type=template&id=47c214de&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/upload/upload_call_log.vue?vue&type=template&id=47c214de& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_template_id_47c214de___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./upload_call_log.vue?vue&type=template&id=47c214de& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/upload/upload_call_log.vue?vue&type=template&id=47c214de&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_template_id_47c214de___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_upload_call_log_vue_vue_type_template_id_47c214de___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/users/Index.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/users/Index.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Index_vue_vue_type_template_id_38473b70___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Index.vue?vue&type=template&id=38473b70& */ "./resources/js/components/users/Index.vue?vue&type=template&id=38473b70&");
/* harmony import */ var _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Index.vue?vue&type=script&lang=js& */ "./resources/js/components/users/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Index.vue?vue&type=style&index=0&lang=css& */ "./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Index_vue_vue_type_template_id_38473b70___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Index_vue_vue_type_template_id_38473b70___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/users/Index.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/users/Index.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/users/Index.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css& ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/style-loader!../../../../node_modules/css-loader??ref--6-1!../../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../../node_modules/postcss-loader/src??ref--6-2!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=style&index=0&lang=css& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js?!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=style&index=0&lang=css&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__) if(__WEBPACK_IMPORT_KEY__ !== 'default') (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));
 /* harmony default export */ __webpack_exports__["default"] = (_node_modules_style_loader_index_js_node_modules_css_loader_index_js_ref_6_1_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_6_2_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_style_index_0_lang_css___WEBPACK_IMPORTED_MODULE_0___default.a); 

/***/ }),

/***/ "./resources/js/components/users/Index.vue?vue&type=template&id=38473b70&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/users/Index.vue?vue&type=template&id=38473b70& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_38473b70___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./Index.vue?vue&type=template&id=38473b70& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/users/Index.vue?vue&type=template&id=38473b70&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_38473b70___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Index_vue_vue_type_template_id_38473b70___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);