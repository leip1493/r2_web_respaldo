<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFromDatetimeAndToDatetimeToLogFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('log_files', function (Blueprint $table) {
            $table->timestamp("from_timestamp")->nullable();
            $table->timestamp("to_timestamp")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('log_files', function (Blueprint $table) {
            $table->dropColumn("from_timestamp");
            $table->dropColumn("to_timestamp");
        });
    }
}
