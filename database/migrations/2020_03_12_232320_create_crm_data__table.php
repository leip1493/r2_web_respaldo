<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCrmDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('crm_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('call_log_id');

            // Business columns
            $table->string("first_name", 255)->nullable();
            $table->string("last_name", 255)->nullable();
            $table->string("title", 255)->nullable();
            $table->string("email", 255)->nullable();
            $table->string("company", 255)->nullable();
            $table->string("address", 100)->nullable();
            $table->string("state", 255)->nullable();
            $table->string("zip", 255)->nullable();
            $table->string("fax", 255)->nullable();
            $table->string("sic", 255)->nullable();
            $table->string("sic_description", 255)->nullable();
            $table->string("employees", 255)->nullable();
            $table->string("sales_volume", 255)->nullable();

            $table->timestamps();
            $table->foreign('call_log_id')->references('id')->on('call_log')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('crm_data');
    }
}
