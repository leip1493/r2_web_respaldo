<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDispositionIdToCallLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('call_log', function (Blueprint $table) {
            $table->unsignedBigInteger('disposition_id')->after('campaign_id');
            $table->foreign('disposition_id')->references('id')->on('dispositions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('call_log', function (Blueprint $table) {
            $table->dropForeign('disposition_id');
        });
    }
}
