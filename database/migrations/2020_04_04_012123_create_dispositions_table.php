<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDispositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dispositions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->enum('final', ['recycle', 'final'])->nullable()->default('recycle');
            $table->enum('contact', ['no contact', 'contact'])->nullable()->default('no contact');
            $table->enum('effective', ['not effective', 'effective'])->nullable()->default('not effective');
            $table->boolean('pipeline')->default(false);
            $table->boolean('decline')->default(false);
            $table->boolean('primary')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dispositions');
    }
}
