<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTelephonicDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('telephonic_data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('call_log_id');

            // Business columns
            $table->string("call_id", 255)->nullable(); //$table->string("call_log_id", 30)->nullable();
            $table->timestamp("timestamp", 0); //$table->string("log_time", 255);
            $table->string("call_type", 255)->nullable();
            $table->string("agent_name", 255)->nullable();
            $table->string("disposition", 255)->nullable();
            $table->string("sub_disposition", 255)->nullable();
            $table->string("btn", 255)->nullable();
            $table->string("ani", 255)->nullable();
            $table->string("dnis", 255)->nullable();
            $table->string("call_time", 255)->nullable(); //$table->string("call_duration", 255)->nullable();
            $table->string("talk_time", 255)->nullable();
            $table->string("hold_time", 255)->nullable();
            $table->string("park_time", 255)->nullable();
            $table->string("acw", 255)->nullable();
            $table->string("campaign", 255)->nullable();
            $table->string("list_name", 255)->nullable();
            // CUSTOM TELEPHONIC DATA
            $table->string("date", 255)->nullable();
            $table->string("start_time", 255)->nullable();
            $table->string("end_time", 255)->nullable();
            $table->longText("call_notes")->nullable();

            $table->timestamps();
            $table->foreign('call_log_id')->references('id')->on('call_log')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('telephonic_data');
    }
}
