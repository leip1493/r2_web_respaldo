<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateXenCallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('xencall', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("log_time", 255);
            $table->string("call_log_id", 30)->nullable();
            $table->string("agent_name", 30)->nullable();
            $table->string("date", 30)->nullable();
            $table->string("start_time", 30)->nullable();
            $table->string("end_time", 30)->nullable();
            $table->string("call_duration", 30)->nullable();
            $table->string("upload_file_name", 150)->nullable();
            $table->longText("call_notes")->nullable();
            $table->integer("campaign_id");
            $table->integer("uploaded_by");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('xencall');
    }
}
