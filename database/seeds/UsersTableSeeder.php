<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "superadmin",
            "email" => "superadmin@mail.com",
            "password" => bcrypt("secret")
        ]);

        User::create([
            "name" => "admin",
            "email" => "admin@mail.com",
            "password" => bcrypt("secret")
        ]);

        User::create([
            "name" => "guest",
            "email" => "guest@mail.com",
            "password" => bcrypt("secret")
        ]);


        // factory(App\User::class, 7)->create();
    }
}
