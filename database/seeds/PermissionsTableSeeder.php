<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class PermissionsTableSeeder extends Seeder
{
    private $ROLES = [
        "SUPER_ADMIN" => "super-admin",
        "ADMIN" => "admin",
        "GUEST" => "guest"
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Permission list
        Permission::create(['name' => 'users.index']);
        Permission::create(['name' => 'users.edit']);
        Permission::create(['name' => 'users.show']);
        Permission::create(['name' => 'users.create']);
        Permission::create(['name' => 'users.destroy']);

        Permission::create(['name' => 'roles.index']);
        Permission::create(['name' => 'roles.edit']);
        Permission::create(['name' => 'roles.show']);
        Permission::create(['name' => 'roles.create']);
        Permission::create(['name' => 'roles.destroy']);

        Permission::create(['name' => 'permissions.index']);
        Permission::create(['name' => 'permissions.edit']);
        Permission::create(['name' => 'permissions.show']);
        Permission::create(['name' => 'permissions.create']);
        Permission::create(['name' => 'permissions.destroy']);

        Permission::create(['name' => 'front.upload.index']);
        Permission::create(['name' => 'front.agents.index']);
        Permission::create(['name' => 'front.dispositions.index']);
        Permission::create(['name' => 'front.agents_hours.index']);
        Permission::create(['name' => 'front.agents_hours_custom.index']);
        Permission::create(['name' => 'front.daily_activity.index']);
        Permission::create(['name' => 'front.scorecard.index']);
        Permission::create(['name' => 'front.call_log.index']);

        Role::create(['name' => $this->ROLES["SUPER_ADMIN"]]);

        //Admin
        $admin = Role::create(['name' => $this->ROLES["ADMIN"]]);
        $admin->givePermissionTo([
            'users.index',
            'users.edit',
            'users.show',
            'users.create',
            'users.destroy'
        ]);
        //$admin->givePermissionTo('products.index');
        //$admin->givePermissionTo(Permission::all());

        //Guest
        $guest = Role::create(['name' => $this->ROLES["GUEST"]]);
        $guest->givePermissionTo([
            'users.index',
            'users.show'
        ]);
        //User Super Admin
        $user = User::find(1); // superadmin User
        $user->assignRole($this->ROLES["SUPER_ADMIN"]);

        $user = User::find(2); // admin user
        $user->assignRole($this->ROLES["ADMIN"]);

        $user = User::find(3); // guest user
        $user->assignRole($this->ROLES["GUEST"]);
    }
}
